class AddPgSearchIndexToRaps < ActiveRecord::Migration[5.0]
  def change
    execute "CREATE INDEX rap_artist_name_idx ON raps USING GIN(artist gin_trgm_ops);"
  end
end
