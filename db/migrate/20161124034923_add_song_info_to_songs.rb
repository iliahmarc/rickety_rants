class AddSongInfoToSongs < ActiveRecord::Migration[5.0]
  def change
    add_column :songs, :copyright, :string
    add_column :songs, :writers, :string
    add_column :songs, :year, :integer
  end
end
