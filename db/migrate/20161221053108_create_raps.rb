class CreateRaps < ActiveRecord::Migration[5.0]
  def change
    create_table :raps do |t|
      t.string :artist
      t.string :title
      t.text :lyrics
      t.integer :rank
      t.string :copyright
      t.string :writers
      t.integer :year

      t.timestamps
    end
  end
end
