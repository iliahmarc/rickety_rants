class AddIndexToRants < ActiveRecord::Migration[5.0]
  def change
    add_index :rants, [:author_id, :created_at]
  end
end
