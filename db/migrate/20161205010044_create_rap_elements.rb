class CreateRapElements < ActiveRecord::Migration[5.0]
  def change
    create_table :rap_elements do |t|
      t.string :word, unique: true
      t.integer :count

      t.timestamps
    end
    add_index :rap_elements, [:count]
  end
end
