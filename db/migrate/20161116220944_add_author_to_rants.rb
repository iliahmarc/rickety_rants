class AddAuthorToRants < ActiveRecord::Migration[5.0]
  def change
    add_reference :rants, :author, foreign_key: true
  end
end
