class AddPgTitleSearchIndexToRaps < ActiveRecord::Migration[5.0]
  def change
    execute "CREATE INDEX rap_song_title_idx ON raps USING GIN(title gin_trgm_ops);"
  end
end
