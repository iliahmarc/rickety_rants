class CreateCountryElements < ActiveRecord::Migration[5.0]
  def change
    create_table :country_elements do |t|
      t.string :word, unique: true
      t.integer :count

      t.timestamps
    end
  end
end
