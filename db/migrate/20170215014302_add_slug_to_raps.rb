class AddSlugToRaps < ActiveRecord::Migration[5.0]
  def change
    add_column :raps, :slug, :string
  end
end
