class CreateSongs < ActiveRecord::Migration[5.0]
  def change
    create_table :songs do |t|
      t.string :artist
      t.string :title
      t.text :lyrics
      t.integer :rank, unique: true

      t.timestamps
    end
  end
end
