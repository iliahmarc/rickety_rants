namespace :rap_elements do
  desc "TODO"
  task seed_rap_elements: :environment do
    include PTableHelper
    include SongsHelper #RapsHelper

    RapElement.destroy_all
    @singularize = true
    seed_elements(Rap)
    @words_output.each do |word, count|
      RapElement.create!(word: word.capitalize, count: count)
    end

    p "#{@freq_sum} total words in #{RapElement.count} RapElements. Global word count: #{@word_total}. Global unique words: #{@unique_words}"

  end

end
