namespace :songs do
  desc "TODO"
  task seed_songs: :environment do

  Song.destroy_all

      #song 1 (one)
  Song.create!(artist:  "Keith Urban",
               title: "Blue Ain't Your Color",
               rank: 1,
               copyright: "Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., BMG Rights Management US, LLC",
               writers: "Clint Lagerberg, Hillary Lindsey, Steven Lee Olsen",
               year: 2016,
               lyrics: "I can see you over there
Starring at your drink
Watchin' that ice sink
All alone tonight

And chances are
You're sittin' here in this bar
‘Cause he ain't gonna treat you right
Well, it's probably not my place
But I'm gonna say it anyway
‘Cause you look like
You haven't felt the fire
Had a little fun
Hadn't had a smile in a little while
Baby

Blue looks good on the sky
Looks good on that neon buzzin' on the wall
But darling, it don't match your eyes
I'm tellin' you
You don't need that guy
It's so black and white
He's stealin' your thunder
Baby, blue ain't your color

I'm not tryna
Be another just
Pick you up
Kinda guy
Tryna drink you up
Tryna take you home
But I just don't understand
How another man
Can take your sun
And turn it ice cold

Well, I've had enough to drink
And it's makin'
Me think that I just might
Tell you if I were a painter I wouldn't change ya
I'd just paint you bright
Baby

'Cause blue looks good on the sky
Looks good on that neon buzzin' on the wall
But darling, it don't match your eyes
I'm tellin' you
You don't need that guy
It's so black and white
He's stealin' your thunder
Baby, blue ain't your color
No no no
Blue ain't your color baby
Blue looks good on the sky
Looks good on that neon buzzin' on the wall
But darling, it don't match your eyes
I'm tellin' you
You don't need that guy
It's so black and white
He's stealin' your thunder
Baby, blue ain't your color

Blue ain't your color, umm mm
No, no baby
Call me baby
Let me light up your world .")
  #song 2 (two)
  Song.create!(artist:  "Florida Georgia Line",
               title: "May We All (f. Tim McGraw)",
               rank: 2,
               copyright: "Warner/Chappell Music, Inc, BMG Rights Management US, LLC, Round Hill Music Big Loud Songs",
               writers: "Rodney Clawson, Jamie Moore",
               year: 2016,
               lyrics: "May we all get to grow up in our red white and blue little town
Get a one star hand me down Ford to try to fix up
With some part time cash from driving a tractor
Find a sweet little thing, wears your ball cap backwards
Kinda place you can't wait to leave but nobody does
'Cause you miss it too much!

May we all know that nothing ain't cool 'til you wear the new one
The sound of a quarter rollin' down a jukebox
Play the Travis Tritt right above the 2Pac
For you get lost down some road
Slow rolling with the top off the back of a Bronco
Buy a cold sixer with a cashed in lotto
She's smilin' with her hair blowing out the window
Where you 'bout to go?
Yeah you learn to fly and if you can't then you just free-fall
May we all

May we all get to see those fields of green turn gold
Watch a marching band play with the harvest moon coming up
And know that fifteen minutes of fame
This thing ain't gonna be what makes us or breaks us but
We'll all be watching the TV the day that it comes

May we all know that nothing ain't cool 'til you wear the new one
The sound of a quarter rollin' down a jukebox
Play the Travis Tritt right above the 2Pac
For you get lost down some road
Slow rolling with the top off the back of a Bronco
Buy a cold sixer with a cashed in lotto
She's smilin' with her hair blowing out the window
Where you 'bout to go?
Yeah you learn to fly and if you can't then you just free-fall
May we all

May we all do a little bit better than the first time
Learn a little something from the worst times
Get a little stronger from the hurt times
May we all get to have a chance to ride the fast one
Walk away wiser when we crashed one
Keep hoping that the best one is the last one
Yeah you learn to fly and if you can't then you just free-fall

May we all
May we all
May we all
May we all
May we all (get to have a chance to ride the fast one)
May we all (walk away wiser when we crashed one)
May we all (keep hoping that the best one is the last one)
May we all (yeah the last one)
Yeah you learn to fly and if you can't then you just free-fall

May we all .")
  #song 3 (three)
  Song.create!(artist:  "Brett Eldredge",
               title: "Wanna Be That Song",
               rank: 3,
               copyright: "Sony/ATV Music Publishing LLC, Peermusic Publishing",
               writers: "Brett Eldredge, Ross Copperman, Travis Hill",
               year: 2015,
               lyrics: "The radio and a sundress
Making my world all a mess
Back corner of a cornfield
Bottle tops and a true spear
Pull the lever, lay the seat back laughin'
You slippin' off your shoes
While the dashboard speakers sing every word of that moon

I wanna be that song that gets you high
Makes you dance, makes you fall
That melody rewinds years
Once disappear, makes time stall
Wanna be those words that fill you up
Pull your windows down and keeps you young
Makes you believe you're right where you belong
I wanna be that song
I wanna be, wanna be, wanna be, wanna be that song
Oh, I wanna be, I wanna, I wanna

I wanna stand with you in the third row
Window booth at a bar
Back pew on a Sunday pourin' out your heart
When the bleachers are crowded
When you're sittin' all alone
When the rain is pourin' and you need something to take you home

Let me be that song that gets you high
Makes you dance, makes you fall
That melody rewinds years
Once disappear, makes time stall
Wanna be those words that fill you up
Pull your windows down and keeps you young
Makes you believe you're right where you belong
I wanna be that song
I wanna be, wanna be, wanna be, wanna be that song
Oh, I wanna be, I wanna, I wanna

When you're searching the horizon
When your eyes look back
When you're standing in the moment
Every life has a soundtrack

Oh I wanna be, I wanna be, oh I wanna be
I wanna be that song that gets you high
Makes you dance, makes you fall
That melody rewinds years
Once disappear, makes time stall
Wanna be those words that fill you up
Pull your windows down and keeps you young
Makes you believe you're right where you belong
I wanna be that song

When the highway's calling
When the bottle's dry
When the sky is falling and you're asking yourself why
Oh, oh, I wanna be, yeah, oh .")

  #song 4 (four)
  Song.create!(artist:  "Old Dominion",
               title: "Song For Another Time",
               rank: 4,
               copyright: "Kobalt Music Publishing Ltd., Warner/Chappell Music, Inc, Sony/ATV Music Publishing LLC",
               writers: "Matt Jenkins, Matt Ramsey, Trevor Rosen, Brad Francis Tursi",
               year: 2015,
               lyrics: "Right now we both know
We're Marina Del Ray
Planes gonna fly away
And you'll be on it
And by this time tomorrow
I'll be singing yesterday
The sunshine's gonna fade
And we can't stop it
So before we turn in
I can't make you love me

Let's be brown eyed girl sweet Caroline
Free fall small town Saturday night
Before you lose that loving feeling
Let's go dancing on the ceiling
Keep on living that teenage dream
Paradise city where the grass is green
Pretty soon I'll be so lonesome I could cry
But that's a song for another time

Just for one more day what do you say
Baby be my pretty woman
'Cause we know Sunday morning's coming down
Let's take a drive you and I down some old country road
Talk about growing old in one of those pink houses
Yeah we might be a candle in the wind
But let's pretend we're
Brown eyed girl sweet Caroline
Free fall small town Saturday night
Before you lose that loving feeling
Let's go dancing on the ceiling
Keep on living that teenage dream,
Paradise city where the grass is green
Pretty soon you will be always on my mind
But that's a song for another time

So before we're singing I will always love you
Let's sing

Brown eyed girl sweet Caroline
Free fall small town Saturday night
Before you lose that loving feeling
Let's go dancing on the ceiling
Keep on living that teenage dream,
Paradise city where the grass is green
Pretty soon I'll be so lonesome I could cry
But that's a song for another time
Yeah, that's a song for another time (brown eyed girl sweet Caroline)
Yeah, that's a song for another time (free fall small town Saturday night)
Yeah, that's a song for another time .")
  #song 5 (five)
  Song.create!(artist:  "Little Big Town",
               title: "Better Man",
               rank: 5,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Taylor Swift",
               year: 2016,
               lyrics: "I know I'm probably better off on my own
Than lovin' a man who didn't know
What he had when he had it
And I see the permanent damage you did to me
Never again, I just wish I could forget when it was magic
I wish it wasn't four am, standing in the mirror
Saying to myself, you know you had to do it I know
The bravest thing I ever did was run

Sometimes, in the middle of the night, I can feel you again
But I just miss you, and I just wish you were a better man
And I know why we had to say goodbye
Like the back of my hand
And I just miss you, and I just wish you were a better man
A better man

I know I'm probably better off all alone
Than needing a man who could change his mind at any given minute
And it's always on your terms
I'm hanging on every careless word
Hoping it might turn sweet again
Like it was in the beginning
But your jealousy, I can hear it now
You're talking down to me like I'll always be around
You push my love away like it's some kind of loaded gun
Boy, you never thought I'd run

Sometimes, in the middle of the night, I can feel you again
But I just miss you, and I just wish you were a better man
And I know why we had to say goodbye
Like the back of my hand
And I just miss you, and I just wish you were a better man
A better man
Better man

I hold onto this pride because these days it's all I have
And I gave you my best and we both know you can't say that
You can't say that
I wish you were a better man
I wonder what we would've become
If you were a better man
We might still be in love
If you were a better man
You would've been the one
If you were a better man
Yeah, yeah

Sometimes, in the middle of the night, I can feel you again
And I just miss you, and I just wish you were a better man
And I know why we had to say goodbye
Like the back of my hand
And I just miss you and I just wish you were a better man
We might still be in love, if you were a better man
Better man .")
  #song 6 (six)
  Song.create!(artist:  "Carrie Underwood",
               title: "Dirty Laundry",
               rank: 6,
               copyright: "Warner/Chappell Music, Inc, Sony/ATV Music Publishing LLC, BMG Rights Management US, LLC",
               writers: "Ashley Gorley, Hillary Lindsey, Miller Crowell, Zach Crowel",
               year: 2015,
               lyrics: "That lipstick on your collar, well, it ain't my shade of pink
And I can tell by the smell of that perfume, it's like forty dollars too cheap
And there's a little wine stain on the pocket of your white cotton thread
Well, you drink beer and whiskey, boy, and you know I don't drink red

Found it over in the corner
Wadded up on the bedroom floor
You shoulda hid it in the closet
You shoulda burned it, you shoulda lost it

Now I'ma have to hang you out to dry, dry, dry
Clothespin all your secrets to the line, line, line
Leave 'em blowing in the wind, just say goodbye to you
All those midnights sneaking in
I'm late again, oh, I'm so sorry
All the Ajax in the world ain't gonna clean your dirty laundry

If the neighbors get to asking, I won't cover nothin' up
I'll tell 'em every little detail, how you drug me through the mud
I'm gonna string up your old button-down and slide it on the porch
Just in case you get the nerve to come knockin' on my door

Yeah, I'ma have to hang you out to dry, dry, dry
Clothespin all your secrets to the line, line, line
Leave 'em blowing in the wind, just say goodbye to you
All those midnights sneaking in
I'm late again, oh, I'm so sorry
All the Ajax in the world ain't gonna clean your dirty laundry

Oh oh oh oh-oh, oh, oh, oh, oh-oh
Found it over in the corner
Wadded up on the bedroom floor
You shoulda hid it in the closet
You shoulda burned it, you shoulda lost it

Now I'ma have to hang you out to dry, dry, dry
Clothespin all your secrets to the line, line, line
Leave 'em blowing in the wind, just say goodbye to you
All those midnights sneaking in
I'm late again, oh, I'm so sorry
All the Ajax in the world ain't gonna clean your dirty laundry
Out to dry
To the line .")
  #song 7 (seven)
  Song.create!(artist:  "Brett Young",
               title: "Sleep Without You",
               rank: 7,
               copyright: "Warner/Chappell Music, Inc, Downtown Music Publishing, Universal Music Publishing Group",
               writers: "Kelly Archer, Justin Ebach, Brett Young",
               year: 2016,
               lyrics: "Oh babe
Never thought I would be like this
Wide awake waitin' on a goodnight kiss
Sippin' ninety proof
Talkin' to the moon

Coutin' down the hours till its two AM
Never thought I would be like this
But as long as the night ends with you in a yellow cab shootin' me a text sayin'
Comin' home soon

As long as I can bet on ya crawlin' into bed after slippin' out ya high heel shoes
I ain't lyin' sayin' havin' a good time
Out with your girls
Girl do what you do
No matter how late
Baby I'll be stayin' up
I can't sleep without you

I'd be tossin' and turnin' all night babe
From the smell of your hair on the pillow case
Even if I tried without you by my side I'd be dreamin with my eyes open
I'd be tossin' and turnin' all night babe
But as long as the night ends with you in a yellow cab shootin' me a text sayin'
Comin' home soon

As long as I can bet on ya crawlin' into bed after slippin' out ya high heel shoes
I ain't lyin sayin havin a good time
Out with your girls, girl do what you do
No matter how late
Baby I'll be stayin' up
I can't sleep without you, no
I can't sleep without you, no, no

I bet that DJ'S playin' your song
And your carryin' on
Baby I love the thought of that long as I know I'm the one you're comin home to
But as long as the night ends with you in a yellow cab shootin' me a text sayin'
Comin' home soon

As long as I can bet on ya crawlin' into bed after slippin' out ya high heel shoes
I ain't lyin sayin havin a good time
Out with your girls, girl do what you do
No matter how late
Baby I'll be stayin' up
I can't sleep without you

Ya no matter how late I'll be stayin up I can't sleep without you no
Can't sleep without you
I can't sleep without you
Never thought I would be like this
Just wide awake waitin' on a goodnight kiss .")
  #song 8 (eight)
  Song.create!(artist:  "Cole Swindell",
               title: "Middle Of A Memory",
               rank: 8,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Zach Crowell, Ashley Gorley, Cole Swindell",
               year: 2016,
               lyrics: "Baby, it just took one look at you
For me to change my one drink order to two
Like we already knew each other
Like we been talking all night
About a minute into our first dance
We got blindsided by your friends
All in a hurry like you had to go
Didn't they know you can't leave someone
Girl, you can't leave someone

In the middle of a dance floor all alone
In the middle of an old school country song
Right when I was just about to lean on in
Why'd you have to go then?
Baby, in the middle of the glow of the neon light
It shoulda, coulda, woulda been the night of our lives
Girl, it ain't right, no
How you gonna leave me right in the middle of a memory?

We were gonna dance till they shut it down
People'd be staring while I spin you 'round
Thinking we were so in love, yeah
They wouldn't know we hadn't even hooked up
I'd get your number and I'd give you mine
And we'd be hanging out tomorrow night
But now I don't know where you are
I'm under these lights right here in the dark

In the middle of a dance floor all alone
In the middle of an old school country song
Right when I was just about to lean on in
Why'd you have to go then?
Baby, in the middle of the glow of the neon light
It shoulda, coulda, woulda been the night of our lives
Girl, it ain't right, no
How you gonna leave me right in the middle of a memory?

Yeah, it's like you walked right out in the middle of a movie
Tore the back half out of a book
And no, you'll never know, girl, what you did to me
It ain't right saying goodbye

In the middle of a dance floor all alone
In the middle of what could've been our song
Right when I was just about to lean on in
Why'd you have to go then?
Baby, in the middle of the glow of the neon light
It shoulda, coulda, woulda been the night of our lives
But, girl, it ain't right, no
How you gonna leave me right in the middle of a memory?
In the middle of a memory
In the middle of a memory .")
  #song 9 (nine)
  Song.create!(artist:  "Thomas Rhett",
               title: "Star Of The Show",
               rank: 9,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Rhett Akins, Ben Hayslip, Thomas Rhett",
               year: 2015,
               lyrics: "Walking down the street hand in mine
It don't keep them other guys
And their wandering eyes from looking at you
But it's alright and that's okay, who can blame 'em anyway?
You're so pretty and you ain't even got a clue

'Cause everywhere we go girl, you're the star of the show
And everybody's wondering and wanting to know
What's your name, who's that girl
With the prettiest smile in the world?
Oh, what gets me the most is you don't even know
That you are, you are, you are the star of the show
The star of the show, baby

Friday night on the town, we walk in and you draw a crowd
Even the band seems to sing for you
Ease on up, order a drink, barkeep says 'it's all on me'
You look at me and laugh like you don't know what to do
Baby, you're so cute

'Cause everywhere we go girl, you're the star of the show
And everybody's wondering and wanting to know
What's your name, who's that girl
With the prettiest smile in the world?
Oh, what gets me the most is you don't even know
That you are, you are, you are the star of the show
The star of the show, baby

Even in a ponytail and a pair of jeans
You're looking like the cover of a magazine
Baby, you're the only one who doesn't see

'Cause everywhere we go girl, you're the star of the show
And everybody's wondering and wanting to know
What's your name, who's that girl
With the prettiest smile in the world?
Oh, what gets me the most is you don't even know
That you are, you are, you are
You are, you are, you are
Baby you are, you are, you are the star of the show
Don't you know you're the star of the show, baby? .")
  #song 10 (ten)
  Song.create!(artist:  "Blake Shelton",
               title: "A Guy With a Girl",
               rank: 10,
               copyright: "Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Bryan Simpson, Ashley Glenn Gorley",
               year: 2016,
               lyrics: "Sometimes I'm the guy with the boys kicking it back
Or the guy with the guitar singing on a country track
I might be the guy with an ice cold can, stirring up dust on some old farmland
When I walk into the party with you girl, you change all that

I'm just the guy with the girl everybody wants to know
Wishin' you were there alone
Wonderin' how I ever got your little hand in mine
Lookin' over at ya like 'ain't she beautiful?'
I'm invisible but I stand right there and smile
You're right beside me, oh and I see the same thing they're seein'
But I don't mind being the guy with the girl, no

It's funny watchin' 'em do the way they do
They come walkin' up to me but they only wanna talk to you
And I don't blame 'em that they're hypnotized
They keep staring at your heartbreak eyes
It's like their heart starts stoppin' when you come walkin' into the room

And I turn into the guy with the girl everybody wants to know
Wishin' you were there alone
Wonderin' how I ever got your little hand in mine
Lookin' over at ya like 'ain't she beautiful?'
I'm invisible but I stand right there and smile
You're right beside me, oh and I see the same thing they're seein'
But I don't mind being the guy with the girl

The guy that don't know how he got her
But he ain't never gonna let her, never gonna let her go
Woah-oh, yeah

I'm just the guy with the girl everybody wants to know
Wishin' you were there alone
Wonderin' how I ever got your little hand in mine
Lookin' over at ya like 'ain't she beautiful?'
I'm invisible but I stand right there and smile
You're right beside me, oh and I see the same thing they're seein'
But I don't mind being the guy with the girl

No, I don't mind being the guy with the girl
(Guy with the girl)
Yeah, I'm just the guy with the girl
I'm always the guy with the girl
I don't mind it, baby
I don't mind it, baby
I'm always the guy with the girl .")
  #song 11 (eleven)
  Song.create!(artist:  "Sam Hunt",
               title: "Take Your Time",
               rank: 11,
               copyright: "Universal Music Publishing Group, Kobalt Music Publishing Ltd",
               writers: "Josh Osborne, Sam Lawry Hunt, Shane L. Mcanally",
               year: 2014,
               lyrics: "I don't know if you were looking at me or not
You probably smile like that all the time
And I don't mean to bother you but
I couldn't just walk by
And not say hi

And I know your name
'Cause everybody in here knows your name
And you're not looking for anything right now
So I don't wanna come on strong
But don't get me wrong
Your eyes are so intimidating
My heart is pounding but
It's just a conversation
No girl I'm not gonna waste it
You don't know me
I don't know you but I want to

And I don't wanna steal your freedom
I don't wanna change your mind
I don't have to make you love me
I just want to take your time

I don't wanna wreck your Friday
I ain't gonna waste my lines
I don't have to take your heart
I just wanna take your time

And I know it starts with hello
And the next thing you know you're trying to be nice
And some guys getting too close
Trying to pick you up
Trying to get you drunk

And I'm sure one of your friends is about to come over here
'Cause she's supposed to save you from random guys
That talk too much and wanna stay too long
It's the same old song and dance but I think you know it well

You could have rolled your eyes
Told me to go to hell
Could have walked away
But you're still here
And I'm still here
Come on let's see where it goes

I don't wanna steal your freedom
I don't wanna change your mind
I don't have to make you love me
I just wanna take your time

I don't have to meet your mother
We don't have to cross that line
I don't wanna steal your covers
I just wanna take your time

Woah, I don't wanna go home with you
Woah, I just wanna be alone with you

I don't wanna steal your freedom
I don't wanna change your mind
I don't have to make you love me
I just wanna take your time

And I don't wanna blow your phone up
I just wanna blow your mind
I don't have to take your heart
I just wanna take your time

No, I ain't gotta call you baby
And I ain't gotta call you mine
I don't have to take your heart
I just wanna take your time, ooh .")
  #song 12 (twelve)
  Song.create!(artist:  "Little Big Town",
               title: "Girl Crush",
               rank: 12,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Liz Rose, Lori Mckenna, Hillary Lindsey",
               year: 2014,
               lyrics: "I got a girl crush
Hate to admit it but
I got a heart rush
Ain't slowing down
I got it real bad
Want everything she has
That smile and that midnight laugh
She's giving you now

I want to taste her lips
Yeah, 'cause they taste like you
I want to drown myself
In a bottle of her perfume
I want her long blonde hair
I want her magic touch
Yeah, 'cause maybe then
You'd want me just as much
I got a girl crush
I got a girl crush

I don't get no sleep
I don't get no peace
Thinking about her
Under your bed sheets
The way that she's whispering
The way that she's pulling you in
Lord knows I've tried,
I can't get her off my mind

I want to taste her lips
Yeah, 'cause they taste like you
I want to drown myself
In a bottle of her perfume
I want her long blonde hair
I want her magic touch
Yeah, 'cause maybe then
You'd want me just as much
I got a girl crush
I got a girl crush
Hate to admit it but
I got a heart rush
It ain't slowing down .")
  #song 13 (thirteen)
  Song.create!(artist:  "Sam Hunt",
               title: "House Party",
               rank: 13,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Jerry Flowers, Zach Crowell, Sam Hunt",
               year: 2014,
               lyrics: "You're on the couch, blowing up my phone
You don't want to come out, but you don't want to be alone
It don't take but two to have a little soiree
If you're in the mood to sit tight right where you are, babe

'Cause I'll be at your door in ten minutes
Whatever you got on, girl, stay in it
You ain't gotta leave the house to have a good time
I'ma bring the good time home to you

We'll have a house party, we don't need nobody
Turn your TV off, break that boom-box out
We'll wake up all the neighbors til the whole block hates us
And the cops will show up and try to shut us down

If you're gonna be a homebody
We're gonna have a house party
If you wanna be a homebody
We're gonna have a house party

Throw a neon tee shirt over the lamp shade
I'll take the furniture, slide it out of the way
Shaking the floor, rattling the roof
We'll go to town like they're in your living room

Let's have a house party, we don't need nobody
Turn your TV off, break that boom-box out
We'll wake up all the neighbors til the whole block hates us
And the cops will show up and try to shut us down

If you're gonna be a homebody
We're gonna have a house party
If you wanna be a homebody
We're gonna have a house party

So I'll be at your door in ten minutes
Whatever you got on, babe, stay in it
You ain't gotta leave the house to have a good time
I'ma bring the good time home to you

We'll have a house party, we don't need nobody
Turn your TV off, break that boom-box out
We'll wake up all the neighbors til the whole block hates us
And the cops will show up and try to shut us down

If you're gonna be a homebody
We're gonna have a house party
If you wanna be a homebody
We're gonna have a house party .")
  #song 14 (fourteen)
  Song.create!(artist:  "Luke Bryan",
               title: "Kick The Dust Up",
               rank: 14,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Dallas Davidson, Chris Destefano, Ashley Gorley",
               year: 2015,
               lyrics: "All week long it's a farmin' town
They're makin' that money grow
Tractors, plows with flashing lights
Backin' up a two lane road
They take one last lap around
That sun up high goes down
And then it's on, come on
Girl kick it on back
Z71 like a Cadillac

We go way out where
There ain't nobody
We turn this cornfield
Into a party
Pedal to the floorboard
Eight up in a four door
Burnin' up a back road song
Park it and we pile out
Baby, watch your step now
Better have your boots on
Kick the dust up
Back it on up
Fill your cup up
Let's tear it up, up
And kick the dust up

Bar downtown they got a line
Of people way out the door
$10 dollar drinks, it's packed inside
I don't know what they're waitin for
Got me a jar full of clear
And I got that music for your ears
And it's like knock, knock, knock goes the diesel
If you really wanna see the beautiful people

We go way out where
There ain't nobody
We turn this cornfield
Into a party
Pedal to the floorboard
Eight up in a four door
Burnin' up a back road song
Park it and we pile out
Baby, watch your step now
Better have your boots on
Kick the dust up
Let's back it on up
Fill your cup up
Let's tear it up, up
And kick the dust up

Just follow me down ‘neath the 32 bridge
Y'alla be glad you did

We go way out where
There ain't nobody
We turn this cornfield
Into a party
Pedal to the floorboard
Eight up in a four door
Burnin' up a back road song
Park it and we pile out
Baby watch your step now
Better have your boots on
Kick the dust up
Back it on up
Fill your cup up
That's what's up, up
Let's kick the dust up .")
  #song 15 (fifteen)
  Song.create!(artist:  "Thomas Rhett",
               title: "Crash and Burn",
               rank: 15,
               copyright: "Warner/Chappell Music, Inc",
               writers: "Christopher Stapleton, Jesse Frasure",
               year: 2015,
               lyrics: "So I guess it's over baby and déjà vu again
Who'd have thought that time don't stop and
Somehow girl, the world keeps spinning and
I guess I've turned myself into a solitary man
Ain't like I'm the only one that's in the shoes
That I am

Do you hear that?
I'm right back
At the sound of lonely calling
Do you hear that?
It's where I'm at
It's the sound of teardrops falling down, down
A slamming door and a lesson learned
I let another lover crash and burn

I know that it might sound jaded
And I have to say
I think love is overrated
But I don't like throwing it away

I know you could probably tell me
Right where I went wrong
Some guys can't have all the luck
If others don't sing sad songs

Do you hear that?
I'm right back
At the sound of lonely calling
Do you hear that?
It's where I'm at
It's the sound of teardrops falling down, down
A slamming door and a lesson learned
I let another lover crash and burn, that's right

Another lover
Another lover crash and burn
Ooh yeah

Do you hear that?
I'm right back
At the sound of lonely calling
Do you hear that?
It's where I'm at
It's the sound of teardrops falling down, down
A slamming door and a lesson learned
I let another lover crash and burn, yeah
I let another lover crash and burn

Mhm
Mhm .")
  #song 16 (sixteen)
  Song.create!(artist:  "Blake Shelton",
               title: "Sangria",
               rank: 16,
               copyright: "Kobalt Music Publishing Ltd., Peermusic Publishing, Songs Music Publishing",
               writers: "John Thomas Harding, Joshua Shaun Osborne, Trevor Joseph Rosen",
               year: 2014,
               lyrics: "You're crashin' into me like waves on the coast
Every time we talk, you move in close
I don't want you stop, I don't want you to stop tonight
We've got the last two glasses on a straw hook bar
Tryin' to remember what number we are
String of white lights making your eyes shine tonight

We're buzzing like that no vacancy sign out front
Your skin is begging to be kissed by a little more than the sun
You take my hand in yours, you lean in and
Your lips taste like sangria
Your lips taste like sangria

Wreckin' ball dancin' down the hallway
You're holding your shoes, wearing my shades
We fall against the door, we fall into a wild warm kiss

We're buzzing like that no vacancy sign out front
Your skin is begging to be kissed by a little more than the sun
You take my hand in yours, you lean in and
Your lips taste like sangria
Your lips taste like sangria

Only thing I want to do tonight
Is drink you like a Spanish wine
Let you let this head of mine keep spinning, spinning around

We're buzzing like that no vacancy sign out front
Your skin is begging to be kissed by a little more than the sun
You take my hand in yours, you lean in and
Your lips taste like sangria
Your lips taste like sangria

Your lips taste like sangria
Your lips taste like sangria

Oh

Only thing I want to do tonight
Is drink you like a Spanish wine
Let you let this head of mine keep spinning, spinning around .")
  #song 17 (seventeen)
  Song.create!(artist:  "Zac Brown Band",
               title: "Homegrown",
               rank: 17,
               copyright: "Peermusic Publishing, Reach Music Publishing, Warner/Chappell Music, Inc",
               writers: "Niko Moon, Wyatt Durrette, Zachry Alexander Brown",
               year: 2015,
               lyrics: "I got a piece of land out in the country side
Lay back and smell the sun warm up the Georgia pine
Been so good to me, takin' it easy

Why would I ever leave?
'Cause I know

I got some good friends that live down the street
Got a good lookin' woman with her arms 'round me
Live in a small town where it feels like home
I've got everything I need, and nothin' that I don't

Homegrown
Homegrown

We got a fire goin', down by the riverside
Sip whiskey out the bottle, livin' like we'll never die
C'mon and stay a while if you don't believe me

Why would I ever leave?
'Cause I know

I got some good friends that live down the street
Got a good lookin' woman with her arms 'round me
Live in a small town where it feels like home
I've got everything I need, and nothin' that I don't

Homegrown
Homegrown
Homegrown
Homegrown

I got some good friends that live down the street
Got a good lookin' woman with her arms 'round me
Here in a small town where it feels like home
I've got everything I need (homegrown) and nothin' that I don't (homegrown)
Everything I need (homegrown) and nothin' that I don't (homegrown)

It's the way that you care for the things you think you want
It's the way that you care for the things you think you want
It's the way that you care for the things you think you want

I've got everything I need, nothin' that I don't (homegrown)
Everything I need, nothin' that I don't (homegrown)
Everything I need, nothin' that I don't (homegrown)
Ooh everything I need, and nothin' that I don't (homegrown) .")
  #song 18 (eighteen)
  Song.create!(artist:  "Chris Janson",
               title: "Buy Me A Boat",
               rank: 18,
               copyright: "Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Chris Dubois, Chris Janson",
               year: 2015,
               lyrics: "I ain't rich, but I damn sure wanna be
Working like a dog all day, ain't working for me
I wish I had a rich uncle that'd kick the bucket
And that I was sitting on a pile like Warren Buffett
I know everybody says money can't buy happiness

But it could buy me a boat
It could buy me a truck to pull it
It could buy me a Yeti 110 iced down with some Silver Bullets
Yeah, I know what they say, money can't buy everything
Well, maybe so
But it can buy me a boat

They call me redneck, white trash and blue collar
I could change all that if I had a couple million dollars
I keep hearing money is the root of all evils
And you can't fit a camel through the eye of a needle
I'm sure that's probably true, but it still sounds pretty cool

‘Cause it could buy me a boat
It could buy me a truck to pull it
It could buy me a Yeti 110 iced down with some silver bullets
Yeah, I know what they say, money can't buy everything
Well, maybe so
But it can buy me a boat

To float down on the water with a beer
I hear the Powerball Lotto is sitting' on a hundred mil'
Well, that would buy me a brand new rod and reel

And it could buy me a boat
It could buy me a truck to pull it
It could buy me a Yeti 110 iced down with some Silver Bullets
Yeah, I know what they say, money can't buy everything
Well, maybe so
But it could buy me a boat

Yeah, I know what they say, money can't buy everything
Well, maybe so
But it could buy me a boat
It could buy me a boat .")
  #song 19 (nineteen)
  Song.create!(artist:  "Keith Urban",
               title: "John Cougar, John Deere, John 3:16",
               rank: 19,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC, Peermusic Publishing",
               writers: "Ross Copperman, Shane Mcanally, Josh Osborne",
               year: 2016,
               lyrics: "I'm a 45 spinning on an old Victrola
I'm a two strike swinger, I'm a Pepsi Cola
I'm a blue jean quarterback saying 'I love you' to the prom queen
In a Chevy
I'm John Wayne, Superman, California
I'm a Kris Kristofferson Sunday morning
I'm a mom and daddy singing along to Don McLean
At the levee

And I'm a child of a backseat freedom, baptized by rock and roll
Marilyn Monroe and the Garden of Eden, never grow up, never grow old
Just another rebel in the great wide open, on the boulevard of broken dreams
And I learned everything I needed to know from John Cougar, John Deere, John 3:16

Hey, hey
Everything I needed
That's right

I'm Mark Twain on the Mississippi
I'm Hemingway with a shot of whiskey
I'm a TV dinner on a tray trying to figure out the Wheel of Fortune
I'm a Texaco star, I'm a Gibson guitar
I'm still a teenage kid trying to go too far
I'm a jukebox waiting in a neon bar for a quarter

I'm a child of a backseat freedom, baptized by rock and roll
Marilyn Monroe and the Garden of Eden, never grow up, never grow old
Just another rebel in the great wide open, on the boulevard of broken dreams
And I learned everything I needed to know from John Cougar, John Deere, John 3:16

I spent a lot of years running from believing,
Looking for another way to save my soul
The longer I live, the more I see it, there's only one way home

I'm a child of a backseat freedom, baptized by rock and roll
Marilyn Monroe and the Garden of Eden, never grow up, never grow old
Just another rebel in the great wide open, on the boulevard of broken dreams
And I learned everything I needed to know from John Cougar, John Deere, John 3:16

I'm a child of a backseat freedom, baptized by rock and roll
Marilyn Monroe and the Garden of Eden, never grow up, never grow old
Just another rebel in the great wide open, on the boulevard of broken dreams
And I learned everything I needed to know from John Cougar, John Deere, John 3:16

Everything I needed
(John Cougar, John Deere, John 3:16)
Everything I needed
(John Cougar, John Deere, John 3:16)
That's right .")
  #song 20 (twenty)
  Song.create!(artist:  "Eric Church",
               title: "Like A Wrecking Ball",
               rank: 20,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Casey Beathard, Eric Church",
               year: 2014,
               lyrics: "I, I been gone I been gone too long singing my songs on the road.
Another town, one more show and I'm coming home.
Don't give a damn what these keys I hold
I'm gonna knock down that front door, and

I'ma find out what that house is made of.
Been too many nights since its felt us make love.
I wanna rock some Sheetrock, knock some pictures off the wall.
Love you baby, like a wrecking ball.

You, look at you send me one more shot sitting on a bathroom sink.
Damn you really turn me on, painting your toenails pink.
Easy baby before you say but if I can make it just one more day, and

That old house is gonna be shaking.
I hope those bricks and boards can take it.
But I won't be surprised if the whole damn place just falls.
I'm gonna rock you baby, like a wrecking ball.

And that old house is gonna be shaking.
Rafter and rocking foundation quaking.
Crash right through the front door, back you up against the wall.
Love you baby take it right there baby rock you baby, like a wrecking ball. .")
  #song 21 (twenty-one)
  Song.create!(artist:  "Old Dominion",
               title: "Break Up With Him",
               rank: 21,
               copyright: "Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Calhoun Enterprises, Hori Pro Entertainment Group",
               writers: "Brad Francis Tursi, George Geoffrey Sprung, Matthew Thomas Ramsey, Trevor Rosen, William Whitfield Sellers",
               year: 2015,
               lyrics: "Hey girl, what's up?
I know it's late, but I knew you'd pick it up
Naw, I ain't drunk
Okay, maybe I do have a little buzz but
That song came on and I just thought what harm could come from one little call?
I know you say you're taken, but I say girl you're talking too long

To tell him that it's over
Then bring it on over
Stringing him along any longer girl, it's just wasting precious time
Girl, you know it can't wait
Rip it off just like a band-aid
The way you look at me, girl, you can't pretend
I know you ain't in love with him, break up with him

I know, you don't wanna break his heart
But that ain't no good reason to be keeping us apart, look
Just tell him, it's you, it ain't him
And maybe you can lie to him and say you'll still be friends
Whatever you got to say to get through to him that you ain't in love
C'mon you can't deny that you and I kinda fit like a glove

So tell him that it's over
Then bring it on over
Stringing him along any longer girl, it's just wasting precious time
Girl, you know it can't wait
Rip it off just like a band-aid
It ain't my business to be all up in
But I know you ain't in love with him, break up with him
I know that you so done with him, break up with him, break up with him

You would've hung up by now if you weren't thinking it too
No pressure, whatever, just do what you gotta do, but if I was you
I'd tell him that it's over
Then bring it on over
Stringing him along any longer girl, it's just wasting precious time
Girl, you know it can't wait
Just rip it off like a band-aid
Yeah, I know I said it, but I'll say it again
I know you ain't in love with him, break up with him
The way you look at me, girl, you can't pretend
I know you ain't in love with him, break up with him
Just break up with him .")
  #song 22 (twenty-two)
  Song.create!(artist:  "Luke Bryan",
               title: "Strip It Down",
               rank: 22,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Jon Nite, Luke Bryan, Ross Copperman",
               year: 2015,
               lyrics: "Let it fade to black
Let me run my fingers down your back
Let's whisper, let's don't talk
Baby, leave my t-shirt in the hall
Like a needle finds a groove
Baby, we'll remember what to do
To drown out every distraction
It's time we made it happen

Strip it down, strip it down
Back to you and me like it used to be
When it was an old back road with an old school beat
Cowboy boots by your little bare feet
Let it out, tell me right now
Everything I need in them white cotton sheets
Dirty dance me slow in the summertime heat
Feel my belt turn loose from these old blue jeans
We both know that we lost it somehow
Let's get it found
Strip it down, down, down

I wanna drop this cell phone now
And let it shatter on the ground
They ain't holdin' nothin' these two hands
Until they're holding you again

Oh, strip it down, strip it down
Back to you and me like it used to be
When it was an old back road with an old school beat
Cowboy boots by your little bare feet
Let it out, tell me right now
Everything I need in them white cotton sheets
Dirty dance me slow in the summertime heat
Feel my belt turn loose from these old blue jeans
We both know that we lost it somehow
Let's get it found
Strip it down, down, down
Strip it down, down, down

I don't wanna let you go
No, not tonight
I just wanna love you so bad, baby
So let's close our eyes

And strip it down, strip it down
Back to you and me like it used to be
When it was an old back road with an old school beat
Cowboy boots by your little bare feet
Let it out, tell me right now
Everything I need in them white cotton sheets
Dirty dance me slow in the summertime heat
Feel my belt turn loose from these old blue jeans
We both know that we lost it somehow
Let's get it found
Strip it down, down, down
Strip it down, down, down
Strip it down, down, down
Strip it down, down, down .")
  #song 23 (twenty-three)
  Song.create!(artist:  "Chris Young",
               title: "I'm Comin' Over",
               rank: 23,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Josh Hoge, Christopher Young, Corey Crowder",
               year: 2015,
               lyrics: "We say goodbye, see you around
We turn our backs then turn back around
We break up, we make up and we make love
We can't seem to let go girl

So I'm comin' over
Runnin' every red light
To Hell with the closure
Save it for another time
Try not to think about you
But it ain't workin'
Why put out a fire when it's still burnin'
Just when I think movin' on is gettin' closer
I'm comin' over

I'm all alone, but you're on my phone
Tellin' me you miss me and that you're at home
Who knows what we are in the mornin'
All I know is I want you

So I'm comin' over
Runnin' every red light
To Hell with the closure
Save it for another time
Try not to think about you
But it ain't workin'
Why put out a fire when it's still burnin'
Just when I think movin' on is gettin' closer
I'm comin' over

Oh I'm comin' over

Yeah we said that we're done and I know that it's late
But you already know, I'm on my way

I'm comin' over
Runnin' every red light
To Hell with the closure
Save it for another time
Try not to think about you
But it ain't workin'
Why put out a fire that's burnin'

I'm comin' over
Runnin' every red light
To Hell with the closure
Save it for another time
Try not to think about you
But it ain't workin'
Why put out a fire when it's still burnin'
Just when I think movin' on is gettin' closer
I'm comin' over

Oh I'm comin' over .")
  #song 24 (twenty-four)
  Song.create!(artist:  "Brett Eldredge",
               title: "Lose My Mind",
               rank: 24,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Heather Lynn Morgan, Brett Eldredge, Ross Copperman, Thomas Decarlo Callaway, Gianfranco Reverberi, Gian Piero Reverberi, Brian Joseph Burton",
               year: 2015,
               lyrics: "You put me on a roller coaster, fly me on a plane
You send me to another planet, get inside my brain
I knew right when I met you, I would never be the same
But I let you take me over, girl, so I'm the one to blame

You make all my screws come loose
Got me perfectly confused
Always find a way to mess me up and drive me wild
I love the way you make me lose my mind
Lose my mind, oh

You make me crazy and I kinda like it
You show me that apple, girl, and I wanna bite it
So crazy that I gotta have it
And I never wanna get outta this straight jacket

You make all my screws come loose
Got me perfectly confused
Always find a way to mess me up and drive me wild
I love the way you make me lose my mind

I feel the walls closing in inside this padded room
Only good thing about it is I'm locked in here with you
I'm always watching you, wondering what you'll do next
But my favorite part about it is, is I always have to guess

You make all my screws come loose
Got me perfectly confused
Always find a way to mess me up and drive me wild
I love the way you make me lose my mind
Lose my mind
Lose my mind .")
  #song 25 (twenty-five)
  Song.create!(artist:  "Lee Brice",
               title: "Drinking Class",
               rank: 25,
               copyright: "Warner/Chappell Music, Inc, Round Hill Music Big Loud Songs, DO Write Music LLC",
               writers: "David R Frasier, Ed Hill, Josh Kear",
               year: 2014,
               lyrics: "We're up when the rooster crows
Clock in when the whistle blows
Eight hours ticking slow
And then tomorrow we'll do it all over again

I'm a member of a blue collar crowd
They can never, nah they can't keep us down
If you gotta, gotta label me, label me proud

I belong to the drinking class
Monday through Friday, man we bust our backs
If you're one of us, raise your glass
I belong to the drinking class

We laugh, we cry, we love
Go hard when the going's tough
Push back, come push and shove
Knock us down, we'll get back up again and again

I'm a member of a good timing crowd
We get rowdy, we get wild and loud
If you gotta, gotta label me, label me proud

I belong to the drinking class
Monday through Friday, man we bust our backs
If you're one of us, raise your glass
I belong to the drinking class

We all know why we're here
A little fun, a little music, a little whiskey, a little beer
We're gonna shake off those long week blues
Ladies, break out your dancing shoes
It don't matter what night it is, it's Friday
It's Saturday and Sunday
I just want to hear you say
I just want to hear you sing it
Y'all sing it with me

We belong to the drinking class
Monday through Friday, man we bust our backs
If you're one of us, raise your glass
We belong to the drinking class

Yeah, we belong to the drinking class
Monday through Friday, man we bust our backs
And if you're one of us, raise your glass
We belong to the drinking class .")
  #song 26 (twenty-six)
  Song.create!(artist:  "Zac Brown Band",
               title: "Loving You Easy",
               rank: 26,
               copyright: "Reach Music Publishing, Warner/Chappell Music, Inc",
               writers: "Al Anderson, Niko Moon, Zachry Alexander Brown",
               year: 2015,
               lyrics: "Every morning when you come downstairs
Hair's a mess but I don't care
No makeup on and shining so bright
My old sweatshirt never fit so right
Dancing around to the radio
Humming the words that you don't know
'Cause I'm finding an angel off my list
Thinking that it don't get no better than this

You make loving you easy
You make loving you all I wanna do
Every little smile, every single touch
Reminds me just how much it all makes
Loving you easy

Wrapped around me late at night
Pillow talk by candlelight
Gonna slow this down and make it last
The best things fly by so fast

You make loving you easy
You make loving you all I wanna do
Every little smile and every single touch
Reminds me just how much it all makes

Oh, I wanna say it again

You make loving you easy
You make loving you all I wanna do
Every little smile, every single touch
Reminds me just how much it all makes
Loving you easy, easy, easy .")
  #song 27 (twenty-seven)
  Song.create!(artist:  "Canaan Smith",
               title: "Love You Like That",
               rank: 27,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Brett Beavers, Canaan Lee Smith, Jim Beavers",
               year: 2015,
               lyrics: "Hey girl, I'm just a small town run around
I get my kicks out on the outskirts of town
I could never do it like a pretty city boy
I'm more a fishin' in the dark nitty gritty boy
So lay back and let me paint you a picture
The way I wanna kiss you is

Slow as the Mississippi
Strong as a fifth of whiskey
Steady as a Tom Petty track
I wanna love, wanna love you like that
Deeper than a sunset sky
Sweeter than muscadine wine
All night 'til the sun comes back
I wanna love, wanna love, wanna love you like that

When I'm with you I can see down the road girl
Not just the gravel one we're travelin' on girl
And I'm bettin' that it's gonna be a wild ride
But I promise that I'm gonna be there by your side
I think we got another memory in the makin'
So baby let's take it, take it
Slow as the Mississippi
Strong as a fifth of whiskey
Steady as a Tom Petty track
I wanna love, wanna love you like that
Deeper than a sunset sky
Sweeter than muscadine wine
All night 'til the sun comes back
I wanna love, wanna love, wanna love you like that

And I ain't gonna do it like a pretty city boy
I'm a fishin' in the dark nitty gritty boy, yeah
So let me love you

Slow as the Mississippi
Strong as a fifth of whiskey
Steady as a Tom Petty track, girl
I wanna love, wanna love you like that
Deeper than a sunset sky
Sweeter than muscadine wine
All night 'til the sun comes back
I wanna love, wanna love, wanna love you like that

Baby
I wanna love, wanna love, wanna love you like that

Yeah!
I could never do it like a pretty city boy
I'm more a fishin' in the dark nitty gritty boy .")
  #song 28 (twenty-eight)
  Song.create!(artist:  "Luke Bryan",
               title: "I See You",
               rank: 28,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: " Luke Bryan, Luke Laird, Ashley Gorley",
               year: 2013,
               lyrics: "Roll in the bar, me and my crew 
Their little plan to get me over you
They're hookin' me up, yeah 
Buyin' me drinks with a thousand girls
There's just one thing

I can't go anywhere, I can't do anything 
No, I can't close my eyes without you in my dreams 
You won't leave me alone, even though I know you're gone
I look around for someone new, but I see you 
Jumpin' up there with the band 
Takin' me by the hand 
Hey boy, come dance with me 
Stuck like a melody in my head
In the bed of my truck 
By the light of the midnight moon 
Baby, I see you

Don't know what you did, but you done it good 
You don't know how bad that I wish I could 
Delete you from my phone, find a girl and take her home
But there's just one thing wrong

I can't go anywhere, I can't do anything 
No, I can't close my eyes without you in my dreams 
You won't leave me alone, even though I know you're gone
I look around for someone new, but I see you 
Jumpin' up there with the band 
Takin' me by the hand 
Hey boy, come dance with me 
Stuck like a melody in my head
In the bed of my truck 
By the light of the midnight moon 
Baby, I see you

Your lips, your eyes 
Girl, since you told me goodbye

I can't go anywhere, I can't do anything 
No, I can't close my eyes without you in my dreams 
You won't leave me alone, even though I know you're gone
I look around for someone new, but I see you 
Jumpin' up there with the band 
Takin' me by the hand 
Hey boy, come dance with me 
Stuck like a melody in my head
In the bed of my truck 
By the light of the midnight moon 
Baby, I see you

Baby, I see you
Baby, I see you .")
  #song 29 (twenty-nine)
  Song.create!(artist:  "Cam",
               title: "Burning House",
               rank: 29,
               copyright: "Peermusic Publishing, Sony/ATV Music Publishing LLC, Cypmp",
               writers: "Samuel Tyler Johnson, Jeffrey Bhasker, Cam Ochs",
               year: 2015,
               lyrics: "I had a dream about a burning house
You were stuck inside
I couldn't get you out
I lay beside you and pulled you close
And the two of us went up in smoke

Love isn't all that it seems
I did you wrong
I'll stay here with you
Until this dream is gone

I've been sleepwalking
Been wondering all night
Trying to take what's lost and broke
And make it right
I've been sleepwalking
Too close to the fire
But it's the only place that I can hold you tight
In this burning house

I see you at a party and you look the same
I could take you back
But people don't really change
Wish that we could go back in time
I'd be the one you thought you'd find

Love isn't all that it seems
I did you wrong
I'll stay here with you
Till this dream is gone

I've been sleepwalking
Been wondering all night
Trying to take what's lost and broke
And make it right
I've been sleepwalking
Too close to the fire
But it's the only place that I can hold you tight
In this burning house

Flames are getting bigger now
In this burning house
I can hold on to you somehow
In this burning house
Oh and I don't wanna wake up
In this burning house

And I been sleepwalking
Been wandering all night
Trying to take what's lost and broke
And make it right
I've been sleep walking
Too close to the fire
But it's the only place that I can hold you tight
In this burning house .")
  #song 30 (thirty)
  Song.create!(artist:  "Smoke",
               title: "A Thousand Horses",
               rank: 30,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Jon Nite, Ross Copperman, Michael Hobby",
               year: 2015,
               lyrics: "She comes rolling right off the tip of my tongue so easy
She'll be the first damn thing I want when I start drinking
I'm breathing her in, breathing her out, once I pick her up I can't put her down

She's smoke
I pull her in nice and slow
She's a habit and I can't let go
Blowing rings around my heart
The one she stole
Watching her sway and go
It's killing me and I know
Can't stop her once you start
She's smoke

She'll go floating around like a downtown ballroom gypsy
She goes great with ice cold beer or a shot of whiskey
Put one in my hand, her on my lips
Man, that's as good as it gets

She's smoke
I pull her in nice and slow
She's a habit and I can't let go
Blowing rings around my heart
The one she stole
Watching her sway and go
It's killing me and I know
Can't stop her once you start
She's smoke

When the night burns out
And we all go home
Smell of sweet perfume
All over your clothes
Like smoke, ooh, ooh
Like smoke ooh, ooh

She's smoke
I pull her in nice and slow
She's a habit and I can't let go
Blowing rings around my heart
The one she stole
Watching her sway and go
It's killing me and I know
Can't stop her once you start
She's smoke
She's smoke

When the night burns out
And we all go home
Smell of sweet perfume
All over your clothes
She's smoke, ooh, ooh
She's smoke, ooh, ooh .")
  #song 31 (thirty-one)
  Song.create!(artist:  "Florida Georgia Line",
               title: "This Is How We Roll (f. Luke Bryan)",
               rank: 31,
               copyright: "Sony/ATV Music Publishing LLC, Round Hill Music Big Loud Songs",
               writers: "BRIAN KELLEY, COLE SWINDELL, LUKE BRYAN, TYLER HUBBARD",
               year: 2012,
               lyrics: "
The mix tape's got a little Hank, little Drake
A little something bumping, thump, thumping on the wheel ride
The mix in our drink's a little stronger than you think
So get a grip, take a sip of that feel right
The truck's jacked up, flat bills flipped back
Yeah you can find us where the party's at

This is how we roll
We hanging round singing out everything on the radio
We light it up with our hands up
This is how we roll
And this is how we do
We're burning down the night shooting bullets at the moon baby
This is how we roll

Yeah baby this is how we roll
We rollin' into town
With nothing else to do we take another lap around
Yeah holla at your boy if you need a ride
If you roll with me yeah you know we rollin' high
Up on and thirty seven needles, windows tinted hard to see though
How fresh my baby is in the shotgun seat oh
Them kisses are for me though, automatic like a free throw
This life I live it might not be for you but it's for me though
Let's roll!

This is how we roll
We hanging round singing out everything on the radio
We light it up with our hands up
This is how we roll
And this is how we do
When the world turns ugly I just turn and look at you baby
This is how we roll

Yeah we're proud to be young
We stick to our guns
We love who we love and we wanna have fun
Yeah we cuss on them Mondays
And pray on them Sundays
Pass is around and we dream about one day

This is how we roll
We hanging round singing out everything on the radio
We light it up with our hands up
This is how we roll
And this is how we ride
We slingin' up the mud, cuttin' through the countryside baby
This is how we roll

Yeah this is how we roll
This is how we roll
Yeah this is how we roll
This is how we do
We're burning down the night shooting bullets at the moon baby
This is how we roll
Yeah this is how we roll .")
  #song 32 (thirty-two)
  Song.create!(artist:  "Jason Aldean",
               title: "Burnin' It Down",
               rank: 32,
               copyright: "Round Hill Songs",
               writers: "Brian Kelley, Chris Tompkins, Rodney Clawson, Tyler Hubbard",
               year: 2014,
               lyrics: "You slip your finger through the tear in my t-shirt
You stirrin' up dirty in the back of my mind
You keep on flirtin'
'Cause you know that it's workin'
You stuck in my head girl writing the lines
Couldn't sing this song without you if I tried
Let's light it up like it's our last night

We're just hanging around
Burnin' it down
Sippin' on some cold Jack Daniel's
Jammin' to some old Alabama with you baby
Laying right here, naked in my bed
I'm just doing my thing
You love it when I sing
Say it makes you feel like an angel
We about to get a little tangled up right about now
So girl let's keep burnin' it down
Burnin' it down
Burnin' it down

Girl when you want it
You know that I'm on it
You know that I love lovin' up on you
Let's hit the switch and let our shadows dance
And light it up like it's our last chance

We're just hanging around
Burnin' it down
Sippin' on some cold Jack Daniel's
Jammin' to some old Alabama with you baby
Laying right here, dreaming in my bed
I'm just doing my thing
You love it when I sing
Say it makes you feel like an angel
We about to get a little tangled up right about now
So girl let's keep burnin' it down
Burnin' it down
Burnin' it down
Burnin' it down
Burnin' it down
Burnin' it down
Burnin' it down

I wanna rock it all night
Baby girl will you rock it out with me?
I wanna crawl through the dark
Just to feel your heartbeat against me
I wanna rock it all night
Baby girl will you rock it out with me?
I wanna crawl through the dark
Just to feel your heartbeat against me

We're just hanging around
Burnin' it down
Sippin' on some cold Jack Daniel's
Jammin' to some old Alabama with you baby
Laying right here naked in my bed
I'm just doing my thing
You love it when I sing
Say it makes you feel like an angel
We about to get a little tangled up right about now
So girl let's keep burnin' it down
Burnin' it down
Burnin' it down
Burnin' it down
Burnin' it down
Burnin' it down
Burnin' it down

I wanna rock it all night
Baby girl will you rock it out with me?
I wanna crawl through the dark
Just to feel your heartbeat against me .")
  #song 33 (thirty-three)
  Song.create!(artist:  "Florida Georgia Line",
               title: "Dirt",
               rank: 33,
               copyright: "Round Hill Music",
               writers: "Chris Tompkins, Rodney Clawson",
               year: 2014,
               lyrics: "You get your hands in it
Plant your roots in it
Dusty head lights dance with your boots in it (dirt)

You write her name on it
Spin your tires on it
Build your corn field, whiskey
Bonfires on it (dirt)
You bet your life on it

Its that elm shade
Red roads clay you grew up on
That plowed up ground That your dad
Damned his luck on That post game party field
You circled up on
And when it rains You get stuck on
Drift a cloud back Behind county roads
That you run up
The mud on her jeans that she peeled off
And hung up
Her blue eyed Summer time smile
Looks so good that it hurts
Makes you wanna build
A 10 percent down
White picket fence house on this dirt

You've mixed some sweat with it
Taken a shovel to it
You've stuck some crosses and some painted
Goal posts through it (dirt)
You know you came from it (dirt)
And some day you'll return to

Its that elm shade
Red roads clay you grew up on
That plowed up ground that your dad
Damned his luck on that post game party field
You circled up on
And when it rains you get stuck on
Drift a cloud back Behind county roads
That you run up
The mud on her jeans that she peeled off
And hung up
Her blue eyed summer time smile
Looks so good that it hurts
Makes you wanna build
A 10 percent down
White picket fence house on this dirt

You came from it,
And some day you'll return to

Its that elm shade
Red roads clay you grew up on
That plowed up ground that your dad
Damned his luck on that post game party field
You circled up on
And when it rains you get stuck on
Drift a cloud back Behind county roads
That you run up
The mud on her jeans that she peeled off
And hung up
Her blue eyed summer time smile
Looks so good that it hurts
Makes you wanna build
A 10 percent down
White picket fence house on this dirt

Makes you wanna build
A 10 percent down
White picket fence house on this dirt

You came from it,
And some day you'll return to it .")
  #song 34 (thirty-four)
  Song.create!(artist:  "Brantley Gilbert",
               title: "Bottoms Up (f. T.I.)",
               rank: 34,
               copyright: "Warner/Chappell Music, Inc",
               writers: "Brantley Keith Gilbert, Brett James, Justin Michael Weaver",
               year: 2014,
               lyrics: "I see you and me riding like Bonnie and Clyde
Goin' 95 burning down 129 yeah
Looking for the law, while I push my luck
She's ridin' shotgun like it ain't no thing
Turn the radio up so the girl can sing right
Pull into the party like 'y'all wassup'
Tonight is bottoms up up
Throw it on down
Rock this quiet, little country town
Get up, drop a tailgate on ya truck
Find a keg and fill ya cup up
Kick it on back
Pretty little mama lookin' at ya like that
Make ya wanna slide on in like 'Girl, what's up'
Yeah tonight is bottoms up up up
Get em up
Tonight is bottoms up up up
Get em up
Damn girl I gotta tip my hat
Never thought a country song would make you move like that yeah
And she's doing it in daisy dukes
Girl she's got ya tappin' on a boys shoulder
Hey dog check this out
And that's how girl do it in the dirty south yeah
She'll have you on your knee 'can I marry you?'

Yeah tonight is bottoms up
Throw it on down
Rock this quiet, little country town
Get up, drop a tailgate on ya truck
Find a keg and fill ya cup up
Kick it on back
Pretty little mama lookin' at ya like that
Make ya wanna slide on in like 'Girl, what's up'
Yeah tonight is bottoms up up up
Get em up
Tonight is bottoms up up up
Get em up get em up get em up

Get em up

Hey y'all whatever ya sippin' get it up in the air one time
Bottoms up

Let's give a toast to the good times
All y'all get your drinks up high
Everybody feelin' alright
Damn right
'Cause tonight it's bottoms up
Throw it on down
Rock this quiet, little country town
Get up, drop a tailgate on ya truck
Find a keg and fill ya cup up
Kick it on back
Pretty little mama lookin' at ya like that
Make ya wanna slide on in like 'Girl, what's up'

Tonight is bottoms up up
Throw it on down
Rock this quiet, little country town
Get up, drop a tailgate on ya truck
Find a keg and fill ya cup up
Kick it on back
Pretty little mama lookin' at ya like that
Make ya wanna slide on in like 'Girl, what's up'
Yeah tonight is bottoms up up up
Get em up
Tonight is bottoms up up up
Get em up .")
  #song 35 (thirty-five)
  Song.create!(artist:  "Luke Bryan",
               title: "Play It Again",
               rank: 35,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Dallas Davidson, Ashley Gorley",
               year: 2013,
               lyrics: "She was sittin' all alone over on the tailgate
Tan legs swingin' by a Georgia plate
I was lookin' for her boyfriend
Thinkin', no way she ain't got one
Soon as I sat down I was fallin' in love
Tryin' to pour a little sugar in her Dixie cup
Talkin' over the speakers in the back of that truck
She jumped up and cut me off

She was like, oh my God, this is my song
I've been listenin' to the radio all night long
Sittin' 'round waitin' for it to come on and here it is
She was like, come here boy, I wanna dance
'Fore I said a word, she was takin' my hand
Spinnin' me around 'til it faded out
And she gave me a kiss
And she said, play it again, play it again, play it again
And I said, play it again, play it again, play it again

I'd have gave that DJ my last dime
If he would have played it just one more time
But a little while later
We were sittin' in the drive in my truck
Before I walked her to the door
I was scannin' like a fool AM, FM, XM too
But I stopped real quick when I heard that groove
Man, you should have seen her light up

She was like, oh my God, this is my song
We've been listenin' to the radio all night long
I can't believe it came back on, but here it is
She was like, come here boy, I wanna dance
'Fore I said a word, she was takin' my hand
Spinnin' in the headlights she gave me a goodnight kiss
And I said, play it again, play it again, play it again
And she said, play it again, play it again, play it again

The next Friday night we were sittin' out under the stars
You should have seen her smile when I brought out my guitar

She was like, oh my God, this is my song
I've been listenin' to the radio all night long
Sittin' 'round waitin' for it to come on and here it is
She was like, come here boy, I wanna dance
'Fore I said a word, she was takin' my hand
Spinnin' in the headlights
She gave me a goodnight kiss
And she said, play it again, play it again, play it again
And I said, play it again, play it again, play it again

Yeah, play it again, play it again, play it again
Somebody, play it again, play it again, play it again .")
  #song 36 (thirty-six)
  Song.create!(artist:  "Kenny Chesney",
               title: "American Kids",
               rank: 36,
               copyright: "Kobalt Music Publishing Ltd.",
               writers: "Luke Robert Laird, Rodney Dale Clawson, Shane L. Mcanally",
               year: 2014,
               lyrics: "Doublewide Quick Stop midnight T-top
Jack in her Cherry Coke town
Momma and Daddy put their roots right here
'Cause this is where the car broke down
Yellow dog school bus kickin' up red dust
Pickin' us up by a barbed wire fence
Mtv on the RCA, no A/C in the vents

We were Jesus save me, blue jean baby
Born in the USA
Trailer park truck stop, faded little map dots
New York to LA
We were teenage dreamin', front seat leanin'
Baby, come give me a kiss
Put me on the cover of the Rolling Stone
Uptown down home American kids
Growin' up in little pink houses
Makin' out on living room couches
Blowin' that smoke on a Saturday night
A little messed up, but we're all alright

Baptist church parkin' lot, tryin' not to get caught
Take her home and give her your jacket
Makin' it to second base, but sayin' you went all the way
Monday afternoon at practice
Sisters got a boyfriend Daddy doesn't like
Now he's sittin' out back, 3030 in his lap
In the blue bug zapper light

We were Jesus save me, blue jean baby
Born in the USA
Trailer park truck stop, faded little map dots
New York to LA
We were teenage dreamin', front seat leanin'
Baby, come give me a kiss
Put me on the cover of the Rolling Stone
Uptown down home American kids
Growin' up in little pink houses
Makin' out on living room couches
Blowin' that smoke on a Saturday night
A little messed up, but we're all alright

We were Jesus save me, blue jean baby
Born in the USA
Trailer park truck stop, faded little map dots
New York to LA
We were teenage dreamin', front seat leanin'
Baby, come give me a kiss
Put me on the cover of the Rolling Stone
Uptown down home American kids
Growin' up in little pink houses
Makin' out on living room couches
Blowin' that smoke on a Saturday night
A little messed up, but we're all alright .")
  #song 37 (thirty-seven)
  Song.create!(artist:  "Lady Antebellum",
               title: "Bartender",
               rank: 37,
               copyright: "Warner/Chappell Music, Inc",
               writers: "Charles Kelley, Dave Haywood, Hillary Scott, Rodney Clawson",
               year: 2014,
               lyrics: "8 o'clock on Friday night I'm still at home
All my girls just keep on blowing up my phone
Saying come on, he ain't worth the pain
Do what you gotta do to forget his name
Now there's only one thing left for me to do
Slip on my favorite dress and sky high leather boots
Check the mirror one last time
And kiss the past goodbye

What I'm really needing now
Is a double shot of Crown
Chase that disco ball around
'Til I don't remember
Go until they cut me off
Wanna get a little lost
In the noise, in the lights

Hey bartender pour 'em hot tonight
'Til the party and music and the truth collide
Bring it 'til his memory fades away
Hey bartender

Tonight I'll let a stranger pull me on the floor
Spin me round and let 'em buy a couple more
But before it goes too far
I'll let him down easy
'Cause tonight it's all about
Dancing with my girls to the DJ
Put that song on replay

What I'm really needing now
Is a double shot of Crown
Chase that disco ball around
'Til I don't remember
Go until they cut me off
Wanna get a little lost
In the noise, in the lights

Hey bartender pour 'em hot tonight
'Til the party and music and the truth collide
Bring it 'til his memory fades away
Hey bartender

I'm feeling that buzz I'm ready to rock
Ain't no way I'm gonna tell you to stop
So pour that thing up to the top
I'm coming in hot

Hey bartender

What I'm really needing now
Is a double shot of Crown
Chase that disco ball around
'Til I don't remember
Go until they cut me off
Wanna get a little lost
In the noise, in the lights

Hey bartender pour 'em hot tonight
'Til the party and music and the truth collide
Bring it 'til his memory fades away
Hey bartender, hey bartender .")
  #song 38 (thirty-eight)
  Song.create!(artist:  "Dierks Bentley",
               title: "Drunk On A Plane",
               rank: 38,
               copyright: "Sony/ATV Music Publishing LLC, Round Hill Music Big Loud Songs, Kobalt Music Publishing Ltd.",
               writers: "Chris Tompkins, Dierks Bentley, Josh Kear",
               year: 2014,
               lyrics: "I took two weeks vacation for the honeymoon
A couple tickets all inclusive down in Cancun
I couldn't get my money back so I'm in seat 7A
I'm getting drunk on a plane

I bet the fella on the aisle thought I was crazy
'Cause I taped your picture to the seatback right beside me
Now I've got empty mini bottles fillin' both our trays
I'm getting drunk on a plane

Buyin' drinks for everybody
But the pilot, it's a party
Got this 737 rocking like a G6
Stewardess is somethin' sexy
Leanin' pourin' Coke and whiskey
Told her about my condition
Got a little mile-high flight attention
It's Mardi Gras up in the clouds
I'm up so high, I may never come down
I'll try anything to drown out the pain
They all know why I'm getting drunk on a plane

We had this date marked on the calendar forever
We'd take that new wed limo airport ride together
I feel like a plastic groom alone there at the top of the cake
So hey, I'm getting drunk on a plane

Buyin' drinks for everybody
But the pilot, it's a party
Got this 737 rocking like a G6
Stewardess is somethin' sexy
Leanin' pourin' Coke and whiskey
Told her about my condition
Got a little mile-high flight attention
It's Mardi Gras up in the clouds
I'm up so high, I may never come down
I'll try anything to drown out the pain
They all know why I'm getting drunk on a plane

On my way home I'll bump this seat right up to first class
So I can drink that cheap champagne out of a real glass
And when we land I'll call her up and tell her kiss my ass
'Cause hey, I'm drunk on a plane

Buyin' drinks for everybody
But the pilot, it's a party
Got this 737 rocking like a G6
Stewardess is somethin' sexy
Leanin' pourin' Coke and whiskey
Told her about my condition
Got a little mile-high flight attention
It's Mardi Gras up in the clouds
I'm up so high, I may never come down
I'll try anything to drown out the pain
They all know why I'm getting drunk on a plane

I'm getting drunk on a plane
I might be passed out
In the baggage claim
But right now
I'm drunk on a plane .")
  #song 39 (thirty-nine)
  Song.create!(artist:  "Sam Hunt",
               title: "Leave The Night On",
               rank: 39,
               copyright: "Universal Music Publishing Group",
               writers: "Josh Osborne, Sam Lawry Hunt, Shane L. Mcanally",
               year: 2014,
               lyrics: "They roll the sidewalks in this town
All up after the sun goes down
They say nothin' good happens here
When midnight rolls around
But layin' down would be in vain
I can't sleep with you on my brain
I ain't anywhere close to tired
Your kiss has got me wired

Girl, you got the beat right, killin' in your Levis
High on your lovin's got me buzzin' like a streetlight
It's still early out in Cali, baby, don't you wanna rally again
We'll find a road with no name, lay back in the slow lane
The sky is dropping Jupiter around us like some old train
We'll be rolling down the windows, I bet you we're catchin' our second wind
We don't have to go home, we can leave the night on
We can leave the night on

Now all the stars are turnin' blue
Just kissed the clock 2:22
Baby, I know what you're wishin' for
I'm wishin' for it too
Now all the lights are flashin' gold
Nobody cares how fast we go
Our soundtrack's in the stereo
This DJ's on a roll

Girl, you got the beat right, killin' in your Levis
High on your lovin's got me buzzin' like a streetlight
It's still early out in Cali, baby, don't you wanna rally again
We'll find a road with no name, lay back in the slow lane
The sky is dropping Jupiter around us like some old train
We'll be rolling down the windows, I bet you we're catchin' our second wind
We don't have to go home, we can leave the night on
We can leave the night on

The sun'll steal the magic from us soon
So let's take one more trip around the moon

Girl, you got the beat right, killin' in your Levis
High on your lovin's got me buzzin' like a streetlight
It's still early out in Cali, baby, don't you wanna rally again
We'll find a road with no name, lay back in the slow lane
The sky is dropping Jupiter all around us like some old train
We'll be rolling down the windows, I bet you we're catchin' our second wind
We dont have to go home, we can leave the night on
We can leave the night on .")
  #song 40 (fourty)
  Song.create!(artist:  "Miranda Lambert",
               title: "Somethin' Bad (f. Carrie Underwood)",
               rank: 40,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Brett James, Christopher Michael Destefano, Priscilla Renea",
               year: 2014,
               lyrics: "Stand on the box, stomp your feet, get clapping
Got a real good feeling something bad about to happen

Oh oh oh
Oh oh oh
Oh oh oh
Oh oh oh

Pulled up to the church but I got so nervous
Had to back it on up, couldn't make it to the service
Grabbed all the cash underneath my mattress
Got a real good feelin' something bad about to happen

Oh oh oh
Oh oh oh

Ran into a girl in a pretty white dress
Rolled down a window, where you heading to next?
Said I'm heading to the bar with my money out of the mattress
Got a real good feeling something bad about to happen

Oh oh oh
Oh oh oh

Stand on the box, stomp your feet, start clapping
I got a real good feeling something bad about to happen
Drinks keep coming, throw my head back laughing
Wake up in the morning' don't know what happened
Whoah, something bad
Whoah, something bad

Now me and that girl that I met on the street
We're rollin' down the road, down to New Orleans
Got a full tank of gas and the money out of the mattress
Got a real good feelin' something bad about to happen

Oh oh oh
Oh oh oh

'Bout to tear it up down in New Orleans
Just like a real-life Thelma and Louise
If the cops catch up, they're gonna call it kidnapping
Got a real good feelin' something bad about to happen

Oh oh oh
Oh oh oh

Stand on the box, stomp your feet, start clapping
I got a real good feeling something bad about to happen (ooh)
Drinks keep coming, throw my head back laughing
Wake up in the morning' don't know what happened
Whoah, something bad
Whoah, something bad

Stand on the box, stomp your feet, start clapping
Got a real good feeling something bad about to happen
Now the drinks keep coming, throw my head back laughing
Wake up in the morning' don't know what happened
(Yeah, yeah, yeah)
Whoah, something bad
Whoah, something bad .")
  #song 41 (fourty-one)
  Song.create!(artist:  "Luke Bryan",
               title: "Drink A Beer",
               rank: 41,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Jim Beavers, Chris Stapleton",
               year: 2013,
               lyrics: "When I got the news today
I didn't know what to say.
So I just hung up the phone.
I took a walk to clear my head,
This is where the walking lead
Can't believe you're really gone
Don't feel like going home

So I'm gonna sit right here
On the edge of this pier
Watch the sunset disappear
And drink a beer

Funny how the good ones go
Too soon, but the good lord knows
The reasons why it gets
Sometimes the greater plan is kinda hard to understand
Right now it don't make sense
I can't make it all make sense

So I'm gonna sit right here
On the edge of this pier
Watch the sunset disappear
And drink a beer

So long my friend, until we meet again
I'll remember you
And all the times we used to
Sit right here on the edge of this pier
Watch the sunset disappear
And drink a beer
Drink a beer, drink a beer. .")
  #song 42 (fourty-two)
  Song.create!(artist:  "Lee Brice",
               title: "I Don't Dance",
               rank: 42,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Mike Curb Music",
               writers: "Dallas Davidson, Lee Brice, Rob Hatch",
               year: 2014,
               lyrics: "I'll never settle down,
That's what I always thought
Yeah, I was that kind of man,
Just ask anyone

I don't dance, but here I am
Spinning you around and around in circles
It ain't my style, but I don't care
I'd do anything with you anywhere
Yes, you got me in the palm of your hand
'Cause, I don't dance

Love's never come my way,
I've never been this far
'Cause you took these two left feet
And waltzed away with my heart

No, I don't dance, but here I am
Spinning you around and around in circles
It ain't my style, but I don't care
I'd do anything with you anywhere
Yes, you got me in the palm of your hand, girl
'Cause, I don't dance
No, I don't dance

I don't dance, but here I am
Spinning you around and around in circles
It ain't my style, but I don't care
Well I'd do anything with you anywhere

I don't dance, but here I am
Spinning you around and around in circles
It ain't my style, but I don't care
I'd do anything with you anywhere
Yeah, you got me in the palm of your hand, girl
'Cause, I don't dance

No, ooh .")
  #song 43 (fourty-three)
  Song.create!(artist:  "Jake Owen",
               title: "Beachin'",
               rank: 43,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Mason Levy, Mike Posner, Jimmy Robbins, Jon Nite, Jaren Johnston",
               year: 2013,
               lyrics: "So just watchin' her blonde hair, sun burn, stare at them, wack caps rolling over
Laid back in a thrift store beach chair, droppin' limes in her Corona
Well she looks back, yeah, she throws me a kiss, like honey I sure want you
And it's a hundred and three between her and me and only 92 in Daytona

And it's sunshine, blue eyes, tan lines, slow tide
Rollin' white sand, cold can koozie in my hand, just a summertime strolling
Chillin', breazin', sippin', singin' whoaoaoh

Beachin'

We got 2-for-1s, we're at a margarita bar, whatever happens happens
And there's a reggae band, full of dread head, just sittin' in the corner laughin'
Well my baby walks over, drops a 20 in a jar, she smiles and shakes it at me
Yeah, she gets 'em goin', she gets 'em playin' a little, don't worry be happy

And it's sunshine, blue eyes, tan lines, slow tide
Rollin' white sand, cold can koozie in my hand, just a summertime strolling
Chillin', breazin', sippin', singin' whoaoaoh

Beachin'

You got a margarita here in my hand, doin' a little drinkin'

Talkin' 'bout sunshine, blue eyes, tan lines, slow tide
Rollin' white sand, cold can koozie in my hand, just a summertime strolling
Chillin', breazin', sippin', singin' whoaoaoh,

Beachin', sunshine, blue eyes, tan lines, slow tide
Rollin' white sand, cold can koozie in my hand .")
  #song 44 (fourty-four)
  Song.create!(artist:  "Dustin Lynch",
               title: "Where It's At",
               rank: 44,
               copyright: "Warner/Chappell Music, Inc, Sony/ATV Music Publishing LLC",
               writers: "Cary Barlowe, Matt Jenkins, Zach Crowell",
               year: 2014,
               lyrics: "It ain't in a high rise looking for a good time shutting down the city lights
It ain't in the water floating like a bobber soaking up that hot sunshine
As good as it gets, no that ain't where it is

It's at 2 am when she's reaching' over
Faded T-shirt hanging off her shoulder
Dressed up, hair down, in a ball cap
Yep yep, as long as I get that
Sweet little something late night kiss
On a plane or a train or way back in the sticks
I swear, if she's there, that's where
Yep yep, that's where it's at.

It ain't in a suped-up shiny red new truck, if she ain't to my right
It ain't in a dive bar, tall can of PBR, poppin' tops rockin' all night
As good as it gets, no that ain't where it is

It's at 2 am when she's reaching' over
Faded T-shirt hanging off her shoulder
Dressed up, hair down, in a ball cap
Yep yep, as long as I get that
Sweet little something late night kiss
On a plane or a train or way back in the sticks
I swear, if she's there, that's where
Yep yep, that's where it's at.

No, it don't matter wherever we're at (No)
No, it don't get no better than that

It ain't in a suped-up shiny red new truck, if she ain't to my right

It's at 2 am when she's reaching' over
Faded T-shirt hanging off her shoulder
Dressed up, hair down, in a ball cap
Yep yep, as long as I get that
Sweet little something late night kiss
On a plane or a train or way back in the sticks
I swear, if she's there, that's where
Yep yep, that's where it's at

Yeah, that's where it's at
Yep, yep, (That's where it's at)
2 Am when she's reaching' over
Faded t-shirt hanging off her shoulder (That's where it's at) .")
  #song 45 (fourty-five)
  Song.create!(artist:  "Eric Church",
               title: "Give Me Back My Hometown",
               rank: 45,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Eric Church, Luke Laird",
               year: 2014,
               lyrics: "Damn, I used to love this view
Sit here and drink a few
Main street and the high school lit up on Friday night
Down there it's another touchdown
Man, this year's team is stout
I can hear them goin' crazy
And up here so am I
Thinkin' about you sittin' there sayin' I hate this, I hate it
If you couldn't stand livin' here why'd you take it, take it

Give me back my hometown
'Cause this is my hometown

All the colors of my youth
The red, the green, the hope, the truth
Are beatin' me black and blue 'cause you're in every scene
My friends try to cheer me up get together at the Pizza Hut
I didn't have the heart to tell them that was our place
These sleepy streetlights on every sidewalk side street
Shed a light on everything that used to be

Give me back my hometown
'Cause this is my hometown

Yeah, yeah, oh, yeah, yeah
Ah ooh, yeah, yeah
You can have my grandma's locket
The knife out of my grandpa's pocket
Yeah my state champion jacket I don't care you can have it
Every made memory every picture, every broken dream
Yeah everything, everything, everything

Give me back my hometown
'Cause this is my hometown

Yeah, yeah, oh, yeah, yeah
Ah ooh, yeah, yeah
Yeah, yeah, oh, yeah, yeah
Ah ooh, yeah, yeah
Ah ooh
Ah ooh
Ah ooh
Ah ooh
Ah ooh .")
  #song 46 (fourty-six)
  Song.create!(artist:  "Thomas Rhett",
               title: "Get Me Some Of That",
               rank: 46,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Cole Swindell, Michael Carter, Michael Ray Carter, Rhett Akins",
               year: 2013,
               lyrics: "Yeah girl, been diggin' on you
Sippin' on drink number two
Tryin' to come up with somethin' smooth
And waitin' on the right time to make my move
But I just can't wait no more
Can't let you slip out that door
Prettiest thing I ever seen before
Got me spinnin' around, I ain't even on the dance floor

You're shakin' that money maker, like a heart breaker, like your college major was
Twistin' and tearin' up Friday nights
Love the way you're wearin' those jeans so tight
I bet your kiss is a soul saver, my favorite flavor, want it now and later
I never seen nothin' that I wanted so bad
Girl, I gotta get me, gotta get me some of that
Yeah gotta get me some of that

Little more what you doin' right there
Swingin' your hips and slingin' your hair
Side to side with your drink in the air
Lord have mercy now girl I swear
Gotta get your number in my phone
Gotta get me some of of you alone
We can worry 'bout it later on
Right now keep makin' this my favorite song

You're shakin' that money maker, like a heart breaker, like your college major was
Twistin' and tearin' up Friday nights
Love the way you're wearin' those jeans so tight
I bet your kiss is a soul saver, my favorite flavor, want it now and later
I never seen nothin' that I wanted so bad
Girl, I gotta get me, gotta get me some of that
Some of that
Yeah, gotta get me some of that

In my ride, by my side, down the highway
In the dark, in my arms, in your driveway
All because of that smile you threw my way
Yeah girl you got a way of

Shakin' that money maker, like a heart breaker, like your college major was
Twistin' and tearin' up Friday nights
Love the way you're wearin' those jeans so tight
I bet your kiss is a soul saver, my favorite flavor, want it now and later
I never seen nothin' that I wanted so bad
Girl, I gotta get me, gotta get me some of that
Gotta get me some of that

Yeah, I gotta get me some of that
Oh girl, I gotta get me some of that
Oh yeah
I bet your kiss is a soul saver, my favorite flavor
I want it now and later .")
  #song 47 (fourty-seven)
  Song.create!(artist:  "Jerrod Niemann",
               title: "Drink to That All Night",
               rank: 47,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Derek George, Liam Fudge, Lance Miller, Brad D. Warren, Brett D. Warren, Armando Perez",
               year: 2014,
               lyrics: "Don't look now oh the parkin lots full
Gonna ride that cow with a Dallas bull
Everybody in the ATL is comin
DJ's got those speakers thumpin
Got a black ford not a white mercedes
Walkin in the front door checkin out the ladies
My buddy says hey boys I'm buyin
The hottest girl in here's givin me the eye and

Everybody knows, its gonna be one of those
I can drink to that all night
That's the stuff I like
That's the kind of party makes you throw your hands up high
Bout to tie one on talkin gone gone gone
Turnin all the wrongs into right
I can drink to that all night

Workin on the sweet thing sittin on a bar stool
Doin shots of Jack girl gonna take it old school
Singin hell yeah to every song their playin
Do ya wanna dance? Baby I'm just sayin

Everybody knows, its gonna be one of those
I can drink to that all night
That's the stuff I like
That's the kind of party makes you throw your hands up high
Bout to tie one on talkin gone gone gone
Turnin all the wrongs into right
I can drink to that all night

I can drink to that all night

Take your cup fill it up
You can't raise it high enough
Take your cup fill it up
You can't raise it high enough

Everybody knows, its gonna be one of those
I can drink to that all night
That's the stuff I like
That's the kind of party makes you throw your hands up
I can drink to that all night
That's the stuff I like
That's the kind of party makes you throw your hands up high
Bout to tie one on talkin gone gone gone
Turnin all the wrongs into right
I can drink to that all night

I can drink to that all night
I can drink to that all night

Take your cup fill it up
You can't raise it high enough
Take your cup fill it up
You can't raise it high enough
Take your cup fill it up
You can't raise it high enough
Take your cup fill it up
You can't raise it high enough .")
  #song 48 (fourty-eight)
  Song.create!(artist:  "Chase Rice",
               title: "Ready Set Roll",
               rank: 48,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Chase Rice, Chris Destefano, Rhett Akins",
               year: 2014,
               lyrics: "Ready
Set
Let's roll
Let's roll

Damn pretty girl ya' went and done it again
Ya' gone and turned your sexy all the way up to ten
I never seen a side ride seat looking so hot
Baby you rock, hit the spot
Like a fireball shot

You got me all high head spinning around and around
I'm down if you're down to burn down this town

Ready, set, let's roll
Ready, set, let's ride
Get ya' little fine ass on the step
Shimmy up inside
Just slide girl by my side girl
Yeah we can run this town
I can rock your world
We can roll'em down, fog'em up
Cruise around, get stuck
Pedal to the metal 'til the sun comes up
I made a deal with the man on the moon
He's gonna put in some overtime
We got all night

Ready, set, let's roll
Ready, set, let's ride

Ready, set, let's roll where the good ears and good times meet
Girl we can rev' it up right 'til we overheat
Just forget about the time get ya' lips on mine
Gotta kiss you get to know your smile
On your mark, get set

Ready, set, let's roll
Ready, set, let's ride
Get ya' little fine ass on the step
Shimmy up inside
Just slide girl by my side girl
Yeah we can run this town
I can rock your world
We can roll'em down, fog'em up
Cruise around, get stuck
Pedal to the metal 'til the sun comes up
I made a deal with the man on the moon
He's gonna put in some overtime
We got all night

Ready, set, let's roll
Ready, set, let's ride

Ready, set, let's roll
Ready, set, let's ride

You got my heart bump bumpin'
When I'm pulling up into ya' drive
Let's hit it 90 to nothing
Couple kids running into the night

Ready, set, let's roll
Ready, set, let's ride
Get ya' little fine ass on the step
Shimmy up inside
Just slide girl by my side girl
Yeah we can run this town
I can rock your world
We can roll'em down, fog'em up
Cruise around, get stuck
Pedal to the metal 'til the sun comes up
I made a deal with the man on the moon
He's gonna put in some overtime
We got all night

Ready, set, let's roll
Ready, set, let's ride

Ready, set, let's roll
Ready, set, let's ride
Get ya' fine little ass on the step
Shimmy up inside

Yeah we can run this town
You know I'll rock your world, come on
Let's roll, let's ride
Let's run this town tonight

Ready
Set
Let's roll
Let's roll .")
  #song 49 (fourty-nine)
  Song.create!(artist: "Luke Bryan",
               title: "Roller Coaster",
               rank: 49,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Cole Swindell, Michael Carter, Michael Ray Carter",
               year: 2013,
               lyrics: "She had a cross around her neck
And a flower in her hand
That I picked from the side of Thomas Drive
On our way to the sand
We found an old wooden chair
Still warm from the sun
Pushed it back, gave me a kiss with Bacardi on her lips
And I was done

And we spent that week wide open
Upside down beside the ocean
I didn't know where it was goin'
Just tryin' to keep my heart on the tracks
I should've known that kind of feelin'
Would last longer than that week did
Blown away and barely breathin'
Sunday came and it was over
Now she's got me twisted
Like an old beach roller coaster

The rest of those days
Turned into long crazy nights
When the music got loud
We'd sneak away from the crowd under the boardwalk lights
And with all the things we said
What I just can't get past
Is the way we let it end
Now I'm wonderin' where she is
Knowin' I can't get that goodbye back

And we spent that week wide open
Upside down beside the ocean
I didn't know where it was goin'
Just tryin' to keep my heart on the tracks
I should've known that kind of feelin'
Would last longer than that week did
Blown away and barely breathin'
Sunday came and it was over
Now she's got me twisted
Like an old beach roller coaster

She's like a song playin' over and over
In my mind, where I still hold her
I had the chance and I should've told her

When we spent that last week wide open
Upside down beside the ocean
I should've known where it was goin'
Still tryin' to keep my heart on the tracks
And I should've known that kind of feelin'
Would last longer than that week did
Blown away and barely breathin'
When Sunday came and it was over
Now she's got me twisted, yeah, I'm still twisted
Like that old beach roller coaster

Like that old beach roller coaster .")
  #song 50 (fifty)
  Song.create!(artist:  "Tim McGraw",
               title: "Meanwhile Back At Mama's (f. Faith Hill)",
               rank: 50,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Tom Douglas, Jaren Johnston, Jeffrey Steele",
               year: 2014,
               lyrics: "Running round in this new truck
Bank let's me borrow from month to month
Running out of credit and find a little cash on the radio

Standing still they're blowing past
Numbers on cars going Nascar fast
What I wouldn't give for a slow down, don't you know
'Cause where I come from, only the horses run
When the day is done, we take it easy

Meanwhile back at Mama's
The porch lights on, come on in if you wanna
Suppers on the stove, and beer's in the fridge
Red sun sinking out low on the ridge
Games on the tube and daddy smoked cigarettes
Whiskey keeps his whistle wet
Funny the things you thought you'd never miss
In a world gone crazy as this

Well I found a girl and we don't fit in here
Talk about how hard it is to breathe here
Even with the windows down, can't catch a southern breeze here
One of these days gonna pack it up and leave here

'Cause meanwhile back at Mama's
The porch lights on, come on in if you wanna
Suppers on the stove, and beer's in the fridge
Red sun sinking out low on the ridge
Games on the tube and daddy smoked cigarettes
Whiskey keeps his whistle wet
Funny the things you thought you'd never miss
In a world gone crazy as this

Oh I miss yeah a little dirt on the road
I miss corn growing in a row
I miss being somebody everybody knows there
Everybody knows everybody
I miss those small town roots
Walking around in muddy boots
The sound of rain on an old tin roof
It's time we head on back

'Cause meanwhile back at Mama's
The for sale signs going up and I'm gonna
Dump this truck and the little I've got
On a loan to own and a 3 acre lot
Put supper on the stove and beer in the fridge
Going for broke, yeah we're gonna be rich
Watch the sun settin' on the ridge
Baby tell me whatcha think about this,

Me and you back at Mama's
Yeah, me and you back at Mama's .")
  #song 51 (fifty-one)
  Song.create!(artist:  "Florida Georgia Line",
               title: "Cruise",
               rank: 51,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Round Hill Music Big Loud Songs, Ole MM, Ole Media Management Lp",
               writers: "Chase Rice, Cornell Haynes, Joey Moi, Brian Kelley, Tyler Hubbard, Jesse Kenneth Robert Rice",
               year: 2012,
               lyrics: "Baby you a song
You make me wanna roll my windows down and cruise

Hey baby

Yeah, when I first saw that bikini top on her
She's poppin' right out of the South Georgia water
Thought, oh, good lord, she had them long tanned legs
Couldn't help myself so I walked up and said

Baby you a song
You make me wanna roll my windows down and cruise
Down a back road blowin' stop signs through the middle
Every little farm town with you
In this brand new Chevy with a lift kit
It'd look a hell lot better with you up in it
So baby you a song
You make me wanna roll my windows down and cruise

She was sippin' on southern and singin' Marshall Tucker
We were falling in love in the sweet heart of summer
She hopped right up into the cab of my truck and said
Fire it up, let's go get this thing stuck

Baby you a song
You make me wanna roll my windows down and cruise
Down a back road blowin' stop signs through the middle
Every little farm town with you
In this brand new Chevy with a lift kit
It'd look a hell lot better with you up in it
So baby you a song
You make me wanna roll my windows down and cruise

When that summer sun fell to its knees
I looked at her and she looked at me
And I turned on those KC lights and drove all night
Cause it felt so right, her and I, man we felt so right

I put it in park and
Grabbed my guitar
And strummed a couple chords
And sang from the heart
Girl you sure got the beat in my chest bumpin'
Hell I can't get you out of my head

Baby you a song
You make me wanna roll my windows down and cruise
Down a back road blowin' stop signs through the middle
Every little farm town with you

Baby you a song
You make me wanna roll my windows down and cruise
Down a back road blowin' stop signs through the middle
Every little farm town with you
In this brand new Chevy with a lift kit
It'd look a hell lot better with you up in it
Come on

Baby you a song
You make me wanna roll my windows down and cruise
Come on girl
Get those windows down and cruise
Aw yea .")
  #song 52 (fifty-two)
  Song.create!(artist:  "Darius Rucker",
               title: "Wagon Wheel",
               rank: 52,
               copyright: "Peermusic Publishing, Sony/ATV Music Publishing LLC, Downtown Music Publishing",
               writers: "Ketch Secor, Bob Dylan",
               year: 2013,
               lyrics: "Heading down south to the land of the pines
I'm thumbing my way into North Caroline
Staring up the road and pray to God I see headlights
I made it down the coast in seventeen hours
Picking me a bouquet of dogwood flowers
And I'm a-hopin' for Raleigh, I can see my baby tonight

So rock me momma like a wagon wheel
Rock me momma any way you feel
Hey, momma rock me
Rock me momma like the wind and the rain
Rock me momma like a south bound train
Hey, momma rock me

I'm running from the cold up in New England
I was born to be a fiddler in an old time string band
My baby plays a guitar, I pick a banjo now
Oh, north country winters keep a-getting me down
Lost my money playing poker so I had to leave town
But I ain't turning back to living that old life no more

So rock me momma like a wagon wheel
Rock me momma any way you feel
Hey, momma rock me
Hey, rock me momma like the wind and the rain
Rock me momma like a south bound train
Hey, momma rock me

Walkin' to the south out of Roanoke
Caught a trucker out of Philly had a nice long toke
But he's a heading west from the Cumberland gap
To Johnson City, Tennessee
And I gotta get a move on before the sun
I hear my baby calling my name and I know that she's the only one
And if I die in Raleigh at least I will die free

So rock me momma like a wagon wheel
Rock me momma any way you feel
Hey, momma rock me
Oh, rock me momma like the wind and the rain
Rock me momma like a south bound train
Hey momma rock me

Oh, so rock me momma like a wagon wheel
Rock me momma any way you feel
Hey, momma rock me (mama rock me, mama rock me)
Rock me momma like the wind and the rain
Rock me momma like a south bound train
Hey, ey yeah momma rock me (you can rock me, rock me) .")
  #song 53 (fifty-three)
  Song.create!(artist:  "Blake Shelton",
               title: "Boys 'Round Here (f. Pistol Annies & Friends",
               rank: 53,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Craig Wiseman, Dallas Davidson, Rhett Akins",
               year: 2013,
               lyrics: "Red red red red red red red red redneck

Well the boys 'round here don't listen to The Beatles
Run ole Bocephus through a jukebox needle
At a honky-tonk, where their boots stomp
All night what? (That's right)
Yeah, and what they call work, digging in the dirt
Gotta get it in the ground 'fore the rain come down
To get paid, to get the girl
In your 4 wheel drive (A country boy can survive)

Yeah the boys 'round here
Drinking that ice cold beer
Talkin' 'bout girls, talkin' 'bout trucks
Runnin' them red dirt roads out, kicking up dust
The boys 'round here
Sending up a prayer to the man upstairs
Backwoods legit, don't take no shit
Chew tobacco, chew tobacco, chew tobacco, spit

Aw heck
Red red red red red red red red red red redneck

Well the boys 'round here, they're keeping it country
Ain't a damn one know how to do the dougie
(You don't do the dougie?) No, not in Kentucky
But these girls 'round here yep, they still love me
Yeah, the girls 'round here, they all deserve a whistle
Shakin' that sugar, sweet as Dixie crystal
They like that y'all and southern drawl
And just can't help it cause they just keep fallin'

For the boys 'round here
Drinking that ice cold beer
Talkin' 'bout girls, talkin' 'bout trucks
Runnin' them red dirt roads out, kicking up dust
The boys 'round here
Sending up a prayer to the man upstairs
Backwoods legit, don't take no shit
Chew tobacco, chew tobacco, chew tobacco, spit

Let me hear you say
(Ooh let's ride)
Through the country side
(Ooh let's ride)
Down to the river side

Hey now girl, hop inside
Me and you gonna take a little ride to the river
Let's ride (That's right)
Lay a blanket on the ground
Kissing and the crickets is the only sound
We out of town
Have you ever got down with a
Red red red red red red red red red red redneck?
Do you wanna get down with a,
Red red red red red red red red red red redneck?
Girl you gotta get down

With the boys 'round here
Drinking that ice cold beer
Talkin' 'bout girls, talkin' 'bout trucks
Runnin' them red dirt roads out, kicking up dust
The boys 'round here
Sending up a prayer to the man upstairs
Backwoods legit, don't take no shit
Chew tobacco, chew tobacco, chew tobacco, spit

Red red red red red red red red redneck
(Ooh let's ride)
I'm one of them boys 'round here
(Ooh let's ride)
Red red red red red red red red redneck
(Ooh let's ride)

Well all I'm thinkin' 'bout is you and me, how we'll be
So come on girl, hop inside
Me and you, we're gonna take a little ride
Lay a blanket on the ground
Kissing and the crickets is the only sound
We out of town
Girl you gotta get down with a
Come on through the country side
Down to the river side .")
  #song 54 (fifty-four)
  Song.create!(artist:  "Luke Bryan",
               title: "Crash My Party",
               rank: 54,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Round Hill Music Big Loud Songs",
               writers: "Ashley Gorley, Rodney Clawson",
               year: 2013,
               lyrics: "It don't matter what plans I got, I can break ‘em.
Yeah, I can turn this thing around at the next red light
And I don't mind telling all the guys “I can't meet ‘em”.
Hell, we can all go raise some hell on any other night

Girl, I don't care, oh I just gotta see what you're wearing.
Your hair, is I put up or falling down?
Oh I just have to see it now.

If you wanna call me, call me, call me you don't have to worry ‘bout it baby.
You can wake me up in the dead of the night; wreck my plans, baby that's alright.
This is a drop everything kind of thing.
Swing on by I will pour you a drink.
The door's unlocked, I'll leave on the lights
Baby you can crash my party anytime.

Ain't a spot downtown that's rockin' the way that you rock me.
Ain't a bar that can make me buzz the way that you do.
I could be on the front row of the best show.
And look down and see your face on my phone.
And I'm gone so long, hang on. I'll meet you in a minute or two.

If you wanna call me, call me, call me you don't have to worry ‘bout it baby.
You can wake me up in the dead of the night; wreck my plans, baby that's alright.
This is a drop everything kind of thing.
Swing on by I will pour you a drink.
The door's unlocked. I'll leave on the lights
Baby you can crash my party anytime.

If it's 2 in the morning and you're feeling lonely and wondering what I'm doing.

Go ahead and call me, call me, call me you don't have to worry ‘bout it baby.
You can wake me up in the dead of the night; wreck my plans, baby that's alright.
This is a drop everything kind of thing.
Swing on by I will pour you a drink.
The door's unlocked. I'll leave on the lights
Baby you can crash my party anytime.

Baby you can crash my party anytime. .")
  #song 55 (fifty-fivve)
  Song.create!(artist:  "Hunter Hayes",
               title: "I Want Crazy",
               rank: 55,
               copyright: "Universal Music Publishing Group",
               writers: "Hunter Easton Hayes, Lori Mckenna, Troy Verges",
               year: 2013,
               lyrics: "I'm booking myself a one way flight
I gotta see the color in your eyes
I'm telling myself I'm gonna be alright
Without you baby is a waste of time

Yeah, our first date, girl, the seasons changed
Got washed away in a summer rain
You can't undo a fall like this
'Cause love don't know what distance is
Yeah, I know it's crazy

But I don't want good and I don't want good enough
I want can't sleep, can't breathe without your love
Front porch and one more kiss, it doesn't make sense to anybody else
Who cares if you're all I think about,
I've searched the world and I know now,
It ain't right if you ain't lost your mind
Yea I don't want easy, I want crazy
Are you with me baby? Let's be crazy, yeah

I wanna be scared, don't wanna know why
Wanna feel good, don't have to be right
The world makes all kinds of rules for love
I say you gotta let it do what it does

I don't want just another hug and a kiss goodnight
Catchin' up calls and a date sometimes
A love that revels and we still believe
We're the kind of crazy people wish that they could be, yeah

Oh, I know we're crazy, yeah

But I don't want good and I don't want good enough
I want can't sleep, can't breathe without your love
Front porch and one more kiss, it doesn't make sense to anybody else
Who cares if you're all I think about,
I've searched the world and I know now,
It ain't right if you ain't lost your mind
Yea I don't want easy, I want crazy
You with me baby? Let's be crazy

No! I don't want good and I don't want good enough
I want can't sleep, can't breathe without your love
Front porch and one more kiss, it doesn't make sense to anybody else
Who cares if you're all I think about,
I've searched the world and I know now,
It ain't right if you ain't lost your mind
Yea I don't want easy, I want crazy

Yeah, look at us baby, tonight the midnight rules are breaking
There's no such thing as wild enough, maybe we just think too much
Who needs to play it safe in love
Let's be crazy!

Who cares if we're crazy, we gotta be crazy
I know that we're crazy, so let's be crazy
Yeah .")
  #song 56 (fifty-six)
  Song.create!(artist:  "Tim McGraw",
               title: "Highway Don't Care",
               rank: 56,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Brad Warren, Brett Warren, Joshua Kear, Mark Irwin",
               year: 2002,
               lyrics: "
  Bet your window's rolled down and your hair's pulled back
And I bet you got no idea you're going way too fast
You're trying not to think about what went wrong
Trying not to stop 'til you get where you goin'
You're trying to stay awake so I bet you turn on the radio and the song goes

I can't live without you, I can't live without you, baby
I can't live without you, I can't live without you, baby, baby

The highway won't hold you tonight
The highway don't know you're alive
The highway don't care if you're all alone, but I do, I do.
The highway won't dry your tears
The highway don't need you here
The highway don't care if you're coming home, but I do, I do.

I bet you got a dead cell phone in your shotgun seat
Yeah, I bet you're bending God's ear talking 'bout me.
You're trying not to let the first tear fall out
Trying not to think about turning around
You're trying not to get lost in the sound but that song is always on, so you sing along

I can't live without you, I can't live without you, baby
I can't live without you I can't live without you baby, oh baby

The highway won't hold you tonight
The highway don't know you're alive
The highway don't care if you're all alone, but I do, I do.
The highway won't dry your tears
The highway don't need you here
The highway don't care if you're coming home, but I do, I do.

I can't live without you, I can't live without you, baby
I can't live without you I can't live without you, baby, oh baby

The highway don't care
The highway don't care
The highway don't care, but I do, I do.

I can't live without you, I can't live without you, baby (The highway don't care, the highway don't care)
I can't live without you I can't live without you, baby, oh baby (The highway don't care, but I do, I do)

I can't live without you, I can't live without you, baby (The highway don't care, the highway don't care)
I can't live without you I can't live without you, baby (The highway don't care, but I do I do)

I can't live without you, I can't live without you, baby (The highway don't care, the highway don't care)
I can't live without you I can't live without you, baby, oh baby (The highway don't care, but I do I do)

I can't live without you, I can't live without you, baby .")
  #song 57 (fifty-seven)
  Song.create!(artist:  "Miranda Lambert",
               title: "Mama's Broken Heart",
               rank: 57,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Brandy Lynn Clark, Shane Mcanally, Kacey Musgraves",
               year: 2011,
               lyrics: "I cut my bangs with some rusty kitchen scissors
I screamed his name til the neighbors called the cops
I numbed the pain at the expense of my liver
Don't know what I did next all I know, I couldn't stop

Word got around to the barflies and the baptists
My mama's phone started ringin' off the hook
I can hear her now sayin' she ain't gonna have it
Don't matter how you feel, it only matters how you look

Go and fix your make up girl it's, just a break up run an'
Hide your crazy and start actin' like a lady 'cause I
Raised you better, gotta keep it together even when you fall apart,
But this ain't my mama's broken heart

I wish I could be just a little less dramatic like a,
Kennedy when Camelot went down in flames
Leave it to me to be holdin' the matches
When the fire trucks show up and there's nobody else to blame

Can't get revenge and keep a spotless reputation
Sometimes revenge is a choice you gotta make
My mama came from a softer generation
Where you get a grip and bite your lip just to save a little face

Go and fix your make up girl it's, just a break up run an'
Hide your crazy and start actin' like a lady 'cause I
Raised you better, gotta keep it together even when you fall apart,
But this ain't my mama's broken heart

Powder your nose, paint your toes line your lips and keep 'em closed
Cross your legs, dot your I's and never let 'em see you cry

Go and fix your make up well it's, just a break up run an'
Hide your crazy and start actin' like a lady 'cause I
Raised you better, gotta keep it together even when you fall apart,
But this ain't my mama's broken heart .")
  #song 58 (fifty-eight)
  Song.create!(artist:  "Blake Shelton",
               title: "Sure Be Cool If You Did",
               rank: 58,
               copyright: "Universal Music Publishing Group, Round Hill Music Big Loud Songs",
               writers: "Chris Tompkins, Jimmy Robbins, Rodney Dale Clawson",
               year: 2013,
               lyrics: "I was gonna keep it real like chill like only have a drink or two
But it turned into a party when I started talking to you
Now you're standing in the neon looking like a high I wanna be on
Baby it's your call,
No pressure at all

You don't have to throw back your pretty pink lemonade shooter
And lean a little closer
You don't have to keep on smiling that smile that's driving me wild
And when the night is almost over
Meet me in the middle of a moonlit Chevy bench seat
And do a little bit of country song, hanging on
You don't have to keep me falling like this
But it'd sure be cool if you did

You can't shoot me down 'cause you've already knocked me dead
Got me falling apart with my heart talking out of my head
Let your mind take a little back road just as far as you wanna go
Baby, I'll do,
Whatever you wanna do, wanna do

You don't have to throw back your pretty pink lemonade shooter
And lean a little closer
You don't have to keep on smiling that smile that's driving me wild
And when the night is almost over
Meet me in the middle of a moonlit Chevy bench seat
And do a little bit of country song, hanging on
You don't have to keep me falling like this
But it'd sure be cool if you did

Have a night that you'll never forget
And now you're standing in the neon looking like a high I wanna be on

You don't have to throw back your pretty pink lemonade shooter
And lean a little closer
You don't have to keep on smiling that smile that's driving me wild
And when the night is almost over
Meet me in the middle of a moonlit Chevy bench seat
And do a little bit of country song, hanging on
You don't have to keep me falling like this
But it'd sure be cool if you did
Yeah, it'd sure be cool if you did .")
  #song 59 (fifty-nine)
  Song.create!(artist:  "Doug E. Fresh & The Get Fresh Crew",
               title: "Runnin' Outta Moonlight",
               rank: 59,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, DO Write Music LLC",
               writers: "Ashley Gorley, Dallas Davidson, Kelley Lovelace",
               year: 2013,
               lyrics: "
  Don't you worry 'bout gettin' fixed up
When you wake up, you're pretty enough
Look out your window at the cloud of dust that's my headlights, that's my truck

Come on baby, don't you keep me waitin'
I gotta go, I've got a reservation tailgate for two underneath the stars
Kiss on your lips when you're in my arms

Whoa, girl every now and then you get a night like this
Whoa, this is one that we don't wanna miss, no

Come on baby let me take you on a night ride
Windows down, sittin' on my side
Tick tock now we're knocking on midnight
Me and you girl runnin' outta moonlight
I wanna hold you 'till the break of dawn
Hear the crickets sing a riverside love song
Hey baby, all we got is all night
Come on now we're runnin' outta moonlight

Girl I bet you thought I lost my mind
Outta the blue pulling into your drive
Wonder why I got you way out here
Have you ever seen a sky this clear

Whoa, girl you never look better than you do right now
Whoa, oh heaven let your light shine down

Come on baby let me take you on a night ride
Windows down, sittin' on my side
Tick tock now we're knocking on midnight
Me and you girl runnin' outta moonlight
I wanna hold you 'till the break of dawn
Hear the crickets sing a riverside love song
Hey baby, all we got is all night
Come on now we're runnin' outta moonlight

Whoa, girl, every now and then you get a night like this,
Whoa, this is one that we don't wanna miss, no

Come on baby let me take you on a night ride
Windows down, sittin' on my side
Tick tock now we're knocking on midnight
Me and you girl runnin' outta moonlight
I wanna hold you 'till the break of dawn
Hear the crickets sing a riverside love song
Hey baby, all we got is all night
Come on now we're runnin' outta moonlight

Hey baby don't it feel so right
Come on now we're runnin' outta moonlight
All I wanna do is hold you tight come on, come on, come on we're runnin' outta moonlight

Whoa, yea we're runnin' outta moonlight, whoa .")
  #song 60 (sixty)
  Song.create!(artist:  "Luke Bryan",
               title: "That's My Kind Of Night",
               rank: 60,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: " Ashley Gorley, Dallas Davidson, Chris Destefano",
               year: 2013,
               lyrics: "I got that real good feel good stuff
Up under the seat of my big black jacked up truck
Rollin' on 35s
Pretty girl by my side

You got that sun tan skirt and boots
Waiting on you to look my way and scoot
Your little hot self over here
Girl hand me another beer, yeah!

All them other boys wanna wind you up and take you downtown
But you look like the kind that likes to take it way out
Out where the corn rows grow, row, row my boat
Floatin' down the Flint River, catch us up a little catfish dinner
Gonna sound like a winner, when I lay you down and love you right
Yeah, that's my kind of night!

Might sit down on my diamond plate tailgate
Put in my country rock hip-hop mixtape
Little Conway, a little T-Pain, might just make it rain

You can hang your t-shirt on a limb
Hit that bank and we can ease on in
Soak us up a little moonlight
You know I know what you like, yeah!

All them other boys wanna wind you up and take you downtown
But you look like the kind that likes to take it way out
Out where the corn rows grow, row, row my boat
Floatin' down the Flint River, catch us up a little catfish dinner
Gonna sound like a winner, when I lay you down and love you right
Yeah, that's my kind of night!
Yeah, that's my kind of night!

My kind of your kind of its this kind of night
We dance in the dark and your lips land on mine
Gonna get our love on
Time to get our buzz on

All them other boys wanna wind you up and take you downtown
But you look like the kind that likes to take it way out
Out where the corn rows grow, row, row my boat
Floatin' down the Flint River, catch us up a little catfish dinner
Gonna sound like a winner, when I lay you down and love you right
Yeah, that's my kind of night!
Yeah, that's my kind of night!
Yeah, that's my kind of night!
Yeah, that's my kind of night!
Come on .")
  #song 61 (sixty-one)
  Song.create!(artist:  "The Band Perry",
               title: "Better Dig Two",
               rank: 61,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC, Downtown Music Publishing",
               writers: "Brandy Lynn Clark, Shane Mcanally, Trevor Rosen",
               year: 2013,
               lyrics: "I told you on the day we wed
I was gonna love you 'til I's dead
Made you wait 'til our wedding night
That's the first and the last time I wear white

So if the ties that bind ever do come loose
Tie them in a knot like a hangman's noose
'Cause I'll go to heaven or I'll go to hell
Before I'll see you with someone else

Put me in the ground
Put me six foot down
And let the stone say:

Here lies the girl whose only crutch
Was loving one man just a little too much
If you go before I do
I'm gonna tell the gravedigger that he better dig two

Well,
It won't be whiskey, won't be meth
It'll be your name on my last breath
If divorce or death ever do us part
The coroner will call it a broken heart

So put me in the ground
Put me six foot down
And let the stone say:

Here lies the girl whose only crutch
Was loving one man just a little too much
If you go before I do
I'm gonna tell the gravedigger that he better dig two

Dig two
Ooh, ooh

I took your name when I took those vows
I meant 'em back then and I mean 'em right now,
Oh, right now

If the ties that bind ever do come loose
If forever ever ends for you
If that ring gets a little too tight
You might as well read me my last rights

And let the stone say:

Here lies the girl whose only crutch
Was loving one man just a little too much
If you go before I do
Gonna tell the gravedigger that he better dig, uh!

Heavy stone right next to mine,
We'll be together 'til the end of time
Don't you go before I do,
I'm gonna tell the gravedigger that he better dig two

I told you on the day we wed
I was gonna love you 'til I's dead .")
  #song 62 (sixty-two)
  Song.create!(artist:  "Taylor Swift",
               title: "We Are Never Ever Getting Back Together",
               rank: 62,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC",
               writers: "Taylor Swift, Max Martin, Johan Shellback",
               year: 2012,
               lyrics: "I remember when we broke up the first time
Saying, 'This is it, I've had enough, 'cause like
We hadn't seen each other in a month
When you said you needed space. (What?)
Then you come around again and say
'Baby, I miss you and I swear I'm gonna change, trust me.'
Remember how that lasted for a day?
I say, 'I hate you,' we break up, you call me, 'I love you.'

Ooh, we called it off again last night
But ooh, this time I'm telling you, I'm telling you

We are never ever ever getting back together,
We are never ever ever getting back together,
You go talk to your friends, talk to my friends, talk to me
But we are never ever ever ever getting back together

Like, ever...

I'm really gonna miss you picking fights
And me falling for it screaming that I'm right
And you would hide away and find your peace of mind
With some indie record that's much cooler than mine

Ooh, you called me up again tonight
But ooh, this time I'm telling you, I'm telling you

We are never, ever, ever, ever getting back together
We are never, ever, ever, ever getting back together
You go talk to your friends, talk to my friends, talk to me
But we are never ever ever ever getting back together

Ooh, yeah, ooh yeah, ooh yeah
Ooh, yeah, ooh yeah, ooh yeah
Ooh, yeah, ooh yeah, ooh yeah
Oh oh oh

I used to think that we were forever ever
And I used to say, 'Never say never...'
Uggg... so he calls me up and he's like, 'I still love you,'
And I'm like... 'I just... I mean this is exhausting, you know, like,
We are never getting back together. Like, ever'

No!

We are never ever ever getting back together
We are never ever ever getting back together
You go talk to your friends, talk to my friends, talk to me
But we are never ever ever ever getting back together

We, ooh, getting back together, ohhh,
We, ooh, getting back together

You go talk to your friends, talk to my friends, talk to me (talk to me)
But we are never ever ever ever getting back together .")
  #song 63 (sixty-three)
  Song.create!(artist:  "Tyler Farr",
               title: "Redneck Crazy",
               rank: 63,
               copyright: "Round Hill Music Big Loud Songs",
               writers: "Chris Tompkins, Josh Kear, Mark Irwin",
               year: 2013,
               lyrics: "Gonna drive like hell through your neighborhood
Park this Silverado on your front lawn
Crank up a little Hank, sit on the hood and drink
I'm about to get my pissed off on

I'm gonna aim my headlights into your bedroom windows
Throw empty beer cans at both of your shadows
I didn't come here to start a fight, but I'm up for anything tonight
You know you broke the wrong heart baby, and drove me redneck crazy

Wish I knew how long it's been going on
How long you've been getting some on the side
Nah, he can't amount to much, by the look of that little truck
Well he wont be getting any sleep tonight

I'm gonna aim my headlights into your bedroom windows
Throw empty beer cans at both of your shadows
I didn't come here to start a fight, but I'm up for anything tonight
You know you broke the wrong heart baby, and drove me redneck crazy
Redneck crazy

Did you think I'd wish you both the best, endless love and happiness
You know that's just not, the kind of man I am
I'm the kind that shows up at your house at 3 AM

I'm gonna aim my headlights into your bedroom windows
Throw empty beer cans at both of your shadows
I didn't come here to start a fight, but I'm up for anything tonight
You gone and broke the wrong heart baby, and drove me redneck crazy
You drove me redneck crazy, oh yeah yeah .")
  #song 64 (sixty-four)
  Song.create!(artist:  "Florida Georgia Line",
               title: "Round Here",
               rank: 64,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Tracy Lynn Curry, Dido Armstrong, Stanley Bernard Benton, Paul Philip Herman, Cordozar Calvin Broadus",
               year: 2012,
               lyrics: "A hammer and a nail, stacking them bails
I'm dog tired by the five o'clock hour
But I'm ready to raise some hell
And Jesse's getting ready, I'm gassing up the Chevy
I'm gonna pick her up at six
I hope she's gonna wear them jeans with the tear
That her mama never fixed

The moon comes up and the sun goes down
We find a little spot on the edge of town
Twist off, sip a little, pass it around
Dance in the dust turn the radio up
And that fireball whiskey whispers
Temptation in my ear
It's a feeling alright, Saturday night
And that's how we do it round here.
Yeah that's how we do it round here

Mud on the grips, wild cherry on her lips
I've been working and trying and flirting and dying
For an all night kind of kiss
And country on the boom box
And candles on the tool box
I'm doing everything right
Got the country boy charm turned all the way on tonight.

Yeah the moon comes up and the sun goes down
We find a little spot on the edge of town
Twist off, sip a little, pass it around
Dance in the dust, turn the radio up
And that fireball whisky whispers
Temptation in my ear
It's a feeling alright, Saturday night
And that's how we do it round here.
Yeah that's how we do it round here.

Yeah the moon comes up and the sun goes down
We find a little spot on the edge of town
Twist off, sip a little, pass it around
Dance in the dust turn the radio up
And that fireball whiskey whispers
Temptation in my ear
It's a feeling alright, Saturday night
And that's how we do it round here.
Yeah that's how we do it round here.
Come on.
Yeah that's how we do it round here.
Yeah that's how we do it round here. .")
  #song 65 (sixty-five)
  Song.create!(artist: "Hunter Hayes",
               title: "Wanted",
               rank: 65,
               copyright: "Universal Music Publishing Group",
               writers: "Vanessa Carlton",
               year: 2011,
               lyrics: "You know I'd fall apart without you
I don't know how you do what you do
'Cause everything that don't make sense about me
Makes sense when I'm with you

Like everything that's green, girl, I need you
But it's more than one and one makes two
Put aside the math and the logic of it
You gotta know you're wanted too

'Cause I wanna wrap you up
Wanna kiss your lips
I wanna make you feel wanted
And I wanna call you mine
Wanna hold your hand forever
And never let you forget it
Yeah, I, I wanna make you feel wanted

Anyone can tell you you're pretty, yeah
And you get that all the time, I know you do
But your beauty's deeper than the make-up
And I wanna show you what I see tonight...

When I wrap you up
When I kiss your lips.
I wanna make you feel wanted
And I wanna call you mine
Wanna hold your hand forever
And never let you forget it
'Cause, baby, I, I wanna make you feel wanted

As good as you make me feel
I wanna make you feel better
Better than your fairy tales
Better than your best dreams
You're more than everything I need
You're all I ever wanted
All I ever wanted

And I just wanna wrap you up
Wanna kiss your lips
I wanna make you feel wanted
And I wanna call you mine
Wanna hold your hand forever
And never let you forget it
Yeah, I wanna make you feel wanted
Baby, I wanna make you feel wanted

You'll always be wanted .")
  #song 66 (sixty-six)
  Song.create!(artist:  "Lady Antebellum",
               title: "Downtown",
               rank: 66,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Natalie Hemby, Luke Laird, Shane Mcanally",
               year: 2013,
               lyrics: "Well, all the parties on the streets are talking, store front mannequins sleeping in lights
We used to smoke while we were jaywalking like it was your birthday every other Saturday night
Knew The bands so we never payed our cover
Wrote our names on the bathroom tiles
We never dressed to impress all the others
They would let us in on a laid back kind of style
But boy you know it's been a while

I don't know why you don't take me downtown like you got anywhere better to be
Talk it up and give me the go round round like a good time tease
I'm only counting on your cancellation
When I should be counting on you at my door
Did you forget about how we went around
I don't know why you don't take me downtown anymore
Oh anymore

I got some platforms sitting in the corner
They wanna stroll on a city sidewalk
I got a dress that 'll show a little uh uh
But you ain't getting uh oh if you don't come pick me up (damn)
Show me off (wow), you might be tired but I'm not

And I don't know why you don't take me down town like you got anywhere better to be
Talk it up and give me the go round round like a good time tease
I'm only counting on your cancellation
When I should be counting on you at my door
Did you forget about how we went around
I don't know why you don't take me downtown anymore

I don't know why you don't take me down town like you got anywhere better to be
Talk it up and give me the go round round like a good time tease
I'm only counting on your cancellation
When I should be counting on you at my door
Did you forget about how we went around
I don't know why you don't take me downtown anymore
Oh anymore
Yeah don't know why you don't take me downtown
I don't know why you don't take me downtown anymore
I just don't get it .")
  #song 67 (sixty-seven)
  Song.create!(artist:  "Billy Currington",
               title: "Hey Girl",
               rank: 67,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Ashley Gorley, Chris Destefano, Rhett Akins",
               year: 2013,
               lyrics: "
  Hey, girl, what's your name, girl
I've been looking at you,
And every guy here's doing the same, girl
It'd be a shame, girl
If I let you just walk on by

'Cause I'm all caught up in your sweet smile
A girl like you comes around every once in awhile
So hot, gotta give it a shot
Gotta get, get a little of what you've got

I know you don't know me
But I can't leave here lonely
Knowing I didn't even try to make you mine
And you might think I'm crazy, girl
But who could blame me
You're looking so fine, got me all tongue tied
And the only line I can think to say is

Hey, girl, whatcha think, girl
You look a little thirsty
Let me go get you something to drink, girl
Before you blink, girl
I'll be right back by your side

And if you want, we can roll out
I know my way around this town
I got a shotgun seat for you
You can tell me where it is you want to take off to

I know you don't know me
But I can't leave here lonely
Knowing I didn't even try to make you mine
And you might think I'm crazy, girl
But who could blame me
You're looking so fine, got me all tongue tied
And the only line I can think to say is, 'Hey, girl'

Hey, hey!

Oh, baby, you're still standing here
I guess you must like what you hear
Hey, girl, whatcha say, girl
About you and me getting away, girl

I know you don't know me
But I can't leave here lonely
Knowing I didn't even try to make you mine
And you might think I'm crazy, girl
But who could blame me
You're looking so fine, got me all tongue tied
And the only line I can think to say is, 'Hey, girl'

Oh, yeah, whoa, hey, girl

You're looking so fine, got me all tongue tied
And the only line I can think to say is

Hey, girl, oh, yeah .")
  #song 68 (sixty-eight)
  Song.create!(artist:  "Brett Eldredge",
               title: "Don't Ya",
               rank: 68,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Ashley Gorley, Brett Eldredge, Chris Destefano",
               year: 2013,
               lyrics: "Girl you cut those jeans just right
I know you didn't buy 'em like that
So baby don't even try that
You dance, oh you move yeah
Like there ain't nobody watchin'
But girl you know I'm watchin'

Don't lie, you've got it all figured out
That smile has got me spinnin' around
Don't even try actin' like it ain't no thing
'Cause I can see you move a little closer, closer
Girl I gotta get to get to know ya, know ya
Everything about ya makes me want ya, want ya
Know what you're doin' baby don't ya, don't ya

You've been lookin' over here all night
And when I look, you look away
Oh I love the little games you play
Yea I bet, you didn't mean, to brush my hand when you walked by me
Like you didn't mean to drive me crazy

Don't lie, you've got it all figured out
That smile, has got me spinnin' around
Don't even try, actin' like it ain't no thing
'Cause I can see you move a little closer, closer
Girl I gotta get to get to know ya, know ya
Everything about ya makes me want ya, want ya
Know what you're doin baby don't ya, don't ya

Wanna get outta here and let me show ya
How good it would feel to hold ya
Put your pretty little head on my shoulder, oh yea

Don't lie, you've got it all figured out
That smile got me spinnin' around
Don't even try actin' like it ain't no thing

No don't lie, you've got it all figured out
Girl that smile has got me spinnin' around
Don't even try, actin' like it ain't no thing

'Cause I can see you move a little closer, closer
Girl I gotta get to get to know ya, know ya
Everything about ya makes me want ya, want ya
Know what you're doin' baby don't ya, don't ya .")
  #song 69 (sixty-nine)
  Song.create!(artist:  "The Band Perry",
               title: "DONE.",
               rank: 69,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: " Jacob Bryant, John Davidson, Neil Perry, Reid Perry",
               year: 2013,
               lyrics: "You've been wearing that crown and tearing me down
It's been a while since you've treated me right
You strung me along for far to long 'cause I never gave up the fight, until now

It's gonna hit you hard til you see stars
It's gonna put through you a world of hurt
Oh I don't believe in getting even but giving what you deserve
Oh my, oh my

Mama always told me that I should play nice
She didn't know you when she gave me that advice
I'm through, with you
You're one bridge I'd like to burn
Bottle up the ashes, smash the urn
I'm through with you, la ti da
I don't wanna be your just for fun
Don't wanna be under your thumb
All I wanna be is done, done

You crossed the line too many times,
I'm gonna put you in your place
You play with dynamite don't be surprised when I blow up in your face
Oh my, oh my

Mama always told me that I should play nice
She didn't know you when she gave me that advice
I'm through, with you
You're one bridge I'd like to burn
Bottle up the ashes, smash the urn
I'm through with you, la ti da
I don't wanna be part of your fun
Don't wanna be under your thumb
All I wanna be is done

with your selfish ways
And all the games you play
I'm through with you and everything you say

Mama told me that I should play nice
She didn't know you when she gave that advice

Mama always told me that I should play nice
But she didn't know you when she gave that advice
I'm through, with you
You're one bridge I'd like to burn
Scatter the ashes, smash the urn
I'm through with you, la ti da (la ti da)
I don't wanna be your just for fun,
Don't wanna be under your thumb
All I wanna be is done
All I wanna be is done

Hey ooh, I wanna be done (so done)
So done (so done) so done .")
  #song 70 (seventy)
  Song.create!(artist:  "Jason Aldean",
               title: "Night Train",
               rank: 70,
               copyright: "Peermusic Publishing, Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Neil Thrasher, Michael William Dulaney",
               year: 2012,
               lyrics: "I've been thinkin' bout you all day baby
Waitin' on that sun to go down
Whatcha say I pick you up after work
Slide over, we slip out to the outskirts of town
Got a blanket and a fifth of comfort
A little something to knock off the edge
It's supposed to get a little cool tonight
Looks like I'm gonna have to hold you tight... yeah

Bout a mile off old mill road
In that spot nobody knows
Park the truck and we take off running
Hurry up, girl I hear it coming
Got a moon and a billion stars
Sound of steel and old box cars
The thought of you is driving me insane
Come on baby lets go listen to the night train

Yeah I hope it's gonna be a long one
If were lucky it's moving slow
Wouldn't mind if it lasted all night
Lying next to you on that hillside lets go

Bout a mile off old mill road
In that spot nobody knows
Park the truck and we take off running
Hurry up girl, I hear it coming
Got a moon and a billion stars
Sound of steel and old box cars
The thought of you is driving me insane
Come on baby lets go listen to the night train

Let's go listen to the night train

Bout a mile off old mill road
In that spot nobody knows
Park the truck and we take off running
Hurry up, girl I hear it coming
Got a moon and a billion stars
Sound of steel and old box cars
The thought of you is driving me insane
Come on baby lets go listen to the night train

Let's go listen to the night train .")
  #song 71 (seventy-one)
  Song.create!(artist:  "Kip Moore",
               title: "Hey Pretty Girl",
               rank: 71,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Kipling Moore, Dan Couch",
               year: 2012,
               lyrics: "Hey pretty girl, won't you look my way?
Love's in the air tonight
You can bet you make this old boys day
Hey pretty girl, won't you look my way?

Hey pretty girl, can I have this dance?
And the next one after that
Gonna make your mind there's a real good chance
Hey pretty girl, can I have this dance?

Hey pretty girl, it feels so right
Just like it's meant to be
All wrapped up in my arms so tight
Hey pretty girl, it feels so right

Life's a lonely, winding ride
Better have the right one by your side
And happiness don't drag its feet
And time moves faster than you think

Hey pretty girl, wanna take you home
My momma's gonna love you
She'll make me sleep on the couch, I know
But hey pretty girl, wanna take you home

Hey pretty girl, lets build some dreams
And a house on a piece of land
Plant some roots and some apple trees
Hey pretty girl, lets build some dreams

Life's a lonely, winding ride
Better have the right one by your side
And happiness don't drag its feet
And time moves faster than you think

Hey pretty girl, you did so good
Our baby's got your eyes
And a fighters heart like I knew she would
Hey pretty girl, you did so good

Hey pretty girl when I see the light
When it's my time to go
Gonna thank the Lord for a real good life
Pretty little girl and a beautiful wife .")
  #song 72 (seventy-two)
  Song.create!(artist:  "Gary Allan",
               title: "Every Storm (Runs Out Of Rain)",
               rank: 72,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Hillary Lindsey, Gary Allan, Matt Warren",
               year: 2013,
               lyrics: "I saw you standing in the middle of the thunder and lightning
I know you're feeling like you just can't win, but you're trying
It's hard to keep on keepin' on, when you're being pushed around
Don't even know which way is up, you just keep spinning down, 'round, down

Every storm runs, runs out of rain
Just like every dark night turns into day
Every heartache will fade away
Just like every storm runs, runs out of rain

So hold your head up and tell yourself that there's something more
And walk out that door
Go find a new rose, don't be afraid of the thorns
'Cause we all have thorns
Just put your feet up to the edge, put your face in the wind
And when you fall back down, keep on rememberin'

Every storm runs, runs out of rain
Just like every dark night turns into day
Every heartache will fade away
Just like every storm runs, runs out of rain

It's gonna run out of pain
It's gonna run out of sting
It's gonna leave you alone
It's gonna set you free
Set you free

Every storm runs, runs out of rain
Just like every dark night turns into day
Every heartache will fade away
Just like every storm runs, runs out of rain

It's gonna set you free,
It's gonna run out of pain,
It's gonna set you free .")
  #song 73 (seventy-three)
  Song.create!(artist:  "Easton Corbin",
               title: "All Over The Road",
               rank: 73,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, Round Hill Music Big Loud Songs",
               writers: "Ashley Gorley, Wade Kirby, Carson Chamberlain",
               year: 2012,
               lyrics: "No sir I ain't been drinking
I ain't even had one beer
This sweet thing's got me buzzing
From whispering in my ear

Just take a peek up in here
At this little hot mess
Mister, you'll understand
I'm doing my best

And I know I'm all over the road
I can't help but go
A little bit of left, a little bit of right
It's hard to drive with her hand over here on my knee
When she's all over me, I'm all outta control
All over the road

Don't wanna get no ticket
Don't wanna cause no wreck
It's hard to concentrate with her pretty little lips on my neck
I say 'girl take it easy'
She laughs, says 'it'll be fine'
How am I supposed to keep it between the lines

Yeah I know I'm all over the road
I can't help but go
A little bit of left, a little bit of right
It's hard to drive with her hand over here on my knee
When she's all over me, I'm all outta control
And all over the road

Something 'bout these wheels rolling
Radio playing gets her going
I'm trying to get her home as fast as I can go

And I know I'm all over the road
I can't help but go
A little bit of left, a little bit of right
It's hard to drive with her hand over here on my knee
Have a little mercy on me
Sir I'm sorry I know
I'm all over the road
All over the road
All over the road .")
  #song 74 (seventy-four)
  Song.create!(artist:  "Carrie Underwood",
               title: "See You Again",
               rank: 74,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "David Hodges, Hillary Lindsey, Carrie Underwood",
               year: 2012,
               lyrics: "Oh oh oh oh oh oh oh oh
Said goodbye, turned around
And you were gone, gone, gone
Faded into the setting sun,
Slipped away
But I won't cry
'Cause I know I'll never be lonely
For you are the stars to me,
You are the light I follow

I will see you again, oh
This is not where it ends
I will carry you with me, oh
'Till I see you again

Oh oh oh oh oh oh oh oh
I can hear those echoes in the wind at night
Calling me back in time
Back to you
In a place far away
Where the water meets the sky
The thought of it makes me smile
You are my tomorrow

I will see you again, oh
This is not where it ends
I will carry you with me, oh
'Till I see you again

Sometimes I feel my heart is breaking
But I stay strong and I hold on 'cause I know
I will see you again, oh
This is not where it ends
I will carry you with me, yeah yeah

I will see you again, oh
This is not where it ends
I will carry you with me, oh
'Till I see you again
Oh oh oh oh oh oh oh oh
'Till I see you again (Oh oh oh oh oh oh oh oh)
'Till I see you again yeah yeah yeah whoa
'Till I see you again
Said goodbye turned around
And you were gone, gone, gone. .")
  #song 75 (seventy-five)
  Song.create!(artist:  "George Strait",
               title: "Give It All We Got Tonight!",
               rank: 75,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Tim James, Mark D. Bright, Philip Eugene O'donnell",
               year: 2013,
               lyrics: "July moonlight shines
Your pretty little head on my shoulder
Pull over on the side of the road
Oh my God, you're something
Like nothing I've ever seen
If I'm asleep girl, let me dream

Baby fall into my kiss
It should just happen like this
Trust it so much that there's no one else but us and
This moment that says it's so right
'Cause that's all we have in this life
Drink up this love, baby, give it all we got tonight

Summer honeysuckle
Leaking through a rolled down window
We both know when that seat lays back
Anything can happen
So imagine it'll never end
Just close your eyes and you can see that we are where we're meant to be

Baby fall into my kiss
It should just happen like this
Trust it so much that there's no one else but us and
This moment that says it's so right
'Cause that's all we have in this life
Baby, drink up this love, give it all we got tonight
Give it all we got tonight

Baby fall into my kiss
It should just happen like this
Trust it so much that there's no one else but us and
This moment that says it's so right
'Cause that's all we have in this life
Drink up this love, c'mon, give it all we got tonight
Give it all we got tonight .")
  #song 76 (seventy-six)
  Song.create!(artist:  "Josh Turner",
               title: "Time Is Love",
               rank: 76,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Ole MM",
               writers: "Tony Martin, Tom Shapiro, Mark Nesler",
               year: 2012,
               lyrics: "I know I gotta put in the hours,
Make the money while the sunlight shines
But anything I gotta get done,
It can get done some other time

Time is love, gotta run,
Love to hang longer,
But I got someone who waits,
Waits for me and right now
She's where I need to be,
Time is love, gotta run

I only get so many minutes,
Don't wanna spend 'em all on the clock
In the time that we spent talkin',
How many kisses have I lost?

Time is love, gotta run,
Love to hang longer,
But I got someone who waits,
Waits for me and right now
She's where I need to be,
Time is love, gotta run.

Gotta fly, fly
Before one more moment goes by

Time is love, gotta run,
Love to hang longer,
But I got someone who waits,
Waits for me and right now
She's where I need to be

Time is love, gotta run,
Love to hang longer,
But I got someone who waits,
Waits for me and right now
She's where I need to be,
Time is love, gotta run.

Time is love, gotta run .")
  #song 77 (seventy-seven)
  Song.create!(artist:  "Brantley Gilbert",
               title: "You Don't Know Her Like I Do",
               rank: 77,
               copyright: "Warner/Chappell Music, Inc",
               writers: "Brantley Keith Gilbert, James E. Mccormick, Jim Mccormick",
               year: 2010,
               lyrics: "Hey ole Friend, thanks for callin'
It's good to know somebody cares
Yeah she's gone, but I don't feel like talkin'
It might be just too much to bear
To hear somebody say it stops hurting
Or to hear somebody say she ain't worth it

'Cause you don't know her like I do
You'll never understand
You don't know what we've been through
That girl's my best friend
And there's no way you're gonna help me
She's the only one who can
No, you don't know how much I've got to lose
You don't know her like I do

I can't forget, I'm drowning in these memories
It fills my soul with all the little things
And I can't cope, it's like a death inside the family
It's like she stole my way to breathe
Don't try to tell me it stops hurting
Don't try to tell me she ain't worth it

She don't know like I do
You'll never understand
You don't know what we've been through
That girl's my best friend
And there's no way you're gonna help me
She's the only one who can
No, you don't know how much I've got to lose
You don't know her like I do, like I do

'Cause you don't know her like I do
You'll never understand
You don't know what we've been through
That girl's my best friend
There's no way you're gonna help me
She's the only one who can
No, you don't know how much I've got to lose
No, you don't know how much I've got to lose
You don't know her like I do, like I do
Not like I do, not like I do, not like I do

You'll never understand
That girl's my best friend, that girl's my best friend
That girl's my best friend .")
  #song 78 (seventy-eight)
  Song.create!(artist:  "Jana Kramer",
               title: "Why Ya Wanna",
               rank: 78,
               copyright: "The Bicycle Music Company",
               writers: "Ashley Gorley, Catt Gravitt, Chris Destefano",
               year: 2012,
               lyrics: "Out of all of the places
In this little town
Yeah, you had to come walkin' in here
And sit down

I'm hidin' and hopin'
My face ain't too red
Since we've been over, I've been tryin' like crazy
To get you out of my head

So, why ya wanna show up in a
Old t-shirt that I love
Why ya gotta tell me that I'm
Lookin' good, don't know what
You were thinkin' you were doin'
Movin' in for a hug
Like you don't know I'm comin' unglued
Why ya gotta, why ya wanna
Make me keep wantin' you

I wish ya had on sunglasses
To cover up those blue eyes
I wish you said somethin' mean
Made me glad that you said goodbye

Why can't you look off somewhere
You catch me starin' at you
Why can't you be cold like any old
Good ex would do

So, why ya wanna show up in a
Old t-shirt that I love
Why ya gotta tell me that I'm
Lookin' good, don't know what
You were thinkin' you were doin'
Movin' in for a hug
Like you don't know I'm comin' unglued
Why ya gotta, why ya wanna
Make me keep wantin' you

Keep wantin' you

Why, why, why
Would you tell me that you'd call me up sometime
Maybe we could get a drink and just catch up
Like that'd be enough
No, that ain't enough

Why ya gotta show up in a
Old T-shirt that I love
Why ya gotta tell me that I'm
Lookin' good, don't know what
You were thinkin', you were doin'
Movin' in for a hug
Like you don't know I'm comin' unglued
Why ya gotta, why ya wanna
Make me keep wantin' you

Why ya gotta, why ya wanna
Make me keep wantin' you

Why ya gotta, why ya wanna
Make me keep wantin' you

Out of all of the places
In this little town
Yeah, you had to come walkin' in here
And sit down .")
  #song 79 (seventy-nine)
  Song.create!(artist:  "Dustin Lynch",
               title: "Cowboys And Angels",
               rank: 79,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Josh Leo, Tim Nichols, Dustin Lynch",
               year: 2012,
               lyrics: "There's a want and there's a need
There's a history between
Girls like her and guys like me
Cowboys and angels
I've got boots and she's got wings
I'm hell on wheels and she's heavenly
I'd die for her and she lives for me
Cowboys and angels

We ride side by side
A cloud of dust, a ray of light
My touch is her temptation
Her kiss is my salvation
She's sweet, I'm wild, we're dangerous
Cowboys and angels

I'm not sure why her path crossed mine
Accident or grand design
Maybe God just kinda likes
Cowboys and angels

We ride side by side
A cloud of dust, a ray of light
My touch is her temptation
Her kiss is my salvation
She's sweet, I'm wild, we're dangerous
Cowboys and angels

There's a want and there's a need
There's a history between
Girls like you and guys like me
Cowboys and angels .")
  #song 80 (eighty)
  Song.create!(artist:  "Gloriana",
               title: "(Kissed You) Good Night",
               rank: 80,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, Round Hill Music Big Loud Songs",
               writers: "Josh Kear, Tom Gossin",
               year: 2012,
               lyrics: "I dropped you off
Just a little after midnight
Sat in my car
Till you turned off your porch light
I should have kissed you
I should have pushed you up against the wall
I should have kissed you
Just like I wasn't scared at all

I turned off the car
Ran through the yard
Back to your front door
Before I could knock
You turned the lock
And met me on the front porch

And I kissed you
Goodnight
And now that I've kissed you
It's a good night good night baby goodnight

You couldn't see me
Watching through the window
Wondering what went wrong
Praying that you wouldn't go
You should have kissed me
You should have pushed me up against the wall
You should have kissed me
I was right on the edge and ready to fall

So I turned off the car
Ran through the yard
Back to your front door
Before I could knock
You turned the lock
And met me on the front porch

And I kissed you
Goodnight
And now that I've kissed you
It's a good night good night baby goodnight

I turned off the car
ran through the yard
back to your front door
Half scared to death can't catch my breath
Aren't these the moments we live for

And I kissed you
Goodnight
And now that I've kissed you
It's a good night good night baby goodnight
It's a good night good night baby goodnight
It's a good night good night baby goodnight
It's a good night good night baby goodnight .")
  #song 81 (eighty-one)
  Song.create!(artist:  "Tim McGraw",
               title: "Emotional Traffic",
               rank: 81,
               copyright: "Peermusic Publishing, Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Spirit Music Group, BMG Rights Management US, LLC",
               writers: "Bryan Simpson, Ashley Gorley",
               year: 2012,
               lyrics: "I know how to hold a grudge
I can send a bridge up in smoke
And I can't count the people I've let down, the hearts I've broke
You ain't gotta dig too deep
If you wanna find some dirt on me
But I'm learning who you've been
Ain't who you've got to be
It's gonna be an uphill climb
Aw honey I won't lie

I ain't no angel
I still got a still few more dances with the devil
I'm cleanin' up my act, little by little
I'm getting there
I can finally stand the man in the mirror I see
I ain't as good as I'm gonna get
But I'm better than I used to be

I've pinned a lot of demons to the ground
I've got a few old habits left
But there's one or two I might need you to help me get
Standin' in the rain so long has left me with a little rust
But put some faith in me
And someday you'll see
There's a diamond under all this dust

I ain't no angel
I still got a still few more dances with the devil
I'm cleanin' up my act, little by little
I'm getting there
I can finally stand the man in the mirror I see
I ain't as good as I'm gonna get
But I'm better than I used to be

I ain't no angel
I still got a still few more dances with the devil
But I'm cleanin' up my act, little by little
I'm getting there
I can finally stand the man in the mirror I see
I ain't as good as I'm gonna get
But I'm better than I used to be .")
  #song 82 (eighty-two)
  Song.create!(artist:  "Eli Young Band",
               title: "Even If It Breaks Your Heart",
               rank: 82,
               copyright: "Warner/Chappell Music, Inc, Spirit Music Group",
               writers: "Eric Paslay, Will Hoge",
               year: 2011,
               lyrics: "Way back on the radio dial,
The fire got lit inside a bright-eyed child
Every note just wrapped around his soul,
From steel guitars to Memphis, all the way to rock and roll

Oh, I can hear em playin'
I can hear the ringin' of a beat up ol guitar
Oh, I can hear em singin',
'Keep on dreamin', even if it breaks your heart'

Downtown is where I used to wander
Old enough to get there but too young to get inside
So I would stand out on the sidewalk,
Listen to the music playin' every Friday night

Oh, I can hear em playin'
I can hear the ringin of a beat up ol' guitar
Oh, I can hear em singin,
'Keep on dreamin', even if it breaks your heart'

Some dreams stay with you forever,
Drag you around but bring you back to where you were
Some dreams keep on gettin' better,
Gotta keep believin' if you wanna know for sure

Oh, I can hear em playin'
I can hear the ringin of a beat up ol' guitar
Oh, I can hear em singin',
'Keep on dreamin', even if it breaks your heart'

Oh, I can hear em playin'
I can hear the ringin' of a beat up ol' guitar
Oh, I can hear em singin',
'Keep on dreamin, even if it breaks your heart'

Keep on dreamin, even if it breaks your heart

Oh oh oh, oh oh oh, keep on dreamin', oh oh oh

oh oh oh, don't let it break your heart .")
  #song 83 (eighty-three)
  Song.create!(artist:  "Easton Corbin",
               title: "Lovin' You Is Fun",
               rank: 83,
               copyright: "Sony/ATV Music Publishing LLC, Round Hill Music Big Loud Songs",
               writers: "Bob Dipiero, Jim Beavers",
               year: 2012,
               lyrics: "You got a cousin who's tellin' you somethin' that doesn't have nothin'
To do with the love that we're in, baby
I hear she's sayin' this game we're playin' should be complicated
If you won't, I'll say it, I think she's crazy ‘cause

Love don't have to be a bunch of drama
A bunch of knock-down, drag-outs cryin' in the rain
It's alright to keep it light now mama, don't you think
We're having such a good time together, and it's only just begun
My heart's never smiled so hard, baby, loving you is fun

It's fun

I'm walking around all over this town, ten feet off the ground
So happy I found you, baby, I love it
There's no self-help book on your shelf, you know too well
They're tryin to sell you somethin' for nothin' ‘cause

Love don't have to be a bunch of drama
A bunch of knock-down, drag-outs cryin' in the rain
It's alright to keep it light now mama, don't you think
We're having such a good time together, and it's only just begun
My heart's never smiled so hard, baby, loving you is fun

Love don't have to be a bunch of drama
A bunch of knock-down, drag-outs cryin' in the rain
It's alright to keep it light now mama, don't you, think
And we're having such a good time together, and it's only just begun
My heart's never smiled so hard, baby, loving you is fun

This ol' heart's never smiled so hard
Lovin' you is fun
Yeah it's, fun
Yeah it's, fun
Lovin' you is fun .")
  #song 84 (eighty-four)
  Song.create!(artist:  "Eric Church",
               title: "Springsteen",
               rank: 84,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Ole MM, BMG Rights Management US, LLC",
               writers: "Jeffery Hyde, Eric Church, Ryan Tyndell, Bruce  Springsteen",
               year: 2011,
               lyrics: "To this day when I hear that song
I see you standin' there all night long
Discount shades, store bought tan
Flip flops and cut-off jeans

Somewhere between that setting sun
I'm on fire and born to run
You looked at me and I was done
And we're, we're just getting started

I was singin' to you, you were singin' to me
I was so alive, never been more free
Fired up my daddy's lighter and we sang, oh

Stayed there 'til they forced us out
And took the long way to your house
I can still hear the sound of you sayin' don't go

When I think about you, I think about seventeen
I think about my old jeep
I think about the stars in the sky
Funny how a melody sounds like a memory
Like the soundtrack to a July Saturday night, Springsteen

I bumped into you by happenstance
You probably wouldn't even know who I am
But if I whispered your name
I bet there'd still be a spark

Back when I was gasoline
And this old tattoo had brand new ink
And we didn't care what your mom would think
About your name on my arm

Baby is it spring or is it summer
The guitar sound or the beat of that drummer
You hear sometimes late at night
On your radio

Even though you're a million miles away
When you hear 'Born in the USA'
You relive those glory days
So long ago

When you think about me, do you think about seventeen
Do you think about my old jeep
Think about the stars in the sky
Funny how a melody sounds like a memory
Like a soundtrack to a July Saturday night
Springsteen, Springsteen

Whoa-oh-oh-oh
Whoa-oh-oh-oh
Whoa-oh-oh-oh

Funny how a melody sounds like a memory
Like a soundtrack to a July Saturday night
Springsteen
Springsteen
Whoa Springsteen

Whoa-oh-oh-oh
Whoa-oh-oh-oh
Whoa-oh-oh-oh

Whoa-oh-oh-oh
Whoa-oh-oh-oh
Whoa-oh-oh-oh

Whoa-oh-oh-oh
Whoa-oh-oh-oh
Whoa-oh-oh-oh

Whoa-oh-oh-oh
Whoa-oh-oh-oh
Whoa-oh-oh-oh .")
  #song 85 (eighty-five)
  Song.create!(artist:  "Luke Bryan",
               title: "Drunk On You",
               rank: 85,
               copyright: "Sony/ATV Music Publishing LLC, Round Hill Music Big Loud Songs",
               writers: "Chris Tompkins, Josh Kear, Rodney Clawson",
               year: 2011,
               lyrics: "Cottonwood fallin' like snow in July
Sunset, riverside, four wheel drive
And a tail light circle
Roll down the windows turn it on up
Pour a little Crown in a Dixie cup
Get the party, started

Girl you make my speakers go boom boom
Dancin' on the tailgate in the full moon
That kinda thing makes a man go mm mm
You're lookin' so good in what's left of those blue jeans
Drip of honey on the money make it gotta be
The best buzz I'm ever gonna find
Hey I'm a little drunk on you
And high on summertime

If you ain't a ten you're a nine point nine
Tippin' n' spillin' that home made wine
On your tied up T-shirt
Every little kiss is drivin' me wild
Thrown little cherry bombs into my fire
Good God all mighty

Girl you make my speakers go boom boom
Dancin' on the tailgate in a full moon
That kinda thing makes a man go mm mmm
You're lookin' so good in what's left of those blue jeans
Drip of honey on the money make it gotta be
The best buzz I'm ever gonna find
Hey, I'm a little drunk on you
And high on summertime

So let's slip on out where it's a little bit darker
And when it gets a little bit hotter
We'll take it off on out in the water

Girl you make my speakers go boom boom
Dancin' on the tailgate in a full moon
That kinda thing makes a man go mm mm
You're lookin' so good in what's left of those blue jeans
Drip of honey on the money make it gotta be,
The best buzz I'm ever gonna find
Hey I'm a little drunk on you
And high on summertime

Yeah I'm a little drunk on you .")
  #song 86 (eighty-six)
  Song.create!(artist:  "Kip Moore",
               title: "Somethin' 'Bout A Truck",
               rank: 86,
               copyright: "BMG Rights Management US, LLC",
               writers: "Dan Couch, Kipling Moore",
               year: 2012,
               lyrics: "Somethin' 'bout a truck in a farmer's field
A no trespass sign, and time to kill
Nobody's gonna get hurt, so what's the big deal
Somethin' 'bout a truck in a farmer's field

Somethin' 'bout beer, sitting on ice
After a long hard day, makes it taste just right
On that dropped tailgate, on a summer night
Somethin' 'bout beer sitting on ice

And there's somethin' 'bout a girl, in a red sun dress
With an ice cold beer pressed against her lips
In that farmer's field, will make a boy a man
There's somethin' 'bout a girl in a red sundress

And there's somethin' 'bout a kiss, that's gonna lead to more
On that dropped tailgate, back behind the corn
The most natural thing you've ever felt before
There's somethin' 'bout a kiss that's gonna lead to more

And there's somethin' 'bout a truck in a field
And a girl in a red sundress with an ice cold beer to her lips

Begging for another kiss
And there's somethin' 'bout you and me and the birds and the bees
And Lord have mercy it's a beautiful thing
Ain't nothin' 'bout it luck, there's somethin' 'bout a truck

Somethin' 'bout a creek, around 2 a.m.
After a few of those beers, you wanna dive on in
You don't need no clothes, so just hang 'em on a limb
There's somethin' 'bout a creek around 2 a.m.

And there's somethin' 'bout a truck in a field
And a girl in a red sundress with an ice cold beer to her lips
Begging for another kiss
And there's somethin' 'bout you and me and the birds and the bees
And Lord have mercy, it's a beautiful thing
Ain't nothin' 'bout it luck, there's somethin' 'bout a truck
Ain't nothin' 'bout it luck, there's somethin' 'bout a truck .")
  #song 87 (eighty-seven)
  Song.create!(artist:  "Kenny Chesney",
               title: "Reality",
               rank: 87,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Brett James, Kenneth Chesney",
               year: 2010,
               lyrics: "For me it's a beach bar
Or on a boat underneath the stars
Or with my band up on a stage
For a while everything's okay

For some it's a fast car
Moonshine in a mason jar
And everybody has their way
Somehow to escape

Reality, yeah, sometimes life
Ain't all that it's cracked up to be
So let's take a chance and live this fantasy
'Cause everybody needs to break free from reality

Yeah, some days it's a bitch, it's a bummer
We need a rock and roll show in the summer
To let the music take us away
Take our minds to a better place

Where we feel that sense of freedom
Leave our worries behind, we don't need 'em
All we need is a sunny day and an old tailgate
And we'll escape

Reality, yeah, sometimes life
Ain't all that it's cracked up to be
So let's take a chance and live this fantasy
'Cause everybody needs to break free from reality

Everybody raise your hands and voice tonight
Set your soul free, let's take a carpet ride
We'll leave it all behind

Reality, yeah, sometimes life
Ain't all that it's cracked up to be
So let's take a chance and live this fantasy
'Cause everybody needs to break free from reality

Come on everybody, break free
Come on everybody, break free .")
  #song 88 (eighty-eight)
  Song.create!(artist:  "Jake Owen",
               title: "Alone With You",
               rank: 88,
               copyright: "Kobalt Music Publishing Ltd., Peermusic Publishing, The Bicycle Music Company, Songs Music Publishing",
               writers: "John Thomas Harding, Cathy Lee Gravitt, Shane Mcanally",
               year: 2011,
               lyrics: "I don't see you laugh
You don't call me back
But you kiss me when you're drunk
I don't know your friends
Don't know where you've been
Why are you the one I want

Don't put your lips up to my mouth and tell me you can't stay
Don't slip your hand under my shirt and tell me it's okay
Don't say it doesn't mater 'cause it's gonna matter to me
I can't be alone with
You've got me out on the edge every time you call
And I know it would kill me if I fall
I can't be alone with you

Please don't chain that door
I can't win this war
Your body's like a pill I shouldn't take

Don't put your lips up to my mouth and tell me you can't stay
Don't slip your hand under my shirt and tell me it's okay
Don't say it doesn't mater 'cause it's gonna matter to me
I can't be alone with
You've got me out on the edge every time you call
And I know it would kill me if I fall
I can't be alone with you

Don't put your lips up to my mouth and tell me you can't stay
Don't slip your hand under my shirt and tell me it's okay
Don't say you're gone love me 'cause you know you're gonna love me and leave
I can't be alone with
You've got me out on the edge every time you call
And I know it would kill me if I fall
I can't be alone with you

I don't see you laugh
You don't call me back
But you kiss me when you're drunk .")
  #song 89 (eighty-nine)
  Song.create!(artist:  "Miranda Lambert",
               title: "Over You",
               rank: 89,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Blake Shelton, Miranda Lambert",
               year: 2011,
               lyrics: "
  Weather man said it's gonna snow
By now I should be used to the cold

Mid-February shouldn't be so scary
It was only December
I still remember the presents, the tree, you and me

But you went away
How dare you, I miss you
They say I'll be OK
But I'm not going to ever get over you

Living alone here in this place
I think of you, and I'm not afraid

Your favorite records make me feel better
'Cause you sing along with every song
I know you didn't mean to give them to me

But you went away
How dare you, I miss you
They say I'll be OK
But I'm not going to ever get over you

It really sinks in, you know
When I see it in stone

'Cause you went away,
How dare you, I miss you
They say I'll be OK
But I'm not going to ever get over you .")
  #song 90 (ninety)
  Song.create!(artist:  "Dierks Bentley",
               title: "Home",
               rank: 90,
               copyright: "Sony/ATV Music Publishing LLC, Roba Music, Universal Music Publishing Group, Mushroom Music Pty Ltd, Kobalt Music Publishing Ltd., Reservoir Media Management Inc, Shapiro Bernstein & Co. Inc.",
               writers: "Grant Black, Stuart John N Crichton, Mari N Lorentzen, Caroline N Reed, Eleanor N Wilson",
               year: 2012,
               lyrics: "West on a plane bound west I see her stretching out below
Land blessed motherland the place where I was born
Scars yeah, she's got her scars sometimes it starts to worry me
'Cause lose, I don't wanna lose sight of who we are

From the mountains high to the wave-crashed coast
There's a way to find better days I know
It's been a long hard ride, got a ways to go
But this is still the place, that we all call home

Free, nothing feels like free
Though it sometimes means we don't get along
'Cause same, no we're not the same
But that's what makes us strong

From the mountains high, to the wave-crashed coast
There's a way to find, better days I know
It's been a long hard ride, got a ways to go
But this is still the place that we all call home oh yeah

Brave got it call it brave to chase that dream across the sea
Names then they sign their names for something they believe
Red how the blood ran red and we laid our dead in sacred ground
Just think wonder what they'd think if they could see us now

It's been a long hard ride, got a ways to go
But this is still the place, that we all call home
It's been a long hard ride and I won't lose hope
This is still the place that we all call home .")
  #song 91 (ninety-one)
  Song.create!(artist:  "Luke Bryan",
               title: "I Don't Want This Night To End",
               rank: 91,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Ben Hayslip, Luke Bryan, Dallas Davidson, Rhett Akins",
               year: 2011,
               lyrics: "Girl, I know I don't know you
But your pretty little eyes so blue,
Are pulling me in like the moon on your skin
I'm so glad you trusted me to slide up on this dusty seat
And let your hair down, and get outta town

Got the stars comin' out,
Over my hood
And all I know now
Is it's going good

You got your hands up,
You're rocking in my truck
You got the radio on,
You're singing every song
I'm set on cruise control
I'm slowly losing hold of everything I got
You're looking so damn hot
And I don't know what road we're on,
Or where we've been from starin' at you
Girl, all I know is I don't want this night to end

Gonna cuss the morning,
When it comes
'Cause I know that the rising sun,
Ain't no good for me
'Cause you'll have to leave
Gonna make the most of every mile
Do anything to make your smile,
Land on my lips
And get drunk on your kiss
The clock on the dash,
Says 3:35
There's plenty of gas,
And the night's still alive

You got your hands up,
You're rocking in my truck
You got the radio on,
You're singing every song
I'm set on cruise control
I'm slowing losing hold of everything I got
You're looking so damn hot
And I don't know what road we're on,
Or where we've been from starin' at you
Girl, all I know is I don't want this night to end

You got your hands up,
You're rocking in my truck
You got the radio on,
You're singing every song
I'm set on cruise control
I'm slowing losing hold of everything I've got
You're looking so damn hot
And I don't know what road we're on,
Or where we've been from starin' at you
Girl, all I know is I don't want this night to end

I don't want this night to end
No I don't want this night to end .")
  #song 92 (ninety-two)
  Song.create!(artist:  "Lee Brice",
               title: "Hard To Love",
               rank: 92,
               copyright: "Warner/Chappell Music, Inc, Mike Curb Music, Capitol Christian Music Group",
               writers: "Ben Glover, Billy Montana, John Ozier",
               year: 2012,
               lyrics: "
  I am insensitive, I have a tendency
To pay more attention to the things that I need
Sometimes I drink to much, sometimes I test your trust,
Sometimes I don't know why you're staying with me

I'm hard to love, hard to love,
Oh I don't make it easy,
I couldn't do it if I stood where you stood
I'm hard to love, hard to love,
You say that you need me,
I don't deserve it but I love that you love me good

I am a short fuse, I am a wrecking ball
Crashing into your heart like I do
You're like a Sunday morning full of grace and full of Jesus
And I wish that I could be more like you

I'm hard to love, hard to love,
Oh I don't make it easy,
I couldn't do it if I stood where you stood,
I'm hard to love, hard to love,
You say that you need me,
I don't deserve it but I love that you love me good
Love me good

Girl you've given me a million second chances
And I don't ever wanna take you for granted,
I'm just a man,
I'm just a man

Hard to love, hard to love,
Oh I don't make it easy,
And I couldn't do it if I stood where you stood
I'm hard to love, hard to love
And you say that you need me,
I don't deserve it but I love that you love me

Hard to love, hard to love,
Oh I don't make it easy,
And I couldn't do it if I stood where you stood
I'm hard to love, hard to love
And you say that you need me,
I don't deserve it but I love that you love me good

You love me good
You love me good .")
  #song 93 (ninety-three)
  Song.create!(artist:  "Lady Antebellum",
               title: "Dancin' Away With My Heart",
               rank: 93,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Round Hill Music Big Loud Songs",
               writers: "Dave Haywood, Josh Kear, Charles Kelley, Hillary Scot",
               year: 2011,
               lyrics: "I finally asked you to dance on the last slow song
Beneath that moon that was really a disco ball
And I can still feel my head on your shoulder
Hoping that song would never be over

I haven't seen you in ages
Sometimes I find myself wondering where you are
For me you'll always be eighteen
And beautiful and dancing away with my heart

I brushed your curls back so I could see your eyes
And the way you moved me was like you were reading my mind
I can still feel you lean into kiss me
I can't help but wonder if you ever miss me

I haven't seen you in ages
Sometimes I find myself wondering where you are
For me you'll always be eighteen
And beautiful and dancing away with my heart

You headed out to college
At the end of that summer and we lost touch
I guess I didn't realize even
At that moment we lost so much

I haven't seen you in ages
Sometimes I find myself wondering where you are
For me you'll always be eighteen
And beautiful and dancing away with my heart

Nah nah nah
Nah nah nah
Nah nah nah

Away with my heart
Nah nah nah
Nah nah nah
Nah nah nah .")
  #song 94 (ninety-four)
  Song.create!(artist:  "Angel Eyes",
               title: "Love and Theft",
               rank: 94,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Spirit Music Group",
               writers: "Jeff Coplan, Eric Gunderson, Eric Paslay",
               year: 1997,
               lyrics: "She likes whiskey with her water
She starts dancing when the stars come out
She ain't your typical preacher's daughter
She'll leave you dreamin' yeah ain't no doubt

There's a little bit of devil in her angel eyes
She's a little bit of heaven with a wild side
Got a rebel heart a country mile wide
There's a little bit of devil in her angel eyes
A little bit of devil in her angel eyes

Saturday night she's rockin' out by the bonfire
Foot hangin' from that tail gate and crankin' up the dial
Come Sunday mornin' she'll be singing with the choir
Drivin' me crazy with that kiss me smile

There's a little bit of devil in her angel eyes
She's a little bit of heaven with a wild side
Got a rebel heart a country mile wide
There's a little bit of devil in her angel eyes
There's a little bit of devil in her angel eyes

I can't stop wonderin' what it would feel like, to hold her all night
She's got that something, that sexy innocence, she must be heaven sent

There's a little bit of devil in her angel eyes
She's a little bit of heaven with a wild side
Got a rebel heart a country mile wide
There's a little bit of devil in her angel eyes

There's a little bit of devil in those angel eyes
She's a little bit of heaven with a wild side
Got a rebel heart a country mile wide
There's a little bit of devil in her angel eyes
There's a little bit of devil in her
In those angel, in those angel eyes .")
  #song 95 (ninety-five)
  Song.create!(artist:  "Lee Brice",
               title: "A Woman Like You",
               rank: 95,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, Hori Pro Entertainment Group, BMG Rights Management US, LLC",
               writers: "Jon Mckenzie Stone, Phil Barton, Johnny Bulford",
               year: 2012,
               lyrics: "Last night, outta the blue
Drifting off to the evening news
She said 'Honey, what would you do
If you'd never met me'
I just laughed, said 'I don't know,
But I could take a couple guesses though'
And then tried to dig real deep,
Said, 'Darling honestly,

I'd do a lot more offshore fishing
I'd probably eat more drive-thru chicken
Take a few strokes off my golf game
If I'd have never known your name
I'd still be driving that old green ‘Nova
I probably never would have heard of yoga
Be a better football fan
But if I was a single man
Alone and out there on the loose
Well I'd be looking for a woman like you.'

I could tell that got her attention
So I said, 'Oh yeah, I forgot to mention,
I wouldn't trade a single day
For a hundred years the other way.'
She just smiled and rolled her eyes,
'Cause she's heard all of my lines
I said, 'Come on on girl, seriously
If I hadn't been so lucky

I'd be shooting pool in my bachelor pad
Playing bass in my cover band
Restocking up cold Bud Light
Play poker every Tuesday night, yeah
I'd have a dirt bike in the shed
And not one throw pillow on the bed
I'd keep my cash in a coffee can
But if I was a single man
Alone and out there on the loose
Well I'd be looking for a woman like you.'

She knows what a mess I'd be if I didn't have her here
But to be sure, I whispered in her ear
'You know I get sick deep-sea fishing
And you make the best fried chicken
I got a hopeless golf game
I love the sound of your name
I might miss that old green ‘Nova
But I love watching you do yoga
I'd take a gold band on my hand
Over being a single man
'Cause honestly I don't know what I'd do
If I'd never met a woman like you.' .")
  #song 96 (ninety-six)
  Song.create!(artist:  "Keith Urban",
               title: "You Gonna Fly",
               rank: 96,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Chris Lucas, Jaren Johnston, Preston Brust",
               year: 2010,
               lyrics: "One, two, three baby don't think twice
Just like that you gotta brand new life
Hop in this truck and run through the red lights

Yeah where you wanna go, baby name the town
We can go up north or head down south
Roll down the windows with the radio loud

Come on turn it up, yeah
Start living your life
On the double, leave your troubles behind
You knew we're gonna be alright

You could be a black bird
On a country street
Hiding from the world with a broken wing
But you better believe, you gonna fly with me
You could be a songbird from the New Orleans
Scared of the rain just scared to sing
But you better believe, you're gonna fly with me

Well, here we are baby in the back of my van
Sun's going down sky's turning red
Stars coming out
Baby look at you now

God know how long but its been a while
Since I heard you laugh and I'd seen that smile
Felt that kiss and I can get use to this
Baby I could get use to this

You could be a black bird
On a country street
Hiding from the world with a broken wing
But you better believe, you gonna fly with me
You could be a songbird from the New Orleans
Scared of the rain just scared to sing
But you better believe, you're gonna fly with me

You're gonna fly
You're gonna fly

You could be a black bird
On a country street
Hiding from the world with a broken wing
But you better believe, you gonna fly with me
You could be a songbird from the New Orleans
Scared of the rain just as scared to sing
But you better believe yeah you better believe oh baby

You could be a black bird
On a country street
Hiding from the world with a broken wing
But you better believe, you gonna fly with me
You could be a songbird from the New Orleans
Scared of the rain just as scared to sing
But you better believe you're gonna fly with me

You're gonna fly
You're gonna fly with me baby
You better believe my honey girl
Ooh yeah, you're goin' to fly .")
  #song 97 (ninety-seven)
  Song.create!(artist:  "Zac Brown Band",
               title: "No Hurry",
               rank: 97,
               copyright: "Peermusic Publishing, Warner/Chappell Music, Inc, Reach Music Publishing",
               writers: "Zachry Alexander Brown, Wyatt Durrette, James Allen Otto",
               year: 2010,
               lyrics: "You know my old car needs washing
And the front yard needs a trim
And the telephone keeps ringing
And the boss man knows I know it's him
And the bills ain't gonna pay themselves
No matter anyway
'Cause I ain't in no hurry today

There's nothing wrong with an old cane fishing pole
And the smell of early spring
Sit down in a fold-up easy chair
On a quiet shady river bank
Let the world go on without me
Wouldn't have it any other way
'Cause I ain't in no hurry today

Ain't in no hurry
I'd be a fool now to worry
About all those things I can't change
And the time that I borrow
Can wait till tomorrow
'Cause I ain't in no hurry today

When I must return
To the cold cold ground
Have 'em take their time
When they lay this sinner down

Heaven knows that I ain't perfect
I've raised a little Cain
And I plan to raise a whole lot more
Before I hear those angels sing
(Gonna get right with the lord)
But there'll be hell to pay
But I ain't in no hurry

Ain't in no hurry
Be a fool now to worry
About all those things I can't change
And the time that I borrow
Can wait 'til tomorrow
'Cause I ain't in no hurry
Ain't in no hurry
Ain't in no hurry today .")
  #song 98 (ninety-eight)
  Song.create!(artist:  "The Band Perry",
               title: "All Your Life",
               rank: 98,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Brian Henningsen, Clara Henningsen",
               year: 2010,
               lyrics: "
  Would you walk to the edge of the ocean?
Just to fill my jar with sand?
Just in case I get the notion,
To let it run through my hands.
Let it run through my hands.

I don't want the whole world,
The sun, the moon and all their light.
I just wanna be the only girl,
You love all your life.
You love all your life.

Would you catch a couple thousand fireflies?
Yeah, and put 'em in a lamp to light my world.
All dressed up in a tux and bowtie.
Hand Delivered to a lonely girl.
To a lonely lonely girl

I don't want the whole world,
The sun, the moon and all their light.
I just wanna be the only girl,
You love all your life.
You love all your life

Lately I've been writing desperate love songs.
I mostly sing them to the wall.
You could be the center piece of my obsession
If you would notice me,
Yeah, ooh yeah

Well I don't want the whole world, no
The sun, the moon and all their light.
I just wanna be the only girl,
You love all your life.
You love all your life

You love all your life, life, yeah. .")
  #song 99 (ninety-nine)
  Song.create!(artist:  "Jason Aldean",
               title: "Fly over States",
               rank: 99,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Hori Pro Entertainment Group, BMG Rights Management US, LLC",
               writers: "Neil Thrasher, Michael Dulane",
               year: 2010,
               lyrics: "A couple guys in first class on a flight
From new York to Los Angeles,
Kinda making small talk killing time,
Flirting with the flight attendants,
Thirty thousand feet above, could be Oklahoma,
Just a bunch of square cornfields and wheat farms,
Man it all looks the same,
Miles and miles of back roads and highways,
Connecting little towns with funny names,
Who'd want to live down there in the middle of nowhere,

They've never drove through Indiana,
Met the men who plowed that earth,
Planted that seed, busted his ass for you and me,
Or caught a harvest moon in Kansas,
They'd understand why God made those fly over states,

I bet that mile long Santa Fe freight train engineer's seen it all
Just like that flatbed cowboy stacking US steel on a 3-day haul
Roads and rails under their feet
Yeah that sounds like a first class seat

On the plains of Oklahoma
Where they windshield sunset in your eyes
Like a watercolor painted sky

You'd think heaven's doors have opened
You'll understand why God made
Those fly over states

Take a ride across the badlands
Feel that freedom on your face
Breathe in all that open space
Meet a girl from Amarillo
You'll understand why God made
Why you'd want to plant your stakes
In those fly over states

Have you ever been through Indiana
On the plains of Oklahoma,
Take a ride .")
  #song 100 (one-hundred)
  Song.create!(artist:  "Carrie Underwood",
               title: "Good Girl",
               rank: 100,
               copyright: "still looking...",
               writers: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               year: 2012,
               lyrics: "Hey, good girl
With your head in the clouds
I bet you I can tell you
What you're thinkin' about
You'll see a good boy
Gonna give you the world
But he's gonna leave you cryin'
With your heart in the dirt

His lips are dripping honey
But he'll sting you like a bee
So lock up all your loving
Go and throw away the key

Hey good girl
Get out while you can
I know you think you got a good man

Why, why you gotta be so blind?
Won't you open up your eyes?
It's just a matter of time 'til you find
He's no good, girl
No good for you
You better get to getting on your goodbye shoes and go, go, go
Better listen to me
He's low, low, low

Hey, good girl
You got a heart of gold
You want a white wedding
And a hand you can hold
Just like you should, girl
Like every good girl does
Want a fairytale ending, somebody to love

But he's really good at lying
Yeah, he'll leave you in the dust
'Cause when he says forever
Well, it don't mean much
Hey good girl
So good for him
Better back away honey
You don't know where he's been

Why, why you gotta be so blind?
Won't you open up your eyes?
It's just a matter of time 'til you find
He's no good, girl
No good for you
You better get to getting on your goodbye shoes and go, go, go
Yeah yeah yeah, he's low
Yeah yeah yeah

Oh, he's no good, girl
Why can't you see?
He'll take your heart and break it
Listen to me, yeah

Why, why you gotta be so blind?
Won't you open up your eyes?
It's just a matter of time 'til you find
He's no good, he's no good

Won't you open up your eyes?
It's just a matter of time 'til you find
He's no good, girl
No good for you
You better get to getting on your goodbye shoes .")
    #song 101 (one-hundred-one)
  Song.create!(artist:  "Miranda Lambert",
               title: "Fastest Girl In Town",
               rank: 101,
               copyright: "Sony/ATV Music Publishing LLC, Nettwerk One Music (canada)ltd",
               writers: "Angaleena Presley, Miranda Lambert",
               year: 2011,
               lyrics: "You got the bullets
I got the gun
I got a hankerin'
For gettin' into somethin'

I hit the bottle
You hit the gas
I heard your '65
Could really haul some ass

I'm feelin' frisky
You're feelin' good
I guess the whiskey
Is doin' what it should

I got the cigarettes
You got a lighter
And when the sun goes down
We'll start a little fire

Ain't no use in tryin'
To slow me down
'Cause you're runnin'
With the fastest girl in town

Ain't your baby
I like 'em crazy

My reputation
Follows me around
Just makes me wanna give 'em
More to talk about

Let's go to town
For a little while
I'll be wearing nothin'
But a tattoo and a smile

Ain't no use in tryin'
To slow me down
'Cause you're runnin'
With the fastest girl in town

Ain't your baby
You're kinda crazy

Come on!

I feel the blue lights
We better run
Throw out the bottle
And I'll have the gun

If he pulls us over
I'll turn on the charm
You'll be in the slammer
And I'll be on his arm

Ain't no use in tryin'
To slow me down
'Cause you're runnin'
With the fastest girl in town

Ain't your baby
Well I told ya I was crazy
No, I ain't nobody's baby

Huh!

He's got the bullets
He's got a gun
I got the hankerin'
For gettin' into somethin' .")
  #song 102 (one-hundred-two)
  Song.create!(artist:  "Blake Shelton",
               title: "Drink On It",
               rank: 102,
               copyright: "Kobalt Music Publishing Ltd., Warner/Chappell Music, Inc, Walt Disney Music Company",
               writers: "Jessi Leigh Alexander, Jon Randall Stewart, Rodney Clawson",
               year: 2011,
               lyrics: "Hey girl, hey where ya goin'
Come back and sit back down
You look too good to be
Heading home so early now
You say you gotta work tomorrow
Got a lot on your mind
Let me buy another round
Girl I think you and I should just

Drink on it
Put our heads together and think on it
Maybe later on we can sleep on it
But for right now
Girl we just need to drink on it

We can talk rocket science
Jesus or politics
How your boyfriend cheated on you
Man he sounds like such a prick
I could use another whiskey
And your Cosmo's gettin' low
While we're trying to figure out
The next place we should go

We can drink on it
Put our heads together and think on it
Maybe later on we can sleep on it
But for right now
Girl we just need to drink on it

This place is closing down
But I don't wanna quit
Gotta good thing going girl
Let's find out what it is

And drink on it
Your place or mine
Girl we can drink on it
Dust off a bottle and drink on it
Feels like we're doin' something right
Let's find a corner of the night

Where you and I can just drink on it
Put our heads together and think on it
Maybe later on we can sleep on it
But for right now
Girl we just need to drink on it

Might make a memory that we won't forget

So let's just drink on it .")
  #song 103 (one-hundred-three)
  Song.create!(artist:  "The Band Perry",
               title: "Postcard From Paris",
               rank: 103,
               copyright: "Warner/Chappell Music, Inc, Sony/ATV Music Publishing LLC, Round Hill Music Big Loud Songs, BMG Rights Management US, LLC",
               writers: "Jeffrey Cohen, Kimberly Perry, Neil Perry, Reid Perry, Kara Dioguardi",
               year: 2010,
               lyrics: "I remember when my heart caught the fever
You were standing all alone in the summer heat
I was with my boyfriend, new boyfriend
He was as sweet as he could be
One look at you and I was through
My heart switched up on me

Like a postcard from Paris
When I've seen the real thing
It's like finding out your diamond
Was from her old promise ring
Coming back from your fortune teller
She read your cards upside down
The meanest thing you ever did is come around
And now I'm ruined
(I'm ruined)

In the evening you can catch me daydreaming
Did that moment send you reeling just like me
I should've gone over, right over
I should've never let you leave
But it's the never knowing that keeps this going
And drives me crazy

Like a postcard from Paris
When I've seen the real thing
It's like finding out your diamond
Was from her old promise ring
Come back from your fortune teller
She read your cards upside down
The meanest thing you ever did is come around

Just when I thought things were alright
My eyes played tricks on my mind
Will I ever be satisfied
'Cause all I ever seem to find

Is a postcard from Paris
When I need the real thing
It's like finding out your diamond
Is from an old promise ring
Come back from your fortune teller
She read your cards upside down
The meanest thing you ever did
The cruelest thing you ever did
The meanest thing you ever did is come around

I am ruined
I'm ruined
I am ruined, yeah
I am ruined, yeah .")
  #song 104 (one-hundred-four)
  Song.create!(artist:  "Eli Young Band",
               title: "Crazy Girl",
               rank: 104,
               copyright: "Mike Curb Music, Ole Media Management Lp",
               writers: "Lee Brice, Liz Rose",
               year: 2011,
               lyrics: "Baby why you wanna cry?
You really oughta know that I
Just have to walk away sometimes
We're gonna do what lovers do
We're gonna have a fight or two
But I ain't ever changin' my mind

Crazy girl, don't you know that I love you?
And I wouldn't dream of goin' nowhere
Silly woman, come here, let me hold you
Have I told you lately?
I love you like crazy, girl

Wouldn't miss a single day
I'd probably just fade away
Without you, I'd lose my mind
Before you ever came along
I was living life all wrong

Crazy girl, don't you know that I love you?
And I wouldn't dream of goin' nowhere
Silly woman, come here, let me hold you
Have I told you lately?
I love you like crazy, girl

Crazy girl
Crazy girl, don't you know that I love you?
And I wouldn't dream of goin' nowhere
Silly woman, come here, let me hold you
Have I told you lately,
I love you like

Crazy, girl, don't you know that I love you?
And I wouldn't dream of goin' nowhere
Silly woman, come here, let me hold you
Have I told you lately?
I love you like crazy, girl

Like crazy
Crazy girl
Like crazy
Crazy girl
Like crazy .")
  #song 105 (one-hundred-five)
  Song.create!(artist:  "Jake Owen",
               title: "Barefoot Blue Jean Night",
               rank: 105,
               copyright: "Kobalt Music Publishing Ltd., Warner/Chappell Music, Inc, Spirit Music Group",
               writers: "Dylan Y Altman, Eric Paslay, Terry Sawchuk",
               year: 2011,
               lyrics: "A full moon shinin' bright
Edge of the water, we were feelin' alright
Back down a country road
The girls are always hot, and the beer is ice cold

Cadillac, horns on the hood
My buddy Frankie had his dad hook him up good
Girls smile when we roll by
They hop in the back, and we cruise to the river side

Never gonna grow up (Whoa-oh)
Never gonna slow down (Whoa-oh)
We were shinin' like lighters in the dark
In the middle of a rock show (Whoa-oh)
We were doin' it right (Whoa-oh)
We were comin' alive (Whoa-oh)
Yeah, caught in a southern summer, barefoot, blue jean night

Blue eyes and auburn hair
Sittin' lookin' pretty by the fire in a lawn chair
New to town, and new to me
Her ruby red lips was sippin' on sweet tea
Shot me in love like a shootin' star
So, I grabbed a beer and an old guitar
Then we sat around till the break of dawn
Howlin' and singin' our favorite song

Never gonna grow up (Whoa-oh)
Never gonna slow down (Whoa-oh)
We were shinin' like lighters in the dark
In the middle of a rock show (Whoa-oh)
We were doin' it right (Whoa-oh)
We were comin' alive (Whoa-oh)
Yeah, caught up in a southern summer, barefoot, blue jean night

Whoa-oh, never gonna grow up, ha
Never gonna slow down
We were shinin' like lighters in the dark
In the middle of a rock show (Whoa-oh)
We were doin' it right (Whoa-oh)
We were comin' alive (Whoa-oh)
Yeah, caught in a southern summer, barefoot, blue jean night

Barefoot, blue jean night (Whoa-oh)
Barefoot, blue jean night (Whoa-oh)
Barefoot, blue jean night (Whoa-oh)
Barefoot, blue jean night (Whoa-oh)
Barefoot, blue jean night (Whoa-oh)
Barefoot, blue jean night .")
  #song 106 (one-hundred-six)
  Song.create!(artist:  "Rodney Atkins",
               title: "Take A Back Road",
               rank: 106,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Rhett Akins, Luke Laird",
               year: 2011,
               lyrics: "Sittin' in six lane backed up traffic
Horns a honkin', I've about had it I'm lookin' for an exit sign
Gotta get outta here, get it all off my mind
Then like a memory from your grandpa's attic
A song comes slippin' through the radio static
Changin' my mood, a little George Strait 1982

And it makes me wanna take a back road
Makes me wanna take the long way home
Put a little gravel in my travel
Unwind, unravel all night long
Makes me wanna grab my honey
Tear down some two lane country who knows
Get lost and get right with my soul
Makes me wanna take, makes me wanna take a back road

I've been cooped up, tied down 'bout forgotten
what a field looks like full of corn and cotton
If I'm gonna hit a traffic jam
Well it better be a tractor man
So sick and tired of this interstate system
I need the curvin', windin', twistin' dusty path to nowhere
With the wind blowin' through my baby's hair

Yeah it makes me wanna take a back road
Makes me wanna take the long way home
Put a little gravel in my travel
Unwind, unravel all night long
Makes me wanna grab my honey
Tear down some two lane country who knows
Get lost and get right with my soul
Makes me wanna take, makes me wanna take a back road
Some old back road

Maybe it's the feelin'
Or maybe it's the freedom
Maybe it's that shady spot
Where we park the truck when things get hot
Girl we park the truck when things get hot

And it makes me wanna take a back road
Makes me wanna take the long way home
Put a little gravel in my travel
Unwind, unravel all night long
Makes me wanna grab you honey
Tear down some two lane country who knows
Get lost and get right with my soul
Makes me wanna take, makes me wanna take a back road

Some old back road
Get back with my soul
All I gotta do is take some old back road
To the shady spot where things get hot girl
Way down, way down, way down some old back road .")
  #song 107 (one-hundred-seven)
  Song.create!(artist:  "Chris Young",
               title: "Tomorrow",
               rank: 107,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Kobalt Music Publishing Ltd., HORI PRO ENTERTAINMENT GROUP",
               writers: "CHRISTOPHER YOUNG, FRANK MYERS, ANTHONY SMITH",
               year: 2011,
               lyrics: "
Tomorrow I'm gonna leave here
I'm gonna let you go and walk away
Like every day I said I would
And tomorrow, I'm gonna listen
To that voice of reason inside
My head telling me that we're no good

But tonight I'm gonna give in one last time
Rock you strong in these arms of mine
Forget all the regrets that are bound to follow
We're like fire and gasoline
I'm no good for you
You're no good for me

We only bring each other tears and sorrow
But tonight, I'm gonna love you
Like there's no

Tomorrow I'll be stronger
I'm not gonna break down and
Call you up when my heart cries out for you
And tomorrow, you won't believe it
But when I pass your house
I won't stop no matter how bad I want to

But tonight I'm gonna give in one last time
Rock you strong in these arms of mine
Forget all the regrets that are bound to follow
We're like fire and gasoline
I'm no good for you, you're no good for me

We only bring each other tears and sorrow
But tonight, I'm gonna love you
Like there's no tomorrow

Oh baby when we're good, you know we're great
But there's too much bad for us to think,
That there's anything worth trying to save

But tonight I'm gonna give in one last time
Rock you strong in these arms of mine
Forget all the regrets that are bound to follow
We're like fire and gasoline
I'm no good for you, you're no good for me
We only bring each other tears and sorrow
But tonight, I'm gonna love you like there's no

Tomorrow, I'm gonna leave here
I'm gonna let you go and walk away
Like every day I said I would .")
  #song 108 (one-hundred-eight)
  Song.create!(artist:  "The Band Perry",
               title: "You Lie",
               rank: 108,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Aaron Henningsen, Brian Henningsen, Clara Henningsen",
               year: 2010,
               lyrics: "It ain't complicated
Well, I've grown to hate it
I never liked the taste of crow but baby I ate it
They tried to warn me
They said that you were ornery
So don't bring me those big brown eyes and tell me that you're sorry
Well you might as well throw gasoline on a fire
The way you lie

You lie like a priceless Persian rug on a rich man's floor
And you lie like a cooped up dog basking in the sunshine on my porch
Well you lie like a penny in the parking lot at the grocery store
It just comes way too natural to you
The way you lie

That ain't my perfume
I bet she had a curfew
You told me you were out with the boys and baby I believed you
So why you lookin' so nervous
You know you're gonna deserve this
I oughta kill you right now and do the whole wide world a service
Well my daddy's gonna straighten you out like a piece of wire, like a piece of wire
The way you lie

You lie like the man with the slick black hair who sold me that Ford
And you lie like a pine tree in the back yard after last month's storm
Well you lie like a penny in the parking lot at the grocery store
It just comes way too natural to you
The way you lie

Well, I'll tell you what I'm gonna do
I'm gonna drive until the big ol' muddy river
I'm gonna park my car in the middle of the mile-long bridge
And then I'm gonna cry well maybe just a little
Then I'm gonna slip off the ring that you put on my finger
And give it a big ol' fling and watch it sink
Down, down, down
There it's gonna lie
Until the Lord comes back around

Because you lie like a priceless Persian rug on a rich man's floor
You lie like a coondog basking in the sunshine on my porch
Well you lie like a penny in the parking lot at the grocery store
It just comes so dang natural to you
The way you lie
The way you lie
Well it's what you do, it's who you are .")
  #song 109 (one-hundred-nine)
  Song.create!(artist:  "Dierks",
               title: "Am I The Only One",
               rank: 109,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC",
               writers: "Jim Beavers, Jon Randall, Dierks Bentley",
               year: 2012,
               lyrics: "Well it was Friday in the p.m.
And just like every weekend
I was ready to throw down
Yeah I get a little tore up
So I call my bros up to meet me out on the town
Well wild man Willy said I like to really
But Idol was on TV
And Ray had a date
With his wife and Nate quit drinking
But he didn't tell me

Am I the only one who wants to have fun tonight?
Is there anybody out there who wants to have a cold beer
Kick it to the morning light
If I have to raise hell all by myself
I will but y'all, that ain't right
It's time to get it on
Am I the only one who wants to have fun tonight?

Well I was flying solo down at Silverado's
The joint looking like a morgue
When this country cutie with a rock and roll booty
Came strutting in through the door
Yelled who wants to dance and fired up the band
With with a Franklin and a shot of patron
She looked right at me and said
What's it gonna be
Are you gonna let me to party alone?

Am I the only one who wants to have fun tonight?
Is there anybody out there who wants to have a cold beer
Kick it to the morning light
If I have to raise hell all by myself
I will but y'all, that ain't right
It's time to get it on
Am I the only one who wants to have fun tonight?

Am I the only one who wants to have fun tonight?
Is there anybody out there who wants to have a cold beer
Kick it to the morning light
If I have to raise hell all by myself
I will but y'all, that ain't right
It's time to get it on
Am I the only one who wants to have fun tonight?

Come on come on
Get your good time on
Let's have a little fun tonight .")
  #song 110 (one-hundred-ten)
  Song.create!(artist:  "Kenny Chesney",
               title: "You And Tequila (f. Grace Potter)",
               rank: 110,
               copyright: "Kobalt Music Publishing Ltd., Universal Music Publishing Group",
               writers: "Matraca Maria Berg, Deana Kay Carter",
               year: 2010,
               lyrics: "Baby, here I am again
Kicking dust in the canyon wind
Waiting for that sun to go down
Made it up Mulholland Drive
Hell bent on getting high
High above the lights of town

'Cause you and Tequila make me crazy
Run like poison in my blood
One more night could kill me, baby
One is one too many, one more is never enough

Thirty days and thirty nights
Been putting up a real good fight
And there were times I thought you'd win
It's so easy to forget
The bitter taste the morning left
Swore I wouldn't go back there again

'Cause you and Tequila make me crazy
Run like poison in my blood
One more night could kill me, baby
One is one too many, one more is never enough

When it comes to you
Oh, the damage I could do
It's always your favorite sins, that do you in

You and Tequila make me crazy
Run like poison in my blood
One more night could kill me, baby
One is one too many, one more is never enough
Never enough, you and tequila, you and tequila make me crazy .")
  #song 111 (one-hundred-eleven)
  Song.create!(artist:  "Blake Shelton",
               title: "Honey Bee",
               rank: 111,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Rhett Akins, Ben Hayslip",
               year: 2011,
               lyrics: "Girl, I've been thinkin' 'bout us
And you know I ain't good at this stuff
But these feelin's pilin' up
Won't give me no rest

This might come out a little crazy
A little sideways, yeah, maybe
I don't know how long it'll take me
But I'll do my best

If you'll be my soft and sweet
I'll be your strong and steady
You be my glass of wine
I'll be your shot of whiskey
You be my sunny day
I'll be your shade tree
You be my honeysuckle
I'll be your honey bee

Yeah, that came out a little country
But every word was right on the money
And I've got you smilin', honey
Right back at me

Now, hold on, 'cause I ain't done
There's more where that came from
Well, you know I'm just havin' fun
But seriously

If you'll be my Louisiana
I'll be your Mississippi
You be my little Loretta
I'll be your Conway Twitty
You be my sugar baby
I'll be your sweet iced tea
You be my honeysuckle
I'll be your honey bee

Your kiss just said it all
I'm glad we had this talk
Nothin' left to do, but fall
In each other's arms

I could've said a 'I love you'
Could've wrote you a line or two
Baby, all I know to do
Is speak right from the heart

If you'll be my soft and sweet
I'll be your strong and steady
You be my glass of wine
I'll be your shot of whiskey
You be my sunny day
I'll be your shade tree
You be my honeysuckle
I'll be your honey bee

You'll be my Louisiana
I'll be your Mississippi
You be my little Loretta
I'll be your Conway Twitty
You be my sugar baby
I'll be your sweet iced tea
You be my honeysuckle
And I'll be your honey bee

I'll be your honey bee .")
  #song 112 (one-hundred-twelve)
  Song.create!(artist:  "Trace Adkins",
               title: "Just Fishin'",
               rank: 112,
               copyright: "DO Write Music LLC, Sony/ATV Music Publishing LLC",
               writers: "Casey Beathard, Ed Hill, Monty Criswell",
               year: 2011,
               lyrics: "I'm lost in her there holdin' that pink rod and reel
She's doin' almost everything but sittin' still
Talkin' 'bout her ballet shoes and training wheels
And her kittens
And she thinks we're just fishin'

I say, 'Daddy loves you, baby' one more time
She says, 'I know. I think I got a bite.'
And all this laughin', cryin, smilin' dyin' here inside's
What I call, livin'

And she thinks we're just fishin' on the riverside
Throwin' back what we could fry
Drownin' worms and killin' time
Nothin' too ambitious
She ain't even thinkin' 'bout
What's really goin' on right now
But I guarantee this memory's a big'in
And she thinks we're just fishin'

She's already pretty, like her mama is
Gonna drive the boys all crazy
Give her daddy fits
And I better do this every chance I get
'Cause time is tickin'
(Yeah it is)

And she thinks we're just fishin' on the riverside
Throwin' back what we could fry
Drownin' worms and killin' time
Nothin' too ambitious
She ain't even thinkin' 'bout
What's really goin' on right now
But I guarantee this memory's a big'in
And she thinks we're just fishin'

She ain't even thinkin' 'bout
What's really goin' on right now
But I guarantee this memory's a big'in
And she thinks we're just fishin'
Yeah, aw, she thinks we're just fishin'
We ain't only fishin'
(This ain't about fishin') .")
  #song 113 (one-hundred-thirteen)
  Song.create!(artist:  "Billy Currington",
               title: "Let Me Down Easy",
               rank: 113,
               copyright: "Warner/Chappell Music, Inc, Sony/ATV Music Publishing LLC, Ole MM, Walt Disney Music Company",
               writers: "Jennifer Hanson, Mark Nesler, Martin Dodson",
               year: 2010,
               lyrics: "There's a little moonlight dancing on the sand
There's a warm breeze blowing by the ocean as you're taking my hand
You need to know where I'm standing now
That I'm right on the edge of giving into ya
Baby it's a long way down

If I fall, can you let me down easy?
If I leave my heart with you tonight
Will you promise me that you're gonna treat it right?
I'm barely hangin' on
If I fall, can you let me down easy?

The scent of your perfume floatin' in the air
You're looking like an angel lying on a blanket with a halo of hair
And those lips look too good to be true
Once I taste that kiss, I know what'll happen
I'll be at the mercy of you

If I fall, can you let me down easy?
If I leave my heart with you tonight
Will you promise me that you're gonna treat it right?
I'm barely hangin' on
If I fall, can you let me down easy?

If I fall, can you let me down easy?
If I leave my heart with you tonight
Will you promise me that you're gonna treat it right?
I'm barely hangin' on
If I fall, can you let me down easy?

If I fall, can you let me down easy
Let me down easy baby .")
  #song 114 (one-hundred-fourteen)
  Song.create!(artist:  "George Strait",
               title: "Here For A Good Time",
               rank: 114,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group, Hori Pro Entertainment Group",
               writers: "Dean Dillon, Bubba Strait, George Strait",
               year: 2011,
               lyrics: "I'm not going to lay around and whine and moan
'Cause somebody done done me wrong
Don't think for a minute
That I'm gonna sit around and sing some old sad song
I believe it's half full, not a half empty glass
Every day I wake up knowing it could be my last

I ain't here for a long time
I'm here for a good time
So bring on some sunshine, to hell with the red wine
Pour me some moonshine
When I am gone put it in stone, he left nothing behind'
I ain't here for a long time
I'm here for a good time

Folks are always dreaming about what they like to do
But I like to do just what I like
I'll take the chance, dance the dance
It might be wrong but then again it might be right
There's no way of knowing what tomorrow brings
Life's too short to waste it I say bring on anything

I ain't here for a long time
I'm here for a good time
So bring on the sunshine to hell with the red wine
Pour me some moonshine

When I'm gone put it in stone,
'He left nothing behind'
I ain't here for a long time
I'm here for a good time
I ain't here for a long time
I'm here for a good time .")
  #song 115 (one-hundred-fifteen)
  Song.create!(artist:  "Zac Brown Band",
               title: "Knee Deep (f. Jimmy Buffett)",
               rank: 115,
               copyright: "Warner/Chappell Music, Inc, Peermusic Publishing, Universal Music Publishing Group, Reach Music Publishing",
               writers: "Coy Bowles, Zac Brown, Wyatt Durrette, Jeffrey Steele",
               year: 2010,
               lyrics: "Gonna put the world away for a minute
Pretend I don't live in it
Sunshine gonna wash my blues away
Had sweet love but I lost it
She got too close so I fought it
Now I'm lost in the world tryin' to find me a better way

Wishin' I was
Knee deep in the water somewhere
Got the blue sky, breeze and it don't seem fair
The only worry in the world
Is the tide gonna reach my chair
Sunrise, there's a fire in the sky
Never been so happy
Never felt so high
And I think I might have found me my own kind of paradise

Wrote a note, said 'Be back in a minute'
Bought a boat and I sailed off in it
Don't think anybody's gonna miss me anyway
Mind on a permanent vacation
The ocean is my only medication
Wishin' my condition ain't ever gonna go away

'Cause now I'm knee deep in the water somewhere
Got the blue sky breeze blowin' wind through my hair
Only worry in the world
Is the tide gonna reach my chair
Sunrise, there's a fire in the sky
Never been so happy
Never felt so high
And I think I might have found me my own kind of paradise

This champagne shore watchin' over me
It's a sweet sweet life livin' by the salty sea
One day you can be as lost as me
Change your geography and maybe you might be

Knee deep in the water somewhere
Got the blue sky breeze blowin' wind through my hair
Only worry in the world
Is the tide gonna reach my chair
Sunrise, there's a fire in the sky
Never been so happy
Never felt so high
And I think I might have found me my own kind of paradise

Come on in
The waters nice
Find yourself a little slice
Grab a backpack
Otherwise you'll never know until you try
When you lose yourself
You find a key to paradise .")
  #song 116 (one-hundred-sixteen)
  Song.create!(artist:  "Lady Antebellum",
               title: "Just A Kiss (Backstage Acoustic)",
               rank: 116,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Dallas Davidson, Hillary Scott, Dave Haywood, Charles Kelley",
               year: 2011,
               lyrics: "Lyin' here with you so close to me
It's hard to fight these feelings
When it feels so hard to breathe
Caught up in this moment
Caught up in your smile

I've never opened up to anyone
So hard to hold back
When I'm holding you in my arms
We don't need to rush this
Let's just take it slow

Just a kiss on your lips in the moonlight
Just a touch of the fire burning so bright
And I don't want to mess this thing up
I don't want to push too far
Just a shot in the dark that you just might
Be the one I've been waiting for my whole life
So baby I'm alright, with just a kiss goodnight

I know that if we give this a little time
It'll only bring us closer
To the love we wanna find
It's never felt so real
No it's never felt so right

Just a kiss on your lips in the moonlight
Just a touch of the fire burning so bright
And I don't want to mess this thing up
I don't want to push too far
Just a shot in the dark that you just might
Be the one I've been waiting for my whole life
So baby I'm alright, with just a kiss goodnight

No I don't want to say goodnight
I know it's time to leave,
But you'll be in my dreams

Tonight
Tonight
Tonight

Just a kiss on your lips in the moonlight
Just a touch of the fire burning so bright
No I don't want to mess this thing up
I don't want to push too far
Just a shot in the dark that you just might
Be the one I've been waiting for my whole life
So baby I'm alright, oh, let's do this right

With just a kiss goodnight
With a kiss goodnight
Kiss goodnight .")
  #song 117 (one-hundred-seventeen)
  Song.create!(artist:  "Justin Moore",
               title: "If Heaven Wasn't So Far Away",
               rank: 117,
               copyright: "Universal Music Publishing Group, Ole MM, Ole Media Management Lp",
               writers: "Brett William Seaborn Jones, Dallas Davidson, Robert Henry Hatch",
               year: 2011,
               lyrics: "Every day I drive to work across Flint River bridge
A hundred yards from the spot where me and grandpa fished
There's a piece of his old fruit stand on the side of Sawmill Road
He'd be there peelin' peaches if it was twenty years ago
What I wouldn't give
To ride around in that old truck with him

If heaven wasn't so far away
I'd pack up the kids and go for the day
Introduce them to their grandpa
Watch 'em laugh at the way he talked
Find my long lost cousin John
The one we left back in Vietnam
Show him a picture of his daughter now
She's a doctor and he'd be proud
Tell 'em we'd be back in a couple of days
In the rear view mirror we'd all watch him wave
And losing them wouldn't be so hard to take
If heaven wasn't so far away

I'd hug all three of those girls we lost
From the class of ninety nine
And I'd find my bird dog Bo and take him huntin' one more time
I'd ask Hank why he took those pills back in fifty three
And Janis to sing the second verse of 'Me and Bobby McGee'
Sit on a cloud and visit for a while
It'd do me good just to see them smile

If heaven wasn't so far away
I'd pack up the kids and go for the day
Introduce them to their grandpa
Watch 'em laugh at the way he talked
I'd find my long lost cousin John
The one we left back in Vietnam
Show him a picture of his daughter now
She's a doctor and he'd be proud
Tell him we'd be back in a couple of days
In the rear view mirror we'd all watch him wave
Yeah losing them wouldn't be so hard to take
If heaven wasn't so far
If heaven wasn't so far
If heaven wasn't so far away

So far away
So far away .")
  #song 118 (one-hundred-eighteen)
  Song.create!(artist:  "Toby Keith",
               title: "Made In America",
               rank: 118,
               copyright: "Kobalt Music Publishing Ltd., DO Write Music LLC",
               writers: "Bobby Pinson, Gregory Scott Reeves, Toby Keith",
               year: 2011,
               lyrics: "My old man's that old man,
Spent his life livin' off the land
Dirty hands, and a clean soul
It breaks his heart seein' foreign cars
Filled with fuel that isn't ours
And wearin' cotton we didn't grow

He's got the red, white,
And blue flyin' high on the farm
Semper fi tattooed on his left arm
Spends a little more in the store for a tag
In the back that says U.S.A.
He won't buy nothin' that he can't fix
With WD Forty and a Craftsman wrench
He ain't prejudice he's just, made in America

Loves his wife, she's that wife
That decorates on the fourth of July
But says, 'Every day's Independence Day'
She's golden rule, teaches school
Some folks say it isn't cool
But she says the Pledge of Allegiance anyway

He's got the red, white, and blue flyin' high on the farm
Semper fi tattooed on his left arm
Spends a little more in the store for a tag
In the back that says U.S.A.
He won't buy nothin' that he can't fix
With WD Forty and a Craftsman wrench
He ain't prejudice he's just, made in America

Born in the heartland, raised up a family
King James and Uncle Sam

Got the red, white, and blue flyin' high on the farm
Semper fi tattooed on his left arm
Spends a little more at the store for a tag
In the back that says U.S.A.
Won't buy nothin' that he can't fix
With WD Forty and a Craftsman wrench
He ain't prejudice he's just, made in America
Made in America, made in America

Yeah my old man's that old man,
Made in America .")
  #song 119 (one-hundred-nineteen)
  Song.create!(artist:  "Luke Bryan",
               title: "Country Girl (Shake It For Me)",
               rank: 119,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Dallas Davidson, Luke Bryan",
               year: 2011,
               lyrics: "Hey girl, go on now!
You know you've got everybody lookin'

Got a little boom in my big truck
Gonna open up the doors and turn it up
Gonna stomp my boots in the Georgia mud
Gonna watch you make me fall in love
Get up on the hood of my daddy's tractor
Up on the tool box, it don't matter
Down on the tailgate
Girl I can't wait
To watch you do your thing

Shake it for the young bucks sittin' in the honky-tonks
For the rednecks rockin' 'til the break of dawn
The DJ spinnin' that country song
C'mon, c'mon, c'mon
Shake it for the birds, shake it for the bees
Shake it for the catfish swimmin' down deep in the creek
For the crickets and the critters and the squirrels
Shake it to the moon, shake it for me girl

Aw, country girl, shake it for me
Girl, shake it for me
Girl, shake it for me
Country girl, shake it for me
Girl, shake it for me
Girl, shake it for me

Sombody's sweet little farmer's child
With a gattle in her Bud to get a little wild
Pony-tail and a pretty smile
Rope me in from a country mile
So come on over here and get in my arms
Spin me around this big ole barn
Tangle me up like grandma's yarn
Yeah, yeah, yeah

Shake it for the young bucks sittin' in the honky-tonks
For the rednecks rockin' 'til the break of dawn
For the DJ spinnin' that country song
C'mon, c'mon, c'mon
Shake it for the birds, shake it for the bees
Shake it for the catfish swimmin' down deep in the creek
For the crickets and the critters and the squirrels
Shake it to the moon, shake it for me girl

Country girl, shake it for me
Girl, shake it for me
Girl, shake it for me
Country girl, shake it for me
Girl, shake it for me
Girl, shake it for me

Guitar!

Now dance, like a dandelion
In the wind on the hill underneath the pines
Yeah, move like the river flows
Feel the kick drum down deep in your toes
All I wanna do is get to holdin' you
And get to knowin' you
And get to showin' you
And get to lovin' you
'Fore the night is through
Baby, you know what to do

Shake it for the young bucks sittin' in the honky-tonks
For the rednecks rockin' 'til the break of dawn
For the DJ spinnin' that country song
C'mon, c'mon, c'mon
Shake it for the birds, shake it for the bees
Shake it for the catfish swimmin' down deep in the creek
For the crickets and the critters and the squirrels
Shake it to the moon, shake it for me girl

Aw, country girl, shake it for me
Girl, shake it for me
Girl, shake it for me
Country girl, shake it for me
Girl, shake it for me
Girl, shake it for me

Country girl, shake it for me
Girl, shake it for me
Girl, shake it for me
Country girl, shake it for me
Girl, shake it for me
Girl, shake it for me .")
  #song 120 (one-hundred-twenty)
  Song.create!(artist:  "Miranda Lambert",
               title: "Heart Like Mine",
               rank: 120,
               copyright: "Kobalt Music Publishing Ltd.",
               writers: "Ashley Monroe, Miranda Lambert, Travis Howard",
               year: 2009,
               lyrics: "I ain't the kind you take home to mama
I ain't the kind to wear no ring
Somehow I always get stronger
When I'm on my second drink

Even though I hate to admit it
Sometimes I smoke cigarettes
The Christian folks say I should quit it
I just smile and say 'God bless'

'Cause I heard Jesus he drank wine
And I bet we'd get along just fine
He could calm a storm and heal the blind
And I bet he'd understand a heart like mine

Daddy cried when he saw my tattoo
But said he loved me anyway
My brother got the brains of the family
So I thought I'd learn to sing

'Cause I heard Jesus he drank wine
And I bet we'd get along just fine
He could calm a storm and heal the blind
And I bet he'd understand a heart like mine

I'll fly away from it all one day
I'll fly away

These are the days that I will remember
When my names called on a roll
He'll meet me with two long stem glasses
Make a toast to me coming home

'Cause I heard Jesus he drank wine
And I bet we'd get along just fine
He could calm a storm and heal the blind
And I bet he'd understand, understand a heart like mine

Oh yes he would .")
  #song 121 (one-twenty-one)
  Song.create!(artist:  "Blake Shelton",
               title: "God Gave Me You",
               rank: 121,
               copyright: "Kobalt Music Publishing Ltd.",
               writers: "Dave Barnes",
               year: 2011,
               lyrics: "I've been a walking heartache
I've made a mess of me
The person that I've been lately
Ain't who I wanna be
But you stay here right beside me
Watch as the storm goes through
And I need you

'Cause God gave me you for the ups and downs
God gave me you for the days of doubt
For when I think I've lost my way
There are no words here left to say, it's true
God gave me you, gave me you

There's more here than what were seeing
A divine conspiracy
That you, an angel lovely
Could somehow fall for me
You'll always be love's great martyr
And I'll be the flattered fool
And I need you, yeah

God gave me you for the ups and downs
God gave me you for the days of doubt
For when I think I've lost my way
There are no words here left to say, it's true
God gave me you

On my own I'm only
Half of what I could be
I can't do without you
We are stitched together
And what love has tethered
I pray we never undo

'Cause God gave me you for the ups and downs
God gave me you for the days of doubt
God gave me you for the ups and downs
God gave me you for the days of doubt
And for when I think I've lost my way
There are no words here left to say, it's true
God gave me you, gave me you
He gave me you .")
  #song 122 (one-twenty-two)
  Song.create!(artist:  "Jason Aldean",
               title: "Don't You Wanna Stay (f. Kelly Clarkson)",
               rank: 122,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC",
               writers: "JASON SELLERS, PAUL JENKINS, ANDY GIBSON",
               year: 2010,
               lyrics: "
I really hate to let this moment go
Touching your skin and your hair falling slow
When a goodbye kiss, feels like this

Don't you wanna stay here a little while
Don't you wanna hold each other tight
Don't you wanna fall asleep with me tonight
Don't you wanna stay here a little while
We can make forever feel this way
Don't you wanna stay

Let's take it slow I don't wanna move too fast
I don't wanna just make love, I wanna make love last

When your up this high, it's a sad goodbye

Don't you wanna stay here a little while
Don't you wanna hold each other tight
Don't you wanna fall asleep with me tonight
Don't you wanna stay here a little while
We can make forever feel this way
Don't you wanna stay

Oh yeah!
Oh you feel so perfect baby
Don't you wanna stay here a little while

Don't you wanna stay here a little while
Don't you wanna hold each other tight
Don't you wanna fall asleep with me tonight
Don't you wanna stay here a little while
We can make forever feel this way
Don't you wanna stay

Don't you wanna stay
Yeah yeah yeah, yeah yeah yeah .")
  #song 123 (one-twenty-three)
  Song.create!(artist:  "Sara Evans",
               title: "A Little Bit Stronger",
               rank: 123,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Luke Robert Laird, Hillary Lee Lindsey, Hillary Dawn Scott",
               year: 2010,
               lyrics: "Woke up late today and I still feel the sting of the pain
But I brushed my teeth anyway
I got dressed through the mess and put a smile on my face
I got a little bit stronger

Riding in the car to work and I'm
Trying to ignore the hurt so I
Turned on the radio,
Stupid song made me think of you
I listened to it for minute
But then I changed it
I'm getting a little bit stronger,
Just a little bit stronger

And I'm done hoping
That we could work it out
I'm done with how it feels,
Spinning my wheels
Letting you drag my heart around
And, oh, I'm done thinking
You could ever change
I know my heart will never be the same
But I'm telling myself I'll be okay
Even on my weakest days
I get a little bit stronger

It doesn't happen overnight but you
Turn around and a month's gone by
And you realize you haven't cried
I'm not giving you a hour or a second or another minute longer
I'm busy getting stronger

And I'm done hoping
That we can work it out
I'm done with how it feels,
Spinning my wheels
Letting you drag my heart around
And, oh, I'm done thinking,
You could ever change
I know my heart will never be the same
But I'm telling myself I'll be okay
Even on my weakest days,
I get a little bit stronger
I get a little bit stronger

Getting along without you baby
I'm better off without you baby
How does it feel without me baby?
I'm getting stronger without you baby

And I'm done hoping we could work it out
I'm done with how it feels,
Spinning my wheels
Letting you drag my heart around
And, oh, I'm done thinking
That you could ever change
I know my heart will never be the same
But I'm telling myself I'll be okay
Even on my weakest days
I get a little bit stronger
I get a little bit stronger

And just a little bit stronger
A little bit,
A little bit,
A little bit stronger
Get a little bit stronger .")
  #song 124 (one-twenty-four)
  Song.create!(artist:  "Keith Urban",
               title: "Long Hot Summer",
               rank: 124,
               copyright: "Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Universal Music Publishing Group",
               writers: "Richard N. Marx, Keith Lionel Urban",
               year: 2010,
               lyrics: "I can't sleep, ain't no sleep a'coming
I'm just lying here thinking 'bout you
I'm in deep, falling deep into the picture
In my mind of everything we're gonna do

Over at the lake and down by the river
You can feel it start to rise
Wanna jump in my car, go wherever you are
'Cause I need you by my side

It's gonna be a long, hot summer, we should be together
With your feet up on the dashboard now
Singing along with the radio, it's such a beautiful sound
But when you say my name in the middle of the day,
I swear I see the stars come out
When you hold my hand in the back of my mind,
Just waiting on the sun to go down, the sun to go down

I wanna see your brown skin shimmer in the sun for the first time
I try to be the one who knows just what to do to you to get me that smile
One chance of meeting, you were walking by me on the street and I said hi
And that was the beginning of my heart spinnin' like these wheels in my head tonight

It's gonna be a long, hot summer, we should be together
With your feet up on the dashboard now
Singing along with the radio, it's such a beautiful sound
But when you say my name in the middle of the day,
I swear I see the stars come out
When you hold my hand in the back of my mind,
Just waiting on the sun to go down, the sun to go down

The only place that I wanna be is where you are
'Cause anymore than a heartbeat away is just too far

It's gonna be a long, hot summer, we should be together
All I really want is more than this moment right now
'Cause when you say my name in the middle of the day,
I swear I see the stars come out
And when you hold my hand and I look into your eyes
Oh I swear it looks like you're waiting on the sun to go down, the sun to go down

I swear it's like you're waiting on the sun to go down
Waiting on the sun to go down
Hey, yeah oh, I'm loving thinking 'bout you
I can't sleep, I'm just lying here thinkin' 'bout you .")
  #song 125 (one-twenty-five)
  Song.create!(artist:  "Darius Rucker",
               title: "This",
               rank: 125,
               copyright: "Warner/Chappell Music, Inc., Universal Music Publishing Group, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "DARIUS C. RUCKER, KARA DIOGUARDI, FRANK MANDEVILLE V. ROGERS",
               year: 2010,
               lyrics: "
  Ask for money, and get advice
  Ask for advice, get money twice
  I'm from the Dirty, but that chico nice
  Y'all call it a moment, I call it life

  One day when the light is glowing
  I'll be in my castle golden
  But until the gates are open
  I just wanna feel this moment (oh)

  I just wanna feel this moment (oh)

  I just wanna feel this moment

  Mr Worldwide
  Christina Aguilera
  Oye mamita, come on, dale, que la cosa esta rica

  (I wanna feel this moment)

  Feel this moment

  Reporting live, from the tallest building in Tokyo
  Long ways from them hard ways
  Bill sos, and oh yeas
  Dade County, always, 305 all day
  Now baby we can parle, oh baby we can party
  She read books, especially about red rooms and tie ups
  I got her hooked, 'cause she seen me in a suit with the red tie tied up
  Meet and greet, nice to meet ya, but time is money
  Only difference is I own it, now let's stop time and enjoy this moment

  One day when the light is glowing
  I'll be in my castle golden
  But until the gates are open
  I just wanna feel this moment (oh)

  I just wanna feel this moment (oh)

  I just wanna feel this moment

  Feel this moment

  I see the future but live for the moment, make sense don't it
  Now make dollars, I mean billions, I'm a genius, I mean brilliant
  The streets is what schooled 'em
  And made 'em slicker than Slick Rick the Ruler
  I've lost a lot, and learned a lot
  But I'm still undefeated like Shooter
  I'm far from cheap, I break down companies with all my peeps
  Baby we can travel the world
  And I can give you an all you can see
  Time is money
  Only difference is I own it, like a stop watch, let's stop time and enjoy this moment

  One day when the light is glowing
  I'll be in my castle golden
  But until the gates are open
  I just wanna feel this moment (oh)

  I just wanna feel this moment (oh)

  I just wanna feel this moment

  Come one, feel this moment

  (Oh) I just wanna feel this moment
  (Oh) I just wanna feel this moment .")
  #song 126 (one-twenty-six)
  Song.create!(artist:  "Taylor Swift",
               title: "Mean",
               rank: 126,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "TAYLOR SWIFT",
               year: 2010,
               lyrics: "
You, with your words like knives
And swords and weapons that you use against me
You, have knocked me off my feet again,
Got me feeling like a nothing
You, with your voice like nails
On a chalk board, calling me out when I'm wounded
You, picking on the weaker man

You can take me down
With just one single blow
But you don't know what you don't know

Someday I'll be living in a big old city
And all you're ever gonna be is mean
Someday I'll be big enough so you can't hit me
And all you're ever gonna be is mean
Why you gotta be so mean?1

You, with your switching sides
And your wildfire lies and your humiliation
You have pointed out my flaws again
As if I don't already see them
I walk with my head down,
Try to block you out 'cause I never impress you
I just want to feel okay again

I bet you got pushed around
Somebody made you cold but the cycle ends right now
'Cause you can't lead me down that road
And you don't know what you don't know

Someday I'll be living in a big old city
And all you're ever gonna be is mean
Someday I'll be big enough so you can't hit me
And all you're ever gonna be is mean
Why you gotta be so mean?

And I can see you years from now in a bar
Talking over a football game
With that same big loud opinion
But nobody's listening, washed up and ranting
About the same old bitter things
Drunk and grumbling on about how I can't sing
But all you are is mean
All you are is mean and a liar and pathetic
And alone in life and mean, and mean, and mean, and mean2

But someday I'll be living in a big old city
And all you're ever gonna be is mean
Yeah someday I'll be big enough
So you can't hit me
And all you're ever gonna be is mean
Why you gotta be so (mean)
Someday I'll be living in a big old city
(Why you gotta be so mean)

And all you're ever gonna be is mean
(Why you gotta be so mean)
Someday I'll be big enough so you can't hit me
(Why you gotta be so mean)
And all you're ever gonna be is mean
Why you gotta be so mean? .")
  #song 127 (one-twenty-seven)
  Song.create!(artist:  "Jason Aldean",
               title: "Dirt Road Anthem",
               rank: 127,
               copyright: "Warner/Chappell Music, Inc",
               writers: "Brantley Gilbert, Christopher Bridges, Colt Ford",
               year: 2010,
               lyrics: "Yeah I'm chilling on a dirt road,
Laid back swervin' like I'm George Jones,
Smoke rollin' out the window,
An ice cold beer sittin' in the console
Memory lane up in the headlights
Has got me reminiscing on the good times
I'm turning off the real life, driving, that's right
I'm hittin' easy street in mud tires

Back in the day pop's farm was the place to go
Load the truck up hit the dirt road,
Jump the barbwire spread the word
Light the bonfire then call the girls
The king in the can and the Marlboro man
Jack and Jim were a few good men
When we learned how to kiss and cuss and fight too
Better watch out for the boys in blue
And all this small town he said she said
Ain't it funny how rumors spread
Like I know something ya'll don't know
Man that talk is getting old
Man mind your business watch your mouth
Before I have to knock your loud mouth out
I'm tired of talking man ya'll ain't listenin'
Them old dirt roads is what ya'll missin'

Yeah I'm chilling on a dirt road,
Laid back swervin' like I'm George Jones,
Smoke rollin' out the window,
An ice cold beer sittin' in the console
Memory lane up in the headlights
Has got me reminiscing on the good times
I'm turning off the real life, driving, that's right
I'm hittin' easy street in mud tires

I sit back and think about them good ol' days
The way we were raised and our southern ways
And we like cornbread and biscuits
If it's broke round here we fix it
I can take ya'll where you need to go
Down to my hood and back in them woods
We do it different 'round here that's right
And we sure do it good and we do it all night
So if you really wanna know how it feels
To get off the road with a truck and four wheel
Jump on in tell yo' friends
And we'll be raising hell where the black top ends

I'm chilling on a dirt road,
Laid back swervin' like I'm George Jones,
Smoke rollin' out the window,
An ice cold beer sittin' in the console
Memory lane up in the headlights
Has got me reminiscing on the good times
I'm turning off the real life, driving, that's right
I'm hittin' easy street in mud tires that's right

Yeah I'm chillin' on a dirt road,
Laid back swervin' like I'm George Jones
Smoke rollin out the window,
An ice cold beer sittin' in the console
Memory lane up in the headlights
Has got me reminiscing on the good times
I'm turning off the real life, driving, that's right
I'm hittin' easy street in mud tires that's right .")
  #song 128 (one-twenty-eight)
  Song.create!(artist:  "Brad Paisley",
               title: "Remind Me (f. Carrie Underwood)",
               rank: 128,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd., Spirit Music Group, Warner/Chappell Music, Inc",
               writers: "Kelley Lovelace, Chris Dubois, Brad Paisley",
               year: 2011,
               lyrics: "We didn't care if people stared
We'd make out in a crowd somewhere
Somebody'd tell us to get a room
It's hard to believe that was me and you

Now we keep saying that we're OK
But I don't want to settle for good not great
I miss the way that it felt back then
I wanna feel that way again

Been so long that you'd forget
The way I used to kiss your neck
Remind me, remind me
So on fire, so in love
Way back when we couldn't get enough
Remind me, remind me

Remember the airport dropping me off
We were kissing goodbye and we couldn't stop
I felt bad cause you missed your flight
But that meant we had one more night

Do you remember how it used to be
We'd turn out the lights and didn't just sleep
Remind me baby remind me

Oh so on fire so in love
That look in your eyes that I miss so much
Remind me, baby remind me

I wanna feel that way
Yeah I wanna hold you close
Oh if you still love me
Don't just assume I know

Oh baby remind me, remind me

Do you remember the way it felt?
You mean back when we couldn't control ourselves
Remind me, yeah remind me

All those things that you used to do
That made me fall in love with you
Remind me, oh baby remind me

Yeah you'd wake up in my old t-shirt
All those mornings I was late for work
Remind me, oh baby remind me yeah
Oh baby remind me baby remind me

Yeah you'd wake up in my old t-shirt
Oh, baby remind me .")
  #song 129 (one-twenty-nine)
  Song.create!(artist:  "Thompson Square",
               title: "Are You Gonna Kiss Me Or Not",
               rank: 129,
               copyright: "Warner/Chappell Music, Inc, Spirit Music Group, Carol Vincent & Assoc LLC",
               writers: "David Lee Murphy, Jim Collins",
               year: 2011,
               lyrics: "We were sittin' up there on your momma's roof
Talkin' 'bout everything under the moon
With the smell of honeysuckle and your perfume
All I could think about was my next move

Oh, but you were so shy, so was I
Maybe that's why it was so hard to believe
When you smiled and said to me
Are you gonna kiss me or not?

Are we gonna do this or what?
I think you know I like you a lot
But you're 'bout to miss your shot
Are you gonna kiss me or not?

It was the best dang kiss that I ever had
Except for that long one after that
And I knew if I wanted this thing to last
Sooner or later I'd have to ask for your hand

So I took a chance
Bought a wedding band and I got down on one knee
And you smiled and said to me
Are you gonna kiss me or not?

Are we gonna do this or what?
I think you know I love you a lot
I think we've got a real good shot
Are you gonna kiss me or not?

So, we planned it all out for the middle of June
From the wedding cake to the honeymoon
And your momma cried
When you walked down the aisle

When the preacher man said, 'Say I do'
I did and you did too, then I lifted that veil
And saw your pretty smile and I said
Are you gonna kiss me or not?

Are we gonna do this or what?
Look at all the love that we got
It ain't never gonna stop
Are you gonna kiss me or not?

Yeah baby, I love you a lot
I really think we've got a shot
Are you gonna kiss me or not? .")
  #song 130 (one-thirty)
  Song.create!(artist:  "Rascal Flatts",
               title: "I Won't Let Go",
               rank: 130,
               copyright: "BMG Rights Management US, LLC",
               writers: "Steve Robson and Jason Sellers",
               year: 2010,
               lyrics: "It's like a storm
That cuts a path
It's breaks your will
It feels like that

You think you're lost
But your not lost on your own
You're not alone

I will stand by you
I will help you through
When you've done all you can do
If you can't cope
I will dry your eyes
I will fight your fight
I will hold you tight
And I won't let go

It hurts my heart
To see you cry
I know it's dark
This part of life
Oh it finds us all (finds us all)
And we're too small
To stop the rain
Oh but when it rains

I will stand by you
I will help you through
When you've done all you can do
And you can't cope
I will dry your eyes
I will fight your fight
I will hold you tight
And I won't let you fall

Don't be afraid to fall
I'm right here to catch you
I won't let you down
It won't get you down
You're gonna make it
Yeah I know you can make it

'Cause I will stand by you
I will help you through
When you've done all you can do
And you can't cope
And I will dry your eyes
I will fight your fight
I will hold you tight
And I won't let go
Oh I'm gonna hold you
And I won't let go
Won't let you go
No I won't .")
  #song 131 (one-thirty-one)
  Song.create!(artist:  "Keith Urban",
               title: "Without You",
               rank: 131,
               copyright: "Peermusic Publishing, Reservoir Media Management Inc",
               writers: "David Sneddon",
               year: 1991,
               lyrics: "I've loved you since the very first day
When I caught you looking my way
A smile and just knew it.
And up until you came along, no one ever heard my song,
Now its climbing with a bullet.

Its nice to have someone, so honestly devoted,
But when it's said and done, girl I hope you know that,

The traveling, the singing, it don't mean nothing without you
The fast cars, the guitars, they are all just second to,
This life, this love that you and I have been dreaming of, for so long
It'd all be as good as gone, without you.

Oh ho, without you, hey hey

Along comes a baby girl, and suddenly my little world,
Just got a whole lot bigger, yes it did.
And people that I barely knew, who love me 'cause I'm part of you
Man it's tough to figure
How two souls can be, miles from one another
But still you and me, have somehow found each other.

The traveling, the singing, it don't mean nothing without you
The fast cars, the guitars, they are all just second to,
This life, this love that you and I have been dreaming of, for so long
It'd all be as good as gone, without you.

Without you, oh oh
Without you I'd survive, but I'd have to have the notion
That I could live this life, just going through the motions

The traveling, the singing, it don't mean nothing without you
The fast cars, the guitars, they are all just second to,
This life, this love that you and I have been building up so high
It's never gonna touch the sky, without you.

Mm, without you
Without you, baby, baby, baby without you
Without you .")
  #song 132 (one-thirty-two)
  Song.create!(artist:  "Brantley Gilbert",
               title: "Country Must Be Country Wide",
               rank: 132,
               copyright: "Warner/Chappell Music, Inc",
               writers: "Brantley Keith Gilbert, Colt Ford, Mike Dekle",
               year: 2010,
               lyrics: "I grew up south of the Mason Dixon
Workin, ' spittin', huntin' and fishin'
Stone cold country by the grace of God

I was gasin' up the other day
And an ol' boy pulled up with a license plate
From Ohio I thought oh good Lord he's lost
From his wranglers to his boots
He reminded me of Chris LeDoux
And that Copenhagen smile
Country must be country wide

In every state, there's a station
Playin' Cash, Hank, Willie, and Waylon
In foreign cars and four wheel drives

There's cowboys and hillbillies
From farm towns to big cities
There ain't no doubt in my mind
Country must be country wide

It ain't where, it's how you live
We weren't raised to take
We were raised to give
The shirt off our back
To anyone in need

We bow our heads before we eat
Before we start our day
Before we fall asleep
Cause in God we trust and we believe

And we see what's wrong
And we know what's right
And ol' Hank he said it all
When he said country folks can survive

In every state, there's a station,
Playin' Cash, Hank, Willie, and Waylon
In foreign cars and four wheel drives

There's cowboys and hillbillies
From farm towns to big cities
There ain't no doubt in my mind
Country must be country wide, yeah get on it dog

In every state, there's a station, c'mon y'all better crank this up
In every state, there's a station
Playin' Cash, Hank, Willie, and Waylon
In foreign cars and four wheel drives

There's cowboys and hillbillies
From farm towns to big cities
There ain't no doubt in my mind, there ain't no doubt in my mind
Country must be country wide .")
  #song 133 (one-thirty-three)
  Song.create!(artist:  "Lee Brice",
               title: "Love Like Crazy",
               rank: 133,
               copyright: "Warner/Chappell Music, Inc, Mike Curb Music",
               writers: "Doug Johnson, Tim James, Timothy A. James",
               year: 2010,
               lyrics: "They called them crazy when they started out
Said seventeen's too young to know what loves about
They've been together fifty-eight years now
That's crazy

He brought home sixty-seven bucks a week
He bought a little two bedroom house on Maple Street
Where she blessed him with six more mouths to feed
Yea that's crazy

Just ask him how he did it, he'll say pull up a seat
It'll only take a minute, to tell you everything
Be a best friend, tell the truth, and overuse I love you
Go to work, do your best, don't outsmart your common sense
Never let your prayin' knees get lazy
And love like crazy

They called him crazy when he quit his job
Said them home computers, boy they'll never take off
He sold his one man shop to Microsoft
And they paid like crazy

Just ask him how he made it
He'll tell you faith and sweat
And the heart of a faithful woman,
Who never let him forget

Be a best friend, tell the truth, and overuse I love you
Go to work, do your best, don't outsmart your common sense
Never let your prayin' knees get lazy
And love like crazy

Always treat your woman like a lady
Never get to old to call her baby
Never let your prayin' knees get lazy
And love like crazy

They called him crazy when they started out
They've been together fifty-eight years now

Ain't that crazy? .")
  #song 134 (one-thirty-four)
  Song.create!(artist:  "Luke Bryan",
               title: "Rain Is A Good Thing",
               rank: 134,
               copyright: "Warner/Chappell Music, Inc, Sony/ATV Music Publishing LLC",
               writers: "Luke Bryan, Dallas Davidson",
               year: 2011,
               lyrics: "My daddy spent his life lookin' up at the sky
He'd cuss kick the dust, sayin' son its way to dry
It clouds up in the city, the weather man complains
But where I come from, rain is a good thing

Rain makes corn, corn makes whiskey
Whiskey makes my baby, feel a little frisky
Back roads are boggin' up, my buddies pile up in my truck
We hunt our hunnies down, we take 'em into town
Start washin' all our worries down the drain
Rain is a good thing

Ain't nothin' like a kiss out back in the barn
Ringin' out our soakin' clothes, ridin' out a thunderstorm
When the tin roof gets to talkin' it's the best love we've made
Yeah where I come from, rain is a good thing

Rain makes corn, corn makes whiskey
Whiskey makes my baby, feel a little frisky
Back roads are boggin' up, my buddies pile up in my truck
We hunt our hunnies down, we take 'em into town
Start washin' all our worries down the drain
Rain is a good thing

Farmer Johnson does a little dance
Creeks on the rise, roll up your pants
Country girls, they wanna cuddle
Kids out playin' in a big mud puddle

Rain makes corn, corn makes whiskey
Whiskey makes my baby, ha ha ha
Back roads are boggin' up, my buddies pile up in my truck
We hunt our hunnies down, we take 'em into town
Start washin' all our worries down the drain
Rain is a good thing

Rain is a good thing
Rain is a good thing
Rain is a good thing .")
  #song 135 (one-thirty-ofive)
  Song.create!(artist:  "Josh Turner",
               title: "Why Don't We Just Dance",
               rank: 135,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Jonathan Singleton, Jim Beavers, Darrell Brown",
               year: 2010,
               lyrics: "Baby, why don't we just turn that TV off?
Three hundred and fifteen channels of nothing but bad news on,
Well it might be me
But the way I see it,
The whole wide world has gone crazy.
So baby, why don't we just dance?

Just a little bitty living room ain't gonna look like much.
But when the lights go down and we move the couch,
It's gonna be more than enough
For my two left feet
And our two hearts beatin'
Nobody's gonna see us go crazy.
So baby, why don't we just dance
Down the hall,
Maybe straight up the stairs?
Bouncing off the wall,
Floating on air,
Baby, why don't we just dance?

Baby, why don't you go put your best dress on,
And those high heeled shoes you love to lose
As soon as the tunes come on?
On second thought,
Just the way you are
Is already driving me crazy.
So baby, why don't we just dance
Down the hall
Maybe straight up the stairs?
Bouncing off the wall,
Floating on air,
Baby, why don't we just dance?
I'll cut a rug

Well it might be me
But the way I see it,
The whole wide world has gone crazy.
So baby, why don't we just dance
(Bouncing off the walls, floating on air)
Oh baby (baby) why don't we just dance? .")
  #song 136 (one-thirty-six)
  Song.create!(artist:  "Josh Turner",
               title: "All Over Me",
               rank: 136,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Dallas Davidson, Rhett Akins, Ben Hayslip",
               year: 2012,
               lyrics: "Well the weather man says, 'It's gonna be a hot one.'
Heard it on the radio
Load up the boat, get your girl if you've got one
A nice little breeze gonna blow

Baby, I'm on my way to come and get 'cha
Meet me at the end of your drive
Grab your shades and your string bikini
And your Coppertone forty five

Bring on the sunshine, bring on a good time
Girl, let me look at you
Jump in the front seat, kick up your bare feet
Honey, let your hair down too
I know a spot down on the river
Underneath the sycamore tree
Save all your kisses up, bring on your sweet love
Pour it all over me
All over me

We can build us a fire when we run out of daylight
Let it light up your face
Lay on a blanket, listen to the river
Get a little carried away

Well, I can taste that kiss on your sweet lips
Ain't gonna want it to end
So when the morning comes, we'll roll on out
And do it all over again

Bring on the sunshine, bring on a good time
Girl, let me look at you
Jump in the front seat, kick up your bare feet
Honey, let your hair down too
I know a spot down on the river
Underneath the sycamore tree
Save all your kisses up, bring on your sweet love
Pour it all over me
All over me

Bring on the sunshine, bring on a good time
Girl, let me look at you
Jump in the front seat, kick up your bare feet
Honey, let your hair down too
I know a spot down on the river
Underneath the sycamore tree
Save all your kisses up, bring on your sweet love
Pour it all over me
All over me

C'mon,
C'mon,
C'mon,
C'mon pour it all over me,
All over me .")
  #song 137 (one-thirty-seven)
  Song.create!(artist:  "Chris Young",
               title: "The Man I Want To Be",
               rank: 137,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Brett James, Tim Nichols",
               year: 2009,
               lyrics: "God, I'm down here on my knees
Cause it's the last place left to fall
Beggin' for another chance
If there's any chance at all
That you might still be listenin'
Lovin' and forgivin' guys like me

I've spent my whole life gettin' it all wrong
And I sure could use your help cause from now on

I wanna be a good man
A 'do like I should' man
I wanna be the kind of man the mirror likes to see
I wanna be a strong man
And admit that I was wrong, man
God I'm asking you to come change me
To the man I wanna be

There's anyway for her and me to make another start
Could you see what you could do
To put some love back in her heart
Cause' it going to take a miracle
After all I've done to really make her see

That I wanna be a stay man
I wanna be a great man
I wanna be the kind of man that she sees in her dreams
God, I wanna be your man
And I wanna be her man
God, I only hope she still believes
In the man I wanna be

Well, I know this late at night that talk is cheap
Lord, don't give up on me

I wanna be a givin' man
I wanna really start livin' man
God, I'm asking you to come change me
To the man I wanna be .")
  #song 138 (one-thirty-eight)
  Song.create!(artist:  "Easton Corbin",
               title: "Roll With It",
               rank: 138,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Will Jennings, Steve Winwood, Lamont Herbert Dozier, Eddie Holland, Brian Holland",
               year: 2010,
               lyrics: "I got my old guitar and some fishin' poles
So baby fill that cooler full of something cold
Don't ask just pack and we'll hit the road runnin'
Honey what do you say

I got just enough money and just enough gas
So pick a place on the map we can get to fast
Where the white sandy beach meets water like glass
And if the tide carries us away

Baby we'll roll with it
Won't think about it too much
Baby let's just go with it
Get out of this ordinary everyday rut

And if we get swept away by one of those perfect days
When the sun is sinking low at dusk
And wind up a little deeper in love
Baby lets roll with it

We get so caught up in catching up
Trying to pay the rent, trying to make a buck
That don't leave much time for time for us
And ain't life too short for that

So open up that bag of pig skins you bought
At the Exxon station the last time we stopped
And you can kick back baby and dance in your socks
On the windshield to some radio rock

And we'll roll with it
Won't think about it too much
Baby let's just go with it
And get out of this ordinary everyday rut

And we get swept away by one of those perfect days
When the sun is sinking low at dusk
And wind up a little deeper in love

Baby we'll roll with it
Won't think about it too much
Sometimes you gotta go with it
Get out of this ordinary everyday rut

And it won't be no thing if it starts to rain
And we have to wait it out in the truck
We might wind up a little deeper in love
So baby lets roll with it

Baby lets roll with it
Baby we'll roll with it
Baby we'll roll with it .")
  #song 139 (one-thirty-nine)
  Song.create!(artist:  "Joe Nichols",
               title: "Gimmie That Girl",
               rank: 139,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Dallas Davidson, Rhett Akins, Ben Hayslip",
               year: 2009,
               lyrics: "Hang up that red dress,
Let down your hair,
Cancel those reservations
There's no need to go nowhere.
As good as your looking right now,
Girl bet your thinkin' I'm crazy,
There's a side of you that I wanna see
That never ceases to amaze me.

Gimme that girl with the hair in a mess
Sleepy little smile with her head on my chest,
That's the you that I like best,
Gimme that girl.
Gimme that girl lovin' up on me,
Old t-shirt and a pair of jeans,
That's the you I wanna see,
Gimme that girl

Gimme the girl that's beautiful,
Without a trace of makeup on,
Barefoot in the kitchen,
Singing her favorite song.
Dancing around like a fool,
Starring in her own little show,
Gimme the girl that the rest of the world,
Ain't lucky enough to know.

Gimme that girl with the hair in a mess
Sleepy little smile with her head on my chest,
That's the you that I like best,
Gimme that girl.
Gimme that girl lovin' up on me,
Old t-shirt and a pair of jeans,
That's the you I wanna see,
Gimme that girl, gimme that girl.

Gimme that girl with the hair in a mess
Sleepy little smile with her head on my chest,
That's the you that I like best,
Gimme that girl.
Gimme that girl lovin' up on me,
Old t-shirt and a pair of jeans,
That's the you I wanna see,
Gimme that girl, gimme that girl .")
  #song 140 (one-fourty)
  Song.create!(artist:  "Blake Shelton",
               title: "All About Tonight",
               rank: 140,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Dallas Davidson, Rhett Akins, Ben Hayslip",
               year: 2011,
               lyrics: "Don't bother telling me what I got coming in the morning
I already know
I got some feelgood pills and a red Gatorade by my bed
Ready to go

I'm gonna do my best to dance with every girl in this bar
Before they shut it down
Then I'll pay my tab, climb in a cab
Hit another spot downtown

It's all about tonight
Good times and the music and laughing and grooving to the band
Everybody's getting right
No worries, we're rocking all kinds of concoctions in our hands
Yeah tomorrow can wait 'til tomorrow
It's all about tonight

Hey pretty thing
I've been looking at you since the moment that you walked in
I got some wild-ass buddies that love spending money
And I see you brought a couple of friends

Just tell me your name
I don't need your number or a date next Saturday
Baby let's act like fools break a few rules
Party the night away

It's all about tonight
Good times and the music and laughing and grooving to the band
Everybody's getting right
No worries, we're rocking all kinds of concoctions in our hands
Yeah tomorrow can wait 'til tomorrow
It's all about tonight

Ha ha yeah, slide

It's all about tonight
Good times and the music and laughing and grooving to the band
Everybody's getting right
No worries, we're rocking all kinds of concoctions in our hands
Yeah tomorrow can wait 'til tomorrow
I said tomorrow can wait 'til tomorrow
It's all about tonight

Yeah it's all about tonight
Come on, it's all about tonight
We're gonna get our swerve on, our buzz on
It's all about tonight
Come on baby, I wouldn't worry about that
It's all about tonight, now, c'mon .")
  #song 141 (one-fourty-one)
  Song.create!(artist:  "Darius Rucker",
               title: "Come Back Song",
               rank: 141,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Casey Beathard, Chris Stapleton, Darius Rucker",
               year: 2010,
               lyrics: "I woke up again this morning
And wouldn't you know it, pouring rain
I went and burned a pot of coffee
And like us I poured it down the drain

'Cause I didn't know I needed you so
And letting you go was wrong
And baby I know you got your radio on
So this is my my bad, come back song

I know I've said I wouldn't miss you
But now I'm saying I'm a fool
You're on the feel good side of leaving
And I'm the backside of a mule

And I didn't know I needed you so
And letting you go was wrong
And baby I know you got your radio on
So this is my so sad come back song

And now I'm laying down without you
In this king size empty bed
And I wish I had my arms around you
But I'll just dream of you instead

'Cause I didn't know I needed you so
And letting you go was wrong
And baby I know you got your radio on
So this is my get back, come back

Hey, I didn't know I needed you so
And letting you go and letting you go was wrong
And baby I know you got your radio on
So this is my my bad, come back, song

Oh yeah yeah

I know I said I wouldn't miss you
I said I wouldn't miss you girl, yeah

Na na na na na na na
Na na na na na na na
Na na na na na na na

We all sang na na na na na na na
Na na na na na na na
Na na na na na, yeah

Come on now

So this is my my get bad, come back, song .")
  #song 142 (one-fourty-two)
  Song.create!(artist:  "Clay Walker",
               title: "She Won't Be Lonely Long",
               rank: 142,
               copyright: "Warner/Chappell Music, Inc, Mike Curb Music, Hori Pro Entertainment Group, Dan Hodges Music, LLC",
               writers: "Douglas M. Johnson, Galen Griffin, Phil O'donnell",
               year: 2010,
               lyrics: "Somethin' 'bout the way she's wearing her dress a little tighter
Somethin' 'bout the way she's starin'--she's lookin' to start a fire
Somethin' 'bout the way she's dancin' and drinkin' chilled patron
If she's lonely now, she won't be lonely long

Heaven help the fool who did her wrong
It's too late, too bad: she's too far gone
He should've thought of that before he left her all alone
If she's lonely now, she won't be lonely long

Somethin' 'bout the way she's blushin'--you can tell she isn't sure
Let you know she's up to something she's never done before
Tonight she wants to hold a stranger, but not the one at home
If she's lonely now, she won't be lonely long

Heaven help the fool who did her wrong
It's too late, too bad: she's too far gone
He should've thought of that before he left her all alone
If she's lonely now, she won't be lonely long

If I had a woman like that
Man I'd let her know
I'd hold her tight
I'd hold her close
Do anything, do everything to let her know
She'd never ever be alone

Heaven help the fool who did her wrong
It's too late, too bad, she's too far gone
He should've thought of that before he left her all alone
If she's lonely now, Lord, if she's lonely now
If she's lonely now, she won't be lonely long .")
  #song 143 (one-fourty-three)
  Song.create!(artist:  "Miranda Lambert",
               title: "The House That Built Me",
               rank: 143,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Allen Shamblin, Tom Douglas",
               year: 2009,
               lyrics: "I know they say you can't go home again.
I just had to come back one last time.
Ma'am I know you don't know me from Adam.
But these hand prints on the front steps are mine.
Up those stairs, in that little back bedroom
Is where I did my homework and I learned to play guitar.
And I bet you didn't know, under that live oak
My favorite dog is buried in the yard.

I thought if I could touch this place or feel it
This brokenness inside me might start healing.
Out here it's like I'm someone else,
I thought that maybe I could find myself
If I could just come in I swear I'll leave.
Won't take nothing but a memory
From the house that built me.

Mama cut out pictures of houses for years.
From 'Better Homes and Garden' magazines.
Plans were drawn, and concrete poured,
And nail by nail and board by board
Daddy gave life to mama's dream.

I thought if I could touch this place or feel it
This brokenness inside me might start healing.
Out here it's like I'm someone else,
I thought that maybe I could find myself.
If I could just come in I swear I'll leave.
Won't take nothing but a memory
From the house that built me.

You leave home, you move on and you do the best you can.
I got lost in this old world and forgot who I am.

I thought if I could touch this place or feel it
This brokenness inside me might start healing.
Out here it's like I'm someone else,
I thought that maybe I could find myself.
If I could walk around I swear I'll leave.
Won't take nothing but a memory
From the house that, built me .")
  #song 144 (one-fourty-four)
  Song.create!(artist:  "Lady Antebellum",
               title: "American Honey",
               rank: 144,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Shane Stevens, Cary Barlowe, Hillary Lindsey",
               year: 2010,
               lyrics: "She grew up on a side of the road
Where the church bells ring and strong love grows
She grew up good
She grew up slow
Like American honey

Steady as a preacher
Free as a weed
Couldn't wait to get goin'
But wasn't quite ready to leave
So innocent, pure and sweet
American honey

There's a wild, wild whisper
Blowin' in the wind
Callin' out my name like a long lost friend
Oh I miss those days as the years go by
Oh nothing's sweeter than summertime
And American honey

Get caught in the race
Of this crazy life
Tryin' to be everything can make you lose your mind
I just wanna go back in time
To American honey, yea

There's a wild, wild whisper
Blowin' in the wind
Callin' out my name like a long lost friend
Oh I miss those days as the years go by
Oh nothing's sweeter than summertime
And American honey

Gone for so long now
I gotta get back to her somehow
To American honey

Ooo there's a wild, wild whisper
Blowin' in the wind
Callin' out my name like a long lost friend
Oh I miss those days as the years go by
Oh nothin's sweeter than summertime
And American honey
And American honey .")
  #song 145 (one-fourty-five)
  Song.create!(artist:  "Keith Urban",
               title: "'Til Summer Comes Around",
               rank: 145,
               copyright: "Universal Music Publishing Group",
               writers: "Keith Lionel Urban, Monty Powell",
               year: 2009,
               lyrics: "Another long summer's come and gone
I don't know why it always ends this way
The boardwalk's quiet and the carnival rides
Are as empty as my broken heart tonight

But I close my eyes and one more time
We're spinning around and you're holding on tightly
The words came out, I kissed your mouth
No Fourth of July has ever burned so brightly
You had to go, I understand
But you promised you'd be back again
And so I wander 'round this town
'Til summer comes around

I got a job working at the old park pier
And every summer now for five long years
I grease the gears, fix the lights, tighten bolts, straighten the tracks
And I count the days til you just might come back

And then I close my eyes and one more time
We're spinning around and you're holding on tightly
The words came out, I kissed your mouth
No Fourth of July has ever burned so brightly
You had to go, I understand
But you swore you'd be back again
And so I'm frozen in this town
'Til summer comes around
And it comes around

And I close my eyes and you and I
Are stuck on the Ferris wheel rocking with the motions
And Hand in hand we cried and laughed
Knowing that love belonged to us girl, if only for a moment
And 'Baby I'll be back again' you whispered in my ear
But now the winter wind is the only sound
And everything is closing down
'Til summer comes around
'Til summer comes around
'Til it comes around
And it comes around .")
  #song 146 (one-fourty-six)
  Song.create!(artist:  "Zac Brown Band",
               title: "Highway 20 Ride",
               rank: 146,
               copyright: "Reach Music Publishing",
               writers: "Wyatt Durrette, Zachry Alexander Brown",
               year: 2008,
               lyrics: "I ride east every other Friday but if I had it my way
Days would not be wasted on this drive
And I want so bad to hold you
Some of the things I haven't told you
Your mom and me just couldn't get along

So I'll drive
And I'll think about my life
And wonder why, I'll slowly die inside
Every time I turn that truck around, right at the Georgia line
And I count the days and the miles back home to you on that Highway 20 ride

A day might come and you'll realize that if you could see through my eyes
There was no other way to work it out
And a part of you might hate me
But son please don´t mistake me For a man that didn't care at all

So when you drive
And the years go flying by
I hope you smile
If I ever cross your mind
It was a pleasure of my life
And I cherished every time
And my whole world
It begins and ends with you
On that Highway 20 ride .")
  #song 147 (one-fourty-seven)
  Song.create!(artist:  "Steel Magnolia",
               title: "Keep On Lovin' You",
               rank: 147,
               copyright: "Peermusic Publishing, Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Spirit Music Group, Calhoun Enterprises",
               writers: "Trent Wayne Willmon, Chris Stapleton",
               year: 2010,
               lyrics: "Maybe life has got you run down
I think it's time girl that we slow down
So take a look it's almost sundown

So why don't you lay right here
Let me just ease your mind
I'm givin' you all my time
And I'm gonna keep on, keep on, lovin' you

Strong and slow
The way that you want me to
Baby my whole life through
I'm gonna keep on, keep on, lovin' you

When that morning sun is dawning
Baby, both of us should call in
We got too much love to fall in

So why don't you lay right here
Let me just ease your mind
I'm givin' you all my time
And I'm gonna keep on, keep on, lovin' you

Strong and slow
The way that you want me to
Baby my whole life through
I'm gonna keep on, keep on, lovin' you

So why don't you lay right here
Let me just ease your mind
I'm givin' you all my time
And I'm gonna keep on, keep on

Strong and slow
The way that you want me to
Baby my whole life through
I'm gonna keep on, keep on, lovin' you

Strong and slow
The way that you want me to
Baby my whole life through
I'm gonna keep on, keep on, keep on, lovin' you

I'm gonna keep on, keep on, lovin' you .")
  #song 148 (one-fourty-eight)
  Song.create!(artist:  "Jason Aldean",
               title: "Crazy Town",
               rank: 148,
               copyright: "Ole MM, Ole Media Management Lp",
               writers: "Brett Jones, Rodney Clawson",
               year: 2009,
               lyrics: "Roll into town step off the bus,
Shake off the where you came from dust,
Grab you guitar walk down the street,
Sign says Nashville Tennessee,
But I have found,

It's a crazy town full of neon dreams,
Everybody plays everybody sings,
Hollywood with a touch of twang,
To be a star you gotta bang bang bang,
Bend those strings till the Hank comes out,
Make all the drunk girls scream and shout,
We love it we hate it we're all just trying to make it,
In this crazy town,

Pay your dues and you play for free,
And you pray for a honky tonk destiny,
You cut your teeth in the smokey bars,
And live off the tips from a pickle jar
Till find a cool new sound
And you smile when the record man shoots you down,

It's a crazy town full of neon dreams,
Everybody plays everybody sings,
Hollywood with a touch of twang,
To be a star you gotta bang bang bang,
Bend those strings till the Hank comes out,
Make all the drunk girls scream and shout,
We love it we hate it we're all just trying to make it,
In this crazy town,

One year they reposes your truck,
And the next you make a couple million bucks,

It's a crazy town full of neon dreams,
Everybody plays everybody sings,
Hollywood with a touch of twang,
To be a star you gotta,
Bend those strings till the Hank comes out,
Make all the drunk girls scream and shout,
We love it we hate it we're all just trying to make it,
We love it we hate it we all came here to make it,
In this crazy town,
It's a crazy town .")
  #song 149 (one-fourty-nine)
  Song.create!(artist:  "Brad Paisley",
               title: "Water",
               rank: 149,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Kobalt Music Publishing Ltd.",
               writers: "BRADLEY BAKER, THOMAS COHEN, SAM KILCOYNE, MELISSA RIGBY, HUW WEBB",
               year: 2010,
               lyrics: "

Inflatable pool full of dad's hot air
I was three years old
Splashin' everywhere
And so began my love affair
With water

On a river bank
With all my friends
A big old rope tied to a limb
And your a big old wuss
If you don't jump in
The water

Yeah when that summer sun starts to beatin' down
And you don't know what to do
Grab your swimming trunks
Ice up that old Igloo
Drive until the map turns blue

Daytona Beach on spring break
Eighteen girls up on stage
White t-shirts about to be sprayed
With water

Oh let'er go boys

Yeah when that summer sun starts to beatin' down
And you don't know what to do
Just go and grab someone you wanna see in a bathing suit
And drive until the map turns blue

You can stay right there
When the daylight's gone
Play truth or dare
And it won't take long
'Fore you and her got nothin' on
But water

All you really need this time of year
Is a pair of shades
And ice cold beer
And a place to sit somewhere near
Water .")
  #song 150 (one-fifty)
  Song.create!(artist:  "Easton Corbin",
               title: "A Little More Country Than That",
               rank: 150,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group, Ole MM, Reservoir Media Management Inc",
               writers: "Wynn Varble, Rory Lee, Donald Poythress",
               year: 2010,
               lyrics: "Uh,
  Lets pull the two seater out baby
  It's wherever you wanna go

  Bobbin' to the music
  This is how we do it, all night (all night)
  Breezin' down the freeway
  Just me and my baby
  In our ride
  Just me and my boss
  No worries at all
  Listening to the
  Aston Martin music, music

  Uh,
  Would've came back for you
  I just needed time,
  To do what I had to do
  Caught in the life,
  I can't let it go
  Whether that's right,
  I won't ever know
  Uh, but here goes nothing

  When I'm alone in my room, sometimes I stare at the wall
  Automatic weapons on the flo' but who can you call
  My down bitch, one who live by the code
  Put this music shit aside, gettin' it in on the road
  Lotta quiet time, pink bottles of Rose
  Exotic red-bottom sole body glitter in gold
  Follwin' fundamentals, I'm followin' in a rental
  I love a nasty girl who swallow what's on the menu
  That money triple up when you get it out of state
  Need a new safe 'cause I'm runnin' out of space
  Elroy Jetson, I'm somewhere outta space
  In my two-seater, she the one that I will take

  Bobbin' to the music
  This is how we do it, all night (all night)
  Breezin' down the freeway
  Just me and my baby
  In our ride
  Just me and my boss
  No worries at all
  Listening to the
  Aston Martin music, music

  Would've came back for you
  I just needed time,
  To do what I had to do
  Caught in the life,
  I can't let it go
  Whether that's right,
  I won't ever know
  Uh, but here goes nothing

  Pull up on the block in a drop-top chicken box
  Mr. K.F.C, V.V'S is in the watch
  Livin' fast where it's all about that money bag
  Never front, you take it there, ain't no coming back
  Top down, right here is where she wanna be
  As my goals unfold right in front of me
  Every time we fuck her soul take a hold of me
  I dig her like pookie that pussy be contollin' me
  That thang keep calling
  Fuck maintain, boy I gotta keep ballin'!
  Pink bottles keep comin'
  James Bond coup pop-clutch, one hundred

  Bobbin' to the music
  This is how we do it, all night (all night)
  Breezin' down the freeway
  Just me and my baby
  In our ride
  Just me and my boss
  No worries at all
  Listening to the
  Aston Martin music, music

  Would've came back for you
  I just needed time,
  To do what I had to do
  Caught in the life,
  I can't let it go
  Whether that's right,
  I won't ever know
  Uh, but here goes nothing

  Aston Martin music, music
  Aston Martin music, music (all night)
  Aston Martin music, music
  Aston Martin music, music
  In our ride
  Aston Martin music, music (all night)
  Aston Martin music, music
  In our ride .")

  #song 151 (one-fifty-one)
  Song.create!(artist:  "Farmer's Daughter",
               title: "Rodney Atkins",
               rank: 151,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Rhett Akins, Marv Green, Ben Hayslip",
               year: 2009,
               lyrics: "Well I heard he needed some help on the farm
Somebody with a truck and two strong arms
Not scared of dirt and willin' to work
Till the sun goes down
So I pulled up and said I'm your man
I could start right now and we shook hands
He said the fence needs fixin',
The peaches need pickin'
And the cows need bringin' round

I was haulin' hay, I was feedin' the hogs
And that summer sun had me sweatin' like a dog
So I cooled off in the creek
Then it was back to work in the daggum heat
I was cussin' out loud, thinkin' bout quitin'
Lookin' back now I'm sure glad I didn't
Cause just when I thought it couldn't get no hotter
I caught glimpse of the farmer's daughter

She was just gettin' home from Panama city
She was all tanned up and my kinda pretty
When her eyes met mine
I was thinkin' that I would sure love my job
As the days got shorter our talks got longer
The kisses got sweeter and the feelings got stronger
So we'd hop in the truck and get all tangled up
Every chance we got
We were down by the river all night long
When the sun came up I was sneakin' her home
And draggin' my butt to work
With the smell of her perfume on my shirt
I'd be on the tractor she'd be on my mind
With that sun beatin' down on this back of mine
Just when I thought it couldn't get no hotter
I fell in love with the farmer's daughter
We got married last spring
Whoa and there ain't no better life for me

I'm still haulin' hay and feedin' the hogs
And that summer sun has me sweatin' like a dog
So I cool off in the creek
And she brings me outta glass of sweet ice tea
I'm on the tractor and shes on my mind
And I can't wait till its quitin' time
And just when I think it can't get no hotter
I come home to the farmer's daughter
Yeah, the farmer's daughter,

Hey baby .")
  #song 152 (one-fifty-two)
  Song.create!(artist:  "Keith Urban",
               title: "I'm In",
               rank: 152,
               copyright: "Universal Music Publishing Group",
               writers: "GEORGIA L. MIDDLEMAN, RADNEY M. FOSTER",
               year: 2009,
               lyrics: "
Love doesn't come with a contract
You give me this I give you that
And it's scary business
Your heart and soul is on the line
Baby why else would I be standing 'round here so tongue-tied

If I knew what I was doing, I'd be doing it right now
I would be the best damn poet
Silver words out of my mouth
Well my words might not be magic
But they cut straight to the truth
So if you need a lover and a friend
Baby I'm in, I'm in

Baby come on in the water's fine
I'll be right here you take your time
Just let me hold you and we'll both take that leap of faith
It's like I told you there's no guarantees when you feel this way

If I knew what I was doing, I'd be doing it right now
I would be the best damn poet
Silver words out of my mouth
Well my words might not be magic
But they cut straight to the truth
So if you need a lover and a friend
Baby I'm in, I'm in

Baby I'm in, and I'm in

Baby come here next to me I'll show you how good it can be
I'll breathe each breath you breathe I can pour out everything I am
Everything thing I am

Oh, and if I knew what I was doing I'd be doing it right now
I would be the best damn poet silver words out of my mouth
Well my words might not be magic
But they cut straight to the truth
So if you need a lover and a friend, yeah
If you need a lover and a friend
Oh baby I'm in, and I'm in,
Baby I'm in, and I'm in

Baby come here next to me I'll show you how good it can be
I'll breathe each breath you breathe I can pour out everything I am
And baby come here next to me I'll show you how good it can be
And baby come here next to me I'll show you how good it can be
Baby come here, baby come here, baby come here .")
  #song 153 (one-fifty-three)
  Song.create!(artist:  "Jerrod Niemann",
               title: "Lover, Lover",
               rank: 153,
               copyright: "Universal Music Publishing Group",
               writers: "Daniel Friend Pritzker",
               year: 2010,
               lyrics: "
  Well the truth,
Well it hurts to say,
I'm gonna pack up my bags
And I'm gonna go away
I'm gonna split
I can't stand it
I'm gonna give it up and quit
Ain't never comin' back
Girl but before I get to go and I got to say
I know you used to love me
But that was yesterday
And the truth I won't fight it
When the love starts burnin' you got to do what's right

Whoa lover, lover, lover,
You don't treat me no good no more
Whoa lover, lover, lover,
You don't treat me no good no more

Well the truth,
Yeah it hurts to say,
I'm gonna pack up my bags
And I'm gonna go away
I'm gonna split
I can't stand it
I'm gonna give it up and quit
Ain't never comin' back
Girl but before I get to go and I got to say
There was a time oh woman
When you used to shake it for me
Now all you do is just treat me cold
Ain't gonna take it no more
Gonna walk out the door

Lover, lover, lover
You don't treat me no good no more,
No good no more, more, more
Lover lover lover
You don't treat me no good no more

Yeah, yeah, yeah

Well I wait up for you almost every night
And I'm hurtin' so bad cause you don't treat me right
Oh woman oh woman you know I love you so but
You're so mean to me baby I'm walkin' out the door

Lover oh lover
Yeah, yeah
No good no more, more, more

I know you used to love me in every way,
But now I'm givin' it up
And I'm tired of cryin' babe
I can't stand it no longer
It hurts me to say,
But I'm packin' up my bags and goin' far away

Lover, oh lover, yeah, yeah
Lover, lover, lover,
You don't treat me no good no more
Lover, lover, lover,
You don't treat me no good no more
Lover, lover, lover,
You don't treat me no good no more
Lover, lover, lover
You don't treat me no good no more .")
  #song 154 (one-fifty-four)
  Song.create!(artist:  "Little Big Town",
               title: "Little White Church",
               rank: 154,
               copyright: "Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Peermusic Publishing",
               writers: "Jimi Westbrook, Karen Fairchild, Kimberly Schlapman, Kimberly B. Roads, Phillip Sweet, Wayne Kirkpatrick",
               year: 2010,
               lyrics: "You've been singing that same old song
Far too long, far too long
Say you'll buy me a shiny ring
But your words don't mean a thing
No more calling me baby
No more loving like crazy

Till you take me down (take me down)
You better take me down (take me down)
Take me down to the little white church
Take me down (take me down) take me down (take me down)
Take me down to the little white church
Take me down

You can't ride this gravy train
Anymore, anyway
There's a price for keeping me
I might be cheap, but I ain't free
No more calling me baby
No more loving like crazy

Till you take me down (take me down)
You better take me down (take me down)
Take me down to the little white church
Take me down (take me down) take me down (take me down)
Take me down to the little white church
Take me down

Come on

Charming devil, silver tongue
Had your fun, now you're done
Mama warned me 'bout your games
She don't like you anyway

No more calling me baby
No more loving like crazy
No more chicken and gravy
Ain't gonna have your baby

Till you take me down (take me down)
You better take me down (take me down)
Take me down to the little white church
Take me down (take me down)
You better take me down (take me down)
Take me down to the little white church
Take me down to the little white church (take me down)
Take me down (take me down to the little white church)
Take me down (take me down)
You better take me down (you better take me down)
Take me down to the little white church
Take me down (take me down)
You better take me down (take me down)
Take me down to the little white church
Take me down (take me down)
You better take me down (take me down)
Take me down to the little white church
Take me down to the little white church, take me down to the little white church .")
  #song 155 (one-fifty-five)
  Song.create!(artist:  "Darius Rucker",
               title: "History In The Making",
               rank: 155,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, Kobalt Music Publishing Ltd",
               writers: "Darius C. Rucker, Frank Mandeville V. Rogers, Clay Mills",
               year: 2008,
               lyrics: "Don't move, baby don't move
Aw look at you, I just want to take this in
The moonlight dancin' off your skin
Our time, let's take our time
I just want to look in your eyes
And catch my breath
'Cause I got a feelin'

This could be one of those memories
We wanna hold on to, cling to
The one we can't forget
Baby, this could be our last first kiss
The thought of forever
What if this was that moment
That chance worth takin'
History in the makin'

Inside, baby, inside
Can you feel the butterflies
Floatin' all around
'Cause I can sure feel 'em now
Tonight, maybe tonight
Is the start of a beautiful ride
That'll never end
And baby I got a feelin'

This could be one of those memories
We wanna hold on to, cling to
The one we can't forget
Baby, this could be our last first kiss
The thought of forever
What if this was that moment
That chance worth takin'
History in the makin'

Right here, right now
Holdin' you in my arms
Ah-ah-ah-ah

This could be one of those memories
We wanna hold on to, we want to cling to
The one we can't forget
Baby, this could be our last first kiss
The thoughts of forever
What if this was that moment
A chance worth takin'
History in the makin'

Ah yeah
History in the makin'
Ah ah
This is a chance worth takin
Ah ah .")
  #song 156 (one-fifty-six)
  Song.create!(artist:  "Carrie Underwood",
               title: "Temporary Home",
               rank: 156,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Zac Maloy, Luke Laird, Carrie Underwood",
               year: 2009,
               lyrics: "Little boy, six years old
A little too used to bein' alone
Another new mom and dad, another school
Another house that'll never be home
When people ask him how he likes this place
He looks up and says with a smile upon his face

'This is my temporary home
It's not where I belong
Windows in rooms that I'm passin' through
This is just a stop, on the way to where I'm going
I'm not afraid because I know this is my
Temporary home.'

Young mom on her own
She needs a little help got nowhere to go
She's lookin' for a job, lookin' for a way out
'Cause a half-way house will never be a home
At night she whispers to her baby girl
Someday we'll find a place here in this world

'This is our temporary home
It's not where we belong
Windows in rooms that we're passin' through
This is just a stop, on the way to where we're going
I'm not afraid because I know this is our
Temporary Home.'

Old man, hospital bed
The room is filled with people he loves
And he whispers don't cry for me
I'll see you all someday
He looks up and says 'I can see God's face'

'This is my temporary Home
It's not where I belong
Windows in rooms that I'm passin' through
This was just a stop, on the way to where I'm going
I'm not afraid because I know this was
My temporary home.'

This is our temporary home .")
  #song 157 (one-fifty-seven)
  Song.create!(artist:  "Billy Currington",
               title: "Little Bit of Everything",
               rank: 157,
               copyright: "Sony/ATV Music Publishing LLC, Ole MM, Universal Music Publishing Group",
               writers: "Billy Currington, Dallas Davidson, Brett Jones",
               year: 2008,
               lyrics: "They wake up in the morning
And they drink their coffee black
They love their women
One beat shy of a heart attack
On the way to work
They might stop by a fishin' hole
That's how country boys roll

Yeah, they work, work, work
All week 'til the job gets done
Weekends they barbecue
And have a cold one
They run on a big old hog
And a pinch of Skoal
That's how country boys roll

Yeah, they're spinnin' their wheels
Castin' their reels
Way back on them old county roads
Singin' in bars
Soupin' up cars
Just to see how fast they'll go
From a ball and a glove
To them fallin' in love
They do everything heart and soul
That's how country boys roll

Well if you don't know your way around
They'll draw you a map
And if you're broke and you ask
They'll give you the shirt off their back
All they need is a little gas
A few dollars to fold
That's how country boys roll

Yeah, they're spinning their wheels
Castin' their reels
Way back on them old county roads
Singin' in bars
Soupin' up cars
Just to see how fast they'll go
From a ball and a glove
To them fallin' in love
They do everything heart and soul
That's how country boys roll

Ah, come on

Yeah, they're cryin' and loud
Humble and proud
They love mama and Jesus and Jones
That's how country boys roll
Don't you know
Yeah, that's how country boys roll .")
  #song 158 (one-fifty-eight)
  Song.create!(artist:  "Uncle Kracker",
               title: "Smile",
               rank: 158,
               copyright: "O/B/O Apra Amcos, Peermusic Publishing, Warner/Chappell Music, Inc, Universal Music Publishing Group, Capitol Christian Music Group, BMG Rights Management US, LLC, Songs Music Publishing",
               writers: "John Thomas Harding, Jeremy Cedric Bose, Blair Daly, Matthew Shafer ",
               year: 2009,
               lyrics: "You're better than the best
I'm lucky just to linger in your light
Cooler then the flip side of my pillow, that's right
Completely unaware
Nothing can compare to where you send me,
Lets me know that it's OK, yeah it's OK
And the moments where my good times start to fade

You make me smile like the sun
Fall out of bed, sing like a bird
Dizzy in my head, spin like a record
Crazy on a Sunday night
You make me dance like a fool
Forget how to breathe
Shine like gold, buzz like a bee
Just the thought of you can drive me wild
Oh, you make me smile

Even when you're gone
Somehow you come along
Just like a flower poking through the sidewalk crack and just like that
You steal away the rain and just like that

You make me smile like the sun
Fall out of bed, sing like a bird
Dizzy in my head, spin like a record
Crazy on a Sunday night
You make me dance like a fool
Forget how to breathe
Shine like gold, buzz like a bee
Just the thought of you can drive me wild
Oh, you make me smile

Don't know how I lived without you
Cause every time that I get around ya
I see the best of me inside your eyes
You make me smile
You make me dance like a fool
Forget how to breathe
Shine like gold, buzz like a bee
Just the thought of you can drive me wild

You make me smile like the sun
Fall out of bed, sing like a bird
Dizzy in my head, spin like a record
Crazy on a Sunday night
You make me dance like a fool
Forget how to breathe
Shine like gold, buzz like a bee
Just the thought of you can drive me wild
Oh, you make me smile
Oh, you make me smile
Oh, you make me smile .")
  #song 159 (one-fifty-nine)
  Song.create!(artist:  "Billy Currington",
               title: "Pretty Good At Drinkin' Beer",
               rank: 159,
               copyright: "Peermusic Publishing",
               writers: "Troy Jones",
               year: 2010,
               lyrics: "I wasn't born for diggin' deep holes
I'm not made for pavin' long roads
I ain't cut out to climb high line poles
But I'm pretty good at drinkin' beer

I'm not the type to work in a bank
I'm no good at slappin' on paint
Don't have a knack for makin' motors crank, no
But I'm pretty good at drinkin' beer

So hand me one more
That's what I'm here for
I'm built for having a ball
I love the nightlife
I love my Bud Light
I like 'em cold and tall

I ain't much for mowin' thick grass
I'm too slow for workin' too fast
I don't do windows so honey don't ask
But I'm pretty good at drinkin' beer

A go getter well maybe I'm not
I'm not known for doin' a lot
But I do my best work when the weather's hot
I'm pretty good at drinkin' beer

So hand me one more
That's what I'm here for
I'm built for having a ball
I love the nightlife
I love my Bud Light
I like 'em cold and tall

I wasn't born for diggin' deep holes
I'm not made for pavin' long roads
I ain't cut out to climb high line poles
But I'm pretty good at drinkin' beer
I'm pretty good at drinkin' beer

O' hand me one more boys,
That's what I'm here for .")
  #song 160 (one-sixty)
  Song.create!(artist:  "Carrie Underwood",
               title: "Undo It",
               rank: 160,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Kara E. Dioguardi, Marty Frederiksen, Luke Laird, Carrie Underwood",
               year: 2010,
               lyrics: "Should've known by the way you pass me by
There was something in your eyes
And it wasn't right
I should've walked but I never had the chance
Everything got out of hand and I let it slide
Now I only have myself to blame
For falling for your stupid games
I wish my life could be the way it was before I saw your face

You stole my happy
You made me cry
You took the lonely and took me for a ride
And I wanna un un un un undo it
You had my heart now I want it back
I'm starting to see everything you lack
Boy you blew it
You put me through it
I wanna un un un un undo it
Na na na na na
Na na na na na
Na na na na na na

Now your photos don't have a picture frame
And I never say your name and I never will
And all your things well I threw them in the trash
And I'm not even sad
Now you only have yourself to blame
For playing all those stupid games
You're always gonna be the same
Oh no you'll never change

You stole my happy
You made me cry
You took the lonely and took me for a ride
And I wanna un un un un undo it
You had my heart now I want it back
I'm starting to see everything you lack
Boy you blew it
You put me through it
I wanna un un un un undo it
Na na na na na
Na na na na na
Na na na na na na

You want my future you can't have it
Still trying to erase you from my past
I need you gone so fast

You stole my happy
You made me cry
You took the lonely and took me for a ride
And I wanna un un un un undo it
You had my heart now I want it back
I'm starting to see everything you lack
Boy you blew it
You put me through it
I wanna un un un un undo it

You stole my happy
You made me cry
You took the lonely and took me for a ride
Boy you blew it
You put me through it
I wanna un un un un undo it .")
  #song 161 (one-sixty-one)
  Song.create!(artist:  "Sugarland",
               title: "Stuck Like Glue",
               rank: 161,
               copyright: "Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Blake Shy Anythony Carter, Jennifer Nettles, Kevin M. Griffin, Kristian Bush",
               year: 2010,
               lyrics: "Absolutely no one who knows me better
No one that can make me feel so good
How did we stay so long together

When everybody
Everybody said we never would
And just when I
I start to think they're right
That love has died

There you go making my heart beat again
Heart beat again, heart beat again
There you go making me feel like a kid
Won't you do it and do it one time,
There you go pulling me right back in
Right back in, right back in
And I know I'm never letting this go

I'm stuck on you
Whoa-oh, whoa-oh
Stuck like glue
You and me baby
We're stuck like glue

Whoa-oh, whoa-oh
Stuck like glue
You and me baby
We're stuck like glue

Some days I don't feel like trying
Some days you know I wanna just give up
When it doesn't matter who's right
Fight about it all night

Had enough
You give me that look
I'm sorry baby let's make up
You do that thing that makes me laugh
And just like that

There you go making my heart beat again
Heart beat again, heart beat again
There you go making me feel like a kid
Won't you do it and do it one time
There you go pulling me right back in
Right back in, right back in
And I know I'm never letting this go

I'm stuck on you
Whoa-oh, whoa-oh
Stuck like glue
You and me baby
We're stuck like glue

Whoa-oh, whoa-oh
Stuck like glue
You and me baby
We're stuck like glue

Whoa-oh, whoa-oh
You almost stay out
Too stuck together from the A-T-L

Whoa-oh, whoa-oh
Feeling kinda sick
Just a spoon full of sugar make it better real quick

I say, whoa-oh, whoa-oh
What'cha gonna do with that
Whoa-oh, whoa-oh
Come on over here with that
Sugar sticky sweet stuff
Come on give me that stuff
Everybody want some
Melodies that get stuck

Up in your head
Whoa-oh, whoa-oh
Up in your head
Whoa-oh, whoa-oh
Up in your head
Whoa-oh, whoa-oh
Up in your head
Whoa-oh, whoa-oh
Whoa-oh, whoa-oh
Stuck like glue
You and me together
Say it's all I wanna do I said

There you go making my heart beat again
Heart beat again, heart beat again
There you go making me feel like a kid
Won't you do it and do it one time
There you go pulling me right back in
Right back in, right back in
And I know (I know)
I'm never letting this go (never letting this go)

There you go making my heart beat again
Heart beat again, heart beat again
There you go making me feel like a kid
Won't you do it and do it one time
There you go pulling me right back in
Right back in, right back in
And I know (know)
I'm never letting this go (never letting this go)
I'm stuck on you

Whoa-oh, whoa-oh
Stuck like glue
You and me baby
We're stuck like glue

Whoa-oh, whoa-oh
Stuck like glue
You and me baby
We're stuck like glue

Whoa-oh, whoa-oh
Stuck like glue
You and me baby
We're stuck like glue .")
  #song 162 (one-sixty-two)
  Song.create!(artist:  "Lady Antebellum",
               title: "I Run To You",
               rank: 162,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Charles Kelley, Dave Haywood, Hillary Scott, Tom Douglas",
               year: 2009,
               lyrics: "I run from hate
I run from prejudice
I run from pessimists
But I run too late
I run my life
Or is it running me
Run from my past
I run too fast
Or too slow it seems
When lies become the truth
That's when I run to you

This world keeps spinning faster
Into a new disaster so I run to you
I run to you baby
And when it all starts coming undone
Baby you're the only one I run to
I run to you

We run on fumes
Your life and mine
Like the sands of time
Slippin' right on through
And our love's the only truth
That's why I run to you

This world keeps spinning faster
Into a new disaster so I run to you
I run to you baby
When it all starts coming undone
Baby you're the only one I run to
I run to you, whoa oh, oh I run to you

This world keeps spinning faster
Into a new disaster so I run to you
I run to you baby
And when it all starts coming undone
Baby you're the only one I run to
I run to you, I run to you yeah, whoa whoa, oh I run to you, I run to you girl

I run to you

I always run to you
Run to you
Run to you .")
  #song 163 (one-sixty-three)
  Song.create!(artist:  "Zac Brown Band",
               title: "Whatever It Is",
               rank: 163,
               copyright: "Reach Music Publishing",
               writers: "Wyatt Durrette, Zachry Alexander Brown",
               year: 2008,
               lyrics: "She's got eyes that cut you like a knife and
Lips that taste like sweet red wine
And pretty legs go to heaven every time
She got a gentle way that puts me at ease
When she walks in the room I can hardly breathe
Got a devastating smile knock a grown man to his knees

She's got whatever it is
It blows me away
She's everything I wanted to say to a woman
But couldn't find the words to say
She's got whatever it is
I don't know what to do
Because every time I try and tell her how I feel
It comes out 'I love you'
You got whatever it is

You know I've never been the type that would ever want to stay
Bring 'em home at night and they're gone the next day
But that all changed when she walked into my life
People ask me what it is
I tell them I don't know
Just something about the woman makes my heart go haywire
She's gonna be my wife

She's got whatever it is
It blows me away
She's everything I wanted to say to a woman
But couldn't find the words to say
She's got whatever it is
I don't know what to do
Because every time I try and tell her how I feel
It comes out 'I love you'
You got whatever it is

'Cause when she loves me
Girl that's how I feel
'Cause when she loves me I'm on top of the world
Because when she loves me I can live forever
When she loves me I am untouchable

She's got whatever it is
It blows me away
She's everything I wanted to say to a woman
But couldn't find the words to say
She's got whatever it is
I don't know what to do
Because every time I try and tell her how I feel
It comes out 'I love you' (I do)
You got whatever it is .")
  #song 164 (one-sixty-four)
  Song.create!(artist:  "Randy Houser",
               title: "Boots On",
               rank: 164,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Brandon Kinney, Randy Houser",
               year: 2008,
               lyrics: "Man, I've been workin' too hard
Ten hour days and I'm tired
Damn this knuckle busted',
Back breakin', no paying job
Know where I'm goin' from here
Hot headed women, cold beer
Kick up my heels for a little while
And do it country style

In my dirty 'ole hat
With my crooked little grin
Granny beaded neck
And these calloused hands
And a muddy pair of jeans
With that Copenhagen ring
No need to change a thing, hey y'all
I'm going out with my boots on

How I keep catching her eye
Man, I keep wondering why
Ain't nothing special 'bout
An 'awe shucks' country boy
Lord, she's sure lookin' good
Like something from Hollywood
She got me thinkin' that I just might
Leave here with her tonight

In my dirty 'ole hat
With my crooked little grin
Granny beaded neck
And these calloused hands
And a muddy pair of jeans
With that Copenhagen ring
No need to change a thing, hey y'all
I'm going out with my boots on

'Cause I am who I am and that's
The man I'm gonna be, yeah
And when the Lord comes callin',
Well, he ain't gonna have
To holler, y'all
There'll be no trouble finding me

In my dirty 'ole hat
With my crooked little grin
Granny beaded neck
And these calloused hands
And a muddy pair of jeans
With that Copenhagen ring
No need to change a thing, hey y'all
I'm going out with my boots on

With my boots on
He's gonna take me home
Lord, with my boots on .")
  #song 165 (one-sixty-five)
  Song.create!(artist:  "Darius Rucker",
               title: "It Won't Be Like This For Long",
               rank: 165,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, Kobalt Music Publishing Ltd., Spirit Music Group, BMG Rights Management US, LLC",
               writers: "Charles Dubois, Ashley Gorley, Ashley Rucker",
               year: 2008,
               lyrics: "He didn't have to wake up
He'd been up all night
Laying there in bed listening
To his new born baby cry
He makes a pot of coffee
He splashes water on his face
His wife gives him a kiss and says
It gonna be OK

It wont be like this for long
One day soon we'll look back laughin'
At the week we brought her home
This phase is gonna fly by
So baby just hold on
It wont be like this for long

Four years later bout four thirty
She's crawling in their bed
And when he drops her off at preschool
She's clinging to his leg
The teacher peels her off of him
He says what can I do
She says now don't you worry
This will only last a week or two

It wont be like this for long
One day soon we'll look back laughin'
At the week we brought her home
This phase is gonna fly by
So baby just hold on
It wont be like this for long

One day soon she'll be a teenager
And at times you'll think she hates him
Then he'll walk her down the isle
And he'll raise her veil
But right now she up and crying
And the truth is that he don't mind
As he kisses her good night
And she says her prayers
He lays down there beside her
Till her eyes are finally closed
And just watching her it breaks his heart
Cause he already knows
It wont be like this for long
One day soon that little girl is gonna be
All grown up and gone
Yeah this phase is gonna fly by
He's trying to hold on
It won't be like this for long
It won't be like this for long .")
  #song 166 (one-sixty-six)
  Song.create!(artist:  "George Straight",
               title: "River Of Love",
               rank: 166,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Billy Burnette, Shawn Camp, Dennis Morgan",
               year: 2008,
               lyrics: "Hey baby won't you take a little ride with me
Have a look around, see what we could see
I've got the paddle, I've got the boat
Come on baby I know she'll float

We'll go rollin' on the river of love
We'll go rollin' on the river of love

Drift away from all these city lights
Might rock a little bit, so hold on tight
Let's get carried away with the gentle flow
Might get caught up in it's undertow

We'll go rollin' on the river of love
Let's go rollin' on the river of love

I got a little ukulele let me sing you a song
I got a stream of kisses 'bout ten miles long
Dancing on the water in the moonlight beach
Sparking down through the sea of dreams

We'll go rollin' on the river of love
Let's go rollin' on the river of love

Yeah we'll go rollin' on the river of love
Let's go rollin' on the river of love

River of love
River of love
River of love
Rollin' on the river of love

Let's go rollin' on the river of love .")
  #song 167 (one-sixty-seven)
  Song.create!(artist:  "Dierks Bentley",
               title: "Sideways",
               rank: 167,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd.",
               writers: "Dierks Bentley, James Beavers",
               year: 2009,
               lyrics: "Hey girl, what's your name?
It's so loud in here I can't hear a thing,
But I sure do like your style,
And I can see you came to rock
In your blue jeans and white tank top,
Man that look drives me wild.

And it's hey now here we go.
DJ don't you play nothing slow
Keep those girls out on the floor
Gotta make them want to come back for more.
Been here since the sun went down,
Be here when it comes back around
Worked all week it's time to play
Gonna get a little bit sideways
Sideways.

Ain't no need to fight
Y'all take that redneck stuff outside
That's what parking lots are for.
Once you're out you ain't coming back
Them velvet ropes ain't got no slack
Man they're busting down the door!

And it's hey now here we go.
DJ don't you play nothing slow
Keep those girls out on the floor
Gotta make them want to come back for more.
Been here since the sun went down,
Be here when it comes back around
Worked all week it's time to play
Gonna get a little bit sideways
Sideways.

That's right

Hey now here we go
(hey now here we go)
DJ don't you play nothing slow
Gotta make 'em wanna (what?) gotta make 'em wanna (what?)
Gotta make 'em wanna come back for more.

It's hey now here we go.
DJ don't you play nothing slow
Keep those girls out on the floor
Gotta make them want to come back for more.
Been here since the sun went down,
Be here when it comes back around
Worked all week it's time to play
Gonna get a little bit sideways
Sideways. Sideways. Sideways.

Hey girl what's your name?
It's so loud in here I can't hear a thing .")
  #song 168 (one-sixty-eight)
  Song.create!(artist:  "Billy Currington",
               title: "People Are Crazy",
               rank: 168,
               copyright: "Peermusic Publishing, Sony/ATV Music Publishing LLC",
               writers: "Troy Jones, Bobby Braddock",
               year: 2008,
               lyrics: "This old man and me, were at the bar and we
Were having us some beers and swaping 'I don't cares'
Talking politics, blond and red-head chicks
Old dogs and new tricks and habits we ain't kicked

We talked about God's grace and all the hell we raised
Then I heard the ol' man say;
'God is great, beer is good and people are crazy'

He said 'I fought two wars,
Been married and divorced'
What brings you to Ohio?
He said 'Damned if I know'
We talked an hour or two about every girl we knew
What all we put 'em through
Like two old boys will do

We pondered life and death
He light a cigarette
He said 'These damn things will kill me yet;
But God is great, beer is good and people are crazy'

Last call its two a.m., I said goodbye to him
I never talked to him again
Then one sunny day, I saw the old man's face
Front page obituary, he was a millionaire
He left his fortune to some guy he barely knew, his kids were mad as hell
But me, I'm doing well
And I drop by today, to just say thanks and pray,
And I left a six-pack right there on his grave and I said;
'God is great, beer is good, and people are crazy'

God is great, beer is good, and people are crazy.

God is great, beer is good, and people are crazy .")
  #song 169 (one-sixty-nine)
  Song.create!(artist:  "Darius Rucker",
               title: "Alright",
               rank: 169,
               copyright: "EMI Music Publishing, Universal Music Publishing Group, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Darius C. Rucker, Frank Mandeville V. Rogers",
               year: 2008,
               lyrics: "Alright, alright
Yeah it's alright, alright

Don't need no five star reservations
I've got spaghetti and a cheap bottle of wine
Don't need no concert in the city
I've got a stereo and the best of Patsy Cline
Ain't got no caviar no Dom Perignon
But as far as I can see, I've got everything I want

Cause I've got a roof over my head,
The woman I love laying in my bed
And it's alright, alright
I've got shoes under my feet
Forever in her eyes staring back at me
And it's alright, alright
And I've got all I need
And it's alright by me

Maybe later on we'll walk down to the river
Lay on a blanket and stare up at the moon
It may not be no French Riviera
But it's all the same to me as long as I'm with you

It may be a simple life, but that's okay
If you ask me baby, I think I've got it made

Cause I've got a roof over my head
The woman I love laying in my bed
And it's alright, alright
I've got shoes under my feet
Forever in her eyes staring back at me
And it's alright, alright
And I've got all I need
And it's alright by me

It's alright by me, yeah yeah
When I lay down at night I thank the Lord above
For giving me everything I ever could dream of

Cause I've got a roof over my head
The woman I love laying in my bed
And it's alright, alright, alright, alright
I've got shoes under my feet
Forever in her eyes staring back at me
And it's alright, alright, alright
And I've got all I need, yeah
I've got all I need
And it's alright by me
Oh yeah, it's alright by me .")
  #song 170 (one-seventy)
  Song.create!(artist:  "Keith Urban",
               title: "Sweet Thing",
               rank: 170,
               copyright: "Warner/Chappell Music, Inc., Universal Music Publishing Group",
               writers: "SCOTT SAUNDERS, JAMES R. ROBERTSON, TIM R. ROLLINSON, TEREPAI RICHMOND, ALEXANDER RICHARD HEWETSON",
               year: 2009,
               lyrics: "
When I picked you up for our first date baby
Well, your pretty blue eyes, they were drivin' me crazy
And the tiny little thought that was so amazing
Is they were looking at me.

I held open the car door for you then you climbed
Inside and slid on over
To the other side, I thought my, oh my

Sweet thing
The moon is high and the night is young
Come on and meet me
In the backyard under the Cottonwood tree
It's a good thing and I'm wishing
C'mon sweet thing
Won't you climb on out of your window
While the world is sleeping
You know I need you
And there's no way I'll be leaving
'Till we're kissing on the porch swing
Oh my little sweet thing

Yeah I know I'm gonna see you first thing tomorrow
But I just couldn't wait so I had to borrow
Uncle Jake's Mustang, its his favorite car
And so I can't stay long

Standing here feeling like a love struck Romeo
All I wanna do is hold you close and steal a little
More time, is that such a crime?

Sweet thing
The moon is high and the night is young
Come on and meet me
In the backyard under the Cottonwood tree
It's a good thing and I'm wishing
C'mon sweet thing
Won't you climb on out of your window
While the world is sleeping
You know I need you
And there's no way I'll be leaving
'Till we're kissing on the porch swing
Oh my little sweet thing

Oh my sweet thing
Sweet thing, sweet thing

Oh my sweet thing
The moon is high and the night is young
Come on and meet me
In the backyard under the cottonwood tree
It's a good thing and tell me I'm not dreaming
C'mon sweet thing
Won't you climb on out of your window
While the world is sleeping
Cause you know I need you
And there's no way I'll be leaving
'Till we're kissing on the porch swing
Oh my little sweet thing

Oh c'mon sweet thing, sweet thing, sweet thing, sweet thing

Yeah, c'mon now a little now
Do do do do do do do do do do do
Oh my little sweet thing, yes you are
Do do do do do do do do do do do .")
  #song 171 (one-seventy-one)
  Song.create!(artist:  "Jason Aldean",
               title: "Big Green Tractor",
               rank: 171,
               copyright: "Warner/Chappell Music, Inc, Spirit Music Group, Carol Vincent & Assoc LLC",
               writers: "David Lee Murphy, Jim Collins",
               year: 2009,
               lyrics: "She had a shiny little beamer with the rag top down,
Sittin' in the drive but she wouldn't get out,
The dogs were all barking and wagging around,
And I just laughed and said y'all get in,
She had on a new dress and she curled her hair,
She was looking too good not to go somewhere,
Said what you want to do baby I don't care,
We can go to the show we can stay out here

And I can take you for a ride on my big green tractor,
We can go slow or make it go faster,
Down through the woods and out to the pasture,
Long as I'm with you it really don't matter,
Climb up in my lap and drive if you want to,
Girl you know you got me to hold on to,
We can go to town but baby if you'd rather,
I'll take you for a ride on my big green tractor

Said we can fire it up and I can show you around,
Sit upon the hill and watch the sun go down,
When the fireflies are dancing and the moon comes out,
We can turn on the lights and head back to the house

Or we can take you for a ride on my big green tractor,
We can go slow or make it go faster,
Down through the woods and out to the pasture,
Long as I'm with you it really don't matter,
Climb up in my lap and drive if you want to,
Girl you know you got me to hold on to,
We can go to town but baby if you'd rather,
I'll take you for a ride on my big green tractor

Just let me dust off the seat,
Put your pretty little arms around me, hell yeah

You can climb up in my lap and drive if you want to,
Girl you know you got me to hold on to,
We can go to town but baby if you'd rather,
I'll take you for a ride on my big green tractor

Oh yeah yeah,
We can go to town,
Or we can go another round,
On my big green tractor .")
  #song 172 (one-seventy-two)
  Song.create!(artist:  "Chris Young",
               title: "Gettin' You Home",
               rank: 172,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Chris Young, Christopher Young, Cory Batten, Kent Evan Blazy",
               year: 2009,
               lyrics: "Tuxedo waiters, black tie,
White table clothes and red wine,
We've been planning, this night,
Looking forward to it, for some time
Now honey I know you love getting dressed up,
And you know I love showing you off,
But watching your baby blue eyes,
Dancing in the candle light glow,
All I can think about, is getting you home

Walking through the front door,
Seeing your black dress hit the floor,
Uh honey there sure ain't nothing, like you loving
Me all night long, and all I can think about is getting you home

I don't need this menu, no I don't,
I already know just what I want,
Did I hear you right, did you tell me,
Go pay the waiter and lets leave,
Now honey I know by that look in your eyes,
And your hand drawing hearts on mine,
That our night out of the house, ain't gonna last too long,
When all you can think about, is getting me home

Walking through the front door,
Seeing your black dress hit the floor,
Uh honey there sure ain't nothing like you loving me
All night long,
And all I can think about, is getting you home

Walking through the front door,
Seeing your black dress hit the floor,
Uh honey there sure ain't nothing like you loving me
All night long,
And all I can think about, all I can think about,
All I can think about, is getting you home .")
  #song 173 (one-seventy-three)
  Song.create!(artist:  "Taylor Swift",
               title: "You Belong With Me",
               rank: 173,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Liz Rose, Taylor Swift",
               year: 2008,
               lyrics: "You're on the phone with your girlfriend
She's upset
She's going off about something that you said
'Cause she doesn't get your humor like I do
I'm in my room
It's a typical Tuesday night
I'm listening to the kind of music she doesn't like
And she'll never know your story like I do'

But she wears short skirts
I wear T-shirts
She's cheer captain
And I'm on the bleachers
Dreaming about the day when you wake up
And find that what you're looking for has been here the whole time

If you can see I'm the one who understands you
Been here all along so why can't you see
You belong with me
You belong with me

Walkin' the streets with you and your worn-out jeans
I can't help thinking this is how it ought to be
Laughing on a park bench, thinking to myself
Hey isn't this easy

And you've got a smile that could light up this whole town
I haven't seen it in a while since she brought you down
You say your fine
I know you better then that
Hey what you doing with a girl like that

She wears high heels
I wear sneakers
She's cheer captain and I'm on the bleachers
Dreaming about the day when you wake up
And find that what what you're looking for has been here the whole time

If you can see that I'm the one who understands you
Been here all along so why can't you see
You belong with me

Standing by and waiting at your back door
All this time how could you not know
Baby, you belong with me
You belong with me

Oh, I remember you drivin' to my house in the middle of the night
I'm the one who makes you laugh
When you know you're about to cry
And I know your favorite songs
And you tell me about your dreams
I think I know where you belong
I think I know it's with me

Can't you see that I'm the one who understands you
Been here all along so why can't you see
You belong with me

Standing by and waiting at your back door
All this time
How could you not know
Baby you belong with me
You belong with me

You belong with me
Have you ever thought just maybe
You belong with me
You belong with me .")
  #song 174 (one-seventy-four)
  Song.create!(artist:  "Jason Aldean",
               title: "She's Country",
               rank: 174,
               copyright: "Warner/Chappell Music, Inc, Sony/ATV Music Publishing LLC, Round Hill Music Big Loud Songs",
               writers: "Daniel Wayne Myrick, Bridgette Tatum",
               year: 2009,
               lyrics: "You boys ever met a real country girl?
I'm talking, true blue, out in the woods, down home, country girl

She's a hot little number in her pick-up truck
Daddy's sweet money done jacked it up
She's a party-all-nighter from South Carolina, a bad mamajama from down in Alabama
She's a raging Cajun, a lunatic from Brunswick, juicy Georgia peach
With a thick southern drawl, sexy swing and walk, brother she's all

Country, (shoot) from her cowboy boots to her down home roots
She's country, from the songs she plays to the prayers she prays,
That's the way she was born and raised, she ain't afraid to stay, country
Brother she's country

A hell raisin sugar when the sun goes down, mama taught her how to rip up a town
Honey dripping honey from the hollering Kentucky,
Get ya flipping kinda trippie like a Mississippi hippie,
She's a Kansas princess, crazy mother trucker, undercover lover
Thick southern drawl, sexy swing and walk,
Brother she's all

Country (shoot) from her cowboy boots to her down home roots
She's country, from the songs she plays to the prayers she prays,
That's the way she was born and raised, she ain't afraid to stay, country
Nothing but country

Brother she's country, from her cowboy boots to her down home roots
Nothing but country
Yea yeah
She's country (shoot) from her cowboy boots to her down home roots
She's country, from the songs she plays to the prayers she prays,
That's the way she was born and raised she ain't afraid to stay, country
Yea she's nothing but country

She's all about the country
From the backwoods she's a homegrown, down to the bone, she's country .")
  #song 175 (one-seventy-five)
  Song.create!(artist:  "Brad Paisley",
               title: "Then",
               rank: 175,
               copyright: "Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Ashley Gorley, Ashley Glenn Gorley, Brad Douglas Paisley, Chris Dubois",
               year: 2009,
               lyrics: "I remember, trying not to stare, the night that I first met you
You had me mesmerized
And three weeks later, in the front porch light
Taking forty-five minutes to kiss goodnight
I hadn't told you yet
But I thought I loved you then

And now you're my whole life
Now you're my whole world
I just can't believe the way I feel about you, girl
Like a river meets the sea,
Stronger than it's ever been.
We've come so far since that day
And I thought I loved you then

And I remember, taking you back to right where I first met you,
You were so surprised
There were people around, but I didn't care
I got down on one knee right there and once again,
I thought I loved you then

But now you're my whole life
Now you're my whole world
I just can't believe, the way I feel about you, girl
Like a river meets the sea,
Stronger than it's ever been.
We've come so far since that day
And I thought I loved you then

And I can just see you, with a baby on the way
And I can just see you, when your hair is turning gray
What I can't see is how I'm ever gonna love you more
But I've said that before

And now you're my whole life
Now you're my whole world
I just can't believe the way I feel about you, girl
We'll look back someday, at this moment that we're in
And I'll look at you and say
And I thought I loved you then
And I thought I loved you then .")
  #song 176 (one-seventy-six)
  Song.create!(artist:  "Brooks & Dunn",
               title: "Cowgirls Don't Cry",
               rank: 176,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Terry A Mcbride, Ronnie G Dunn",
               year: 2006,
               lyrics: "Her daddy gave her, her first pony
Then taught her to ride
She climbed high in that saddle
Fell I don't know how many times
He taught her a lesson that she learned
Maybe a little too well

Cowgirls don't cry, ride, baby, ride
Lessons in life are gonna show you in time
Soon enough you gonna know why
It's gonna hurt every now and then
If you fall get back on again
Cowgirls don't cry

She grew up, she got married
But never was quite right
She wanted a house, a home and babies
He started comin' home late at night
She didn't let him see it break her heart
She didn't let him see her fall apart

'Cause cowgirls don't cry, ride, baby, ride
Lessons of life are gonna show you in time
Soon enough you gonna know why
It's gonna hurt every now and then
If you fall get back on again
Cowgirls don't cry

Phone rang early one mornin'
Her momma's voice, she'd been cryin'
Said it's your daddy, you need to come home
This is it, I think he's dyin'
She laid the phone down by his head
Last words that he said

Cowgirls don't cry, ride, baby, ride
Lessons of life show us all in time
Too soon God will let you know why
If you fall get right back on
The good Lord calls everybody home
Cowgirl don't cry .")
  #song 177 (one-seventy-seven)
  Song.create!(artist:  "Rodney Atkins",
               title: "It's America",
               rank: 177,
               copyright: "Warner/Chappell Music, Inc., BMG RIGHTS MANAGEMENT US, LLC",
               writers: "JAMES, BRETT/PETRAGLIA, ANGELO THOMAS",
               year: 2009,
               lyrics: "
Drivin' down the street today I saw a sign for lemonade
They were the cutest kids I'd ever seen in this front yard
As they handed me my glass, smilin' thinkin' to myself
Man, what a picture-perfect postcard this would make of America

It's a high school prom, it's a Springsteen song, it's a ride in a Chevrolet
It's a man on the moon and fireflies in June and kids sellin' lemonade
It's cities and farms, it's open arms, one nation under God
It's America

Later on when I got home, I flipped the TV on
I saw a little town that some big twister tore apart
And people came from miles around just to help their neighbors out
And I was thinkin' to myself I'm so glad that I live in America

It's a high school prom, it's a Springsteen song, it's a ride in a Chevrolet
It's a man on the moon and fireflies in June and kids sellin' lemonade
It's cities and farms, it's open arms, one nation under God
It's America!

Now we might not always get it all right
There's no place else I'd rather build my life

'Cause it's a kid with a chance, it's a rock 'n roll band
It's a farmer cuttin' hay
It's a big flag flyin' in a summer wind
Over a fallen hero's grave

It's a high school prom, it's a Springsteen song
It's a welcome home parade, yeah
It's a man on the moon and fireflies in June and kids sellin' lemonade
It's cities and farms, it's open arms, one nation under God
It's America! It's America! Oh, oh yeah, woo! .")
  #song 178 (one-seventy-eight)
  Song.create!(artist:  "Toby Keith",
               title: "God Love Her",
               rank: 178,
               copyright: "Franklin Road Music, Roba Music, Sony/ATV Music Publishing LLC, Reservoir Media Management Inc",
               writers: "Toby Keith, Vicki McGeehee",
               year: 2008,
               lyrics: "Just a girl born in Dixie
Washed in the blood
And raised on the banks
Of the Mississippi mud
She always had a thing
About fallin' in love with a bad boy

Yeah, they could see it all comin'
But her daddy never dreamed
She'd grow up that fast
You know what I mean
The way a girl gets
When she turns 17
Kinda crazy

She's a rebel child
And a preacher's daughter
She was baptized in dirty water
Her mama cried the first time
They caught her with me
They knew they couldn't stop her

She holds tight to me and the Bible
On the back seat of my motorcycle
Left her daddy standin' there
Preachin' to the choir
You see, God love her
Oh me and God love her

She kissed her mama goodbye
Said I'll be sure 'n phone ya
She called from a truck stop
In Tucson Arizona
With amazing grace
We made California line
And then my gypsy life
Started takin' it's toll
And the fast lane got empty
And out of control
And just like an angel
She saved my soul from the devil

Yeah she's a rebel child
And a preacher's daughter
She was baptized in dirty water
Her mama cried the first time
They caught her with me
They knew they couldn't stop her

She holds tight to me and the Bible
On the back seat of my motorcycle
Left her daddy standin' there
Preachin' to the choir
You see, God love her
Oh me and God love her

She holds tight to me and the Bible
On the back seat of my motorcycle
Left her daddy standin' there
Preachin' to the choir
You see, God love her
Oh me and God love her

God love her
Me and God love her .")
  #song 179 (one-seventy-nine)
  Song.create!(artist:  "Rascal Flatts",
               title: "Summer Nights",
               rank: 179,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Mike Busbee, Brett James, Gary Levox",
               year: 2009,
               lyrics: "It's summer nights!
Yeah,

Come on ladies
It's time to pop that top
And fellas, I know you're ready to rock
We went crazy cooped all winter long
And school is out, so let's get it on
Flip flop tans and some white sand
I know the perfect spot

Well the sunset better set soon
So we can get in the mood
Things start getting all heated up
When it starts getting cool, yeah

Summer nights
Everybody, are you with me?
Let that igloo cooler mark your piece of paradise
Summer nights
Everybody's feeling sexy
Holler if you're ready for some summer nights
Come on.

Oh, oh yeah.

Now fellas, you better watch your step
Don't let them teeny french bikinis
Make you loose your breath
Back to the ladies
Ya'll keep doing ya'lls thing
Cause everything about you makes me wanna scream

The sun is getting low
There it goes, here we go
Here comes the moon, yeah
Things start getting all heated up
When it starts getting cool, yeah

Summer nights
Everybody, are you with me?
Let that igloo cooler mark your piece of paradise
Summer nights
Everybody's feeling sexy
Holler if you're ready for some summer nights
Come on.

Whoah,
Oh,

It's a party down in,
Padre Big bonfire on the beach
It's Coronas in Daytona y'all
Where it's wild and it's free

Summer nights
Everybody, are you with me?
Let that igloo cooler mark your piece of paradise
Summer nights
Everybody's feeling sexy
Holler if you're ready for some summer nights
Come on.

Summer nights
Everybody, are you with me?
Let that igloo cooler mark your piece of paradise
Summer nights
Everybody's feeling sexy
Holler if you're ready for some summer nights

Yeah, oh are you ready?
Are you ready?
Are you ready?
For some summer nights

Yeah baby
Summer nights
It's summer nights
Come on .")
  #song 180 (one-eighty)
  Song.create!(artist:  "George Strait",
               title: "Living For The Night",
               rank: 180,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Hori Pro Entertainment Group",
               writers: "Dean Dillon, George Strait, Bubba Strait",
               year: 2009,
               lyrics: "Everyday is a lifetime without you
Hard to get through, since you've been gone
So I do the only thing I know how to, to get by
I'm living for the night

I've drawn all the curtains in this old house
To keep the sun out and off my face
Friends stop by to check-in 'cause I've checked out
I tell them I'm fine, I'm living for the night

I can't hide the tears I cry, the pain that came with your goodbyes
The memories that keep me out of sight
Every night I venture out, into those neon arms that hold me tight
I'm living for the night

I'm a whole lot easier to talk to,
When I've had a few, I settle down
Whiskey kills the man you've turned me into
And I come alive
I'm living for the night

Daylight can't hide the tears I cried, the pain that came with your goodbyes
The memories that keep me out of sight
Every night I venture out, into those neon arms that hold me tight
I'm living for the night
I'm living for the night

Everyday is a lifetime without you .")
  #song 181 (one-eighty-one)
  Song.create!(artist:  "Toby Keith",
               title: "American Ride",
               rank: 181,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Dave Pahanish, Joe West",
               year: 2009,
               lyrics: "Winter gettin' colder, summer gettin' warmer
Tidal wave comin' cross the Mexican border
Why buy a gallon, it's cheaper by the barrel
Just don't get busted singin' Christmas carols

That's us, that's right
Gotta love this American ride
Both ends of the ozone burnin'
Funny how the world keeps turning
Look ma, no hands
I love, this American ride
Gotta love this American ride

Momma gets her rocks off watchin' Desperate Housewives
Daddy works his ass off payin' for the good life
Kids on the YouTube learnin' how to be cool
Livin' in a cruel world, it pays to be a mean girl

That's us, that's right
Gotta love this American ride
Both ends of the ozone burnin'
Funny how the world keeps turning
Look ma, no hands
I love, this American ride
Gotta love this American ride

Poor little infa-Miss America's down
She gained five pounds and lost her crown
Quick fix plastic surgical antidote
Got herself a record deal, can't even sing a note

Plasma gettin' bigger, Jesus gettin' smaller
Spill a cup of coffee make a million dollars
Customs got a thug with an aerosol can
If the shoe don't fit, the fits gonna hit the shan

That's us
That's right
Gotta love this American ride
Both ends of the ozone burnin'
Funny how the world keeps turning
Hot dog, hot damn
I love, this American ride
Gotta love this American ride, oh yeah .")
  #song 182 (one-eighty-two)
  Song.create!(artist:  "Brad Paisley",
               title: "Welcome To The Future",
               rank: 182,
               copyright: "Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Brad Paisley, Charles Christopher Dubois",
               year: 2008,
               lyrics: "When I was ten years old
I remember thinking how cool it would be
When we were going on an eight-hour drive
If I could just watch TV
And I'd have given anything
To have my own Pac-Man game at home
I used to have to get a ride down to the arcade
Now I've got it on my phone

Hey, glory, glory, hallelujah
Welcome to the future

My grandpa was in World War 2
He fought against the Japanese
He wrote a hundred letters to my grandma
Mailed them from his base in the Philippines
I wish they could see this now
The world they saved has changed you know
'Cause I was on a video chat this morning
With a company in Tokyo

Hey, everyday's a revolution
Welcome to the future

Hey, look around it's all so clear
Hey, wherever we were going, well we're here
Hey, so many things I never thought I'd see
Happening right in front of me

I had a friend in school
Running back on the football team
They burned a cross in his front yard
For asking out the homecoming queen
I thought about him today
And everybody who'd seen what he'd seen
From a woman on a bus
To a man with a dream

Hey, wake up Martin Luther
Welcome to the future
Hey, glory, glory, hallelujah
Welcome to the future .")
  #song 183 (one-eighty-three)
  Song.create!(artist:  "Jake Owen",
               title: "Don't Think I Can't Love You",
               rank: 183,
               copyright: "Kobalt Music Publishing Ltd., Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Joshua Ryan Owen, Kendell Wayne Marvel, Jimmy Ritchey",
               year: 2009,
               lyrics: "Well, I learned the hard way real early in life
That money sure don't grow on a tree
And there's a few things that a dollar can't buy
The best things in life, they come free

So girl, I can't buy you a big diamond ring
No house on a hill full of life's finer things
And I'll tell you right now there's a whole lot that I just can't do
Oh, but baby, don't think I can't love you

And when I get home after workin' all day
I'm beat down, girl dead on my feet
But as tired as I am, if you wanna play
Well, playin' sounds real good to me

So girl, I can't buy you a big diamond ring
No house on a hill full of life's finer things
And I'll tell you right now there's a whole lot that I just can't do
Oh, but baby, don't think I can't love you

So girl, I can't buy you a big diamond ring
No house on a hill full of life's finer things
And I'll tell you right now there's a whole lot that I just can't do
Oh, but baby, don't think I can't love you .")
  #song 184 (one-eighty-four)
  Song.create!(artist:  "Taylor Swift",
               title: "White Horse",
               rank: 184,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Ole MM, Ole Media Management Lp",
               writers: "Mary Ann Kennedy, Pam Rose, Kye Fleming",
               year: 2008,
               lyrics: "Say you're sorry
That face of an angel
Comes out just when you need it to
As I paced back and forth all this time
'Cause I honestly believed in you
Holding on
The days drag on
Stupid girl,
I should have known, I should have known

That I'm not a princess, this ain't a fairy tale
I'm not the one you'll sweep off her feet,
Lead her up the stairwell
This ain't Hollywood, this is a small town,
I was a dreamer before you went and let me down
Now it's too late for you
And your white horse, to come around

Maybe I was naive,
Got lost in your eyes
And never really had a chance
My mistake I didn't know to be in love
You had to fight to have the upper hand
I had so many dreams
About you and me
Happy endings
Now I know

I'm not a princess, this ain't a fairy tale
I'm not the one you'll sweep off her feet,
Lead her up the stairwell
This ain't Hollywood, this is a small town,
I was a dreamer before you went and let me down
Now it's too late for you
And your white horse, to come around

And there you are on your knees,
Begging for forgiveness, begging for me
Just like I always wanted but I'm so sorry

'Cause I'm not your princess, this ain't a fairytale
I'm gonna find someone someday who might actually treat me well
This is a big world, that was a small town
There in my rear view mirror disappearing now
And its too late for you and your white horse
Now its too late for you and your white horse, to catch me now

Oh, whoa, whoa, whoa
Try and catch me now
Oh, it's too late
To catch me now .")
  #song 185 (one-eighty-five)
  Song.create!(artist:  "Carrie Underwood",
               title: "I Told You So (f. Randy Travis)",
               rank: 185,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd.",
               writers: " ",
               year: 2007,
               lyrics: "Suppose I called you up tonight and told you that I loved you
And suppose I said 'I wanna come back home'.
And suppose I cried and said 'I think I finally learned my lesson'
And I'm tired a-spendin' all my time alone.

If I told you that I realized you're all I ever wanted
And it's killin' me to be so far away.
Would you tell me that you loved me too and would we cry together?
Or would you simply laugh at me and say

'I told you so, oh I told you so
I told you some day you'd come crawling back and asking me to take you in
I told you so, but you had to go
Now I found somebody new and you will never break my heart in two again'.

If I got down on my knees and told you I was yours forever
Would you get down on yours too and take my hand?

Would we get that old time feelin', would we laugh and talk for hours
The way we did when our love first began?

Would you tell me that you'd missed me too and that you've been so lonely
And you waited for the day that I return.
And we'd live in love forever and that I'm your one and only
Or would you say the tables finally turned?

Would you say

'I told you so, oh I told you so
I told you some day you come crawling back and asking me to take you in
I told you so, but you had to go
Now I found somebody new and you will never break my heart in two again'.

'Now I found somebody new and you will never break my heart in two again' .")
  #song 186 (one-eighty-six)
  Song.create!(artist:  "Zac Brown Band",
               title: "Toes",
               rank: 186,
               copyright: "Peermusic Publishing, Warner/Chappell Music, Inc, Reach Music Publishing, BMG Rights Management US, LLC",
               writers: "Zachry Alexander Brown, Wyatt Durrette, John Hopkins, Shawn Eric Mullins",
               year: 2008,
               lyrics: "I got my toes in the water, ass in the sand
Not a worry in the world, a cold beer in my hand
Life is good today Life is good today

Well, the plane touched down just about three o'clock
And the city's still on my mind
Bikinis and palm trees danced in my head
I was still in the baggage line
Concrete and cars are there own prison bars like this life I'm living in
But the plane brought me farther
I'm surrounded by water
And I'm not going back again

I got my toes in the water, ass in the sand
Not a worry in the world, a cold beer in my hand
Life is good today Life is good today

Adiós and vaya con Dios
Yeah I'm leaving GA
And if it weren't for tequila and pretty senoritas
I'd have no reason to stay
Adiós and vaya con Dios
Yeah I'm leaving GA
Gonna lay in the hot sun and roll a big fat one
And grab my guitar and play

Four days flew by like a drunk Friday night
As the summer drew to an end
They can't believe that I just couldn't leave
And I bid adieu to my friends
Because my bartender she's from the islands
Her body's been kissed by the sun
And coconut replaces the smell of the bar
And I don't know if its her or the rum

I got my toes in the water, ass in the sand
Not a worry in the world, a cold beer in my hand
Life is good today Life is good today

Adiós and vaya con Dios
A long way from GA
Yes and all the muchachas they call me big poppa
When I throw pesos their way
Adiós and vaya con Dios
A long way from GA
Someone do me a favor and pour me some Jaeger
And I'll grab my guitar and play

Adiós and vaya con Dios
Going home now to stay
The senoritas don't care-o when there's no dinero
You got no money to stay
Adiós and vaya con Dios
Going home now to stay

And put my ass in a lawn chair
Toes in the clay
Not a worry in the world a PBR on the way
Life is good today
Life is good today .")
  #song 187 (one-eighty-seven)
  Song.create!(artist:  "Kenny Chesney",
               title: "Down The Road",
               rank: 187,
               copyright: "Beginner Music, Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Mac Mcanally",
               year: 2008,
               lyrics: "When I was a boy

Four houses down from me
Was a family with an only child
She was the only girl
In this whole world that can make me smile

Down the road
I made up reasons to go
Down the road

Somewhere inside of me
There was something she took a liking to
I asked her to marry me
She said she really wanted to

Down the road
To see what life's gonna hold
Down the road

Her momma wants to know
If I'm washed in the blood or just in the water
Her daddy wants to know
If I make enough to take his daughter

Down the road
Before he could let her go
Down the road

Now down the street from here
There's an engineer with an only son
And our baby girl says
She believes that he is the only one

Down the road

Her momma wants to know
Is he washed in the blood or just in the water
And I want to know
That he makes enough to take my daughter

Down the road
When it comes time to go
Down the road

Down the road
You know I want to help her go
Down the road

Down, down, down that road
Down, down, down that road .")
  #song 188 (one-eighty-eight)
  Song.create!(artist:  "Dierks Bentley",
               title: "Feel That Fire",
               rank: 188,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, Reservoir Media Management Inc",
               writers: "Brad Douglas Warren, Brett Daniel Warren, Brett Beavers, Dierks Bentley",
               year: 2009,
               lyrics: "She wants her nails painted black
She wants the toy in the crackerjack
She wants to ride the bull at the rodeo

She wants to wear my shirt to bed
She wants to make every stray a pet
'N drive around in my truck with no place to go

But she needs to feel that fire
The one that lets her know for sure
She's everything I want and more
A real desire, does she know I'd walk alone out on the wire
To make her feel that fire

She wants a cabin in the woods
She wants to stand where nobody stood
And someday she wants a couple kids of her own
She wants to make love on a train
And someday's she only wants a break
Hey but she wants what she wants, but man I know I know I know

She needs to feel that fire
The one that lets her know for sure
She's everything I want and more
A real desire, does she know I'd walk alone out on the wire
To make her feel that fire
Yeah, feel that fire

So as long as there's a breath to take
A smile to share, a prayer to pray
A chance to hold her hand to fan the flame

She's gonna feel that fire
The one that lets her know for sure
She's everything I want and more
A real desire, does she know I'd walk alone out on the wire
To make her feel that fire
Oh feel that fire
She wants her nails painted black
She wants the toy in the crackerjack
She wants to ride the bull at the rodeo .")
  #song 189 (one-eighty-nine)
  Song.create!(artist:  "Kenny Chesney",
               title: "Out Last Night",
               rank: 189,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Brett James, Kenneth Chesney",
               year: 2009,
               lyrics: "We went out last night
Like we swore we wouldn't do
Drank to much beer last night
A lot more than we wanted to
There were girls from Argentina and Arkansas
Maine, Alabama, and Panama
All mixed together and having a ball

Yeah we went out last night
One thing started leadin' to another
Out last night
Hittin' on everybody and their mother
There were two karaoke girls drunk on a dare
Singing 'I Got You Babe' by Sonny and Cher
Yeah life was good everywhere
We went out last night

Well you know I'm a music man
I grew up in East Tennessee
But last night I was everything
When I got a few drinks in me
I was a doctor, a lawyer, a senator's son
Brad Pitt's brother and a man on the run
Anything I thought would get the job done

Yeah we went out last night
One thing started leadin' to another
Out last night
Hittin' on everybody and their mother
There were people doin' body shots up on the bar
Jimmy in a fist fight out by the car
Everybody was some kind of star
When we went out last night

Well the fact that I'm still breathing
Means that I must have survived
And that I'll live to go out with
My friends again tonight

Oh we went out last night
One thing started leadin' to another
Out last night
Everybody started lovin' on each other
They were dancin' on the tables, howlin' at the moon
Pairing off together and pretty soon
There was not a soul in sight
When we went out last night .")
  #song 190 (one-ninety)
  Song.create!(artist:  "James Otto",
               title: "Just Got Started Lovin' You",
               rank: 190,
               copyright: "Warner/Chappell Music, Inc, Carol Vincent & Assoc LLC",
               writers: "David Vincent Williams, James Otto, James Allen Otto, Jim Femino",
               year: 2008,
               lyrics: "You don't have to go now honey
Call and tell 'em you won't be in today
Baby there ain't nothin' at the office
So important it can't wait
I'm thankful for the weekend
But two days in heaven just ain't gonna do
This is gonna take forever darlin'
Girl I just got started lovin' you

What's the point in fightin' what we're feelin'
We both know we'll never win
Ain't this what we're missin'
Let's just stop all this resistin' and give in
Let me wrap my arms around you
You know you don't want to leave this room
Come back and let me hold you darlin'
Girl I just got started lovin' you

What can I say I've never felt this way
Girl you're like a dream come true
After all the love we've made
It sure would be a shame
If we let this moment end so soon

So won't you lay back down beside me
Girl just like I know you're wantin' to
Trust me when I tell you darlin'
Girl I just got started lovin' you

What can I say I've never felt this way
Girl you're like a dream come true
After all the love we've made
It sure would be a shame
If we let this moment end so soon

I'm thankful for the weekend
But two days in heaven just ain't gonna do
This is gonna take forever darlin'
Girl I just got started lovin' you
Come back and let me hold you darlin'
Girl I just got started lovin' you .")
  #song 191 (one-ninety-one) 2005
  Song.create!(artist:  "George Strait",
               title: "I Saw God Today",
               rank: 191,
               copyright: "BLIND MULE MUSIC, BIG RED TOE MUSIC, EXTREMELY LOUD MUSIC, STEEL WHEELS MUSIC",
               writers: "Rodney Clawson, Monty Criswell, Wade Allen Kirby",
               year: 2008,
               lyrics: "
Just walked down the street to the coffee shop
Had to take a break
I'd been by her side for eighteen hours straight

Saw a flower growin' in the middle of the sidewalk
Pushin' up through the concrete
Like it was planted right there for me to see

The flashin' lights, the honkin' horns
All seemed to fade away

In the shadow of that hospital at 5:08
I saw God today

I've been to church, I've read the Book
I know He's here, but I don't look
Near as often as I should
Yea, I know I should

His fingerprints are everywhere
I just slowed down to stop and stare
Opened my eyes, and man, I swear
I saw God today

Saw a couple walking by, they were holding hands
Man she had that glow
Yea, I couldn't help but notice she was starting to show

Stood there for a minute taking in the sky
Lost in that sunset
A splash of amber melted into shades of red

I've been to church, I've read the Book
I know He's here, but I don't look
Near as often as I should
Yea, I know I should

His fingerprints are everywhere
I just slowed down to stop and stare
Opened my eyes, and man, I swear
I saw God today

Got my face pressed up against the nursery glass
She's sleeping like a rock
My name on her wrist, wearing tiny pink socks

She's got my nose, she's got her mama's eyes
My brand new baby girl
She's a miracle
I saw God today .")
  #song 192 (one-ninety-two)
  Song.create!(artist:  "Alan Jackson",
               title: "Small Town Southern Man",
               rank: 192,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Alan Jackson",
               year: 2008,
               lyrics: "Born the middle son
Of a farmer
And a small town
Southern man
Like his daddy's
Daddy before him
Brought up
Workin' on the land
Fell in love
With a small town woman
And they married up
And settled down
Natural way of life
If you're lucky
For a small town
Southern man

First there came
Four pretty daughters
For this small town
Southern man
Then a few years
Later came another
A boy, he wasn't planned
Seven people
Living all together
In a house built
With his own hands
Little words with love
And understanding
From a small town
Southern man

And he bowed
His head to Jesus
And he stood
For Uncle Sam
And he only loved
One woman
He was always proud
Of what he had
He said
His greatest contribution
Is the ones
You leave behind
Raised on the ways
And gentle kindness
Of a small town
Southern man
Raised on the ways
And gentle kindness
Of a small town
Southern man

Callous hands
Told the story
For this small town
Southern man
He gave it all
To keep it all together
And keep his family
On his land
Like his daddy
Years wore out his body
Made it hard
Just to walk
And stand

You can break the back
But you can't break
The spirit
Of a small town
Southern man

And he bowed
His head to Jesus
And he stood
For Uncle Sam
And he only loved
One woman
He was always proud
Of what he had
He said
His greatest contribution
Is the ones
You leave behind
Raised on the ways
And gentle kindness
Of a small town
Southern man
Raised on the ways
And gentle kindness
Of a small town
Southern man

Finally death
Came callin'
For this small town
Southern man
He said it's alright
'Cause I see angels
And they got me
By the hand
Don't you cry
And don't you worry
I'm blessed
And I know I am
'Cause God
Has a place in heaven
For a small town
Southern man

And he bowed
His head to Jesus
And he stood
For Uncle Sam
And he only loved
One woman
He was always proud
Of what he had
He said
His greatest contribution
Is the ones
You leave behind
Raised on the ways
And gentle kindness
Of a small town
Southern man
Raised on the ways
And gentle kindness
Of a small town
Southern man .")
  #song 193 (one-ninety-three)
  Song.create!(artist:  "Darius Rucker",
               title: "Don't Think I Don't Think About It",
               rank: 193,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Clay Mills, Darius Rucker",
               year: 2008,
               lyrics: "I left out in a cloud of taillights and dust
Swore I wasn't coming back, said I'd had enough
Saw you in the rear view standing, fading from my life
But I wasn't turnin' 'round
No not this time

But don't think I don't think about it
Don't think I don't have regrets
Don't think it don't get to me
Between the work and the hurt and the whiskey
Don't think I don't wonder 'bout
Could've been, should've been all worked out
I know what I felt, and I know what I said
But don't think I don't think about it

When we make choices, we gotta live with them
Heard you found a real good man and you married him
I wonder if sometimes I cross your mind
Where would we be today
If I never drove that car away?

But don't think I don't think about it
Don't think I don't have regrets
Don't think it don't get to me
Between the work and the hurt and the whiskey
Don't think I don't wonder 'bout
Could've been, should've been all worked out
Yeah I know what I felt, and I know what I said
But don't think I don't think about it

But don't think I don't think about it
But don't think I don't think about it
Don't think I don't have regrets
Don't think it don't get to me
Between the work and the hurt and the whiskey
Don't think I don't wonder 'bout
Could've been, should've been all worked out
I know what I felt, and I know what I said
But don't think I don't think about it, no no

Don't think I don't
Don't think I don't .")
  #song 194 (one-ninety-four)
  Song.create!(artist:  "Blake Shelton",
               title: "Home",
               rank: 194,
               copyright: "Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Universal Music Publishing Group",
               writers: "Amy Foster- Gillies, Michael Buble, Alan Chang",
               year: 2007,
               lyrics: "Another summer day
Has come and gone away
In Paris and Rome
But I wanna go home

Maybe surrounded by
A million people I
Still feel all alone
I wanna go home
Oh, I miss you, you know

And I've been keeping all the letters that I wrote to you
Each one a line or two
I'm fine baby, how are you?
I would send them but I know that it's just not enough
My words were cold and flat
And you deserve more than that

Another airplane
Another sunny place
I'm lucky I know
But I wanna go home
I've got to go home

Let me go home
I'm just too far from where you are
I wanna come home

And I feel just like I'm living someone else's life
It's like I just stepped outside
When everything was going right
And I know just why you could not
Come along with me
This was not your dream
But you always believed in me

Another winter day has come and gone away
In even Paris and Rome
And I wanna go home
Let me go home

And I'm surrounded by
A million people I
Still feel alone
And I wanna go home
Oh, I miss you, you know

Let me go home
I've had my run
Baby, I'm done
I'm coming back home
Let me go home
It will all be all right
I'll be home tonight
I'm coming back home .")
  #song 195 (one-ninety-five)
  Song.create!(artist:  "Phil Vassar",
               title: "Love Is A Beautiful Thing",
               rank: 195,
               copyright: "Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Craig Michael Wiseman, Jeffrey Steele",
               year: 2008,
               lyrics: "Looks like everybody's here
Had to put some foldin' chairs in the vestibule
Yeah, it's gettin' full
Even old Aunt Ruby came
Her first time on an airplane
It's her sister's girl
She wouldn't miss it for the world

And all them kids jumpin' in the pews
And Mr. Charlie in his lime-green suit
'Is a handsome man'
Remarks Widow Callahan
Uncle Joe and Uncle Jake
Haven't spoken since '98
Just said, 'Hello'
It's a good day to let it go

So let the angels gather
Let the music play
Let the preacher get to preachin' all the 'do you take's'
Love is a beautiful thing
Throw the rice in the air
Let the church bells ring
Tie the cans to the back of that limousine
Love is a beautiful thing

Daddy's waitin' with the bride
Yeah, she helps him with his tie
She sees a tear
He says, 'Man, it's hot in here' hmm
He hugs his little girl and asks,
'How did you grow up so fast?
God, I wish your Momma could be here for this'

And everybody stands and smiles
As she goes walkin' down the aisle
In her Momma's gown
And Daddy breaks on down
Gran and Gramps in the second row
Stood right there fifty years ago
And said their vows
Yeah, I guess it's workin' out

So let the angels gather
Let the music play
Let the preacher get to preachin' all the 'do you take's'
Love is a beautiful thing
Throw the rice in the air
Let the church bells ring
Tie the cans to the back of that limousine
Love is a beautiful thing

A little table full'a gifts
Catering's covered dish
And the wedding band
Well, that's on her hand
And Tommy's teasin' Lorelei
He pushed her down and made her cry
And neither one knows

When the angels gather
And the music plays
And the preacher gets to preachin' all the 'do you take's'
Love is a beautiful thing
Throw the rice in the air
Let the church bells ring
Tie the cans to the back of that limousine
Love
Two people in love is a beautiful thing .")
  #song 196 (one-ninety-six)
  Song.create!(artist:  "Trace Adkins",
               title: "You're Gonna Miss This",
               rank: 196,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., BMG Rights Management US, LLC",
               writers: "Ashley Gorley, Lee Miller",
               year: 2007,
               lyrics: "She was staring out the window of that SUV
Complaining, saying 'I can't wait to turn eighteen'
She said 'I'll make my own money, and I'll make my own rules'
Momma put the car in park out there in front of the school
She kissed her head and said 'I was just like you'

You're gonna miss this
You're gonna want this back
You're gonna wish these days hadn't gone by so fast
These are some good times
So take a good look around
You may not know it now
But you're gonna miss this

Before she knows it she's a brand new bride
In her one-bedroom apartment, and her daddy stops by
He tells her 'It's a nice place'
She says 'It'll do for now'
Starts talking about babies and buying a house
Daddy shakes his head and says 'Baby, just slow down'

You're gonna miss this
You're gonna want this back
You're gonna wish these days hadn't gone by so fast
These are some good times
So take a good look around
You may not know it now
But you're gonna miss this

Five years later there's a plumber workin' on the water heater
Dog's barkin', phone's ringin'
One kid's cryin', one kid's screamin'
She keeps apologizin'
He says 'They don't bother me
I've got two babies of my own
One's thirty six, one's twenty three
Huh, it's hard to believe, but

You're gonna miss this
You're gonna want this back
You're gonna wish these days hadn't gone by so fast
These are some good times
So take a good look around
You may not know it now
But you're gonna miss this

You're gonna' miss this
Yeah
You're gonna' miss this .")
  #song 197 (one-ninety-seven)
  Song.create!(artist:  "Keith Anderson",
               title: "I Still Miss You",
               rank: 197,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Keith Anderson, Tim Nichols, Jason Sellers",
               year: 2008,
               lyrics: "I've changed the presets in my truck
So those old songs don't sneak up
They still find me and remind me
Yeah you come back that easy
Try restaurants I've never been to
Order new things off the menu
That I never tried cause you didn't like
Two drinks in you were by my side

I've talked to friends
I've talked to myself
I've talked to God
I prayed liked hell but I still miss you
I tried sober I tried drinking
I've been strong and I've been weak
And I still miss you
I've done everything move on like I'm supposed to
I'd give anything for one more minute with you
I still miss you
I still miss you baby

I never knew til you were gone
How many pages you were on
It never ends I keep turning
And line after line and you are there again
I dont know how to let you go
You are so deep down in my soul
I feel helpless so hopeless
Its a door that never closes
No I don't know how to do this

I've talked to friends
I've talked to myself
I've talked to God
I prayed liked hell but I still miss you
I tried sober I tried drinking
I've been strong and I've been weak
And I still miss you
I've done everything
Move on like I'm supposed to
I'd give anything for one more minute with you
I still miss you yeah

I've talked to friends
I've talked to myself
I've talked to God
I prayed liked hell but I still miss you
I tried sober I tried drinking
I've been strong and I've been weak
And I still miss you
I've done everything
Move on like I'm supposed to
I'd give anything for one more minute with you
I still miss you yeah

I still miss you
I still miss you, yeah, yeah .")
  #song 198 (one-ninety-eight)
  Song.create!(artist:  "Rodney Atkins",
               title: "Cleaning this Gun (Come on in Boy)",
               rank: 198,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Marla Cannon-Goodman, Casey Michael Beathard",
               year: 2006,
               lyrics: "The Declaration of Independence
Think I could tell you that first sentence
But then I'm lost

I can't begin to count the theories
I've had pounded in my head
That I forgot

I don't remember all that Spanish
Or the Gettysburg address
But there is one speech from high school
I'll never forget

Come on in, boy, sit on down
And tell me about yourself
So you like my daughter, do you now?
Yeah, we think she's something else
She's her daddy's girl
Her mama's world
She deserves respect
That's what she'll get
Ain't it, son?
Hey, y'all, run along and have some fun
I'll see you when you get back
Bet I'll be up all night
Still cleanin' this gun

Well, now that I'm a father
I'm scared to death one day my daughter
Is gonna find
That teenage boy I used to be
That seems to have just one thing on his mind

She's growin' up so fast
It won't be long before
I'll have to put the fear of God into
Some kid at the door

Come on in, boy, sit on down
And tell me about yourself
So you like my daughter, do you now?
Yeah, we think she's something else
She's her daddy's girl
Her mama's world
She deserves respect
That's what she'll get
Ain't it, son?
Hey, y'all, run along and have some fun
I'll see you when you get back
Bet I'll be up all night
Still cleanin' this gun

Now it's all for show
Ain't nobody gonna get hurt
It's just a daddy thing
And, hey, believe me, it works

Come on in, boy, sit on down
And tell me about yourself
So you like my daughter, do you now?
Yeah, we think she's something else
She's her daddy's girl
Her mama's world
She deserves respect
That's what she'll get
Ain't it, son?
Hey, y'all, run along and have some fun
I'll see you when you get back
Bet I'll be up all night
Still cleanin' this gun

Son, now y'all buckle up and have her back
By te... let's say about 9:30
Drive safe .")
  #song 199 (one-ninety-nine)
  Song.create!(artist:  "Brad Paisley",
               title: "I'm Still A Guy",
               rank: 199,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Spirit Music",
               writers: "Kelley Lovelace, Lee Miller, Brad Paisley",
               year: 2007,
               lyrics: "When you see a deer you see Bambi
And I see antlers up on the wall
When you see a lake you think picnic
And I see a large mouth up under that log
You're probably thinking that you're going to change me
In some ways well maybe you might
Scrub me down, dress me up all but no matter what
Remember I'm still a guy

When you see a priceless French painting
I see drunk, naked girls
You think that riding a wild bull sounds crazy
And I'd like to give it a whirl
Well love makes a man do some things he ain't proud of
And in a weak moment I might
Walk your sissy dog, hold your purse at the mall
But remember, I'm still a guy

I'll pour out my heart
Hold your hand in the car
Write a love song that makes you cry
Then turn right around knock some jerk to the ground
'Cause he copped a feel as you walked by

I can hear you now talking to your friends
Saying 'Yeah girls he's come a long way'
From dragging his knuckles and carrying a club
And building a fire in a cave
But when you say a backrub means only a backrub
Then you swat my hand when I try
Well, what can I say at the end of the day
Honey, I'm still a guy

And I'll pour out my heart
Hold your hand in the car
Write a love song that makes you cry
Then turn right around knock some jerk to the ground
'Cause he copped a feel as you walked by

These days there's dudes getting facials
Manicured, waxed and botoxed
With deep spray-on tans and creamy lotiony hands
You can't grip a tackle-box

With all of these men lining up to get neutered
It's hip now to be feminized
I don't highlight my hair
I've still got a pair
Yeah honey, I'm still a guy

All my eyebrows ain't plucked
There's a gun in my truck
Oh thank God, I'm still a guy .")
  #song 200 (two-hundred)
  Song.create!(artist:  "Toby Keith",
               title: "She Never Cried In Front Of Me",
               rank: 200,
               copyright: "Tokeco Tunes, Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC, DO Write Music LLC",
               writers: "Toby Keith, Bobby Pinson",
               year: 2008,
               lyrics: "It's 7:35
She's someone else's wife
And I can get on with my life
And that thrills me
She married him today
Her daddy gave the bride away
I heard a tear rolled down her face
And that kills me
'Cause now I, can see why
She's finally crying

How was I supposed to know
She was slowly letting go
If I was putting her through hell
Hell, I couldn't tell
She could've given me a sign
And opened up my eyes
How was I supposed to see
She never cried in front of me

Yeah maybe I might've changed
It's hard for me to say
But the story's still the same
And it's a sad one
And I'll always believe
If she ever did cry for me
They were tears that you can?t see
You know the bad ones
And now I, can see why
She's finally crying

How was I supposed to know
She was slowly letting go
If I was putting her through hell
Hell, I couldn't tell
She could've given me a sign
And opened up my eyes
How was I supposed to see
She never cried in front of me

Without a doubt, I know now
How it outta be
'Cause she's gone and it's wrong
And it bothers me
Tomorrow I'll still be asking myself

How was I supposed to know
She was slowly letting go
If I was putting her through hell
Hell, I couldn't tell
She could've given me a sign
And opened up my eyes
How was I supposed to see
She never cried in front of me

How was I supposed to see
She never cried in front of me
Well, I couldn't tell .")

  #song 201 (two-hundred-one)
  Song.create!(artist:  "Alan Jackson",
               title: "Good Time",
               rank: 201,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Jackson, Alan Eugene",
               year: 2008,
               lyrics: "Work, work all week long
Punchin' that clock from dusk 'till dawn
Countin' the days 'till Friday night
That's when all the conditions are right
For a good time
I need a good time

Yeah, I've been workin' all week
And I'm tired and I don't wanna sleep
I wanna have fun
It's time for a good time

I cashed my check, cleaned my truck
Put on my hat, forgot about work
Sun goin' down, head across town
Pick up my baby and turn it around
Good time,
Oh, I need a good time

Yeah, I've been workin' all week
And I'm tired and I don't wanna sleep
I wanna have fun
It's time for a good time

Hey!

Pig in the ground, beer on ice
Just like ol' Hank taught us about
Singin' along, Bocephus songs
Rowdy friends all night long
Good time
Lord, we're having a good time

Yeah, I've been workin' all week
And I'm tired and I don't wanna sleep
I wanna have fun
It's time for a good time

Woah

Heel toe dosey doe
Scootin' our boots, swingin' doors
B n' D Kix and Dunn
Honkin' tonk heaven, double shotgun
Good time,
Lord, we're havin' a good time

Yeah, I've been workin' all week
And I'm tired and I don't wanna sleep
I wanna have fun
It's time for a good time

Shot of Tequila, beer on tap
Sweet southern woman set on my lap
G with an O, O with an D
T with n' I and an M and n' E
And a good time
Shew, good time
I've been workin' all week
And I'm tired and I don't wanna sleep
I wanna have fun
It's time for a good time

Ah, turn it up now

A Shot of Tequila
Beer on tap
A good looking woman
To sit on my lap

A G with an O, an O with a D
A T with an I an M with an E
That spells good time
A good time

Yeah, I've been workin' all week
And I'm tired and I don't wanna sleep
I wanna have fun
It's time for a good time

Twelve o'clock, two o'clock three o'clock four
Five o'clock we know were that's gonna go
Closing the door, shuttin' em down
Head for that Waffle House way across town
Good time
Oh, we're havin' a good time.
Oh, I've been workin' all week
And I'm tired and I don't wanna sleep
I wanna have fun
It's time for a good time

Yeah, I've been workin' all week
And I'm tired and I don't wanna sleep
I wanna have fun
It's time for a good time

Yeah, I've been workin' all week
And I'm tired and I don't wanna sleep
I wanna have fun
It's time for a good time

Oh, yeah, a good time

I need a good time

Yeah, a good time .")
  #song 202 (two-hundred-two)
  Song.create!(artist:  "Jimmy Wayne",
               title: "Do You Believe Me Now",
               rank: 202,
               copyright: "Reservoir Media Management Inc",
               writers: "Dave Pahanish, Joe West, Tim Johnson",
               year: 2008,
               lyrics: "Do you remember the day I turned to you and said
I didn't like the way he was looking at you, yeah
How he made you laugh, you just couldn't get what I was saying
It was my imagination

So do you believe me now, I guess I really wasn't that crazy
And I knew what I was talkin' about, every time the sun goes down
Hes the one thats holdin' you, baby, yeah, me I'm missin' you way across town
So do you believe me now

I'm kickin' myself for being the one foolish enough
Givin' him the chance to step in my shoes
Oh, he was bidin' his time when he saw our love
Was havin' a moment of weakness, he was there between us

So do you believe me now, I guess I really wasn't that crazy
And I knew what I was talkin' about, every time the sun goes down
Hes the one thats holdin' you, baby, yeah, me I'm missin' you way across town
So do you believe me now

Oh, yeah, I bet now you see the light
Oh, oh, yeah, whats the use in being right
Were not the lonely one tonight

Yeah, every time the sun goes down, hes the one thats holding you, baby
Yeah, me, I'm missing you way across town, so do you believe me now .")
  #song 203 (two-hundred-three)
  Song.create!(artist:  "Chris Cagle",
               title: "What Kinda Gone",
               rank: 203,
               copyright: "Spirit Music Group, Hori Pro Entertainment Group",
               writers: "Candy Cameron, Chip Davis, Dave Berg",
               year: 2008,
               lyrics: "I heard the door slam, and I couldn't tell
Was it just the wind, or was she mad again? (aww hell)
She's gettin' in her car. I hollar Baby, is there something wrong?'
Thought I heard her say something sounding like 'I'm gone'.
But these days gone can mean so many things.

There's gone for good, and there's good and gone
And there's gone with the long before it
I wish she'd been just a little more clear.
Well, there's gone for the day and gone for the night
And gone for the rest of your dod' gone life.
Is it a whiskey night, or just a couple beers?
I mean what kinda' gone are we talkin' 'bout here?

Well, it's gettin' dark out, she ain't back yet
Ain't called home, turned off the phone. (aww man)
Ha, this might not be good. I woulda' stopped her when she went to leave
But it didn't call 'cause I didn't really think what I'm thinkin' now.
I'm still not sure what gone is all about.

There's gone for good, and there's good and gone
And there's gone with the long before it
I wish she'd been just a little more clear.
Well, there's gone for the day and gone for the night
And gone for the rest of your dod' gone life.
Is it a whiskey night, or just a couple beers?
I mean what kinda' gone are we talkin' 'bout here?

Is it the kinda' gone where she's at her moms?
Coolin' down, she'll come around
Or the kind that says you had your chance
And she ain't comin' back.

There's gone for good, and there's good and gone
And there's gone with the long before it
I wish she'd been just a little more clear.
Well, there's gone for the day and gone for the night
And gone for the rest of your dod' gone life.
Is it a whiskey night, or just a couple beers?
I mean what kinda' gone are we talkin' 'bout here?

What kinda' gone are we talkin' 'bout,
What kinda' gone are we talkin' 'bout here?
What kinda' gone .")
  #song 204 (two-hundred-four)
  Song.create!(artist:  "Kenny Chesney",
               title: "Everybody Wants To Go To Heaven",
               rank: 204,
               copyright: "SPIRIT MUSIC GROUP",
               writers: "MARTY DODSON, JIM COLLINS",
               year: 2009,
               lyrics: "
Preacher told me last Sunday morning
Son, you better start living right
You need to quit the women and whiskey
And carrying on all night

Don't you wanna hear him call your name
When you're standing at the pearly gates
I told the preacher, 'Yes I do'
But I hope they don't call today
I ain't ready

Everybody wants to go to heaven
Have a mansion high above the clouds
Everybody want to go to heaven
But nobody want to go now

Said preacher maybe you didn't see me
Throw an extra twenty in the plate
There's one for everything I did last night
And one to get me through today
Here's a ten to help you remember
Next time you got the good Lord's ear
Say I'm coming but there ain't no hurry
I'm having fun down here
Don't you know that

Everybody wants to go to heaven
Get their wings and fly around
Everybody want to go to heaven
But nobody want to go now

Someday I want to see those streets of gold in my halo
But I wouldn't mind waiting at least a hundred years or so

Everybody wanna go to heaven
It beats the other place there ain't no doubt
Everybody wanna go to heaven
But nobody wanna go now

Everybody wanna go to heaven
Hallelujah, let me hear you shout
Everybody wanna go to heaven
But nobody wanna go now
I think I speak for the crowd .")

  #song 205 (two-hundred-five)
  Song.create!(artist:  "Carrie Underwood",
               title: "Just A Dream",
               rank: 205,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Gordie Sampson, Hillary Lindsey, Steven Mcewan",
               year: 2007,
               lyrics: "It was two weeks after the day she turned eighteen
All dressed in white, going to the church that night
She had his box of letters in the passenger seat
Sixpence in her shoe
Something borrowed, something blue
And when the church doors opened up wide
She put her veil down trying to hide the tears
Oh she just couldn't believe it
She heard the trumpets from the military band
And the flowers fell out of her hands

Baby, why'd you leave me, why'd you have to go
I was counting on forever, now I'll never know
I can't even breathe
It's like I'm looking from a distance,
Standing in the background
Everybody's saying, he's not coming home now,
This can't be happening to me
This is just a dream

The preacher man said let us bow our heads and pray
Lord please lift his soul and heal this hurt
Then the congregation all stood up
And sang the saddest song that she ever heard
Then they handed her a folded up flag

And she held on to all she had left of him
Oh, and what could've been
And then guns rang one last shot
And it felt like a bullet in her heart

Baby, why'd you leave me, why'd you have to go
I was counting on forever, now I'll never know
I can't even breathe
It's like I'm looking from a distance,
Standing in the background
Everybody's saying, he's not coming home now,
This can't be happening to me
This is just a dream

Baby, why'd you leave me, why'd you have to go
I was counting on forever, now I'll never know
Oh I'll never know
It's like I'm looking from a distance,
Standing in the background
Everybody's saying, he's not coming home now,
This can't be happening to me
This is just a dream

Oh, this is just a dream
Just a dream, (yeah, yeah) .")
  #song 206 (two-hundred-six)
  Song.create!(artist:  "Keith Urban",
               title: "You Look Good In My Shirt",
               rank: 206,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Walt Disney Music Company",
               writers: "Tony Martin, Tom Shapiro, Mark Nesler",
               year: 2002,
               lyrics: "When you walked up behind me and covered my eyes
And whispered in my ear, guess who
I rattled off names like I really didn't know
But all along I knew it was you

And, the longer we talked, the more we laughed
And wondered why we didn't last
It had been a long time, but later last night
Baby, we caught up real fast

And maybe it's a little too early
To know if this is gonna work
All I know is you're sure looking
Good in my shirt
That's right
You look good in my shirt

Well now I'm not saying that we solved overnight
Every way that we went wrong
Oh, but what I'm seeing I'd sure love seeing
Every morning from now on

And maybe it's a little too early
To know if this is gonna work
All I know is you're sure looking
Good in my shirt

See'mon now
Aww that's right
Oh you look so fine

And maybe it's a little too early
To know if this is gonna work
All I know is you're sure looking
Good in my shirt
That's right
You look good in my shirt

And maybe it's a little too early
To know if this is gonna work
All I know is you're sure looking
Good in my shirt
That's right
You look good in my shirt

You look good in my shirt .")
    #song 207 (two-hundred-seven)
  Song.create!(artist:  "Lady Antebellum",
               title: "Love Don't Live Here",
               rank: 207,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Hillary Dawn Scott, Dave Haywood, Charles B. Kelley",
               year: 2008,
               lyrics: "Well, this heart of mine
Has been hardened like a stone
It might take some time
To get back what is gone
But I'm movin' on
And you don't haunt my dreams
Like you did before
When I would curse your name
Hey

Well, I heard the news
That you were back in town
Just passin' through
To claim your lost and found
But I'm over you
And there ain't nothin' that
You could say or do
To take what you did back

You've got nerve to waltz right in
And think what's mine is yours again

I been doin' fine without you
Forgettin' all the love we once knew
And girl, I ain't the one that slammed that door
But now you say you've changed your thinkin'
But I ain't got a heart for breakin'
So go and pick your bags up off my floor
'Cause love don't live here anymore
Oh, no

Well baby, you can try
To tell me how it is
And try to justify
Everything you did
But honey, I'm no fool
And I been down this road
Too many times with you
I think it's best you go

Well, I got one thing left to say
Is you can lay in the bed you've made

I been doin' fine without you
Forgettin' all the love we once knew
And girl, I ain't the one that slammed that door
But now you say you've changed your thinkin'
But I ain't got a heart for breakin'
So go and pick your bags up off my floor
'Cause love don't live here anymore
No, no
Love don't live here anymore
Oh no, oh no

Well you don't live here anymore
Oh you don't live here anymore
Oh no .")
    #song 208 (two-hundred-eight)
  Song.create!(artist:  "Brad Paisley",
               title: "Waitin' On A Woman",
               rank: 208,
               copyright: "EMI Music Publishing, Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Wynn Varble, Don Sampson",
               year: 2005,
               lyrics: "Sittin' on a bench at West Town Mall
He sat down in his overalls and asked me
You waitin' on a woman
I nodded yeah and said how 'bout you
He said son since nineteen fifty-two I've been
Waitin' on a woman

When I picked her up for our first date
I told her I'd be there at eight
And she came down the stairs at eight-thirty
She said I'm sorry that I took so long
Didn't like a thing that I tried on
But let me tell you son she sure looked pretty
Yeah she'll take her time but I don't mind
Waitin' on a woman

He said the wedding took a year to plan
You talk about an anxious man, I was nervous
Waitin' on a woman
And then he nudged my arm like old men do
And said, I'll say this about the honeymoon, it was worth it
Waitin' on a woman

And I don't guess we've been anywhere
She hasn't made us late I swear
Sometimes she does it just 'cause she can do it
Boy it's just a fact of life
It'll be the same with your young wife
Might as well go on and get used to it
She'll take her time 'cause you don't mind
Waitin' on a woman

I've read somewhere statistics show
The man's always the first to go
And that makes sense 'cause I know she won't be ready
So when it finally comes my time
And I get to the other side
I'll find myself a bench, if they've got any
I hope she takes her time, 'cause I don't mind
Waitin' on a woman

Honey, take your time, cause I don't mind
Waitin' on a woman .")
    #song 209 (two-hundred-nine)
  Song.create!(artist:  "Rascal Flatts",
               title: "Every Day",
               rank: 209,
               copyright: "Universal Music Publishing Group",
               writers: "Alissa Hayden Moreno, Jeffrey Steele",
               year: 2007,
               lyrics: "You could've bowed down gracefully
But you didn't
You knew enough to know
To leave well enough alone

But you wouldn't
I drive myself crazy
Tryin' to stay out of my own way
The messes that I make

But my secrets are so safe
The only one who gets me
Yeah, you get me
It's amazing to me

How every day
Every day, every day
You save my life

I come around all broken down and
Crowded out
And you're comfort
Sometimes the place I go
Is so deep and dark and desperate
I don't know, I don't know

How every day
Every day, every day
You save my life

Sometimes I swear, I don't know if
I'm comin' or goin'
But you always say something
Without even knowin'

That I'm hangin' on to your words
With all of my might and it's alright
Yeah, I'm alright for one more night
Every day

Every day, every day, every day
Every day, every day
You save me, you save me, oh, oh, oh
Every day
Every, every, every day

Every day you save my life .")
    #song 210 (two-hundred-ten)
  Song.create!(artist:  "Montgomery Gentry",
               title: "Back When I Knew It All",
               rank: 210,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd., Spirit Music Group, Hori Pro Entertainment Group, Dan Hodges Music, LLC",
               writers: "Gary Hannan, Phil O'donnell, Trent Willmon",
               year: 2008,
               lyrics: "At the ripe old age of nineteen
I bought a short bed pick up chicks machine
Life ran on beer and gasoline
A half a lap ahead of the law

I had a fake I d that got me into 'tuffies'
Love was a word I used to get lucky
Was a big time spender with that plastic money
Back when I knew it all

Back when the world was flat and mama and daddy didn't have a clue
That was back
Back when a pitcher of beer and a couple shots made me bulletproof
Back when god was a name I used in vain to get a point across when I got ticked off
Lord I'm learning so much more
Than back when I knew it all

I found out credit cards don't mean you're rich
And beer and gasoline don't mix
Yeah step side trucks can't jump a ditch
And those 'big house' rooms sure are small
I've learned that love is a woman that will settle you down
A Sunday sermon can turn life around
Man I can't believe all the answers I've found
Since, back when I knew it all

Back when the world was flat and mama and daddy didn't have a clue
That was back
Back when a pitcher of beer and a couple shots made me bulletproof
Back when god was a name I used in vain to get a point across when I got ticked off
Lord I'm learning so much more
Than back when I knew it all

I've done some growing up
And I'm still growing up
So I know I'll never be as smart as I once was
That was back

Back when the world was flat and mama and daddy didn't have a clue
That was back
Back when a pitcher of beer and a couple shots made me bulletproof
Back when god was a name I used in vain to get a point across when I got ticked off
Lord I'm learning so much more
Than back when I knew it all .")
    #song 211 (two-hundred-eleven)
  Song.create!(artist:  "Rascal Flatts",
               title: "Winner At A Losing Game",
               rank: 211,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Reservoir One Music, Reservoir Media Management Inc",
               writers: "Gary Levox, Jay Demarcus, Joe Don Rooney",
               year: 2007,
               lyrics: "Baby, look here at me
Have you ever seen me this way
I've been fumblin' for words
Through the tears and the hurt and the pain
I'm gonna lay it all out
On the line tonight
And I think that it's time
To tell this uphill fight goodbye

Have ever had to love someone
That just don't feel the same
Tryin' to make somebody care for you
The way I do
Is like tryin' to catch the rain
And if love is really forever
I'm a winner at a losin' game

I know that baby, you tried
To find me somewhere inside of you
But you know you can't lie
Girl, you can't hide the truth
Sometimes two hearts
Just can't dance to the same beat
So I'll pack up my things
And I'll take what remains of me

Have ever had to love someone
That just don't feel the same
Tryin' to make somebody care for you
The way I do
Is like tryin' to catch the rain
And if love is really forever
I'm a winner at a losin' game

I know that I'll never be the man that you need or love
Yeah, baby it's killin' me to stand here and see
I'm not what you've been dreamin' of

Have ever had to love someone
That just don't feel the same
Tryin' to make somebody care for you
The way I do
Is like tryin' to catch the rain
And if love is really forever
I'm a winner at a losin' game

Oh, oh, if love is really forever
I'm a winner at a losin' game
Ooo, I'm tired of losing
Oh, oh, oh .")
    #song 212 (two-hundred-twelve)
  Song.create!(artist:  "Kenny Chesney",
               title: "Better As A Memory",
               rank: 212,
               copyright: "Peermusic Publishing",
               writers: "Lady Goodman, Scooter Carusoe",
               year: 2007,
               lyrics: "I move on like a sinner's prayer
Letting go like a levee breaks
Walk away as if I don't care
Learn to shoulder my mistakes
I'm built to fade like your favorite song
I get reckless when there's no need
Laugh as your stories ramble on
Break my heart but it won't bleed
My only friends are pirates, it's just who I am
I'm better as a memory than as your man

I'm never sure when the truth won't do
I'm pretty good on a lonely night
I move on the way a storm blows through
I never stay, but then again, I might
I struggle sometimes to find the words
Always sure until I doubt
Walk a line until it blurs
Build the walls too high to climb out
I'm honest to a fault, it's just who I am
I'm better as a memory than as your man

I see you lean in, you're bound to fall
I don't wanna be that mistake
I'm just a dreamer, nothing more
You should know it before it gets too late

'Cause good-bye's are like a roulette wheel
You never know where they're gonna land
First you're spinning, then you're standing still
Left holding a losing hand
One day you're gonna find someone
Right away, you'll know it's true
That all of your seeking's done
Was just a part of the passing through
Right there in that moment
You'll finally understand
That I was better as a memory than as your man
Better as a memory than as your man .")
    #song 213 (two-hundred-thirteen)
  Song.create!(artist:  "Carrie Underwood",
               title: "All-American Girl",
               rank: 213,
               copyright: "EMI Music Publishing, Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: " Kelley Lovelace, Ashley Gorley, Carrie Underwood",
               year: 2007,
               lyrics: "
Since the day they got married,
He'd been praying for a little baby boy.
Someone he could take fishing,
Throw the football and be his pride and joy.

He could already see him holding that trophy,
Taking his team to State.
But when the nurse came in with a little pink blanket,
All those big dreams changed.

And now he's wrapped around her finger,
She's the center of his whole world.
And his heart belongs to that sweet little beautiful, wonderful, perfect all-American girl.

Sixteen short years later,
She was falling for the senior football star.
Before you knew it he was dropping passes,
Skipping practice just to spend more time with her.

The coach said 'Hey son, what's your problem?
Tell me, have you lost your mind?'
Daddy said 'You'll lose your free ride to college.
Boy you better tell her goodbye'.

But now he's wrapped around her finger,
She's the center of his whole world.
And his heart belongs to that sweet little beautiful, wonderful, perfect all-American

And when they got married and decided to have one of their own,
She said 'Be honest, tell me what you want?'
And he said 'Honey, you wanna know,
Sweet, little, beautiful, one just like you.
Oh, a beautiful, wonderful, perfect all-American.'

Now he's wrapped around her finger,
She's the center of his whole world.
And his heart belongs to that sweet little beautiful, wonderful, perfect all-American girl
All American girl .")
    #song 214 (two-hundred-fourteen)
  Song.create!(artist:  "Brad Paisley",
               title: "Letter To Me",
               rank: 214,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Brad Paisley",
               year: 2007,
               lyrics: "If I could write a letter to me
And send it back in time to myself at seventeen
First I'd prove it's me by saying
Look under your bed, there's a Skoal can and a Playboy
No one else would know you hid
And then I'd say I know it's tough
When you break up after seven months
And yeah I know you really liked her
And it just don't seem fair
But I'll I can say is pain like that is fast and it's rare

And oh, you got so much going for you
Going right
But I know at seventeen
It's hard to see past Friday night
She wasn't right for you
But still you feel like there's
A knife sticking out of your back
And you wondering if you'll survive
But you'll make it through this and you'll see
You're still around to write this letter to me

At the stop sign at Tomlinson and 8th
Always stop completely, don't just tap your brakes
And when you get a date with Bridget
Make sure the tank is full
On second thought forget it, that one turns out kinda cool
Each and every time you have a fight
Just assume you're wrong and Dad is right
And you should really thank Ms. Brinkman
She spent so much extra time
It like she sees the diamond underneath
And she's polishing you till you shine

And oh you got so much going for you
Going right
But I know at seventeen
It's hard to see past Friday night
Tonight's the bonfire rally
But you're staying home instead
Because if you fail Algebra
Mom and Dad will kill you dead
Trust me you'll squeak by and get a C
And you're still around to write this letter to me

You got so much up ahead
You'll make new friends
You should see your kids and wife
And I'd end by saying have no fear
These are nowhere near
The best years of your life

I guess I'll see you in the mirror
When you're a grown man
P.S. go hug Aunt Rita every chance you can

And oh you got so much going for you
Going right
But I know at seventeen
It's hard to see past Friday night
I wish you'd study Spanish
I wish you'd take a typing class
I wish you wouldn't worry, let it be
I'd say have a little faith and you'll see

If I could write a letter to me
To me .")
    #song 215 (two-hundred-fifteen)
  Song.create!(artist:  "Gary Allan",
               title: "Watching Airplanes",
               rank: 215,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "James Beavers, Jonathan Singleton",
               year: 2007,
               lyrics: "Sitting out here on the hood of this truck looking up
At a caramel colored sunset sky
Checking my watch doing the math in my head
Counting back words to when you said goodbye
Well those runway lights are getting brighter

I'm just sitting out here watching airplanes
Take off and fly
Trying to figure out which one you might be on
And why you don't love me anymore
Right now I'm sitting out here watching airplanes

I would've lied could've cried should've tried harder
Done anything to make you stay
I wonder what you'd do if you looked out your window
Saw me running down the runway just like I was crazy
That fence is too high so am I

So I'm just sitting out here watching airplanes
Take off and fly
Trying to figure out which one you might be on
And why you don't love me anymore
By now I know you're thirty thousand feet above me
But a million miles away, a million miles away
By now I know I ought to act like you don't love me

I'm just sitting out here watching airplanes
Take off and fly
Trying to figure out which one you might be on
And why you don't love me anymore
Right now I'm sitting out here watching airplanes

Yeah I'm just sitting out here watching airplanes
Go by, by, by
I'm just sitting out here watching airplanes
Baby bye, bye, bye .")
    #song 216 (two-hundred-sixteen)
  Song.create!(artist:  "Carrie Underwood",
               title: "Last Name",
               rank: 216,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Hillary Lindsey, Carrie Underwood, Luke Laird",
               year: 2007,
               lyrics: "Last night, I got served, a little bit too much of that poison baby
Last night, I did things I'm not proud of, and I got a little crazy
Last night, I met a guy on the dance floor, and I let him call me 'baby'

And I don't even know his last name
My momma would be so ashamed
It started out 'hey cutie, where you from?'
Then it turned into 'oh no! what have I done?'
And I don't even know his last name

He left, the club, about around three o'clock in the morning
His Pinto, is sitting there in the parking lot, when it should of been a warning
I had no clue, what I was getting into, so I blame it on the Cuervo
Oh where did my manners go

And I don't even know his last name
My momma would be so ashamed
It started off 'hey cutie, where you from?'
And then it turned into 'oh no! what have I done?'
And I don't even know his last name, oh here we go

Today, I woke up, thinkin' 'bout Elvis, somewhere in Vegas I'm not sure
How I got here, or how this ring on my left hand just appeared
Outta nowhere, I gotta go, take the chips, and the Pinto, and hit the road
They say what happens here, stays here,
All of this will disappear, but there's just one little problem

I don't even know my last name
My momma would be so ashamed
It started off 'hey cutie, where you from?'
And then it turned into 'oh no! what have I done?'
And I don't even know my last name

What have I done, what have I done, what have I done
Well what have I done, I don't even know my last name
And it turned into 'oh no! what have I done?'
And I don't even know my last name

It started off 'hey cutie, where you from?'
And then it turned into 'oh no! what have I done?'
And I don't even know my last name .")
    #song 217 (two-hundred-seventeen)
  Song.create!(artist:  "Sugarland",
               title: "All I Want To Do",
               rank: 217,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, Kobalt Music Publishing Ltd., BMG Rights Management US, LLC, DO Write Music LLC",
               writers: "Bobby Pinson, Kristian Bush, Jennifer Nettles",
               year: 2008,
               lyrics: "I don't want to get up baby,
Let's turn off the phone
I don't want to go to work today
Or even put my makeup on
I've got better things to do
Than my to do list anyway
Hide under the covers
And waste away the day

Let's just lay here and be lazy,
Baby drive me crazy
All I want to do
All I want to do
Is love you

I got my whole life to change the world
And climb the ladders
Looking at you looking at me
Is the only thing that matters
Come a little closer baby,
We can talk without the words
Hang a sign on the door,
Please do not disturb

Let's just lay here and be lazy,
Baby drive me crazy
All I want to do
All I want to do
Is love you

Give me a kiss, from that Elvis lip,
You don't want to miss this,
All I want to do
All I want to do
Is love you

All I really want to do is,
All I really want to do is,
All I really want to do is love you,
Love you, love you
Come a little closer baby,
We can talk without the words
Hang a sign on the door, please do not,
Please do not, please do not,
Please do not disturb

When I lay down in the evening all
I really want to do,
When I wake up, when I wake up in the morning baby,
All I really want to do .")
    #song 218 (two-hundred-eighteen)
  Song.create!(artist:  "Taylor Swift",
               title: "Should've Said No",
               rank: 218,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Taylor Swift",
               year: 2006,
               lyrics: "Strange to think the songs we used to sing
The smiles, the flowers, everything
Is gone
Yesterday I found out about you
Even now just looking at you
Feels wrong
You say that you'd take it all back
Given one chance; it was a moment of weakness
And you said 'yes'

You should've said no
You should've gone home
You should've thought twice before you let it all go
You should've known that word like what you did with her
Would get back to me (get back to me)
And I should've been there
In the back of your mind
I shouldn't be asking myself why
You shouldn't be beggin' for forgiveness at my feet
You should've said no
Baby, and you might still have me

You can see that I've been cryin'
Baby, you know all the right things
To say
But do you, honestly
Expect me to believe
We could ever be the same?
You say that the past is the past
You need one chance; it was a moment of weakness
And you said 'yes'

You should've said no
You should've gone home
You should've thought twice before you let it all go
You should've known that word like what you did with her
Would get back to me (get back to me)
And I should've been there
In the back of your mind
I shouldn't be asking myself why
You shouldn't be beggin' for forgiveness at my feet
You should've said no
Baby, and you might still have me

I can't resist
Before you go, tell me this
Was it worth it?
Was she worth this?

No, no
No, no, no, no

You should've said no
You should've gone home
You should've thought twice before you let it all go
You should've known that word like what you did with her
Would get back to me (get back to me)
And I should've been there
In the back of your mind
I shouldn't be asking myself why
You shouldn't be beggin' for forgiveness at my feet
You should've said no
Baby, and you might still have me .")
    #song 219 (two-hundred-nineteen)
  Song.create!(artist:  "Brooks & Dunn",
               title: "Put A Girl In It",
               rank: 219,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Ole MM",
               writers: "Rhett Akins, Dallas Davidson, Ben Hayslip",
               year: 2007,
               lyrics: "You can buy you a brand new truck
Chrome it all out, jack it way up
You can build you a house up high on a hill
With a pool and a pond and a view to kill
You can make all the money in sight
But you ain't livin' a good life

'Til you put a girl in it, you ain't got nothin'
What's it all worth without a little lovin'
Put a girl in it, some huggin' and some kissin'
If your world's got somethin' missin'
Just put a girl in it

You can buy a boat and a shiny set of skis
Have some fun in the sun, float around in the breeze
You can lay out on a blanket by the lake
Drink a cold beer, polish off another day
Kick on back and watch the sky turn red
A sunset ain't a sunset

'Til you put a girl in it, you ain't got nothin'
What's it all worth without a little lovin'
Put a girl in it, some huggin' and some kissin'
If your world's got somethin' missin'
Just put a girl in it

You can write you a country song
The DJ won't put it on
They won't dance or sing along

'Til you put a girl in it, you ain't got nothin'
What's it all worth without a little lovin'
Put a girl in it, some huggin' and some kissin'
If your world's got somethin' missin'
Just put a girl in it

If you're ridin' in your truck
Put a girl in it
If you're gonna have a party
Put a girl in it
If you wanna live the good life
Better put a girl in it .")
    #song 220 (two-hundred-twenty)
  Song.create!(artist:  "Taylor Swift",
               title: "Picture To Burn",
               rank: 220,
               copyright: "Universal Music Publishing Group, Sony/ATV Music Publishing LLC",
               writers: "Sony/ATV Music Publishing LLC, Ole MM, Ole Media Management Lp",
               year: 2006,
               lyrics: "State the obvious,
I didn't get my perfect fantasy
I realize you love yourself more than you could ever love me
So go and tell your friends that I'm obsessive and crazy
That's fine!
I'll tell mine
You're gay
By the way

I hate that
Stupid old pickup truck
You never let me drive
You're a redneck heartbreak
Who's really bad at lying
So watch me strike a match
On all my wasted time
As far as I'm concerned you're
Just another picture to burn

There's no time for tears,
I'm just sitting here
Planning my revenge
There's nothing stopping me
From going out with all of your best friends
And if you come around saying sorry to me
My daddy's gonna show you how sorry you'll be

'Cause I hate that
Stupid old pickup truck
You never let me drive
You're a redneck heartbreak
Who's really bad at lying
So watch me strike a match
On all my wasted time
As far as I'm concerned you're
Just another picture to burn

If you're missing me,
You'd better keep it to yourself
'Cause coming back around here
Would be bad for your health

'Cause I hate that
Stupid old pickup truck
You never let me drive

You're a redneck heartbreak
Who's really bad at lying

So watch me strike a match
On all my wasted time
In case you haven't heard

I really really hate that
Stupid old pickup truck
You never let me drive
You're a redneck heartbreak
Who's really bad at lying
So watch me strike a match
On all my wasted time
As far as I'm concerned you're
Just another picture to burn

Burn, burn, burn, baby, burn
Just another picture to burn
Baby, burn .")
#song 221 (two-hundred-twenty-one)
  Song.create!(artist:  "Rodney Atkins",
               title: "Watching You",
               rank: 221,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Teddy Natalia Noemi Sinclair, Emile Haynie, Jeffrey Bhasker",
               year: 2006,
               lyrics: "Driving through town, just my boy and me
With a happy meal in his booster seat
Knowing that he couldn't have the toy
Till his nuggets were gone
A green traffic light turned straight to red
I hit my brakes and mumbled under my breath
As fries went a flying and his orange drink covered his lap
Well, then my four year old said a four letter word
That started with 's',  and I was concerned
So I said, 'Son, now where did you learn to talk like that?'

He said, 'I've been watching you, dad, ain't that cool?
I'm your buckaroo, I wanna be like you
And eat all my food, and grow as tall as you are
We got cowboy boots and camo pants
Yeah, we're just alike, hey, ain't we dad?
I wanna do everything you do
So I've been watching you.'

We got back home, and I went to the barn
I bowed my head, and I prayed real hard
Said, 'Lord, please help me help my stupid self.'
Then this side of bedtime later that night
Turning on my son's Scooby Doo night light
He crawled out of bed, and he got down on his knees
He closed his little eyes, folded his little hands
And spoke to God like he was talking to a friend
And I said, 'Son, now where'd you learn to pray like that?'

He said, 'I've been watching you, dad, ain't that cool?
I'm your buckaroo, I wanna be like you
And eat all my food, and grow as tall as you are
We like fixing things and holding mama's hand
Yeah, we're just alike, hey, ain't we, dad?
I wanna do everything you do
So I've been watching you.'

With tears in my eyes, I wrapped him in a hug
Said, 'My little bear is growing up.'
He said, 'But when I'm big, I'll still know what to do.'

'Cause I've been watching you, dad, ain't that cool?
I'm your buckaroo, I wanna be like you
And eat all my food, and grow as tall as you are
Then I'll be as strong as Superman
We'll be just alike, hey, won't we, dad?
When I can do everything you do
'Cause I've been watching you .")
  #song 222 (two-hundred-twenty-two)
  Song.create!(artist:  "Billy Currington",
               title: "Good Directions",
               rank: 222,
               copyright: "BMG Rights Management US, LLC, Dan Hodges Music, LLC",
               writers: "Luke Bryan, Rachel Thibodeau",
               year: 2005,
               lyrics: "I was sittin' there sellin' turnips on a flatbed truck
Crunchin' on a pork rind when she pulled up
She had to be thinkin' this is where rednecks come from
She had Hollywood written on her license plate
She was lost and lookin' for the interstate
Needin' directions, and I was the man for the job

I told her way up yonder past the caution light
There's a little country store with an old Coke sign
You gotta stop in and ask Miss Bell for some of her sweet tea
Then a left will take you to the interstate
But a right will bring you right back here to me

I was sittin' there thinkin' 'bout her pretty face
Kickin' myself for not catchin' her name
I threw my hat and thought, 'you fool, that it coulda been love.
I knew my old Ford couldn't run her down
She probably didn't like me anyhow
So I watched her disappear in a cloud of dust

I told her way up yonder past the caution light
There's a little country store with an old Coke sign
You gotta stop in and ask Miss Bell for some of her sweet tea
Then a left will take you to the interstate
But a right will bring you right back here to me

Is this Georgia heat playin' tricks on me
Or am I really seein' what I think I see?
The woman of my dreams comin' back to me

She went way up yonder past the caution light
Don't know why, but somethin' felt right
When she stopped in and asked Miss Bell for some of her sweet tea
Mama gave her a big 'ol glass and sent her right back here to me

Thank God for good directions... and turnip greens .")
  #song 223 (two-hundred-twenty-three)
  Song.create!(artist:  "Kenny Chesney",
               title: "Never Wanted Nothin' More",
               rank: 223,
               copyright: "EMI Music Publishing, Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Ronnie Bowman, Christopher Stapleton",
               year: 2007,
               lyrics: "I couldn't wait to turn sixteen
And drive all the boys around
Foot on the gas and hands on the wheel
Was all I could think about

A little rust in the bed of that truck
And a four speed on the floor
Five hundred dollars
It was mine all mine
And I never wanted nothing more

I took Katie down by the river
With a six dollar bottle of wine
Just a fool tryin' to play it cool
Hopin' she'd let me cross the line

And I was prayin' that she couldn't tell
I'd never been that far before
The first time's a one time feeling
And I never wanted nothin' more
No I never wanted nothin' more

Well, I'm what I am and I'm what I'm not
And I'm sure happy with what I've got
I live to love and laugh a lot
And that's all I need

My buddies all tried to change my mind
But I told them that I thought it through
Well Katie laughed and my momma cried
When they heard me say I do

Her little ring was a little thing
But it was all that I could afford
Now shes mine all mine
Till the day I die
And I never wanted nothing more
No I never wanted nothing more

Well, I'm what I am and I'm what I'm not
And I'm sure happy with what I've got
I live to love and laugh a lot
And that's all I need

One Sunday I listened to the preacher
And I knew he was preaching to me
I couldn't help it I walked up front
And I got down on my knee

Right then and there I swear
I changed when I found the Lord
Glory Hallelujah good God Almighty
I never wanted nothing more
No I never wanted nothing more

Well, I'm what I am and I'm what I'm not
And I'm sure happy with what I've got
I live to love and laugh a lot
And that's all I need

I never wanted nothing more
And I never wanted nothing more .")
  #song 224 (two-hundred-twenty-four)
  Song.create!(artist:  "Rodney Atkins",
               title: "These Are My People",
               rank: 224,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group, Spirit Music Group",
               writers: "David Allen Berg, Melvern Rivers Ii Rutherford",
               year: 2006,
               lyrics: "Well, we grew up down by the railroad tracks, shootin' bibis at old beer cans
Chokin' on the smoke from a Lucky Strike somebody lifted off his old man
We were football flunkies, southern rock junkies, crankin' up the stereos
Singing loud and proud to 'Gimmie Three Steps', 'Simple Man', and 'Curtis Low'
We were big, ya know

Got some discount knowledge at the Jr. College where we majored in beer and girls
It was all real funny till we ran out of money and they threw us out into the world
Yeah, the kids that thought they'd run this town ain't a-runnin' much of anything
Just lovin' and laughin' and bustin' our asses, and we all call it all livin' the dream

But these are my people, this is where I come from
We're givin' this life everything we got and then some
It ain't always pretty, but it's real
It's the way we were made, wouldn't have it any other way
These are my people

Well, we take it all week on the chin with a grin till we make it to Friday night
And it's church league softball, holler 'bout a bad call, preacher breaking up the fight
Then later on at the Green Light tavern, well, everybody's gatherin' as friends
And the beer's a pourin' till Monday mornin', and we start it all over again

And these are my people, this is where I come from
We're givin' this life everything we got and then some
It ain't always pretty, but it's real
It's the way we were made, wouldn't have it any other way
These are my people

We fall down, and we get up
We walk proud, and we talk tough
We got heart, and we got nerve
Even if we are a bit disturbed

Woo, come on

These are my people, this is where I come from
We're givin' this life everything we got and then some
It ain't always pretty, but it's real
It's the way we were made, wouldn't have it any other way, ah naw
These are my people, yeah, woo .")
  #song 225 (two-hundred-twenty-five)
  Song.create!(artist:  "Rascal Flatts",
               title: "Take Me There",
               rank: 225,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Neil Thrasher, Kenneth Chesney, Wendell Mobley",
               year: 2007,
               lyrics: "
There's a place in your heart nobody's been
Take me there
Things nobody knows, not even your friends
Take me there

Tell me 'bout your mama, your daddy, your hometown, show me around
I wanna see it all, don't leave anything out

I wanna know everything about you then
And I wanna go down every road you've been
Where your hopes and dreams and wishes live
Where you keep the rest of your life hid
I wanna know the girl behind that pretty stare
Take me there

Your first real kiss, your first true love
You were scared, show me where
You learned about life, spent your summer nights
Without a care

I wanna roll down Main Street, the back roads
Like you did when you were a kid
What made you who you are
Tell me what your story is

I wanna know everything about you then
And I wanna go down every road you've been
Where your hopes and dreams and wishes live
Where you keep the rest of your life hid
I wanna know the girl behind that pretty stare
Take me there

Yeah, I wanna know everything about you
Yeah, everything about you baby
I wanna go down every road you've been
Where your hopes and dreams and wishes live
Where you keep the rest of your life hid
I wanna know the girl behind that pretty stare
Take me, take me, take me there

I wanna roll down Main Street
I wanna know your hopes and your dreams
Take me, take me there .")

  #song 226 (two-hundred-twenty-six)
  Song.create!(artist:  "Big & Rich",
               title: "Lost In The Moment",
               rank: 226,
               copyright: "Reservoir Media Management Inc, Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "John D. Rich, Keith Anderson, Rodney Clawson",
               year: 2007,
               lyrics: "See your mama and the candles, and the tears and roses
I see your daddy walk his daughter down the isle
And my knees start to tremble as I tell the preacher
Don't she look beautiful tonight?

All the wonderful words in my head I've been thinking
Ya know I wanna say 'em all just right
I lift your veil and angels start singing
Such a heavenly sight

Lost in this moment with you
I am completely consumed
My feeling's so absolute
There's no doubt
Sealing our love with a kiss
Waited my whole life for this
Watching all my dreams come true
Lost in this moment with you

I smell the jasmine floating in the air like a love song
Watch my words draw sweet tears from your eyes
Bow our heads while the preacher talks to Jesus
Please bless this brand new life, yeah

Lost in this moment with you
I am completely consumed
My feeling's so absolute
There's no doubt
Sealing our love with a kiss
Waited my whole life for this
Watching all my dreams come true
Lost in this moment with you

Lost in this moment with you
I am completely consumed
My feeling's so absolute
There's no doubt
Sealing our love with a kiss
Waited my whole life for this
Watching all my dreams come true
Lost in this moment with you

Lost in the moment
In this moment with you
Lost in the moment
Lost in the moment
In this moment with you
Lost in the moment .")
  #song 227 (two-hundred-twenty-seven)
  Song.create!(artist:  "Carrie Underwood",
               title: "Wasted",
               rank: 227,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Marv Green, Troy Verges, Hillary Lindsey",
               year: 2005,
               lyrics: "Standing at the back door
She tried to make it fast
One tear hit the hard wood
It fell like broken glass
She said sometimes love slips away
And you just can't get it back
Let's face it

For one split second
She almost turned around
But that would be like pouring rain drops
Back into a cloud
So she took another step and said
I see the way out, and I'm gonna take it

I don't wanna spend my life jaded
Waiting to wake up one day and find
That I let all these years go by
Wasted

Another glass of whiskey but it still don't kill the pain
So he stumbles to the sink and pours it down the drain
He said it's time to be a man and stop living for yesterday
Gotta face it

'Cause I don't wanna spend my life jaded
Waiting to wake up one day and find
That I let all these years go by
Wasted

Oh, I don't wanna keep on wishing, missing
The still of the morning, the color of the night
I ain't spending no more time
Wasted

She kept drivin' along
Till the moon and the sun were floating side-by-side
He looked in the mirror and his eyes were clear
For the first time in a while, hey, yeah

Oh, I don't wanna spend my life jaded
Waiting to wake up one day and find
That I let all these years go by
Wasted

Oh, I don't wanna keep on wishing, missing
The still of the morning, the color of the night
I ain't spending no more time
Wasted

Oh, I don't wanna spend my life jaded
Waiting to wake up one day and find
That I let all these years go by
Wasted, yeah, yeah

Oh, I don't wanna keep on wishing, missing
The still of the morning, the color of the night
I ain't spending no more time
Wasted .")
  #song 228 (two-hundred-twenty-eight)
  Song.create!(artist:  "Tim McGraw",
               title: "If You're Reading This",
               rank: 228,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Tim McGraw, Brad Warren, Brett Warren",
               year: 2007,
               lyrics: "If you're reading this
My mama's sitting there
Looks like I only got a one-way ticket over here
I sure wish I could give you one more kiss
And war was just a game we played when we were kids
Well, I'm laying down my gun
I'm hanging up my boots
I'm up here with God
And we're both watching over you

So lay me down
In that open field out on the edge of town
And know my soul is where my mama always prayed that it would go
If you're reading this, I'm already home

If you're reading this
Halfway around the world
I won't be there to see the birth of our little girl
I hope she looks like you
I hope she fights like me
She stands up for the innocent and the weak
I'm laying down my gun
Hanging up my boots
Tell dad, I don't regret that I've followed in his shoes

So lay me down
In that open field out on the edge of town
And know my soul is where my mama always prayed that it would go
If you're reading this, I'm already home

If you're reading this, there is going to come a day
You move on and find someone else and that's okay
Just remember this
I'm in a better place
Soldiers live in peace, and angels sing amazing grace

So lay me down
In that open field out on the edge of town
And know my soul is where my mama always prayed that it would go
If you're reading this, I'm already home .")
  #song 229 (two-hundred-twenty-nine)
  Song.create!(artist:  "Dierks Bentley",
               title: "Free And Easy (Down The Road I Go)",
               rank: 229,
               copyright: "Reservoir Media Management Inc",
               writers: "Dierks Bentley, Robbie Harrington, Rodney Janzen, Steven Beavers",
               year: 2006,
               lyrics: "Pair of boots and a sack of clothes
Free and easy down the road I go
Hanging memories on the high line poles
Free and easy down the road I go
Free and easy down the road I go

Ragweed's rocking on the radio
Free and easy down the road I go
So I keep rolling like an old banjo
Free and easy down the road I go

Got the sun shining on me like a big spotlight
So I know everything is gonna be alright

Ain't no telling where the wind might blow

Free and easy down the road I go
Living life like a Sunday stroll
Free and easy down the road I go
Free and easy down the road I go

If you only get to go around one time
I'm gonna sit back and try to enjoy the ride
I could make a million or wind up broke
Free and easy down the road I go
Can't take it with you when you go so

Free and easy down the road I go
Someday I know it's gonna take me home so
Free and easy down the road I go
Free and easy down the road I go .")
  #song 230 (two-hundred-thirty)
  Song.create!(artist:  "Sugarland",
               title: "Settlin'",
               rank: 230,
               copyright: "EMI Music Publishing, Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd.",
               writers: "Kristian Bush, Tim Owens, Jennifer Nettles",
               year: 2006,
               lyrics: "Fifteen minutes left to throw me together
For mister right now, not mister forever
Don't know why I even try when I know how it ends
Looking like another: 'maybe we could be friends.'
I've been leaving it up to fate
It's my life, so it's mine to make

I ain't settling for just getting by
I've had enough so-so for the rest of my life
Tired of shooting too low, so raise the bar high
Just enough ain't enough this time
I ain't settling for anything less than everything, yeah

With some good red wine and my brand new shoes
Gonna dance up a blue streak around my living room
Take a chance on love and try how it feels
With my heart wide open, now you know I will
Find what it means to be the girl
Change her mind and change her world

I ain't settling for just getting by
I've had enough so-so for the rest of my life
Tired of shooting too low, so raise the bar high
Just enough ain't enough this time
I ain't settling for anything less than everything

I ain't settling for just getting by
I've had enough so-so for the rest of my life
Tired of shooting too low, so raise the bar high
Just not giving up this time

I ain't settling for just getting by
I've had enough so-so for the rest of my life
Tired of shooting too low, so raise the bar high
I ain't settling, no, no, no, no, no, no

So raise the bar high .")
  #song 231 (two-hundred-thirty-one)
  Song.create!(artist:  "Rascal Flatts",
               title: "Stand",
               rank: 231,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Blair Daly, Danny Orton",
               year: 2006,
               lyrics: "You feel like a candle in a hurricane
Just like a picture with a broken frame
Alone and helpless, like you've lost your fight
But you'll be all right, you'll be all right

Cause when push comes to shove
You taste what you're made of
You might bend till you break
'Cause it's all you can take
On your knees, you look up
Decide you've had enough
You get mad, you get strong
Wipe your hands, shake it off
Then you stand, then you stand

Life's like a novel with the end ripped out
The edge of a canyon with only one way down
Take what you're given before it's gone
And start holdin' on, keep holdin' on

'Cause when push comes to shove
You taste what you're made of
You might bend till you break
'Cause it's all you can take
On your knees, you look up
Decide you've had enough
You get mad, you get strong
Wipe your hands, shake it off
Then you stand, yeah, then you stand

Every time you get up
And get back in the race
One more small piece of you
Starts to fall into place

Cause when push comes to shove
You taste what you're made of
You might bend till you break
'Cause it's all you can take
On your knees, you look up
Decide you've had enough
You get mad, you get strong
Wipe your hands, shake it off
Then you stand, then you stand

Yeah, then you stand, yeah
Yeah, baby
Woo hoo, woo hoo, woo hoo, woo hoo
Then you stand, yeah, yeah .")
  #song 232 (two-hundred-thirty-two)
  Song.create!(artist:  "George Strait",
               title: "It Just Comes Natural",
               rank: 232,
               copyright: "Warner/Chappell Music, Inc, Spirit Music Group",
               writers: "Jim Collins, Marv Green",
               year: 2006,
               lyrics: "Sun shines, clouds rain
Train whistles blow and guitars play
Preachers preach, farmers plow
Wishes go up, and the world goes round

And I love you
It just come natural
It just comes natural

Seasons change, rivers wind
Tumble weeds roll, and the stars shine
Wind howls, dawn breaks
Cowboys riding time slips away

And I love you
It just come natural
It's what I was born to do
Don't have to think it through
Baby, it's so easy loving you
It just come natural

It's what I was born to do
Don't have to think it through
Baby, it's so easy loving you

Fire burns, waves crash
Seeds grow and good things last
Ships sail, dreams fly
Night falls and full moons rise

And I love you
It just come natural
And I love you
It just come natural
It just come natural
It just come natural .")
  #song 233 (two-hundred-thirty-three)
  Song.create!(artist:  "Emerson Drive",
               title: "Moments",
               rank: 233,
               copyright: "Peermusic Publishing, Warner/Chappell Music, Inc",
               writers: "Sam Tate, Kathleen Anne Wright, David A Berg",
               year: 2006,
               lyrics: "I was coming to the end of a long, long walk
When a man crawled out of a cardboard box
Under the E. Street Bridge
Followed me on to it
I went out halfway across
With that homeless shadow tagging along
So I dug for some change
Wouldn't need it anyway
He took it lookin' just a bit ashamed
He said, 'you know, I haven't always been this way.'

I've had my moments, days in the sun
Moments I was second to none
Moments when I knew I did what I thought I couldn't do
Like that plane ride coming home from the war
That summer my son was born
And memories like a coat so warm
A cold wind can't get through
Lookin' at me now, you might not know it
But I've had my moments

I stood there tryin' to find my nerve
Wondering if a single soul on earth
Would care at all
Miss me when I'm gone
That old man just kept hanging around
Lookin' at me, lookin' down
I think he recognized
That look in my eyes
Standing with him there, I felt ashamed
I said, 'you know, I haven't always been this way.'

I've had my moments, days in the sun
Moments I was second to none
Moments when I knew I did what I thought I couldn't do
Like the day I walked away from the wine
For a woman who became my wife
And a love that, when it was right
Could always see me through
Lookin' at me now, you might not know it
But I've had my moments

I know somewhere 'round a trashcan fire tonight
That old man tells his story one more time
He says

I've had my moments, days in the sun
Moments I was second to none
Moments when I knew I did what I thought I couldn't do
Like that cool night on the E. Street Bridge
When a young man almost ended it
I was right there, wasn't scared a bit
And I helped to pull him through
Lookin' at me now, you might not know it
Oh, lookin' at me now, you might not know it
But I've had my moments, I've had my moments, I've had my moments, I've had my moments .")
  #song 234 (two-hundred-thirty-four)
  Song.create!(artist:  "Martina McBride",
               title: "Anyway",
               rank: 234,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "MARTINA MCBRIDE, BRAD WARREN, BRETT WARREN",
               year: 2007,
               lyrics: "
You can spend your whole life building
Something from nothin'
One storm can come and blow it all away
Build it anyway

You can chase a dream
That seems so out of reach
And you know it might not never come your way
Dream it anyway

God is great
But sometimes life ain't good
And when I pray
It doesn't always turn out like I think it should
But I do it anyway
I do it anyway

This world's gone crazy
It's hard to believe
That tomorrow will be better than today
Believe it anyway

You can love someone with all your heart
For all the right reasons
In a moment they can choose to walk away
Love 'em anyway

God is great
But sometimes life ain't good
And when I pray
It doesn't always turn out like I think it should
But I do it anyway
I do it anyway

You can pour your soul out singing
A song you believe in
That tomorrow they'll forget you ever sang
Sing it anyway
Yeah, sing it anyway

I sing
I dream
I love
Anyway .")
  #song 235 (two-hundred-thirty-five)
  Song.create!(artist:  "Montgomery Gentry",
               title: "Lucky Man Lyrics",
               rank: 235,
               copyright: "Sony/ATV Music Publishing LLC, Peermusic Publishing, Universal Music Publishing Group",
               writers: "David Kim, Geun Soo Lee, Se Hwan Kim, Dong Hyun Shin",
               year: 2006,
               lyrics: "I have days where I hate my job
This little town, and the whole world too
Last Sunday when the Bengals lost
Lord, it put me in a bad mood

I have moments when I curse the rain
Then complain when the sun's too hot
I look around at what everyone has
And I forget about all I've got

But I know I'm a lucky man
God's given me a pretty fair hand
Got a house and a piece of land
A few dollars in a coffee can
My old truck's still running good
My ticker's ticking like they say it should
I got supper in the oven, a good woman's loving
And one more day to be my little kid's dad
Lord, knows I'm a lucky man

Got some friends who would be here fast
I could call 'em any time of day
Got a brother who's got my back
Got a mama who I swear's a saint
Got a brand new rod and reel
Got a full week off this year
Dad had a close call last spring
It's a miracle he's still here

But I know I'm a lucky man
God's given me a pretty fair hand
Got a house and a piece of land
A few dollars in a coffee can
My old truck's still running good
My ticker's ticking like they say it should
I got supper in the oven, a good woman's loving
And one more day to be my little kid's dad
Lord, knows I'm a lucky man

My old truck's still running good
My ticker's ticking like they say it should
I got supper in the oven, a good woman's loving
And even my bad days ain't that bad
Yeah, I'm a lucky man
I'm a lucky lucky man .")
  #song 236 (two-hundred-thirty-six)
  Song.create!(artist:  "Tracy Lawrence",
               title: "Find Out Who Your Friends Are",
               rank: 236,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Casey Beathard, Edward Monroe Hill ",
               year: 2007,
               lyrics: "Run your car off the side of the road
Get stuck in a ditch way out in the middle of nowhere
Get yourself in a bind, lose the shirt off your back
Need a floor, need a couch, need a bus fare

This is where the rubber meets the road
This is where the cream is gonna rise
This is what you really didn't know
This is where the truth don't lie

You find out who your friends are
Somebody's gonna drop everything
Run out and crank up their car
Hit the gas get their fast
Never stop to think 'what's in it for me?' or 'it's way too far.'
They just show on up with their big old heart
You find out who you're friends are

Everybody wants to slap your back
Wants to shake your hand
When you're up on top of that mountain
But let one of those rocks give way then you slide back down look up
And see who's around then

This ain't where the road comes to an end
This ain't where the bandwagon stops
This is just one of those times when
A lot of folks jump off

You find out who your friends are
Somebody's gonna drop everything
Run out and crank up their car
Hit the gas get there fast
Never stop to think 'what's in it for me?' or 'it's way too far.'
They just show on up with that big old heart
You find out who you're friends are

When the water's high
When the weather's not so fair
When the well runs dry
Who's gonna be there?

You find out who your friends are
Somebody's gonna drop everything
Run out and crank up their car
Hit the gas get there fast
Never stop to think 'what's in it for me?' or 'it's way too far.'
They just show on up with their big old heart
You find out who you're friends are, you find out who you're friends are

Run your car off the side of the road
Get stuck in a ditch way out in the middle of nowhere
Get yourself in a bind, lose the shirt off your back
Need a floor, need a couch, need a bus fare .")
  #song 237 (two-hundred-thirty-seven)
  Song.create!(artist:  "Tim McGraw",
               title: "Last Dollar (Fly Away)",
               rank: 237,
               copyright: "Roba Music, Sony/ATV Music Publishing LLC, Reservoir Media Management Inc",
               writers: "Kenny Alphin",
               year: 2007,
               lyrics: "One, two, three, like a bird, I sing
'Cause you've given me the most beautiful set of wings
And I'm so glad you're here today
'Cause tomorrow I might have to go and fly away
(Hey!)

When I'm down to my last dollar
I walk right through my shoes
Just a small reminder
Of the hell that I've gone through
But look at me still smiling
'Cause I'm wondering what I'll do
Since I ain't got nothing
I got nothing to lose
Everybody say: ha ha ha, ha ha ha
My friends are always giving me
Watches, hats, and wine
That's how I know this is serious
That's how I know it's time
I don't have to worry about things that I don't have
'Cause if I ain't got nothing
I got nothing to hold me back

And one, two, three, like a bird, I sing
'Cause you've given me the most beautiful set of wings
And I'm so glad you're here today
'Cause tomorrow I might have to go and fly away
Fly away, fly away
Fly away, fly away

There's nothing that's worth keeping me
From places I should go
From Happy-ville to Loving-land
I'm gonna tour from coast to coast
I'm leaving everything behind
There's not much that I need
'Cause If I ain't got nothing
I'm foot-loose and fancy-free

And one, two, three, like a bird, I sing
'Cause you've given me the most beautiful set of wings
And I'm so glad you're here today
'Cause tomorrow I might have to go and fly away
Fly away, fly away
Fly away, fly away

Look at me so free
Nothing's holding me down (down)
Look at me so free
Can't keep my feet on the ground

One, two, three, like a bird, I sing
'Cause you've given me the most beautiful set of wings
And I'm so glad you're here today
'Cause tomorrow I might have to go and

One, two, three, like a bird, I sing
'Cause you've given me the most beautiful set of wings
And I'm so glad you're here today
'Cause tomorrow I might have to go and fly away
Fly away, fly away
Fly away, fly away

Fly away!

One, two, three, like a bird, I sing
'Cause you giving me the most beautiful set of wings
One, two, three, like a bird, I sing
'Cause you giving me the most beautiful set of wings
One, two, three, like a bird, I sing
'Cause you giving me the most beautiful set of wings
One, two, three, like a bird, I sing
'Cause you giving me the most beautiful set of wings .")
  #song 238 (two-hundred-thirty-eight)
  Song.create!(artist:  "Toby Keith",
               title: "Love Me If You Can",
               rank: 238,
               copyright: "Ole MM, Round Hill Music Big Loud Songs",
               writers: "Chris Wallin, Craig Wiseman",
               year: 2007,
               lyrics: "Sometimes I think that war is necessary
Every night I pray for peace on Earth
I hand out my dollars to the homeless
But believe that every able soul should work

My father gave me my shotgun
That I'll hand down to my son
Try to teach him everything it means

I'm a man of my convictions
Call me wrong, call me right
But I bring my better angels to every fight
You may not like where I'm going
But you sure know where I stand
Hate me if you want to
Love me if you can

I stand by my right to speak freely
But I worry 'bout what kids learn from TV
And before all of debatin' turns to angry words and hate
Sometimes we should just agree to disagree
And I believe that Jesus looks down here and sees us
And if you ask him he would say

I'm a man of my convictions
Call me wrong, call me right
But I bring my better angels to every fight
You may not like where I'm going
But you sure know where I stand
Hate me if you want to
Love me if you can

I'm a man of my convictions
Call me wrong, call me right
But I bring my better angels to every fight
You may not like where I'm going
But you sure know where I stand
Hate me if you want to
Love me if you can .")
#song 239 (two-hundred-thirty-nine)
  Song.create!(artist:  "Alan Jackson",
               title: "A Woman's Love",
               rank: 239,
               copyright: "Warner/Chappell Music, Inc",
               writers: "Alan Eugene Jackson",
               year: 1998,
               lyrics: "I have felt it
And I have held it
I have known
A woman's love
I have tasted
And I have wasted
A woman's love
And I know
I'll never understand
All the little things
that make It grand
A woman's love
And I know
I'll never come Face to face
With any thing
that takes the Place
Of a woman's love

Well I have kiss it
Tried to resist it
I have missed
A woman's love
And I have hated
Taken for granted
A woman's love

Well I have needed
And I have pleaded
And I have lost
A woman's love

And I have worshipped
And I have cursed it
A woman's love
Well I adore it
Thank god for it
A woman's love .")
  #song 240 (two-hundred-fourty)
  Song.create!(artist:  "Brad Paisley",
               title: "Ticks",
               rank: 240,
               copyright: "EMI Music Publishing, Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd., Spirit Music Group, BMG Rights Management US, LLC",
               writers: "Kelley Lovelace, Brad Paisley, Tim Owens",
               year: 2007,
               lyrics: "Every time you take a sip
In this smoky atmosphere
You press that bottle to your lips
And I wish I was your beer
In the small there of your back
Your jeans are playing peekaboo
I'd like to see the other half
Of your butterfly tattoo

Hey, that gives me an idea
Let's get out of this bar
Drive out into the country
And find a place to park

'Cause I'd like to see you out in the moonlight
I'd like to kiss you way back in the sticks
I'd like to walk you through a field of wildflowers
And I'd like to check you for ticks

I know the perfect little path
Out in these woods I used to hunt
Don't worry babe, I've got your back
And I've also got your front
Now, I'd hate to waste a night like this
I'll keep you safe you wait and see
The only thing allowed to crawl all over you
When we get there is me

You know every guy in here tonight
Would like to take you home
But I've got way more class than them
Babe, that ain't what I want

'Cause I'd like to see you out in the moonlight
I'd like to kiss you way back in the sticks
I'd like to walk you through a field of wildflowers
And I'd like to check you for ticks

You never know where one might be
There's lots of places that are hard to reach
I gotcha

I'd like to see you out in the moonlight
I'd like to kiss you, baby, way back in the sticks
I'd like to walk you through a field of wildflowers
And I'd like to check you for ticks
Oh, I'd sure like to check you for ticks .")
#song 241 (two-hundred-fourty-one)
  Song.create!(artist:  "Keith Urban",
               title: "I Told You So",
               rank: 241,
               copyright: "Roba Music, Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "William Alexander N Hart, Tyrone Peterson",
               year: 2006,
               lyrics: "You said you needed your space
I wasn't where you wanted to be
I didn't stand in your way
I only want you to be happy
And, so, how surprised am I to see you here tonight?

Oh, can't you see
That for worse or for better we're better together?
Please just come back home
No, don't say that you're sorry
And I won't say I told you so

Sometimes in our life
We get to where we wonder if
The long road that we're on
Is headin' in the same direction
Well, when it comes to you and me
We're right where I know we should be

Oh, can't you see
That for worse or for better we're better together?
Please just come back home
No, don't say that you're sorry
And I won't say I told you so

Sometimes it's like we're deep in nothing but love
The slightest thing can grow so foolishly
Remind me, please
Oh, can't you see
That for worse or for better we're better together?
Please just come back home
No, don't say that you're sorry
You don't gotta say you're sorry, baby
Oh, can't you see
That for worse or for better we're better together?
Please just come back home
No, don't say that you're sorry
And I won't say I told you so
And I won't say I told you so
But I told you so
Should've known better than to leave me, baby .")
  #song 242 (two-hundred-fourty-two)
  Song.create!(artist:  "Toby Keith",
               title: "High Maintenance Woman",
               rank: 242,
               copyright: "Warner/Chappell Music, Inc, Franklin Road Music",
               writers: "Toby Keith, Danny Simpson, Tim Wilson",
               year: 2007,
               lyrics: "I see her layin' by the poolside every day
She ain't got a lot on
She ain't got a lot to say

She wouldn't look my way
But, buddy, what do you expect?
I'm just the fix-it-up boy at the apartment complex

And she'll go out dancin' 'bout 7:15
Climb into the back of a long limousine
I know where she's goin'
She's goin' downtown
I'm goin' downtown too, and take a look around

She's my baby doll
She's my beauty queen
She's my movie star
Best I ever seen
I ain't hooked it up yet
But I'm tyin' hard as I can
It's just a high maintenance woman
Don't want no maintenance man

I'm just sittin' 'round waitin' on a telephone call
After water pipe exploded in the living room wall
If your washer and dryer need a repair
You know the handyman's waitin'
And he'll be right there

Twenty-four hours
Seven days a week
If it's gettin' clogged up or maybe startin' to leak
Just ring up my number, baby, give me a try
You know I got all the tools
And I can satisfy

She's my baby doll
She's my beauty queen
She's my movie star
Best I ever seen
I ain't asked her out yet
'Cause I don't know if I can
You see, a high maintenance woman
Don't want no maintenance man

She's my baby doll
She's my beauty queen
She's my movie star
Best I ever seen
I ain't hooked it up yet
But I'm tryin' hard as I can
It's just a high maintenance woman
Don't want no maintenance man
Ain't no high maintenance woman
Gonna fall for a maintenance man .")
  #song 243 (two-hundred-fourty-three)
  Song.create!(artist:  "Kenny Chesney",
               title: "Beer in Mexico",
               rank: 243,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Kenneth Chesney",
               year: 2005,
               lyrics: "Starin' out into the wild blue yonder
So many thoughts to sit and ponder
'Bout life and love and lack of
And this emptiness in my heart

Too old to be wild and free still
Too young to be over the hill
Should I try to grow up
But who knows where to start

So I just sit right here and have another beer in Mexico
Do my best to waste another day
Sit right here and have another beer in Mexico
Let the warm air melt these blues away

Sun comes up and sun sinks down
And I seen 'em both in this tourist town
Up for days in a rage
Just tryin' to search my soul

From the answers and the reasons why
I'm at these crossroads in my life
And I really don't know
Which way to go

So I just sit right here and have another beer in Mexico
Do my best to waste another day
Sit right here and have another beer in Mexico
Let the warm air melt these blues away

Maybe I'll settle down, get married
Or stay single and stay free
Which road I travel
Is still a mystery to me

So I just sit right here, have another beer in Mexico
Do my best to waste another day
Sit right here and have another beer in Mexico
Let the warm air melt these blues away

Down in Mexico .")
  #song 244 (two-hundred-fourty-four)
  Song.create!(artist:  "Trace Adkins",
               title: "Ladies Love Country Boys",
               rank: 244,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Hori Pro Entertainment Group",
               writers: "Jamey Johnson, George Teren, Rivers Rutherford",
               year: 2006,
               lyrics: "She grew up in the city in a little subdivision
Her daddy wore a tie
Mama never fried a chicken
Ballet, straight A's, most likely to succeed

They bought her a car after graduation
Sent her down south for some higher education
Put her on the fast track, to a law degree

Now she's comin' home to visit
Holdin' the hand
Of a wild-eyed boy
With a farmer's tan

And she's ridin' in the middle of his pickup truck
Blarin' Charlie Daniels, yellin', 'Turn it up!'
They raised her up a lady
But there's one thing they couldn't avoid
Ladies love country boys

Yeah, you know mamas and daddies want better for their daughters
Hope they'll settle down with a doctor or a lawyer
In their uptown, ball gown, hand-me-down royalty

They never understand
Why their princess falls
For some camouflage britches
And a southern boy drawl

Or why she's ridin' in the middle of a pickup truck
Blarin' Hank Jr., yellin', 'Turn it up!'
They raised her up a lady
But there's one thing they couldn't avoid
Ladies love country boys

You can train 'em
You can try to teach 'em right from wrong
But it's still gonna turn 'em on

When they go ridin' in the middle of a pickup truck
Blarin' Lynyrd Skynyrd, yellin', (Turn it up!)
You can raise her up a lady
But there's one thing you just can't avoid
Ladies love country boys

They love us country boys, yeah
Oh, yeah, they can't stand it
It's that country thing, you know
Yeah, singin' na na
All those pretty ladies
I like the na na .")
  #song 245 (two-hundred-fourty-five)
  Song.create!(artist:  "Brooks & Dunn",
               title: "Proud Of The House We Built",
               rank: 245,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Ronnie Dunn, Terry Mcbride, Marv Green",
               year: 2007,
               lyrics: "I dropped to my knees in that field on your daddy's farm
Asked you to marry me, all I had to give was my heart
While other kids went diving into swimming holes
You and me dove off into the great unknown
We were barely gettin' by, takin' care of each other
Then I became a daddy; you became a mother
Was an uphill battle nearly every day
Lookin' back I wouldn't have it any other way

I'm proud of the house we built
It's stronger than sticks, stones, and steel
It's not a big place sittin' up high on some hill
A lot of things will come and go but love never will
Oh, I'm proud
I'm proud of the house we built

Still workin' our way through the land of milk and honey
At the end of the day there's always more bills than money
I close my eyes at night and I still feel
The same fire in my heart I felt out in that field

I'm proud of the house we built
It's stronger than sticks, stones, and steel
It's not a big place sittin' up high on some hill
A lot of things will come and go but love never will
I'm proud
Oh I'm proud of the house we built

Oh, look at us today
Whoa, we've come such a long long way

I'm proud of the house we built
It's stronger than sticks, stones, and steel
Not a big place sittin' up high on some hill
A lot of things will come and go but love never will

I'm proud of the house we built
It's stronger than sticks, stones, and steel
It's not a big place sittin' up high on some hill
A lot of things will come and go but love never will
I'm proud
Oh I'm proud of this house
I'm proud, yeah I'm proud of the house
I'm proud
Yeah I'm proud of the house we built .")
  #song 246 (two-hundred-fourty-six)
  Song.create!(artist:  "Jason Michael Carroll",
               title: "Livin' Our Love Song",
               rank: 246,
               copyright: "Universal Music Publishing Group, Kobalt Music Publishing Ltd.",
               writers: "Glen Mitchell, Jason Michael Carroll, Tim Galloway ",
               year: 2007,
               lyrics: "Baby, when I look at you with your hair fallin' down and your baby blues
Standing there across the room, I get so lost in the way you move
It makes me reminisce back to years ago on a night like this
Teary eyed as you took my hand, and I told you that I'd be your man
So many have come, so many things have gone
One thing that's stayed the same is our love is still growing strong

Baby, just look at us: all this time, and we're still in love
Something like this just don't exist
Between a backwoods boy and a fairy tale princess
People said it would never work out
Living our dreams has shattered all doubts
It feels good to prove 'em wrong
Living our love song

Oh darling, would you look at me
With my heart beating fast and my shaking knees
It's pretty hard to believe after all these years I still need you this badly
You're dancing in my arms with a spotlight moon in a sea of stars
Girl, we've come so far; everything I want is everything that you are
I just want to lay you down
Say I love you without a sound
I think you know what I'm talking about

Baby, just look at us: all this time, and we're still in love
Something like this just don't exist
Between a backwoods boy and a fairy tale princess
People said it would never work out
Living our dreams has shattered all doubts
It feels good to prove 'em wrong
Living our love song

Baby, just look at us: all this time, and we're still in love
Something like this just don't exist
Between a backwoods boy and a fairy tale princess
People said it would never work out
Living our dreams has shattered all doubts
It feels good to prove 'em wrong
Living our love song .")
  #song 247 (two-hundred-fourty-seven)
  Song.create!(artist:  "Taylor Swift",
               title: "Teardrops On My Guitar",
               rank: 247,
               copyright: "Sony/ATV Music Publishing LLC, Ole MM",
               writers: "Liz Rose, Taylor Swift",
               year: 2006,
               lyrics: "Drew looks at me
I fake a smile so he won't see
That I want and I'm needing
Everything that we should be

I'll bet she's beautiful, that girl he talks about
And she's got everything that I have to live without

Drew talks to me
I laugh because it's so damn funny
But I can't even see
Anyone when he's with me

He says he's so in love, he's finally got it right
I wonder if he knows he's all I think about at night

He's the reason for the teardrops on my guitar
The only thing that keeps me wishing on a wishing star
He's the song in the car
I keep singing, don't know why I do

Drew walks by me
Can't he tell that I can't breathe?
And there he goes, so perfectly
The kind of flawless I wish I could be

She'd better hold him tight, give him all her love
Look in those beautiful eyes and know she's lucky cause

He's the reason for the teardrops on my guitar
The only thing that keeps me wishing on a wishing star
He's the song in the car
I keep singing, don't know why I do

So I drive home alone, as I turn off the light
I'll put his picture down and maybe get some sleep tonight

He's the reason for the teardrops on my guitar
The only one who's got enough of me to break my heart
He's the song in the car
I keep singing, don't know why I do

He's the time taken up, but there's never enough
And he's all that I need to fall into
Drew looks at me
I fake a smile so he won't see .")
  #song 248 (two-hundred-fourty-eight)
  Song.create!(artist:  "Keith Urban",
               title: "Stupid Boy",
               rank: 248,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group, Spirit Music Group",
               writers: "David Allen Berg, Sarah Buxton, Nancy Bryant",
               year: 2006,
               lyrics: "Well she was precious, like a flower
She grew wild, wild but innocent
A perfect prayer in a desperate hour
She was everything beautiful and different
Stupid boy, you can't fence that in
Stupid boy, it's like holdin' back the wind

She laid her heart and soul right in your hands
And you stole her every dream, and you crushed her plans
She never even knew she had a choice
And that's what happens when the only voice she hears is telling her she can't
Stupid boy
Stupid boy

So, what made you think you could take a life
And just push it, push it around
I guess to build yourself up so high
You had to take her and break her down
Well

She laid her heart and soul right in your hands
And you stole her every dream, and you crushed her plans
She never even knew she had a choice
And that's what happens when the only voice she hears is telling her she can't

You stupid boy
You always had to be right
And now you lost the only thing that ever made you feel alive, oh ho
Yeah, yeah

She laid her heart and soul right in your hands, yeah
And you stole her every dream and you crushed her plans, yes you did
She never even knew she had a choice
And that's what happens when the only voice she hears is telling her she can't

You stupid boy
Oh, I'm the same old, same old stupid boy
It took a while for her to figure out she could run
But when she did, she was long gone, long gone .")
  #song 249 (two-hundred-fourty-nine)
  Song.create!(artist:  "Brad Paisley",
               title: "She's Everything",
               rank: 249,
               copyright: "EMI Music Publishing, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Wil Nance, Brad Paisley",
               year: 2005,
               lyrics: "She's a yellow pair of running shoes
A holey pair of jeans
She looks great in cheap sunglasses
She looks great in anything
She's: 'I want a piece of chocolate.'
'Take me to a movie.'
She's: 'I can't find a thing to wear.'
Now and then she's moody
She's a Saturn with a sunroof
With her brown hair a-blowing
She's a soft place to land
And a good feeling knowing
She's a warm conversation
That I wouldn't miss for nothing
She's a fighter when she's mad
And she's a lover when she's loving

And she's everything I ever wanted
And everything I need
I talk about her
I go on and on, and on
'Cause she's everything to me

She's a Saturday out on the town
And a church girl on Sunday
She's a cross around her neck
And a cuss word 'cause it's Monday
She's a bubble bath and candles
Baby, come and kiss me
She's a one glass of wine
And she's feeling kinda tipsy
She's the giver I wish I could be
And the stealer of the covers
She's a picture in my wallet
Of my unborn children's mother
She's the hand that I'm holding
When I'm on my knees and praying
She's the answer to my prayer
And she's the song that I'm playing

And she's everything I ever wanted
And everything I need
I talk about her
I go on and on, and on
'Cause she's everything to me

She's the voice I love to hear
Someday when I'm ninety
She's that wooden rocking chair
I want rocking right beside me
Every day that passes
I only love her more
Yeah, she's the one
That I'd lay down my own life for
And she's everything I ever wanted
And everything I need
She's everything to me
Yeah, she's everything to me
Everything I ever wanted
And everything I need
She's everything to me .")
  #song 250 (two-hundred-fifty)
  Song.create!(artist:  "Luke Bryan",
               title: "All My Friends Say",
               rank: 250,
               copyright: "Warner/Chappell Music, Inc, Ole MM, Ole Media Management Lp, Hori Pro Entertainment Group, BMG Rights Management US, LLC",
               writers: "Luke Bryan, Jeffrey David Stevens, Lonnie Wilson",
               year: 2007,
               lyrics: "I got smoke in my hair
My clothes thrown everywhere
Woke up in my rocking chair
Holding a beer in my hand
Sporting a neon tan

My stereo cranked up
I can't find my truck
How'd I get home from the club?
Ain't got a clue what went down
So I started calling around

And all my friends say
I started shooting doubles
When you walked in
All my friends say
I went a little crazy
Seeing you with him
You know I don't remember a thing
But they say I sure was raising some cane
I was a rock star, party hard
Getting over you, comeback kid
Yeah, I must have did
What all my friends say
Yeah, yeah, yeah

I found my billfold
I cried oh, no, no
Good time Charlie got me, now I'm broke
But it was worth acting like a fool
Yeah, girl, I must have really showed you

And all my friends say
I started shooting doubles
When you walked in
All my friends say
I went a little crazy
Seeing you with him
You know I don't remember a thing
But they say I sure was raising some cane
I was a rock star, party hard
Getting over you, comeback kid
Yeah, I must have did
What all my friends say
Yeah, yeah, yeah

I was Elvis rocking on the bar
Working the crowd, pouring out my heart

And all my friends say
I started shooting doubles
When you walked in
All my friends say
I went a little crazy
Seeing you with him
You know I don't remember a thing
But they say I sure was raising some cane
I was a rock star, party hard
Getting over you, comeback kid
Yeah, I must have did
What all my friends say
Yeah, yeah, yeah .")
  #song 251 (two-hundred-fifty-one)
  Song.create!(artist:  "Brad Paisley",
               title: "Online",
               rank: 251,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Brad Paisley, Charles Dubois, Chris Dubois, John Lovelace, Kelley Lovelace",
               year: 2007,
               lyrics: "I work down at the Pizza Pit
And I drive and old Hyundai
I still live with my mom and dad
I'm 5'3 and overweight

I'm a sci-fi fanatic
Mild asthmatic
Never been to 2nd base
But there's a whole another me
That you need to see
Go check out Myspace

'Cause online I'm down in Hollywood
I'm 6'5 and I look damn good
I drive a Maserati
I'm a black belt in Karate
And I love a good glass of wine

It turns girls on that I'm mysterious
I tell 'em I don't want nothing serious
'Cause even on a slow day I can have a three way
Chat with two women at one time

I'm so much cooler online
So much cooler online

I get home, I kiss my mom
And she fixes me a snack
I head down to my basement bedroom
And fire up my Mac

In real life, the only time I've
Ever even been to L.A.
Was when I got the chance with the marching band
To play tuba in the Rose Parade

Online I live in Malibu
I posed for Calvin Kline, I've been in GQ
I'm single and I'm rich
And I got a set of six pack abs that'll blow your mind

It turns girls on that I'm mysterious
I tell 'em I don't want nothing serious
'Cause even on a slow day I can have a three way
Chat with two women at one time

I'm so much cooler online
Yeah I'm cooler online

When you got my kinda stacks, it's hard to get a date
Let alone a real girlfriend
But I grow another foot
And I lose a bunch of weight every time I log in

Online I'm out in Hollywood
I'm 6'5 and I look damn good
Even on a slow day, I can have a three way
Chat with two women at one time

I'm so much cooler online
Yeah I'm cooler online

I'm so much cooler online
Yeah I'm cooler online

Hey, I'm cooler online
(Yeah he's cooler online)

I'm so much cooler online

I'm so much cooler online
(Yeah he's cooler, yeah)

Oh yeah .")
#song 252 (two-hundred-fifty-two)
  Song.create!(artist:  "Rodney Atkins",
               title: "If You're Going Through Hell",
               rank: 252,
               copyright: "Spirit Music Group",
               writers: "Annie Tate, Dave Berg, Sam Tate",
               year: 2006,
               lyrics: "Well, you know those times when you feel like there's a sign there on your back
Says I don't mind if ya kick me seems like everybody has
Things go from bad to worse, you'd think they can't get worse than that
And then they do

You step off the straight and narrow and you don't know where you are
Use the needle of your compass to sew up your broken heart
Ask directions from a genie in a bottle of Jim Beam
And she lies to you
That's when you learn the truth

If you're going through hell
Keep on going, don't slow down
If you're scared, don't show it
You might get out before the devil even knows you're there

Well, I been deep down in that darkness I been down to my last match
Felt a hundred different demons breathing fire down my back
And I knew that if I stumbled I'd fall right into the trap
That they were laying, yeah

But the good news is there's angels everywhere out on the street
Holding out a hand to pull you back upon your feet
The one's that you been dragging for so long you're on your knees
You might as well be praying
Guess what I'm saying

If you're going through hell
Keep on going, don't slow down
If you're scared, don't show it
You might get out before the devil even knows you're there

Yeah, if you're going through hell
Keep on moving, face that fire
Walk right through it
You might get out before the devil even knows you're there

If you're going through hell
Keep on going, don't slow down
If you're scared, don't show it
You might get out before the devil even knows you're there

Yeah, if you're going through hell
Keep on moving, face that fire
Walk right through it
You might get out before the devil even knows you're there
Yeah, you might get out before the devil even knows you're there, yeah .")
  #song 253 (two-hundred-fifty-three)
  Song.create!(artist:  "Brad Paisley",
               title: "The World",
               rank: 253,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, Kobalt Music Publishing Ltd., BMG Rights Management US, LLC",
               writers: "Kelley Lovelace, Brad Paisley, Lee Miller",
               year: 2005,
               lyrics: "To the teller down at the bank
You're just another checking account
To the plumber that came today
You're just another house
At the airport ticket counter
You're just another fare
At the beauty shop at the mall
Well you're just another head of hair
Well that's alright, that's ok
If you don't feel important, honey
All I've got to say is

To the world
You may be just another girl
But to me
Baby, you are the world

To the waiter at the restaurant
You're just another tip
To the guy at the ice cream shop
You're just another dip
When you can't get reservations
'Cause you don't have the clout
Or you didn't get an invitation
'Cause somebody left you out
That's alright, that's okay
When you don't feel important honey
All I've got to say is

To the world
You may be just another girl
But to me
Baby, you are the world

You think you're one of millions but you're one in a million to me
When you wonder if you matter, baby look into my eyes
And tell me, can't you see you're everything to me

That's alright, that's okay
When you don't feel important honey
All I've gotta say is

To the world
You may be just another girl
But to me
Baby, you are the world .")
  #song 254 (two-hundred-fifty-four)
  Song.create!(artist:  "Kenny Chesney",
               title: "Summertime",
               rank: 254,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Craig Wiseman, Steve Mc Ewan",
               year: 2005,
               lyrics: "Summertime is finally here
That old ballpark, man, is back in gear
Out on forty nine
Man I can see the lights

School's out and the nights roll in
Man, just like a long lost friend
You ain't seen in a while
And can't help but smile

And it's two bare feet on the dashboard
Young love and an old Ford
Cheap shades and a tattoo
And a Yoo-Hoo bottle on the floorboard

Perfect song on the radio
Sing along 'cause it's one we know
It's a smile, it's a kiss
It's a sip of wine, it's summertime
Sweet summertime

Temperature says ninety three
Down at the Deposit and Guarantee
But that swimmin' hole
It's nice and cold

Bikini bottoms underneath
But the boys' hearts still skip a beat
When them girls shimmy off
Them old cutoffs

And it's two bare feet on the dashboard
Young love and an old Ford
Cheap shades and a tattoo
And a Yoo-Hoo bottle on the floorboard

Perfect song on the radio
Sing along 'cause it's one we know
It's a smile, it's a kiss
It's a sip of wine, it's summertime
Sweet summertime

The more things change
The more they stay the same
Don't matter how old you are
When you know what I'm talkin' 'bout
Yeah baby when you got

Two bare feet on the dashboard
Young love and an old Ford
Cheap shades and a tattoo
And a Yoo-Hoo bottle rollin' on the floorboard

Perfect song on the radio
Sing along 'cause it's one we know
It's a smile, it's a kiss
It's a sip of wine, it's summertime
Sweet summertime .")
  #song 255 (two-hundred-fifty-five)
  Song.create!(artist:  "Rascal Flatts",
               title: "What Hurts The Most",
               rank: 255,
               copyright: "Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Jeffrey Steele, Stephen Paul Robson",
               year: 2006,
               lyrics: "I can take the rain on the roof of this empty house
That don't bother me
I can take a few tears now and then and just let 'em out
I'm not afraid to cry every once in a while
Even though goin' on with you gone still upsets me
There are days every now and again I pretend I'm okay
But that's not what gets me

What hurts the most was being so close
And havin' so much to say
And watchin' you walk away
And never knowin' what could've been
And not seein' that lovin' you
Is what I was trying to do

It's hard to deal with the pain of losin' you everywhere I go
But I'm doing it
It's hard to force that smile when I see our old friends and I'm alone
Still harder gettin' up, gettin' dressed, livin' with this regret
But I know if I could do it over
I would trade, give away all the words that I saved in my heart
That I left unspoken

What hurts the most was being so close
And havin' so much to say
And watchin' you walk away
And never knowin' what could've been
And not seein' that lovin' you
Is what I was trying to do, oh

What hurts the most was being so close
And havin' so much to say
And watchin' you walk away
And never knowin' what could've been
And not seein' that lovin' you
Is what I was trying to do

That's what I was trying to do, ooh .")
  #song 256 (two-hundred-fifty-six)
  Song.create!(artist:  "Carrie Underwood",
               title: "Jesus, Take The Wheel",
               rank: 256,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Reservoir One Music, Reservoir Media Management Inc, BMG Rights Management US, LLC",
               writers: "Brett James, Hillary Lindsey, Gordy Sampson",
               year: 2005,
               lyrics: "She was driving last Friday on her way to Cincinnati on a snow white Christmas Eve
Going home to see her mama and her daddy with the baby in the backseat
Fifty miles to go, and she was running low on faith and gasoline
It'd been a long hard year
She had a lot on her mind, and she didn't pay attention
She was going way too fast
Before she knew it she was spinning on a thin black sheet of glass
She saw both their lives flash before her eyes
She didn't even have time to cry
She was so scared
She threw her hands up in the air

Jesus, take the wheel
Take it from my hands
'Cause I can't do this on my own
I'm letting go
So give me one more chance
And save me from this road I'm on
Jesus, take the wheel

It was still getting colder when she made it to the shoulder
And the car came to a stop
She cried when she saw that baby in the backseat sleeping like a rock
And for the first time in a long time
She bowed her head to pray
She said, 'I'm sorry for the way
I've been living my life
I know I've got to change
So from now on tonight

Jesus, take the wheel
Take it from my hands
'Cause I can't do this on my own
I'm letting go
So give me one more chance
And save me from this road I'm on.'

Oh, Jesus, take the wheel

Oh, I'm letting go
So give me one more chance
Save me from this road I'm on
From this road I'm on
Jesus, take the wheel
Oh, take it, take it from me
Oh, why, oh .")
  #song 257 (two-hundred-fifty-seven)
  Song.create!(artist:  "Josh Turner",
               title: "Would You Go With Me",
               rank: 257,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Shawn Camp, John Scott Sherrill",
               year: 2006,
               lyrics: "Would you go with me if we rolled down streets of fire
Would you hold on to me tighter as the summer sun got higher
If we roll from town to town and never shut it down

Would you go with me if we were lost in fields of clover
Would we walk even closer until the trip was over
And would it be okay if I didn't know the way

If I gave you my hand would you take it
And make me the happiest man in the world
If I told you my heart couldn't beat one more minute without you, girl
Would you accompany me to the edge of the sea
Let me know if you're really a dream
I love you so, so would you go with me

Would you go with me if we rode the clouds together
Could you not look down forever
If you were lighter than a feather
Oh, and if I set you free, would you go with me

If I gave you my hand would you take it
And make me the happiest man in the world
If I told you my heart couldn't beat one more minute without you, girl
Would you accompany me to the edge of the sea
Help me tie up the ends of a dream
I gotta know, would you go with me
I love you so, so would you go with me .")
  #song 258 (two-hundred-fifty-eight)
  Song.create!(artist:  "Keith Urban",
               title: "Tonight I Wanna Cry",
               rank: 258,
               copyright: "Universal Music Publishing Group",
               writers: "Keith Lionel Urban, Monty Powell",
               year: 2004,
               lyrics: "Alone in this house again tonight
I got the TV on, the sound turned down and a bottle of wine
There's pictures of you and I on the walls around me
The way that it was and could have been surrounds me
I'll never get over you walkin' away

I've never been the kind to ever let my feelings show
And I thought that bein' strong meant never losin' your self-control
But I'm just drunk enough to let go of my pain
To hell with my pride, let it fall like rain
From my eyes
Tonight I want to cry

Would it help if I turned a sad song on
'All By Myself' would sure hit me hard now that you're gone
Or maybe unfold some old yellow lost love letters
It's gonna hurt bad before it gets better
But I'll never get over you by hidin' this way

I've never been the kind to ever let my feelings show
And I thought that bein' strong meant never losin' your self-control
But I'm just drunk enough to let go of my pain
To hell with my pride, let it fall like rain
From my eyes
Tonight I want to cry

I've never been the kind to ever let my feelings show
And I thought that bein' strong meant never losin' your self-control
But I'm just drunk enough to let go of my pain
To hell with my pride, let it fall like rain
From my eyes
Tonight I want to cry .")
  #song 259 (two-hundred-fifty-nine)
  Song.create!(artist:  "The Wreckers",
               title: "Leave The Pieces",
               rank: 259,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Jennifer Hanson, Bill Austin",
               year: 2006,
               lyrics: "You're not sure that you love me
But you're not sure enough to let me go
Baby it ain't fair you know
To just keep me hangin' 'round

You say you don't wanna hurt me
Don't wanna to see my tears
So why are you still standing here
Just watching me drown

And it's alright, yeah I'll be fine
Don't worry 'bout this heart of mine
Just take your love and hit the road
There's nothing you can do or say
You're gonna break my heart anyway
So just leave the pieces when you go

Now you can drag out the heartache
Baby you can make it quick
Really get it over with
And just let me move on

Don't concern yourself
With this mess you've left for me
I can clean it up, you see
Just as long as you're gone

And it's alright, yeah I'll be fine
Don't worry 'bout this heart of mine
Just take your love and hit the road
There's nothing you can do or say
You're gonna break my heart anyway
So just leave the pieces when you go

You not making up your mind
Is killing me and wasting time
I need so much more than that
Yeah, yeah, yeah, yeah, yeah

And it's alright, yeah I'll be fine
Don't worry 'bout this heart of mine
Just take your love and hit the road
There's nothing you can do or say
You're gonna break my heart anyway
So just leave the pieces when you go

Leave the pieces when you go
Oh yeah
Leave the pieces when you go
Yeah, yeah, yeah, yeah,
Yeah, yeah, yeah, yeah,
Yeah, yeah, yeah, yeah, yeah,
Leave the pieces when you go .")
  #song 260 (two-hundred-sixty)
  Song.create!(artist:  "Bon Jovi",
               title: "Who Says You Can't Go Home (f. Jennifer Nettles)",
               rank: 260,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Richard Sambora, Jon Bon Jovi",
               year: 2005,
               lyrics: "I spent twenty years trying to get out of this place
I was looking for something I couldn't replace
I was running away from the only thing I've ever known
Like a blind dog without a bone
I was a gypsy lost in the twilight zone
I hijacked a rainbow and crashed into a pot of gold

I been there, done that
But I ain't looking back on the seeds I've sown
Saving dimes, spending too much time on the telephone
Who says you can't go home?

Who says you can't go home?
There's only one place they call me one of their own
Just a hometown boy born a rolling stone
Who says you can't go home?
Who says you can't go back?
I been all around the world and as a matter of fact
There's only one place left I wanna go
Who says you can't go home?
It's alright, it's alright, it's alright, it's alright, it's alright

I went as far as I could
I tried to find a new face
There isn't one of these lines that I would erase
I lived a million miles of memories on that road
With every step I take, I know that I'm not alone
You take the home from the boy
But not the boy from his home
These are my streets, the only life I've ever known
Who says you can't go home?

Who says you can't go home?
There's only one place they call me one of their own
Just a hometown boy born a rolling stone
Who says you can't go home?
Who says you can't go back?
Been all around the world and as a matter of fact
There's only one place left I wanna go
Who says you can't go home?

It doesn't matter where you are
It doesn't matter where you go
If it's a million miles away or just a mile up the road
Take it in
Take it with you when you go
Who says you can't go home?

Who says you can't go back?
Been all around the world and as a matter of fact
There's only one place left I wanna go
Who says you can't go home?
It's alright, it's alright, it's alright, it's alright, it's alright
Who says you can't go home?
It's alright, it's alright, it's alright, it's alright, it's alright
Who says you can't go home?
It's alright, it's alright, it's alright, it's alright, it's alright
Who says you can't go home? .")
#song 261 (two-hundred-sixty-one)
  Song.create!(artist:  "Jason Aldean",
               title: "Why",
               rank: 261,
               copyright: "Sony/ATV Music Publishing LLC, Roba Music, Warner/Chappell Music, Inc, Universal Music Publishing Group, Musicworks International",
               writers: "Shannon Brown, Ed Hill, Shaye Smith",
               year: 2005,
               lyrics: "It's three A.M. and I finally say
I'm sorry for acting that way
I didn't really mean to make you cry
Oh baby, sometimes I wonder why

Does it always have to come down
To you leaving
Before I'll say 'I love you'
Why do I always use the words
That cut the deepest
When I know how much it hurts you
Oh baby why, do I do that to you

I know I'd never let you walk away
So why do I push you 'til you break
And why are you always on the verge of good-bye
Before I'll show you how I really feel inside

Does it always have to come down
To you leaving
Before I'll say 'I love you'
Why do I always use the words
That cut the deepest
When I know how much it hurts you
Oh baby why, do I do that to you

Why do I always use the words that cut the deepest
When I know how much it hurts you
Oh baby why, do I do that to you
Why do I do that to you .")
  #song 262 (two-hundred-sixty-two)
  Song.create!(artist:  "Little Big Town",
               title: "Bring It On Home",
               rank: 262,
               copyright: "Warner/Chappell Music, Inc",
               writers: "E. Tyler Hayes, Greg Bieck, Tyler Hayes, Wayne Kirkpatrick",
               year: 2005,
               lyrics: "You've got someone here
Wants to make it all right
Someone to love you more than life
Right here
You got willing arms that'll hold you tight
A hand to lead you all through the night
Right here
I know your heart can get all tangled up inside
But don't you keep it to yourself

When your long day is over
And you can barely drag your feet
The weight of the world
Is on your shoulders
I know what you need
Bring it on home to me

You know I know you
Like the back of my hand
But you know I'm gonna do all that I can
Right here
Gonna lie with you
Till you fall asleep
When the morning comes I'm still gonna be right here
Oh Yes, I am
(Oh)
Take your worries and just drop them at the door
Baby, leave it all behind

When your long day is over
And you can barely drag your feet
The weight of the world
Is on your shoulders
I know what you need
Bring it on home to me

Baby, let me be your safe harbor
Don't let the water come
And carry you away

When your long day is over
And you can barely drag your feet
The weight of the world
Is on your shoulders
I know what you need
Bring it on home to me

Oh, bring it on home
Yeah, bring it on home to me
Home to me
Oh, bring it on, bring it on home to me

You got someone here wants to make it all right
Someone who loves you more than life
Right here .")
  #song 263 (two-hundred-sixty-three)
  Song.create!(artist:  "Steve Holy",
               title: "Brand New Girlfriend",
               rank: 263,
               copyright: "EMI Music Publishing, Sony/ATV Music Publishing LLC, BMG Rights Management US, LLC",
               writers: "Shane Minor",
               year: 2006,
               lyrics: "She said, 'I need sometime to find myself
I need a little space to think
And maybe we should start seeing other people
Baby, things are moving way too fast for me'
So I picked up what was left of my pride
And i, I put on my walking shoes
And I got up on that high road
And I did what any gentleman would do

I um, I got a brand new girlfriend!
We went and jumped off the deep end
Flew out to l. A. for the weekend
Spent the whole day lyin' on a beach
Wearin' nothin' but a smile
Playin' kissy-kissy smoochy-smoochy
Talkin' mooshy-mooshy 'bout nothin'
Man I think I'm onto somethin'
Because I feel just like a kid again
I got a brand new girlfriend

I love it when she calls me buttercup
She laughs and says I left the toilet seat up
She pops the top for me, a cold beer
And says my buddy's always welcome here
When I get hungry she takes me out
I ridin' shotgun like a shitzu hound
My tails a waggin' my tongue's hangin' out
It makes me wanna shout

I got a brand new girlfriend!
We went and jumped off the deep end
Flew out to l. A. for the weekend
Spent the whole day lyin' on a beach
Wearin' nothin' but a smile
Playin' kissy-kissy smoochy-smoochy
Talkin' mooshy-mooshy 'bout nothin'
Man I think I'm onto somethin'
Because I feel just like a kid again
I got a brand new girlfriend

Woo! cute cute cute

She likes to write our names in the sand
She's hearing wedding bells and making plans
She's gonna hang around a while I guess
And she hadn't even told me she loves me yet

I got a brand new girlfriend!
We went and jumped off the deep end
We fly to l. A. for the weekend
Spent the whole day lyin' on a beach
Wearin' nothin' but a smile
Playin' kissy-kissy smoochy-smoochy
Talkin' mooshy-mooshy 'bout nothin'
Man I think I'm onto somethin'
You know I feel just like a kid again
I got a brand new girlfriend
She makes me feel just like a kid again
I got a brand new girlfriend .")
  #song 264 (two-hundred-sixty-four)
  Song.create!(artist:  "Carrie Underwood",
               title: "Don't Forget To Remember Me",
               rank: 264,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Ashley Gorley, Morgane Hayes, Kelley Lovelace",
               year: 2005,
               lyrics: "Eighteen years had come and gone
For momma they flew by
But for me they drug on and on
We were loadin' up that Chevy
Both tryin' not to cry
Momma kept on talkin' puttin' off goodbye
Then she took my hand and said
Baby don't forget

Before you hit the highway you better stop for gas
And there's a fifty in the ashtray if you run short on cash
Here's a map and here's a bible if you ever lose your way
Just one more thing before you leave
Don't forget to remember me

This downtown apartment sure makes me miss home
And those bills there on the counter keep tellin' me I'm on my own
Just like every Sunday I called mama up last night
And even when it's not I tell her everything's all right
Before we hung up I said
Hey mama don't forget

To tell my baby sister I'll see her in the fall
Tell mamaw that I miss her yeah I should giver her a call
And make sure that you tell daddy that I'm still his little girl
Yeah I still feel like I'm where I'm supposed to be
But don't forget to remember me

Tonight I find myself kneelin' by the bed to pray
Haven't done this in a while so I don't know what to say
But lord I feel so small sometimes in this big old place
Yeah I know there's more important things
But don't forget to remember me .")
  #song 265 (two-hundred-sixty-five)
  Song.create!(artist:  "George Strait",
               title: "Give It Away",
               rank: 265,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Bill Anderson, Buddy Cannon, Jamey Johnson",
               year: 2006,
               lyrics: "She was stormin' through the house that day
And I could tell she was leavin'
And I thought, 'aw, she'll be back.'
Until we turned around and pointed at the wall and said

That picture from out honeymoon
That night in 'Frisco Bay
Just give it away
She said give it away

That big four poster king sized bed
Where so much love was made
Just give it away
She said just give it away

Just give it away
There ain't nothing in this house worth fightin' over
Oh, we're both tired of fightin' anyway
Just give it away

Oh, I tried to move on
But I found that each woman I held
Just reminded me of that day

When that front door swung wide open
She flung her diamond ring
Said, give it away
Just give it away

And I said, now honey
Don't you even want your half of everything
She said give it away
Just give it away

Just give it away
There ain't nothing in this house worth fightin' over
Oh, we're both tired of fightin' anyway
Just give it away

So, I'm still right here where she left me
Along with all the other things that she don't care about anymore
Hmm

Like that picture from our honeymoon
That night in 'Frisco Bay
She said give it away
Well, I can't give it away
That big four poster king sized bed
Where all our love was made
She said give it away
Well, I can't give it away

I've got furnished house
A diamond ring
And a lonely, broken heart
Full of love
And I can't even give it away .")
  #song 266 (two-hundred-sixty-six)
  Song.create!(artist:  "Toby Keith",
               title: "A Little Too Late",
               rank: 266,
               copyright: "O/B/O Apra Amcos, Hori Pro Entertainment Group",
               writers: "Dean Dillon, Scott Emerick, Toby Keith",
               year: 2006,
               lyrics: "It's a little too late
I'm a little too gone,
A little too tired of just hangin' on
So I'm letting go while I'm still strong enough to
It's got a little too sad
I'm a little too blue
It's a little too bad
You were too good to be true
I'm big time over you baby
It's a little too late

No I don't want to want to talk about what we can do about us anymore
Only time you and me wastin' is the time it takes to walk right out that door
Yeah talk about water under the bridge,
You should know by now girl that's all this is

It's a little too late
I'm a little too gone,
A little too tired of just hangin' on
So I'm letting go while I'm still strong enough to
It's got a little too sad
I'm a little too blue
It's a little too bad
You were too good to be true
I'm big time over you baby
It's a little too late

There was a time,
This heart of mine,
Would take you back every time
Don't you know
It's been two packs of cigarettes
A sleepless night
A nervous wreck, a day ago.
Now you ain't got no business coming around
I'm closing up shop
Shuttin' us down

It's a little too late
I'm a little too gone,
A little too tired of just hangin' on
So I'm letting go while I'm still strong enough to
It's got a little too sad
I'm a little too blue
It's a little too bad
You were too good to be true
I'm big time over you baby
It's a little too late

I'm big time over you baby,
It's a little too late .")
  #song 267 (two-hundred-sixty-seven)
  Song.create!(artist:  "LeAnn Rimes",
               title: "Somethings Gotta Give",
               rank: 267,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Tony Carl Mullins, Craig Michael Wiseman",
               year: 2005,
               lyrics: "Jenny's got a job, a cat named Jake,
Thirty one candles on her birthday cake
Next year
Thought by now she'd have a man
Two car seats and a minivan
But it still ain't here (hey!)

She's been lookin' for Mr. Right so long
But all she's found is Mr. Wrong
And that's the pits
She's drawn a line that she won't cross
Her and time are facing off
She says something's gotta give

Something's gotta give me butterflies
Something's gotta make me feel alive
Something's gotta give me dreams at night
Something's gotta make me feel alright
I don't know where it is
But something's gotta give

Friday night she had a date
Cell phone junky a half hour late
That's the biz baby
She's riding out of this twist of fate
She's had all that she can take
She says something's gotta give

Something's gotta give me butterflies
Something's gotta make me feel alive
Yeah something's gotta give me dreams at night
Something's gotta make me feel alright
I don't know where it is
But something's gotta give

I swear
There's got to be a meant to be for me out there
Somewhere someday
I'm gonna find someone, somehow, someway
Yea yeah, ah ah

Jenny's got a job, a cat named Jake,
Thirty one candles on her birthday cake
Next year

Oh she thought by now she'd have a man
Two car seats and a minivan
She says something's got,
Something's got,
Something's got

Something's gotta give me butterflies
Something's gotta make me feel alive
Yeah something's gotta give me dreams at night
Something's gotta make me feel alright
I don't know what it is
But something's gotta
I don't know where it is
But something's gotta
I don't know what it is
Yeah but something's gotta give
Ah something's got to give .")
  #song 268 (two-hundred-sixty-eight)
  Song.create!(artist:  "Keith Anderson",
               title: "Every Time I Hear Your Name",
               rank: 268,
               copyright: "EMI Music Publishing, Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Thomas Jay Hambridge, Keith Anderson, Jeffrey Steele",
               year: 2005,
               lyrics: "Fin'lly got over that song of ours; stopped chasin' little red sports cars,
To check the license plates an' I quit drivin' by your place.
Back makin' the rounds at our old haunts Honky Tonks, restaurants.
An' seein' some of our old friends it feels good to dance again.
An' I can fin'lly smell your perfume an' not look around the room for you.
An' I can walk right by your picture in a frame an' not feel a thing.

But when I hear your name,
I feel rain fallin' right out of the blue sky.
An' it's the fifth of May, an' I'm right there starin' in your eyes.
An' nothin's changed, an' we're still same.
An' I get lost in the innocence of a first kiss,
An' I'm hangin' on to every word rollin' off of your lips
An' that's all it takes, an' I'm in that place,
Everytime I hear your name.

Got someone special in my life everyone thinks she'd make a great wife.
Dad says he thinks she's the one reminds him of Mom when she was young,
But it's way too soon to be talkin 'bout rings; don't wanna rush into anything.
She's getting over someone too, kinda like me an' you.
An' she talks about him every once in a while, an' I just nod my head an' smile,
'Cause I know exactly what she's goin' through yeah, I've been there too.

An' when the conversation turns to you,
I get caught in a 'you were the only one for me',
Kinda thought, an' your face is all that I see.
I know I can't go back when I still go back.
An' there we are, parked down by the riverside,
An' I'm in your arms about to make love for the first time,
An' that's all it takes, an' I'm in that place,
Every time I hear your name.

So I'm thinkin' 'bout the words I left unsaid.
(Every time I hear your name.)
Stop tryin' the change the things I can't change.
(Every time I hear your name.)
In my heart I know you're gone, but in my head,

I feel rain fallin' right out of the blue sky.
An' it's the fifth of May, an' I'm right there starin' in your eyes.
That's all it takes, an' I'm in that place.
An' there we are, parked down by the riverside,
An' I'm in your arms about to make love for the first time.
An' I can't explain, but I'm in that place,
Every time I hear your name.
Every time I hear your name.
(Every time I hear your name.)
Oh, oh, oh, oh.
(Every time I hear your name.)
Ah ah.
Every time I hear your name .")
  #song 269 (two-hundred-sixty-nine)
  Song.create!(artist:  "Dierks Bentley",
               title: "Settle For A Slowdown",
               rank: 269,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Tony Martin, Brett Beavers, Dierks Bentley",
               year: 2005,
               lyrics: "I must look just like a fool, here in the middle of the road,
Standin' there in your rear view, gettin' soaked to the bone
This land is flat as it is mean, a man can see for a hundred miles
So I'm still prayin' I might see, the glow of a brake light

But your wheels just turn, down the road ahead
If it hurts at all, you ain't showed it yet
I keep lookin' for the slightest sign
That you might miss, what you left behind
I know there's nothin' stopping you now
But I'd settle for a slowdown

I held on longer than I should, believin' you might change your mind
And those bright lights of Hollywood, would fade in time

But your wheels just turn, down the road ahead
If it hurts at all, you ain't showed it yet
I keep lookin' for the slightest sign
That you might miss, what you left behind
I know there's nothin' stopping you now
But I'd settle for a slowdown

But your wheels just turn, down the road ahead
If it hurts at all, you ain't showed it yet
You're just a tiny dot, on that horizon line
Come on tap those brakes, baby just one time
I know there's nothin' stoppin' you now
I'm not asking you to turn back around
I'd settle for a slowdown
Come on just slowdown
I'd settle for a slowdown .")
  #song 270 (two-hundred-seventy)
  Song.create!(artist:  "Tim McGraw",
               title: "When The Stars Go Blue",
               rank: 270,
               copyright: "BMG Rights Management US, LLC",
               writers: "R Adams",
               year: 2006,
               lyrics: "Dancin' when the stars go blue
Dancin' when the evening fell
Dancin' in your wooden shoes
In a wedding gown

Dancin' out on 7th street
Dancin' through the underground
Dancin' with the marionette
Are you happy now?

Where do you go when you're lonely?
Where do you go when you're blue?
Where do you go when you're lonely?
I'll follow you
When the stars go blue
Stars go blue
Stars go blue
Stars go blue

Laughin' with your pretty mouth
Laughin' with your broken eyes
Laughin' with your lover's tongue
In a lullaby

Where do you go when you're lonely?
Where do you go when you're blue?
Where do you go when you're lonely?
I'll follow you
When the stars go blue
Stars go blue
Stars go blue
Stars go blue .")
  #song 271 (two-hundred-seventy-one)
  Song.create!(artist:  "Josh Turner",
               title: "Your Man",
               rank: 271,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Jace Everett, Charles Dubois, Christopher Stapleton",
               year: 2006,
               lyrics: "
Baby, lock the doors and turn the lights down low
Put some music on that's soft and slow
Baby, we ain't got no place to go
I hope you understand

I've been thinking 'bout this all day long
Never felt a feeling quite this strong
I can't believe how much it turns me on
Just to be your man

There's no hurry, don't you worry
We can take our time
Come a little closer, let's go over
What I had in mind

Baby, lock the doors and turn the lights down low
Put some music on that's soft and slow
Baby, we ain't got no place to go
I hope you understand

I've been thinking 'bout this all day long
Never felt a feeling quite this strong
I can't believe how much it turns me on
Just to be your man

Ain't nobody ever love nobody
The way that I love you
We're alone now
You don't know how long I've wanted to

Baby, lock the doors and turn the lights down low
Put some music on that's soft and slow
Baby, we ain't got no place to go
I hope you understand

I've been thinking 'bout this all day long
Never felt a feeling quite this strong
I can't believe how much it turns me on
Just to be your man .")
  #song 272 (two-hundred-seventy-two)
  Song.create!(artist:  "Brad Paisley",
               title: "When I Get Where I'm Going (f. Dolly Parton)",
               rank: 272,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "George Teren, Rivers Rutherford",
               year: 2005,
               lyrics: "When I get where I'm going
On the far side of the sky
The first thing that I'm gonna do
Is spread my wings and fly
I'm gonna land beside a lion
And run my fingers through his mane
Or I might find out what it's like
To ride a drop of rain

Yeah, when I get where I'm going
There'll be only happy tears
I will shed the sins and struggles
I have carried all these years
And I'll leave my heart wide open
I will love and have no fear
Yeah, when I get where I'm going
Don't cry for me down here

I'm gonna walk with my grandaddy
And he'll match me step for step
And I'll tell him how I missed him
Every minute since he left
Then I'll hug his neck

Yeah, when I get where I'm going
There'll be only happy tears
I will shed the sins and struggles
I have carried all these years
And I'll leave my heart wide open
I will love and have no fear
Yeah, when I get where I'm going
Don't cry for me down here

So much pain and so much darkness
In this world we stumble through
All these questions I can't answer
So much work to do
But when I get where I'm going
And I see my Maker's face
I'll stand forever in the light
Of His amazing grace
Yeah when I get where I'm going
There'll be only happy tears
Hallelujah
I will love and have no fear
When I get where I'm going
Yeah, when I get where I'm going .")
  #song 273 (two-hundred-seventy-three)
  Song.create!(artist:  "Gary Allan",
               title: "Life Ain't Always Beautiful",
               rank: 273,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Tommy Lee James, Cynthia Evelyn Thomson",
               year: 2005,
               lyrics: "Life ain't always beautiful
Sometimes it's just plain hard
Life can knock you down
It can break your heart.

Life ain't always beautiful
You think you're on your way
And it's just a dead end road
At the end of the day.

But the struggles make you stronger
And the changes make you wise
And happiness has it's own way
Of taking it's own sweet time.

No, life ain't always beautiful
Tears will fall sometimes
Life ain't always beautiful
But it's a beautiful ride.

Life ain't always beautiful
Some days I miss your smile
I get tired of walking
All these lonely miles.

And wish for just one minute
That I could see your pretty face
Guess, I can dream
But life don't work that way.

But the struggles make me stronger
And the changes make me wise
And happiness has it's own way
Of taking it's sweet time.

No, life ain't always beautiful
But I know I'll be fine
Life ain't always beautiful
But it's a beautiful ride.

What a beautiful ride .")
  #song 274 (two-hundred-seventy-four)
  Song.create!(artist:  "Montgomery Gentry",
               title: "She Don't Tell Me To",
               rank: 274,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Round Hill Music Big Loud Songs",
               writers: "Bob Dipiero, Tom Shapiro, Rivers Rutherford",
               year: 2005,
               lyrics: "Every now an' then, on my home
I stop at a spot where the wild flowers grow, an' I pick a few
Cause she don't tell me to
I go out with my boys all right
But most of the time I call it a night before they do
Cause she don't tell me to
Sunday mornin', I'm in church
An' my butt an' my back an' necktie hurt, but I'm in the pew
She don't tell me to

Any other woman I know would have tried
To control me and it would be over
Blame it on my goin' on my own way attitude
All of that stubbornness melts away
When I wake with her head on my shoulder
An' I know I've got to love her
Until my life is through
Cause she don't tell me to

Well, I got demons and I've got pride
But when I'm wrong, I apologize like she's mine to lose
Cause she don't tell me to
Well, I got dreams in this heart of mine
But nothin' that I wouldn't lay aside if she asked me to
Cause she don't tell me to
An' she don't even know
That she keeps lookin' for the next right thing to do
Cause she don't tell me to
Yeah, yeah

Any other woman I know would have tried
To control me and it would be over
Blame it on my goin' on my own way attitude
And all of that stubbornness melts away
When I wake with her head on my shoulder
An' I know I've got to love her
Until my life is through
What else can I do
What else can I do
Whoa, I love her
Cause she don't tell me to
She don't tell me to

Every now an' then, on my home
I stop at a spot where the wild flowers grow, an' I pick a few
Yes I do .")
  #song 275 (two-hundred-seventy-five)
  Song.create!(artist:  "Jack Ingram",
               title: "Wherever You Are",
               rank: 275,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Jeremy S. Stover, Steve Bogard",
               year: 2006,
               lyrics: "This desert wind is burnin' my face again
God I'm missin' you
Been runnin' blind under a broken sky
With regrets I was sorting through
But lesson learned baby
I've made the turn

Wherever you are
No matter how far
Girl I'm gonna find my way to you
Through rivers of rain
Over mountains of pain
Do whatever on earth I've gotta do
I'll follow the dream I'll follow my heart
Girl I've gotta be
Wherever you are

So many miles to where we said goodbye
To the street of shattered dreams
I'm prayin' hard you didn't start a life
With someone who's not me
And if you're just gone
Girl I'll be movin' on

Wherever you are
No matter how far
Girl I'm gonna find my way to you
Through rivers of rain
Over mountains of pain
Do whatever on earth I've gotta do
I'll follow the dream I'll follow my heart
Girl I've gotta be
Wherever you are

Let the sunrise find me searchin'
Let the west wind carry my plea
Give this changed man one more last chance
Open your arms to me

Wherever you are
Through rivers of rain
Over mountains of pain
Do whatever on earth I've gotta do
I'll follow the dream I'll follow my heart
Girl I've gotta be
Wherever you are
Wherever you are
Wherever you are
Wherever you are .")
#song 276 (two-hundred-seventy-six)
  Song.create!(artist:  "Blake Shelton",
               title: "Nobody But Me",
               rank: 276,
               copyright: "Warner/Chappell Music, Inc, Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Shawn Camp, Phillip Brian White",
               year: 2004,
               lyrics: "Don't waste your time, lookin' over your shoulder
Those loves from the past ain't gettin' no closer
When I look in my future, you're all I can see
So honey don't go lovin' on nobody but me

Nobody but me, gonna love you like you oughta be loved, darlin'
Nobody but me, gonna cry if you ever leave
Now you can do what you want to, but I'm askin' pretty please
Don't go lovin' on nobody but me

I took my time to tell you how I feel
Just because I took so long, don't mean that it isn't real.
I ain't got no diamond, but I'm down on my knees, honey
Don't go lovin' on nobody but me

Nobody but me, gonna love you like you oughta be loved, darlin'
Nobody but me, gonna cry if you ever leave
Now you can do what you want to but I'm askin' pretty please, honey
Don't go lovin' on nobody but me

Nobody but me, gonna love you like you oughta be loved, darlin'
Nobody but me gonna cry if you ever leave
Now you can do what you want to, but I'm askin' pretty please, honey
Don't go lovin' on nobody but me

Don't go lovin 'on nobody, but me .")
  #song 277 (two-hundred-seventy-seven)
  Song.create!(artist:  "Brooks & Dunn",
               title: "Believe",
               rank: 277,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Nikki Hassman, Scott Miller",
               year: 2003,
               lyrics: "Old man Wrigley lived in that white house
Down the street where I grew up
Momma used to send me over with things
We struck a friendship up
I spent a few long summers out on his old porch swing

Says he was in the war when in the navy
Lost his wife, lost his baby
Broke down and asked him one time
How ya keep from going crazy
He said I'll see my wife and son in just a little while
I asked him what he meant
He looked at me and smiled, said

I raise my hands, bow my head
I'm finding more and more truth in the words written in red
They tell me that there's more to life than just what I can see
Oh I believe

Few years later I was off at college
Talkin' to mom on the phone one night
Getting all caught up on the gossip
The ins and outs of the small town life
She said oh by the way son, old man Wrigley has died.

Later on that night, I laid there thinkin' back
Thought 'bout a couple long-lost summers
I didn't know whether to cry or laugh
If there was ever anybody deserved a ticket to the other side
It'd be that sweet old man who looked me in the eye, said

I raise my hands, bow my head
I'm finding more and more truth in the words written in red
They tell me that there's more to life than just what I can see
Oh I believe

I can't quote the book
The chapter or the verse
You can't tell me it all ends
In a slow ride in a hearse
You know I'm more and more convinced
The longer that I live
Yeah, this can't be
No, this can't be
No, this can't be all there is

I raise my hands, bow my head
I'm finding more and more truth in the words written in red
They tell me that there's more to life than just what I can see
Oh I believe

Oh, I
I believe (I believe)
I believe (I believe)
I believe (I believe)
I believe (I believe)
I believe (I believe)
I believe (I believe)
I believe (I believe) .")
  #song 278 (two-hundred-seventy-eight)
  Song.create!(artist:  "Kenny Chesney",
               title: "Living In Fast Forward",
               rank: 278,
               copyright: "Universal Music Publishing Group, Carol Vincent & Assoc LLC",
               writers: "David Lee Murphy, Melvern Rivers Ii Rutherford",
               year: 2005,
               lyrics: "The body's a temple; that's what we're told
I've treated this one like an old honky-tonk
Greasy cheeseburgers and cheap cigarettes
One day, they'll get me if they ain't got me yet

'Cause I'm living in fast forward
A hillbilly rock star out of control
I'm living in fast forward
Now I need to rewind real slow

My friends all grew up; they settled down
Built nice little houses on the outskirts of town
They work in their office, drive SUVs
They pray for their babies, and they worry 'bout me

'Cause I'm living in fast forward
A hillbilly rock star out of control
I'm living in fast forward
Now I need to rewind real slow

I'm always runnin'
Son-of-a-gunnin'
I've had a good time, it's true
But the way I've been goin'
It's time that I toned it
Down just a notch or two

Oh yeah

Well, I'm living in fast forward
A hillbilly rock star out of control
I'm living in fast forward
Now I need to rewind real slow

Yeah, I'm living in fast forward
A hillbilly rock star out of control
I'm living in fast forward
Now I need to rewind real slow

Yeah, I need to rewind real slow
Yeah, I still got some miles to go .")
  #song 279 (two-hundred-seventy-nine)
  Song.create!(artist:  "Brooks & Dunn",
               title: "Building Bridges (f. Sheryl Crow, Vince Gill)",
               rank: 279,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Larry C Willoughby, Hank Devito",
               year: 2005,
               lyrics: "Since you've gone, my heart said something's wrong.
How long can this keep goin' on?
I'm still blue over losin' you.
What else am I gonna do?

I'm buildin' bridges straight to your heart,
An' all of this distance won't keep us apart,
Won't keep us apart.

Talk to me, talk to me about sympathy.
Don't leave me beggin' on my knees.
Since you've gone, my heart says something's wrong.
How long can this keep goin' on?

I'm buildin' bridges straight to your heart,
An' all of this distance won't keep us apart,
Won't keep us apart.

I'm buildin' bridges straight to your heart,
An' all of this distance won't keep us apart,
Won't keep us apart.

I'm buildin' bridges, (I'm buildin' bridges.)
Straight to your heart,
And all of this distance won't keep us apart,
Won't keep us apart.

Straight to your heart.
Whoa, I,
Whoa I, I'm buildin' bridges,
Staright to your heart .")
  #song 280 (two-hundred-eighty)
  Song.create!(artist:  "Heartland",
               title: "I Loved Her First",
               rank: 280,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Elliot Park, Elliott Park, Walt Aldridge",
               year: 2006,
               lyrics: "Look at the two of you dancing that way
Lost in the moment and each other's face
So much in love, you're alone in this place
Like there's nobody else in the world

I was enough for her not long ago
I was her number one, she told me so
And she still means the world to me
Just so you know
So be careful when you hold my girl
Time changes everything, life must go on
And I'm not gonna stand in your way

I loved her first
I held her first
And a place in my heart will always be hers
From the first breath she breathed
When she first smiled at me
I knew the love of a father runs deep
And I prayed that she'd find you someday
But it's still hard to give her away
I loved her first

How could that beautiful woman with you
Be that same freckled face kid that I knew?
The one that I read all those fairy tales to
And tucked into bed all those nights
And I knew the first time I saw you with her
It was only a matter of time

I loved her first
I held her first
And a place in my heart will always be hers
From the first breath she breathed
When she first smiled at me
I knew the love of a father runs deep
And I prayed that she'd find you someday
But it's still hard to give her away
I loved her first

From the first breath she breathed
When she first smiled at me
I knew the love of a father runs deep
Someday you might know what I'm going through
When a miracle smiles up at you
Yeah, I loved her first .")
  #song 281 (two-hundred-eighty-one)
  Song.create!(artist:  "Toby Keith",
               title: "Get Drunk And Be Somebody",
               rank: 281,
               copyright: "Tokeco Tunes, O/B/O Apra Amcos",
               writers: "Scott Emerick, Toby Keith",
               year: 2006,
               lyrics: "Yeah the big boss man, he likes to crack that whip
I ain't nothing but a number on his time card slip,
I give him forty hours and a piece of my soul,
Puts me somewhere at the bottom of his totem pole,
Hell I don't even think he knows my name...

Well all week long I'm a real nobody,
But I just punched out and its paycheck Friday,
Weekends here, good God almighty,
I'm going to get drunk and be somebody
Yeah, yeah, yeah...

My baby cuts hair at a beauty boutique,
Just blowin' and goin' till she dead on her feet,
They walk right in and sit right down,
She gives them what they want and then she spins them around,
Hey I don't think they even know her name...

All week long she's a real nobody,
But I just picked her up and its paycheck Friday,
Weekends here, good God almighty,
Baby lets get drunk and be somebody
Yeah, yeah, yeah...

Well just average people, in an everyday bar,
Driving from work in our ordinary cars,
And I like to come here with the regular Joes,
Drink all you want, be the star of the star
Of the show

All week long bunch of real nobodies,
But we just punched out and its paycheck Friday,
Weekends here, good God almighty,
People lets get drunk (lets get drunk!)
All week long we're some real nobodies,
But we just punched out and its paycheck Friday,
Weekends here, good God almighty,
People lets get drunk and be somebody
Yeah, yeah, yeah... .")
  #song 282 (two-hundred-eighty-two)
  Song.create!(artist:  "Dierks Bentley",
               title: "Every Mile A Memory",
               rank: 282,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Reservoir One Music, Kobalt Music Publishing Ltd.",
               writers: "Steve Bogard, Brett Beavers, Dierks Bentley",
               year: 2006,
               lyrics: "Country roads, old theater marquee signs
Parkin' lots, and billboards flyin' by
Spanish moss, little hick town squares
Wild roses on a river bank
Girl, it's almost like you're there

Oh, every mile, a memory; every song, another scene
From some old movie going back in time, you and me
Every day, a page turned down; every night, a lonesome sound
Like a freight train rollin' through my dreams
Every mile, a memory

Red sun down, out across the western sky
Takes me back to the fire in your eyes
Texas stars in a purple night
Not seein' 'em with you baby
Oh, they never do look right, no

Every mile, a memory; every song, another scene
From some old movie going back in time, you and me
Every day, a page turned down; every night, a lonesome sound
Like a freight train rollin' through my dreams
Every mile, a memory

Funny how no matter where I run
'Round every bend I only see
Just how far I haven't come

Every mile, a memory; every song, another scene
From some old movie going back in time,
Every day, a page turned down; every night, a lonesome sound
Like a freight train rollin' through my dreams
Every mile, a memory
Every mile, a memory
Every mile, a memory .")
  #song 283 (two-hundred-eighty-three)
  Song.create!(artist:  "Joe Nickols",
               title: "Size Matters (Someday)",
               rank: 283,
               copyright: "Universal Music Publishing Group",
               writers: "Byron Hill, Thomas Michael Dekle",
               year: 2005,
               lyrics: "Someday she wants a big ol' house
Sittin' on a big ol' hill
And a mile long tree lined driveway
For her big ol' Coupe DeVille
Yes, someday she wants a big ol' bank account
With too much to spend
But right now all she wants is a man

With a big ol' heart
Who can love her like nobody can
Big ol' kisses that go on and on
And never end
With a big ol' smile
He'll fill her world with laughter
Size matters, size matters

Someday she wants a big ol' ring
With a big ol' rock that shines
And a big ol' walk-in closet
With shoes of every kind
Yeah, someday she wants a big ol' boat
She can lay around gettin' a tan
But right now all she wants is a man

With a big ol' heart
Who can love her like nobody can
Big ol' kisses that go on and on
And never end
With a big ol' smile
He'll fill her world with laughter
Size matters, size matters

With a big ol' heart
Who can love her like nobody can
Big ol' kisses that go on and on
And never end
With a big ol' smile
He'll fill her world with laughter
Size matters, size matters
Size matters .")
  #song 284 (two-hundred-eighty-four)
  Song.create!(artist:  "Craig Morgan",
               title: "That's What I Love About Sunday",
               rank: 284,
               copyright: "Sony/ATV Music Publishing LLC, Ole MM, Ole Media Management Lp",
               writers: "Adam Dorsey, Mark Narmore",
               year: 2005,
               lyrics: "Raymond's in his Sunday best
He's usually up to his chest in oil and grease
There's the martins walking in
With that mean little freckle-faced kid
Who broke a window last week
Sweet miss Betty likes to sing off key
In the pew behind me

That's what I love about Sunday
Sing along as the choir sways
Every verse of amazing grace
And then we shake the preacher's hand
Go home into your blue jeans
Have some chicken and some baked beans
Pick a backyard football team
Not do much of anything
That's what I love about Sunday

I stroll to the end of the drive
Pick up the Sunday times, grab a coffee cup
Looks like sally and rob finally tied the knot
Well, it's about time
It's thirty-five cents off a ground round
Baby, cut that coupon out

That's what I love about Sunday
Cat-nappin' on a porch swing
You curled up next to me
The smell of jasmine wakes us up
Take a walk down a back road
Tackle box and a cane pole
Carve our names in that white oak
Steal a kiss as the sun fades
That's what I love about Sunday

New believers getting baptized
Mama's hands raised up high
Havin' a hallelujah good time
A smile on everybody's face
That's what I love about Sunday

That's what I love about Sunday .")
  #song 285 (two-hundred-eighty-five)
  Song.create!(artist:  "Toby Keith",
               title: "As Good As I Once Was",
               rank: 285,
               copyright: "Tokeco Tunes, Sony/ATV Music Publishing LLC, Round Hill Music Big Loud Songs",
               writers: "Scott Emerick, Toby Keith",
               year: 2005,
               lyrics: "She said I've seen you in here before,
I said I've been here a time or two,
She said hello my name is Bobbi Jo
Meet my twin sister Betty Lou
And we're both feeling kinda wild tonight,
You're the only cowboy in this place,
If your up for a rodeo,
I'll put a big Texas smile on your face.

I said girls,
I ain't as good as I once was,
I got a few years on me now,
But there was a time,
Back in my prime,
When I could really lay it down,
If you need some love tonight,
Then I might have just enough,
I ain't as good once was,
But I'm as good once, as I ever was.

I still hang out with my best friend Dave,
I've known him since we were kids at school.
Last night he had a few shots,
Got in a tight spot
Hustling a game of pool,
With a couple of red neck boys,
And one great big fat biker man,
I heard David yell across the room,
Hey Buddy how bout a helping hand.

I said Dave!
I ain't as good as I once was,
My how the years have flown,
But there was a time,
Back in my prime,
When I could really hold my own,
But if you want a fight tonight,
Guess those boys don't look all that tough,
I ain't as good once was,
But I'm as good once, as I ever was.

I used to be hell on wheels,
Back when I was a younger man
Now my body says 'Oh, You can't do this boy',
But my pride says 'Oh yes you can'

I ain't as good as I once was,
That's just the cold, hard truth, (huh)
I still throw a few back,
Talk a little smack,
When I'm feeling bullet proof,
So don't double, dog dare me now,
Cause I'd have to call your bluff,
I ain't as good as I once once was,
But I'm as good once, as I ever was.

May not be good as I once was,
But I'm as good once, as I ever was .")
  #song 286 (two-hundred-eighty-six)
  Song.create!(artist:  "Rascal Flatts",
               title: "Bless The Broken Road",
               rank: 286,
               copyright: "BMG Rights Management US, LLC",
               writers: "Jeff Hanna, Marcus Hummon, Robert E. Boyd",
               year: 2004,
               lyrics: "I set out on a narrow way many years ago
Hoping I would find true love along the broken road
But I got lost a time or two
Wiped my brow and kept pushing through

I couldn't see how every sign pointed straight to you
That every long lost dream lead me to where you are
Others who broke my heart, they were like northern stars
Pointing me on my way into your loving arms

This much I know is true
That God blessed the broken road
That led me straight to you
Yes it did

I think about the years I spent just passing through
I'd like to have the time I lost and give it back to you
But you just smile and take my hand
You've been there you understand
It's all part of a grander plan that is coming true

Every long lost dream led me to where you are
And others who broke my heart they were like northern stars
Pointing me on my way into your loving arms
This much I know it's true
That God blessed the broken road
That led me straight to you
Yeah

And now I'm rolling home
Into my lover's arms
This much I know is true
That God blessed the broken road
That led me straight to you

That God blessed the broken road
Ooh, ooh
That led me straight to you .")
  #song 287 (two-hundred-eighty-seven)
  Song.create!(artist:  "Sugarland",
               title: "Something More",
               rank: 287,
               copyright: "Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Greater Good Songs, BMG Rights Management US, LLC",
               writers: "Jennifer Nettles, Kristen Hall, Kristian Bush",
               year: 2004,
               lyrics: "Monday, hard to wake up
Fill my coffee cup, I'm out the door
Yeah the freeway, standing still today
Is gonna make me late, and that's for sure
I'm running out of gas and out of time
Never gonna make it there by nine

There's gotta be something more
Gotta be more than this
I need a little less hard time
I need a little more bliss
I'm gonna take my chances
Taking a chance I might
Find what I'm looking for
There's gotta be something more

Five years and there's no doubt
That I'm burnt out, I've had enough
So now boss man, here's my two weeks
I'll make it short and sweet, so listen up
I could work my life away, but why?
I got things to do before I die

There's gotta be something more
Gotta be more than this
I need a little less hard time
I need a little more bliss
I'm gonna take my chances
Taking a chance I might
Find what I'm looking for
There's gotta be something more

Some believe in destiny, and some believe in fate
I believe that happiness is something we create
You best believe that I'm not gonna wait
'Cause there's gotta be something more

I get home 7:30, the house is dirty, but it can wait
Yeah, cause right now I need some downtime
To drink some red wine and celebrate
Armageddon could be knocking at my door
But I ain't gonna answer that's for sure
There's gotta be something more! .")
  #song 288 (two-hundred-eighty-eight)
  Song.create!(artist:  "Rascal Flatts",
               title: "Fast Cars And Freedom",
               rank: 288,
               copyright: "Reservoir Media Management Inc, BMG Rights Management US, LLC",
               writers: "Gary Levox, Neil Thrasher, Wendell Mobley, Wendell Lee Mobley",
               year: 2004,
               lyrics: "
Starin' at you takin' off your makeup
Wondering why you even put it on
I know you think you do but, baby, you don't need it
Wish that you could see what I see it when it's gone
I see a dust trail following an old red Nova
Baby blue eyes, your head on my shoulder

Wait, baby, don't move, right there it is
T-shirt hanging off a dogwood branch
That river was cold, but we gave love a chance
Yeah, yeah, to me
You don't look a day over fast cars and freedom
That sunset river bank first time feeling

Yeah, smile and shake your head as if you don't believe me
I'll just sit right here and let you take me back
I'm on that gravel road, look at me
On my way to pick you up; you're standing on the front porch
Looking just like that, remember that?

I see a dust trail following an old red Nova
Baby blue eyes, your head on my shoulder

I see a dust trail following an old red Nova
Baby blue eyes, your head on my shoulder
You don't look a day over fast cars and freedom
That sunset river bank first time feeling .")
  #song 289 (two-hundred-eighty-nine)
  Song.create!(artist:  "Josh Gracin",
               title: "Nothin' To Lose",
               rank: 289,
               copyright: "Universal Music Publishing Group",
               writers: "Kevin Stuart James Savigar, Marcel Francois Chagnon",
               year: 2004,
               lyrics: "It was noon time, down time, break time
Summertime, Miller Time, anytime
She was looking pretty fine
A red light, green light, go
All fired upside downtown
She was shaking me all around
I was tuned in, going nowhere, second wind
Jonesing, drooling, feeling good, if you would

Oh yeah, by the way she moves
She's got me rolling in dirt in a white t-shirt
Breaker Breaker 1-9 she's a big ol' flirt
By now she's got me pretty tied up
Tied down, any way I chose
I got nothin' to lose

In like Flynn, I was
Looking for the win
Just trying anything, hey
Baby, where you been
All my life I've been looking for
Someone like you
Falling head over heels
Hey what can I do

Oh yeah, by the way she moves
She's got me rolling in dirt in a white t-shirt
Breaker Breaker 1-9 she's a big ol' flirt
By now she's got me pretty tied up
Tied down, any way I chose
I got nothin' to lose

Now I'm in the fast lane going 98
By now I know she can
Smooth operate me
I know now she's no goody two shoes
But hey I got myself nothin' to lose

Oh yeah, by the way she moves
She's got me rolling in dirt in a white t-shirt
Breaker Breaker 1-9 she's a big ol' flirt
By now she's got me pretty tied up
Tied down, any way I chose
I got nothin' to lose

Oh yeah, by the way she moves
She's got me rolling in dirt in a white t-shirt
Breaker Breaker 1-9 she's a big ol' flirt
By now she's got me pretty tied up
Tied down, any way I chose
I got nothin' to lose

Noon time, down time, break time
Summertime, Miller Time, any time
She was looking pretty fine
In like Flynn I was looking for the win
Just trying anything, hey baby where you been
All my life I been looking for someone like you
Falling head over heels hey what can I do
I got nothin' to lose, yeah .")
  #song 290 (two-hundred-ninety)
  Song.create!(artist:  "Sugarland",
               title: "Baby Girl",
               rank: 290,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Greater Good Songs, BMG Rights Management US, LLC",
               writers: "Kristian Bush, Jennifer Nettles, Kristen Hall, Simone Simonton, Troy Bieser, Robert Hartley",
               year: 2004,
              lyrics: "
They say this town
The stars stay up all night
Don't know, can't see 'em
For the glow of the neon lights
And it's a long way from here
To the place where the home fires burn
Well, it's two thousand miles and one left turn

Dear Mom and Dad,
Please send money, I'm so broke that it ain't funny.
I don't need much just enough to get me through.
Please don't worry cause I'm alright
I'm playing here at the bar tonight.
This time I'm gonna make our dreams come true
Well I love you more than anything in the world.
Love,
Your baby girl

Black top, blue sky
Big town full of little white lies
Everybody's your friend, you can never be sure
They promise fancy cars and diamond rings
All sorts of shiny things
Girl, you'll remember what your knees are for

Dear Mom and Dad,
Please send money, I'm so broke that it ain't funny.
I don't need much just enough to get me through.
Please don't worry cause I'm alright
I'm playing here at the bar tonight.
This time I'm gonna make our dreams come true
Well I love you more than anything in the world.
Love,
Your baby girl

I know that I'm on my way
I can tell every time I play
And I know it's worth all the dues I pay
When I can write to you and say

Dear Mom and Dad,
I'll send money, I'm so rich that it ain't funny
It ought ta be more than enough to get you through
Please don't worry cause I'm alright
I'm staying here at the Ritz tonight
What ta ya know we made our dreams come true!
And there are fancy cars and diamond rings
But you know that they don't mean a thing
They all add up to nothing compared to you
Well, remember me in ribbons and curls
I still love you more than anything in the world.
Love,
Your baby girl
Baby girl
Baby girl .")
  #song 291 (two-hundred-ninety-one)
  Song.create!(artist:  "Keith Urban",
               title: "Making Memories Of Us",
               rank: 291,
               copyright: "BMG Rights Management US, LLC",
               writers: "Rodney Crowell",
               year: 2004,
               lyrics: "I'm gonna be here for you baby
And I'll be a man of my own word
Speak the language in a voice that you have never heard
I wanna sleep with you forever
And I wanna die in your arms
In a cabin by a meadow where the wild bees swarm

And I'm gonna love you like nobody loves you
And I'll earn your trust making memories of us

I wanna honor your mother
And I wanna learn from your paw
I wanna steal your attention like a bad outlaw
And I wanna stand out in a crowd for you
A man among men
I wanna make your world better than it's ever been

And I'm gonna love you like nobody loves you
And I'll earn your trust making memories of us

We'll follow the rainbow
Wherever the four winds blow
And there'll be a new day
Comin' your way
I'm gonna be here for you from now on
This you know somehow
You've been stretched to the limits but it's alright now

And I'm gonna make you a promise
If there's life after this
I'm gonna be there to meet you with a warm, wet kiss
Yes I am

And I'm gonna love you like nobody loves you
And I'll earn your trust makin' memories of us
Ohhh
And I'm gonna love you like nobody loves you
And I'll win your trust makin memories of us
Mmmmm
Ohhhhhhhh Oh Baby Mmmmmmm .")
  #song 292 (two-hundred-ninety-two)
  Song.create!(artist:  "Faith Hill",
               title: "Mississippi Girl",
               rank: 292,
               copyright: "Roba Music, Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, Reservoir Media Management Inc",
               writers: "John D. Rich, Adam Shoenfeld",
               year: 2005,
               lyrics: "Yeah

Well, it's a long way from Star, Mississippi
To the big stage I'm singing on tonight
And sometimes the butterflies still get me
When I'm in the spotlight

And some people seem to think that I've changed
That I'm different than I was back then
But in my soul, I know that I'm the same way
That I've really always been

'Cause a Mississippi girl don't change her ways
Just 'cause everybody knows her name
Ain't big headed from a little bit of fame
I still like wearing my old ball cap
Riding my kids around piggy back
They might know me all around the world
But, y'all, I'm still a Mississippi girl

Woo!

Well, I spent a few weeks in California
They put my face on the big movie screen
But that don't mean I've forgotten where I came from
That's just me chasing dreams

'Cause a Mississippi girl don't change her ways
Just 'cause everybody knows her name
Ain't big headed from a little bit of fame
I still like wearing my old ball cap
Riding my kids around piggy back
They might know me all around the world
But, y'all, I'm still a Mississippi girl

Woo!

'Cause a Mississippi girl don't change her ways
Just 'cause everybody knows her name
Ain't big headed from a little bit of fame
Still like wearing my old ball cap
Riding my kids around piggy back
They might know me all around this world
But, y'all, I'm still a Mississippi, girl

Mississippi girl!
Oh oh
Mississippi girl!
Mississippi girl!
Yeah, yeah, oh
Oh oh oh oh oh
Mississippi girl!
Na,
Na .")
  #song 293 (two-hundred-ninety-three)
  Song.create!(artist:  "Montgomery Gentry",
               title: "Gone",
               rank: 293,
               copyright: "Warner/Chappell Music, Inc, Sony/ATV Music Publishing LLC, BMG Rights Management US, LLC, Round Hill Music Big Loud Songs",
               writers: "Bob Dipiero, Jeffrey Steele ",
               year: 2004,
               lyrics: "This ain't no temporary, typical, tearful good-bye, uh uh uh
This ain't no breakin' up and wakin' up and makin' up one more time, uh uh uh
This is gone (gone) gone (gone) gone (gone) gone

Gone like a freight-train, gone like yesterday
Gone like a soldier in the civil war, bang bang
Gone like a '59 Cadillac
Like all the good things that ain't never coming back
She's gone (gone) gone (gone) gone (gone) gone, she's gone

This ain't no give it time, I'm hurtin' but maybe we can work it out, uh uh uh
Won't be no champagne, red rose, romance, second chance, uh uh uh
This is gone (gone) gone (gone) gone (gone) gone

Gone like a freight-train, gone like yesterday
Gone like a soldier in the civil war, bang bang
Gone like a '59 Cadillac
Like all the good things that ain't never coming back
She's gone (gone) gone (gone) gone (gone) gone
She's gone

She's gone (gone) gone (gone) gone (gone) gone, she's gone

Gone like a freight-train, gone like yesterday
Gone like a soldier in the civil war, bang bang
Gone like a '59 Cadillac
Like all the good things that ain't never coming back
She's gone (gone) she's gone (gone) she's gone (gone) she's gone
She's gone

Gone like a freight-train, gone like yesterday
Gone like a soldier in the civil war, bang bang
Gone like a '59 Cadillac
Like all the good things
Well, she's gone

Long gone, done me wrong
Never comin' back, my baby's gone
Lonely at home, sittin' all alone
She's packed her bags and now she's gone
Never comin' back, she's gone
No no never, no no never, no never comin' back .")
  #song 294 (two-hundred-ninety-four)
  Song.create!(artist:  "Brad Paisley",
               title: "Mud On The Tires",
               rank: 294,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Brad Paisley, Chris Dubois",
               year: 2003,
               lyrics: "I've got some big news
The bank finally came through
And I'm holdin' the keys to a brand new Chevrolet
Have you been outside, it sure is a nice night
How about a little test drive
Down by the lake?

There's a place I know about where the dirt road runs out
And we can try out the four-wheel drive
Come on now what do you say
Girl, I can hardly wait to get a little mud on the tires.

'Cause it's a good night
To be out there soakin' up the moonlight
Stake out a little piece of shore line
I've got the perfect place in mind.
It's in the middle of nowhere, only one way to get there
You got to get a little mud on the tires.

Moonlight on a duck blind
Catfish on a trot line
Sun sets about nine this time of year
We can throw a blanket down
Crickets singin' in the background
And more stars that you can count on a night this clear.

I tell you what we need to do
Is grab a sleepin' bag or two
And build us a little campfire
And then with a little luck we might just get stuck
Let's get a little mud on the tires.

'Cause it's a good night
To be out there soakin' up the moonlight
Stake out a little piece of shore line
I've got the perfect place in mind.
It's in the middle of nowhere, only one way to get there
You got to get a little mud on the tires.

And then with a little luck we might just get stuck
Let's get a little mud on the tires .")
  #song 295 (two-hundred-ninety-five)
  Song.create!(artist:  "Brooks & Dunn",
               title: "It's Getting Better All The Time",
               rank: 295,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Don Cook, Ronnie Bowman",
               year: 2004,
               lyrics: "I don't stop breathin' every time the phone rings
My heart don't race when someone's at my door
I've almost given up thinkin' you're ever gonna call
I don't believe in magic anymore
I just don't lie awake at night,
Askin' God to get you off my mind

It's gettin' better all the time,
It's gettin' better all the time

Yeah, I got to work on time again this mornin'
This old job is all that I got a left
And no one even noticed I'd been cryin'
But, at least I don't have whiskey on my breath
Yeah, I think I'm gonna make it
'Cause God won't make a mountain I can't climb

It's gettin' better all the time,
It's gettin' better all the time

God, I hope you're happy
Girl, I wish you well
I just might get over you
You can't ever tell

I always thought that I'd do somethin' crazy
If I ever saw you out with someone else
But when the moment came last night,
I couldn't say a word
I stood there in the dark hall by myself
Yeah, I could have said a million things
But all I did was keep it locked inside

It's gettin' better all the time .")
  #song 296 (two-hundred-ninety-six)
  Song.create!(artist:  "Kenny Chesney",
               title: "Anything But Mine",
               rank: 296,
               copyright: "Peermusic Publishing",
               writers: "Scooter Carusoe",
               year: 2004,
               lyrics: "Walking along beneath the lights of that miracle mile
Me and Mary making our way into the night
You can hear the cries from the carnival rides, the pinball
Bells, and the skee ball signs
Watching the summer sun fall out of sight

There's a warm wind coming in from off of the ocean
Making it's way past the hotel walls to fill the street
Mary is holding both of her shoes in her hand
Said she likes to feel the sand beneath her feet

And in the morning I'm leaving, making my way back to Cleveland
So tonight I hope that I will do just fine
And I don't see how you could ever be
Anything but mine

There's a local band playing at the seaside pavilion
And I got just enough cash to get us in
And as we are dancing, Mary's wrapping her arms around me

And I can feel the sting of summer on my skin

In the midst of the music
I tell her I love her
And we both laugh, cause we know it isn't true
Oh, but Mary, there's a summer drawing to an end tonight
And there's so much that I long to do to you

But in the morning I'm leaving, making my way back to Cleveland
So tonight I hope that I will do just fine
And I don't see how you could ever be
Anything but mine

And in the morning I'm leaving, making my way back to Cleveland
So tonight I hope that I will do just fine
And I don't see how you could ever be
Anything but mine

Mary, I don't see how you could ever be
Anything but mine

In the morning I'm leaving, making my way back to Cleveland
So tonight I hope that I will do just fine
Hey, I don't see how you could ever be
Anything but mine .")
  #song 297 (two-hundred-ninety-seven)
  Song.create!(artist:  "Jo Dee Messina",
               title: "My Give A Damn's Busted",
               rank: 297,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Mike Curb Music, BMG Rights Management US, LLC",
               writers: "Tony Martin, Tom Shapiro, Joe Diffie",
               year: 2005,
               lyrics: "Well you filled up my head
With so many lies.
You twisted my heart
'til something snapped inside.
I'd like to give it one more try
But my give a damn's busted.

You can crawl back home
Say you were wrong,
Stand out in the yard
And cry all night long.
Go ahead and water the lawn.
My give a damn's busted.

I really want to care,
I want to feel somethin'
Let me dig a little deeper...
Naw...
Sorry...
Nothin'

You can say you've got issues.
You can say you're a victim.
It's all your parents fault,
I mean, after all you didn't pick 'em
Maybe somebody else's got time to listen.
My give a damn's busted.

Well your therapist says
It was all a mistake
A product of the prozac
And your co-dependent ways
So ... who's your enabler these days?
My give a damn's busted.

I really want to care,
I want to feel somethin'
Let me dig a little deeper...
Naw...
Still nothin

It's a desperate situation,
No tellin' what you'll do.
If I don't forgive you,
You say your life is through.
Come on ... give me somethin' I can use.
My give a damn's busted.

Well, I really want to care
I want to feel somethin'
Let me dig a little deeper...
Naw, man...
Sorry

Just nothin'
No
You've really done it this time (haha)
My give a damn's busted .")
  #song 298 (two-hundred-ninety-eight)
  Song.create!(artist:  "Keith Urban",
               title: "You're My Better Half",
               rank: 298,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "John Shanks, Keith Urban",
               year: 2004,
               lyrics: "Car door slams, it's been a long day at work
I'm out on the freeway and I'm wondering if it's all worth
The price that I pay, sometimes it doesn't seem fair
I pull into the drive and you're standing there
And you look at me
And give me that come-here-baby smile
It's all gonna be alright
You take my hand
You pull me close and you hold me tight

And it's the sweet love that you give to me
That makes me believe we can make it through anything
'Cause when it all comes down
And I'm feeling like I'll never last
I just lean on you 'cause baby
You're my better half

They say behind every man is a good woman
But I think that's a lie
'Cause when it comes to you I'd rather have you by my side
You don't know how much I count you to help me
When I've given everything I got and I just feel like giving in
And you look at me
And give me that come-here-baby smile
It's all gonna be alright
You take my hand
Yeah you pull me close and you hold me tight

And it's the sweet love that you give to me
That makes me believe we can make it through anything
'Cause when it all comes down
And I'm feeling like I'll never last
I just lean on you 'cause baby
You're my better half

Well, you take my hand
Yeah you pull me close and I understand

It's the sweet love that you give to me
That makes me believe that we can make it through anything

Oh baby, it's the sweet love that you give to me
That makes me believe we can make it through anything
'Cause when it all comes down
And I'm feeling like I'll never last
I just lean on you 'cause baby
You're my better half

Oh, oh baby you're my better half
Oh, hey baby you're my better half .")
  #song 299 (two-hundred-ninety-nine)
  Song.create!(artist:  "Dierks Bentley",
               title: "Lot Of Leavin' Left To Do",
               rank: 299,
               copyright: "Sony/ATV Music Publishing LLC, Reservoir Media Management Inc",
               writers: "Brett Beavers, Deric Ruttan, Deric James Ruttan, Dierks Bentley",
               year: 2005,
               lyrics: "These old boots still got a lot of ground
They ain't covered yet
There's at least another million miles
Under these old bus treads
So if you think I'm gonnna settle down
I've got news for you
I still got a lot of leavin' left to do

And as long as there's a song
Left in this old guitar
This life I'm bound to lead
Ain't for the faint of heart
So you won't fall for me
If you know what's good for you
'Cause I still got a lot of leavin' left to do

I guess the Lord made me hard to handle
So lovin' me might be a long shot gamble
So before you go and turn me on
Be sure that you can turn me loose
'Cause I still got a lot of leavin' left to do

Girl, you look like you might be an angel
So I won't lie
I could love you like the devil
If you wanted me to tonight
And we could talk about forever for a day or two
But I still got a lot of leavin' left to do

I guess the Lord made me hard to handle
So lovin' me might be a long shot gamble
So before you go and turn me on
Be sure that you can turn me loose
'Cause I still got a lot of leavin' left to do

Yeah, I still got a leavin' left to do .")
  #song 300
  Song.create!(artist:  "Montgomery Gentry",
               title: "Something To Be Proud Of",
               rank: 300,
               copyright: "Ole MM, Ole Media Management Lp, BMG Rights Management US, LLC",
               writers: "Chris Wallin, Jeffery Steele",
               year: 2004,
               lyrics: "There's a story that my daddy tells religiously
Like clockwork every time he sees an opening
In a conversation about the way things used to be
Well I'd just roll my eyes and make a B-line for the door
But I'd always wind up starry-eyed, cross-legged on the floor
Hanging on to every word
Man, the things I heard
It was harder times and longer days
Five miles to school, uphill both ways
We were cane switch raised, and dirt floor poor
'Course that was back before the war
Yeah, your uncle and I made quite a pair
Flying F-15's through hostile air
He went down but they missed me by a hair
He'd always stop right there and say...

That's something to be proud of
That's a life you can hang your hat on
That's a chin held high as the tears fall down
A gut sucked in, a chest stuck out
Like a small town flag a-flyin'
Or a newborn baby cryin'
In the arms of the woman that you love
That's something to be proud of

Son graduate from college, that was mama's dream
But I was on my way to anywhere else when I turned 18
'cause when you gotta fast car you think you've got everything
I learned quick those GTO's don't run on faith
I ended up broken down in some town north of L.A.
Working maximum hours for minimum wage
Well, I fell in love, next thing I know
The babies came, the car got sold
I sure do miss that old hot rod
But you sure save gas in them foreign jobs
Dad, I wonder if I ever let you down
If you're ashamed how I turned out
Well, he lowered his voice, then he raised his brow
Said, let me tell ya right now

That's something to be proud of
That's a life you can hang your hat on
You don't need to make a million
Just be thankful to be workin'
If you're doing what you're able
And putting food there on the table
And providing for the family that you love
That's something to be proud of

And if all you ever really do is the best you can
Well, you did it man

That's something to be proud of
That's a life you can hang your hat on
That's a chin held high as the tears fall down
A gut sucked in, a chest stuck out
Like a small town flag a-flyin'
Or a newborn baby cryin'
In the arms of the woman that you love
That's something to be proud of
That's something to be proud of
Yeah, that's something to be proud of
That's something to be proud of
Now that's something to be proud of .")
  #song 301
  Song.create!(artist:  "Andy Griggs",
               title: "If Heaven",
               rank: 301,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Gretchen Peters",
               year: 2004,
               lyrics: "If heaven was an hour, it would be twilight
When the fireflies start their dancin on the lawn
And suppers on the stove, and mammas laughin
And everybodys workin day is done

If heaven was a town it would be my town
On a summer day in 1985
And everything i wanted was out there waitin
And everyone i loved was still alive

don't cry a tear for me now baby
There comes a time we all must say goodbye
And if that's what heavens made of
You know what I ain't afraid to die

If heaven was a pie it would be cherry
Cool and sweet and heavy on the top
And just one bite would satisfy your hunger
Thered always be enough for everyone

If heaven was a train it sure would be a fast one
To take this weary travler round the bend
And if heaven was a tear it'd be my last one
And you'd be in my arms again

don't cry a tear for me now baby
There comes a time we all must say goodbye
And if that's what heavens made of
You know what I ain't afraid to die .")
  #song 302
  Song.create!(artist:  "Sara Evans",
               title: "A Real Fine Place To Start",
               rank: 302,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "George Ducas, Radney Foster",
               year: 2005,
               lyrics: "I'm gonna do it darlin'
I could waste time tryin' to figure it out
I'm jumpin' anyhow
I've never been this far

Didn't know love could run so deep
Didn't know I'd loose this much sleep

Holdin' you close
Chasin' that moon
Spendin' all night lovin' just who you are
Sparks flyn' in the dark

Shootin' out lights
Runnin' down dreams
Figurin' out what love really means
Baby givin' you my heart
Is a real fine place to start

Somethin' is goin' on
I can't explain but sure can touch
It's callin' both of us
Stronger than any fear or doubt
It's changin' everything I see
It's changin' you it's changin' me

Holdin' you close
Chasin' that moon
Spendin' all night lovin' just who you are
Sparks flyin' in the dark

Shootin' out lights
Runnin' down dreams
Figurin' out what love really means
Baby givin' you my heart
Is a real fine place to start

Right here right now's
The perfect spot
The perfect time
The perfect moment
When your skin is next to mine

Yeah, yeah

Holdin' you close
Chasin' that moon
Spendin' all night lovin just who you are
Sparks flyn' in the dark

Shootin' out lights
Runnin' down dreams
Figurin' out just what love really means
Baby givin' you my heart
Is a real fine place, a real fine place to start

Oh yeah
Oh oh ooh .")
  #song 303
  Song.create!(artist:  "George Strait",
               title: "You'll Be There",
               rank: 303,
               copyright: "Universal Music Publishing Group, Nettwerk One Music (canada)ltd",
               writers: "Cory Mayo",
               year: 2005,
               lyrics: "Hope is an anchor and love is a ship, time is the ocean, and life is a trip
You don't know where you're going, 'till you know where you're at
And if you can't read the stars, well you better have a map
A compass and a conscience, so you don't get lost at sea
Or on some ol' lonely island, where no one wants to be

From the beginning of creation, I think our maker had a plan
For us to leave these shores and sail beyond the sand
And let the good light guide us through the waves and the wind
To the beaches in a world where we have never been
And we'll climb up on a mountain, y'all we'll let our voices ring
Those who've never tried it, they'll be the first to sing

Whoa, my, my
I'll see you on the other side
If I make it
And it might be a long hard ride
But I'm gonna take it
Sometimes it seems that I don't have a prayer
Let the weather take me anywhere
But I know that I wanna go
Where the streets are gold
'Cause you'll be there
Oh, my, my

You don't bring nothing with you here
And you can't take nothing back
I ain't never seen a hearse, with a luggage rack
So I've torn my knees up prayin'
Scarred my back from fallin' down
Spent so much time flying high, till I'm face first in the ground
So if you're up there watchin' me, would you talk to God and say,
Tell Him I might need a hand to see you both someday

Whoa, my, my
So I'll see you on the other side
If I make it
And it might be a long hard ride
But I wanna take it
Sometimes it seems that I don't have a prayer
Let the weather take me anywhere
But I know that I wanna go
Where the streets are gold
'Cause you'll be there
Oh, my, my
'Cause you'll be there
Oh, my ,my .")
  #song 304
  Song.create!(artist:  "Joe Nichols",
               title: "What's A Guy Gotta Do",
               rank: 304,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Don Sampson, Joe Nichols, Kelley Lovelace",
               year: 2004,
               lyrics: "What's, a, guy gotta do to get a girl in this town
Don't want to be alone when the sun goes down
Just a sweet little somethin' to put my arms around
What's a guy gotta do to get a girl in this town

Well ask anybody I'm a pretty good guy
And the looks decent wagon didn't pass me by
There ain't nothin' in my past that I'm tryin' hard to hide
And I don't understand why I gotta wonder why

What's a guy gotta do to get a girl in this town
Don't want to be alone when the sun goes down
Just a sweet little somethin' to put my arms around
What's a guy gotta do to get a girl in this town

Cruise all around the right parking lots
Little time gets killed a lot a bull gets shot
One who'll think I'm kinda cute and laugh at every joke I got
When I get to thinkin' maybe she's athinkin' maybe not

What's a guy gotta do to get a girl in this town
Don't want to be alone when the sun goes down
Just a sweet little somethin' to put my arms around
What's a guy gotta do to get a girl in this town

Had an old man tell me 'Boy if you were smart
You'd hit the produce isle at the Super Wal-Mart'
So I bumped into a pretty girl's shopping cart
But all I did was break her eggs and bruise her artichoke hearts

What's a guy gotta do to get a girl in this town
Don't want to be alone when the sun goes down
Just a sweet little somethin' to put my arms around
What's a guy gotta do to get a girl in this town

What's a guy gotta do to get a girl in this town .")
  #song 305
  Song.create!(artist:  "Brooks & Dunn",
               title: "Play Something Country",
               rank: 305,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Ronnie Dunn, Terry Mcbride",
               year: 2005,
               lyrics: "Yes, she blew through the door like TNT,
Put her hand on her hip, pointed a finger at me.
Said: I'm a whiskey drinkin', cowboy chasin', helluva time.
I like Kenny, Keith, Allan and Patsy Cline.
I'm a full grown Queen Bee lookin' for honey.
Ha-ooh-hoo, aw, play somethin' country.

Yeah, the band took a break,
The DJ played P Diddy.
She said: I didn't come here to hear,
Somethin' thumpin' from the city.
Said: I, I shaved my legs, I paid my money.
Ha-ooh-hoo, play somethin' country.
Ha-ooh-hoo, aw, play somethin' country.

Crank up the band, play the steel guitar.
Hank it up a little, let's rock this bar.
Threw back a shot; yelled: I'm a George Strait junkie.
Ha-ooh-hoo, play somethin' country.
Ha-ooh-hoo, aw, play somethin' country.

Yeah, the bartender yelled: why'all, it's closin' time.
She got this wild look on her face,
An' said: Your truck or mine,
I know a place down the road,
It's kinda funky.
Ha-ooh-hoo, all out in the country.
Ha-ooh-hoo, now, play somethin' country.

Crank up the band, play the steel guitar.
Hank it up a little, let's rock this bar.
Threw back a shot; yelled: I'm a George Strait junkie.
Ah-ooh-hoo, play somethin' country.
Ha-ooh-hoo, now, play somethin' country.

Crank up the band, play the steel guitar.
Hank it up a little, let's rock this bar.
Threw back a shot; yelled: I'm a George Strait junkie.
Ah-ooh-hoo, play somethin' country.
Ha-ooh-hoo, play somethin' country .")
  #song 306
  Song.create!(artist:  "Jamie O'Neal",
               title: "Somebody's Hero",
               rank: 306,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, Reservoir Media Management Inc, BMG Rights Management US, LLC",
               writers: "Ed Hill, Jamie O'neal, Shaye Smith",
               year: 2005,
               lyrics: "She's never pulled anybody from a burning building
She's never rocked central park to a half a million
Fans, screaming out her name, she's never hit a shot to win the game
She's never left her footprints on the moon
She's never made a solo hot air balloon ride
Around the world, no, she's just your everyday average girl (but)

She's somebody's hero
A hero to her baby with a skinned up knee
A little kiss is all she needs
The keeper of the cheerios
The voice that brings snow white to life
Bedtime stories every night
That smile lets her know
She somebody's hero

She didn't get a check every week like a nine to fiver
But she's been a waiter, and a cook and taxi driver
For 20 years, there at home, until the day her girl was grown
Giving all her love to her was her life's ambition
But now her baby's movin' on, and she'll soon be missin'
Her, but not today, those are tears of joy runnin' down her face

She's somebody's hero
A hero to her daughter in the wedding dress
She gave her wings to leave the nest
It hurts to let her baby go
Down the aisle she walks right by
Looks back into her mother's eyes
That smile lets her know
She somebody's hero

30 years have flown right past
Her daughter's starin' at all the photographs
Of her mother, and she wishes she could be like that
Oh, but she already is

She's somebody's hero
A hero to her mother in a rockin' chair
She runs a brush through her silver hair
The envy of the nursing home
She drops by every afternoon
Feeds her momma with a spoon
That smile lets her know
Her mother's smile lets her know
She's somebody's hero .")
  #song 307
  Song.create!(artist:  "Brad Paisley",
               title: "Alcohol",
               rank: 307,
               copyright: "EMI Music Publishing, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Brad Paisley",
               year: 2005,
               lyrics: "I can make anybody pretty
I can make you believe any lie
I can make you pick a fight
With somebody twice
Your size

Well I've been known to cause a few breakups
And I've been known to cause a few births
I can make you new friends
Or get you fired from work.

And since the day I left Milwaukee
Lyncheburg Bordeaux, France
Been making the bars
With lots of big money
And helping white people dance
I got you in trouble in high school
And college now that was a ball
You had some of the best times
You'll never remember with me
Alcohol, alcohol

I got blamed at your wedding reception
For your best man's embarrassing speech
And also for those naked pictures of you at the beach
I've influenced kings and world leaders
I helped Hemingway write like he did
And I'll bet you a drink or two that I can make you
Put that lampshade on your head

And since the day I left Milwaukee
Lyncheburg Bordeaux, France
Been making a fool out of folks
Just like you
And helping white people dance
I am medicine and I am poison
I can help you up or make you fall
You had some of the best times
You'll never remember with me
Alcohol, alcohol

And since the day I left Milwaukee
Lyncheburg Bordeaux, France
Been making the bars
With lots of big money
And helping white people dance
I got you in trouble in high school
And college now that was a ball
You had some of the best times
You'll never remember with me
Alcohol, alcohol .")
  #song 308
  Song.create!(artist:  "Craig Morgan",
               title: "Redneck Yacht Club",
               rank: 308,
               copyright: "Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Steve Williams, Thom Sheppard",
               year: 2005,
               lyrics: "I'm meetin' my buddies out on the lake
We're headed out to a special place (We love!)
That just a few folks know
There's no signin' up, no monthly dues
Take your Johnson, your Mercury or your Evinrude and fire it up
Meet us out at party cove
Come on in the waters fine
Just idle on over, and toss us a line

Basstrackers, Bayliners and a party barge,
Strung together like a floatin' trailer park
Anchored out and gettin' loud all summer long
Side by side there's five houseboat front porches
Astroturf, lawn chairs and tiki torches
Regular joes, rockin' the boat that's us
The Redneck Yacht Club

Bermuda's, flip-flops and a tank top tan
He popped his first top at 10 a.m., (That's Bob!)
He's our president
We're checkin' out the girls on the upper deck
Rubbin' in the 15 S-P-F, (Its hot! Whoo!)
Everybody's jumpin' in
Later on when the sun goes down
We'll pull out the jar and that old guitar
And pass 'em around

Basstrackers, Bayliners and a party barge,
Strung together like a floatin' trailer park
Anchored out and gettin' loud all summer long
Side by side there's five houseboat front porches
Astroturf, lawn chairs and tiki torches
Regular joes, rockin' the boat that's us
The Redneck Yacht Club

When the party's over and we're all alone
We'll be makin' waves, in a no wake zone

Basstrackers, Bayliners and a party barge,
Strung together like a floatin' trailer park
Anchored out and gettin' loud all summer long
Side by side there's five houseboat front porches
Astroturf, lawn chairs and tiki torches
Regular joes, rockin' the boat that's us
The Redneck Yacht Club

Redneck Yacht Club
(Na, na, na)
Redneck Yacht Club
(Na, na, na) .")
  #song 309
  Song.create!(artist:  "LeAnn Rimes",
               title: "Probably Wouldn't Be This Way",
               rank: 309,
               copyright: "Universal Music Publishing Group",
               writers: "John Davis Kennedy, Tammi Lynn Kidd",
               year: 2005,
               lyrics: "Got a date a week from Friday with a preacher's son
Everybody says he's crazy; I'll have to see

I finally moved to Jackson when the summer came
I won't have to pay that boy to rake my leaves

I'm probably going on and on
It seems I'm doing more of that these days

I probably wouldn't be this way
I probably wouldn't hurt so bad
I never pictured every minute without you in it
Oh, you left so fast
Sometimes I see you standing there
Sometimes it's like I'm losing touch
Sometimes I feel that I'm so lucky to have had the chance to love this much
God gave me a moment's grace
'Cause if I never see your face
I probably wouldn't be this way

Mama says that I just shouldn't speak to you
Susan says that I should just move on

You ought to see the way these people look at me
When they see me 'round here talking to this stone

Everybody thinks I've lost my mind
But I just take it day by day

I probably wouldn't be this way
I probably wouldn't hurt so bad
I never pictured every minute without you in it
Oh, you left so fast
Sometimes I see you standing there
Sometimes I feel an angel's touch
Sometimes I feel that I'm so lucky to have had the chance to love this much
God gave me a moment's grace
'Cause if I never see your face
I probably wouldn't be this way

I probably wouldn't be this way

Got a date a week from Friday with a preacher's son
Everybody says I'm crazy
Guess I'll have to see .")
  #song 310
  Song.create!(artist:  "Trace Adkins",
               title: "Songs About Me",
               rank: 310,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Reservoir Media Management Inc, BMG Rights Management US, LLC",
               writers: "Ed Hill, Shaye Smith",
               year: 2005,
               lyrics: "I met a guy on the red eye
He spotted my guitar
And said what do you do?
I said, I sing for a living,
Country music mixed with
A little rock and a little blues
He said I'm sorry
But I've never been crazy
'Bout that twang and trains and hillbilly thing
What ever made you want to sing stuff like that?
I just looked at him and laughed and said

'Cause it's songs about me
And who I am
Songs about loving and living
And good hearted women and family and God
Yeah they're all just
Songs about me
Songs about me

So I offered him tickets
I said you'll see what I mean
If you show up tonight
He said I doubt you'll change my opinion
I'll be kind of busy, but hey man, I'll try

Then later on when we finished our songs
About scars and cars and broken hearts
I saw him, he was standing there
Right next to the stage
And he shouted
Man you were right
It was like you sang those

Songs about me
And who I am
Songs about loving and living
And good hearted women and family and God
Yeah they're all just
Songs about me
Songs about me

So I'll just keep on singing
'Til I hear the whole world singing those

Songs about me
And who I am
Songs about loving and living
And good hearted women and family and God
Yeah they're all just
Songs about me
Songs about me .")
  #song 311
  Song.create!(artist:  "Blake Shelton",
               title: "Some Beach",
               rank: 311,
               copyright: "Roba Music, Sony/ATV Music Publishing LLC, Ole MM, Ole Media Management Lp",
               writers: "Rory Lee N Feek, Paul Lester Overstreet",
               year: 2004,
               lyrics: "Driving down the interstate
Running thirty minutes late
Singing Margaritaville and minding my own
Some foreign car drivin' dude with a road rage attitude
Pulled up beside me talking on his cell phone
He started yelling at me like I did something wrong
He flipped me the bird an' then he was gone

Some beach
Somewhere
There's a big umbrella casting shade over an empty chair
Palm trees are growing and warm breezes blowing
I picture myself right there
On some beach, somewhere

I circled the parking lot trying to find a spot
Just big enough I could park my old truck
A man with a big cigar was getting into his car
I stopped and I waited for him to back up
But from out of no where a Mercedes Benz
Came cruisin' up and whipped right in

Some beach
Somewhere
There's no where to go when you got all day to get there
There's cold margaritas and hot Senoritas smiling with long dark hair
On some beach
Somewhere

I sat in that waiting room
It seemed like all afternoon
The nurse finally said doc's ready for you
You're not gonna feel a thing we'll give you some Novocaine
That tooth will be fine in a minute or two
Then he stuck that needle down deep in my gum
And he started drilling before I was numb

Some beach
Somewhere
There's a beautiful sunset burning up the atmosphere
There's music and dancing and lovers romancing
In the salty evenin' air
On some beach
Somewhere
On some beach, somewhere .")
  #song 312
  Song.create!(artist:  "Keith Urban",
               title: "Better Life",
               rank: 312,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Allan Pineda, Jaime Gomez, Will Adams, Stacy Ferguson, Printz Board",
               year: 2004,
               lyrics: "Friday night and the moon is high
I'm wide awake just watchin' you sleep
And I promise you you're gonna have
More than just the things that you need
We ain't got much now; we're just starting out
But I know somehow Paradise is coming

Someday, baby
You and I are gonna be the ones
Good luck's gonna shine
Someday, baby
You and I are gonna be the ones
So hold on, we're headed for a better life

Oh, now there's a place for you and me
Where we can dream as big as the sky
I know it's hard to see it now
But, baby, someday we're gonna fly
This road we're on, you know it might be long
But my faith is strong
It's all that really matters

Someday, baby
You and I are gonna be the ones
Good luck's gonna shine
Someday, baby
You and I are gonna be the ones
So hold on, we're headed for a better life

So hold on, hold on
C'mon, baby, hold on
Yeah, we're gonna have it all, and oh

Someday, baby
You and I are gonna be the ones
Good luck's gonna shine
Someday, baby
You and I are gonna be the ones
So hold on, we're headed for a better life

Someday, baby
You and I are gonna be the ones
Good luck's gonna shine
Someday, baby
You and I are gonna be the ones
So hold on, we're headed for a better life

A better life now, oh ho, a better life
Hey, we're gonna leave this all behind us, baby, wait and see
We're headed for a better life, you and me
We're gonna break the chains that bind and finally we'll be free
We're gonna be the ones that have it all, you and me
Just hold on tight now, baby, whoa .")
  #song 313
  Song.create!(artist:  "Josh Gracin",
               title: "Stay With Me (Brass Bed)",
               rank: 313,
               copyright: "Sony/ATV Music Publishing LLC, Reservoir Media Management Inc, BMG Rights Management US, LLC",
               writers: "Brett James, Jedd Hughes, Terry Mcbride",
               year: 2004,
               lyrics: "Baby, the clock on the wall is lying
It's not really that late
It's too cold outside to be walkin' around
In the streets of this town
Anywhere, anything you have to be can wait

Why don't you (baby)stay with me
Share all your secrets tonight
We can make believe
Morning sun never will rise
Come and lay you're head on this big brass bed
We'll be all right
As long as you stay with me, yeah

Baby, there's just no use hiding
The way that I'm feeling right now
With you standing there baby I swear
I can't help but stare
Girl, you're wearin' me out, wearin' me out

Baby, stay with me
Share all your secrets tonight
We can make believe
Morning sun never will rise
Come and lay you're head on this big brass bed
We'll be all right
As long as you stay with me, yeah

Baby don't go
It looks like it's starting to rain
And it's so warm here in this apartment
Wrapped up in this blanket so stay

Stay with me
Share all your secrets tonight
We can make believe
Morning sun never will rise
Come and lay you're head on this big brass bed
We'll be all right
As long as you stay
I'm all right as long as you stay with me

Stay with me .")
  #song 314
  Song.create!(artist:  "LeAnn Rimes",
               title: "Nothin' Bout Love Makes Sense",
               rank: 314,
               copyright: "Sony/ATV Music Publishing LLC, Round Hill Music Big Loud Songs",
               writers: "Gary Burr, Joel Feeney, Kylie Sackley",
               year: 2005,
               lyrics: "Like a cloud full of rain shouldn't hang in the sky
Ice shouldn't burn or a bumble bee fly
If you feel so happy, then why do you cry
Oh, nothin' 'bout love makes sense

Like an ocean liner shouldn't float on the sea
A pearl in an oyster or a circus of fleas
Someone so perfect can't be fallin' for me
Oh nothin' 'bout love makes sense

Nothin' 'bout love is less than confusin'
You can win when you're losin'
Stand when you're fallin'
I can't figure it out
Nothin' 'bout love can make an equation
Nothin' short of amazin'
Wish I could explain it
But I didn't know how

The way that we dance
The reason we dream
That big Italian tower
Oh, how does it lean
Somethin' so strong shouldn't make me this weak
Oh, nothin' 'bout love makes sense

Nothin' 'bout love is less than confusin'
You can win when you're losin'
Stand when you're fallin'
I can't figure it out
Nothin' 'bout love can make an equation
Nothin' short of amazin'
Wish I could explain it
But I didn't know how

Like the lights of Las Vegas glowin' out of the sand
A jumbo shrimp or a baby grand
How you touch my heart when you hold my hand
Oh, nothin' 'bout love makes sense

Oh, nothin' 'bout love makes sense
Oh, nothin' 'bout love makes sense
Oh, nothin' 'bout love makes sense .")
  #song 315
  Song.create!(artist:  "Tim McGraw",
               title: "Live Like You Were Dying",
               rank: 315,
               copyright: "Warner/Chappell Music, Inc, Round Hill Music Big Loud Songs, BMG Rights Management US, LLC",
               writers: "Craig Michael Wiseman, James Timothy Nichols, Tim Nichols",
               year: 2004,
               lyrics: "He said
'I was in my early forties
With a lot of life before me
And a moment came that stopped me on a dime
I spent most of the next days
Looking at the x-rays
Talkin' 'bout the options
And talkin' 'bout sweet time'
I asked him
'When it sank in
That this might really be the real end
How's it hit you
When you get that kind of news?
Man, what'd you do?'

He said
'I went skydiving
I went Rocky Mountain climbing
I went 2.7 seconds on a bull named Fumanchu
And I loved deeper
And I spoke sweeter
And I gave forgiveness I'd been denying'
And he said
'Someday I hope you get the chance
To live like you were dying'

He said
'I was finally the husband
That most of the time I wasn't
And I became a friend a friend would like to have
And all of a sudden going fishin'
Wasn't such an imposition
And I went three times that year I lost my dad
I finally read the Good Book, and I
Took a good, long, hard look
At what I'd do if I could do it all again
And then

I went skydiving
I went Rocky Mountain climbing
I went 2.7 seconds on a bull named Fumanchu
And I loved deeper
And I spoke sweeter
And I gave forgiveness I'd been denying'
And he said
'Someday I hope you get the chance
To live like you were dying
Like tomorrow was a gift
And you've got eternity
To think about
What you'd do with it
What could you do with it
What did I do with it?
What would I do with it?

Skydiving
I went Rocky mountain climbing
I went 2.7 seconds on a bull named Fumanchu
And I loved deeper
And I spoke sweeter
And I watched an eagle as it was flying'
And he said
'Someday I hope you get the chance
To live like you were dying
To live like you were dying
To live like you were dying .")
  #song 316
  Song.create!(artist:  "Alan Jackson",
               title: "Remember When",
               rank: 316,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Alan Jackson",
               year: 2003,
               lyrics: "Remember when I was young and so were you
And time stood still and love was all we knew
You were the first, so was I
We made love and then you cried
Remember when

Remember when we vowed the vows and walked the walk
Gave our hearts, made the start, it was hard
We lived and learned, life threw curves
There was joy, there was hurt
Remember when

Remember when old ones died and new were born
And life was changed, disassembled, rearranged
We came together, fell apart
And broke each other's hearts
Remember when

Remember when the sound of little feet
Was the music we danced to week to week
Brought back the love, we found trust
Vowed we'd never give it up
Remember when

Remember when thirty seemed so old
Now lookin' back, it's just a steppin' stone
To where we are, where we've been
Said we'd do it all again
Remember when

Remember when we said when we turned gray
When the children grow up and move away
We won't be sad, we'll be glad
For all the life we've had
And we'll remember when

Remember when
Remember when .")
  #song 317
  Song.create!(artist:  "Keith Urban",
               title: "You'll Think Of Me",
               rank: 317,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Ty Lacy, Darrell Brown, Dennis Matkosky",
               year: 2002,
               lyrics: "I woke up early this morning around four a.m.
With the moon shining bright as headlights on the interstate
I pulled the covers over my head and tried to catch some sleep
But thoughts of us kept keeping me awake
Ever since you found yourself in someone else's arms
I've been tryin' my best to get along
But that's okay
There's nothing left to say, but

Take your records, take your freedom
Take your memories I don't need 'em
Take your space and take your reasons
But you'll think of me
And take your cap and leave my sweater
'Cause we have nothing left to weather
In fact I'll feel a whole lot better
But you'll think of me, you'll think of me

I went out driving trying to clear my head
I tried to sweep out all the ruins that my emotions left
I guess I'm feeling just a little tired of this
And all the baggage that seems to still exist
It seems the only blessing I have left to my name
Is not knowing what we could have been
What we should have been
So

Take your records, take your freedom
Take your memories I don't need 'em
Take your space and take your reasons
But you'll think of me
And take your cap and leave my sweater
'Cause we have nothing left to weather
In fact I'll feel a whole lot better
But you'll think of me

Someday I'm gonna run across your mind
Don't worry, I'll be fine
I'm gonna be alright
While you're sleeping with your pride
Wishin' I could hold you tight
I'll be over you
And on with my life

So take your records, take your freedom
Take your memories I don't need 'em
And take your cap and leave my sweater
'Cause we have nothing left to weather
In fact I'll feel a whole lot better
But you'll think of me

So take your records, take your freedom
Take your memories I don't need 'em
Take your space and all your reasons
But you'll think of me
And take your cap and leave my sweater
'Cause we got nothing left to weather
In fact I'll feel a whole lot better
But you'll think of me, you'll think of me, yeah

And you're gonna think of me
Oh someday baby, someday .")
  #song 318
  Song.create!(artist:  "Kenny Chesney, Uncle Kracker",
               title: "When The Sun Goes Down",
               rank: 318,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Reservoir Media Management Inc, BMG Rights Management US, LLC",
               writers: "Brett James",
               year: 2004,
               lyrics: "Sun tan toes, ticklin' the sand,
Cold drink chillin' in my right hand,
Watchin' you sleep in the evening light,
Restin' up for a long long night.

'Cause when the sun goes down, we'll be groovin',
When the sun goes down we'll be feelin' alright,
When the sun sinks down, over the water,
Everything gets hotter when the sun goes down.

All day long, just taking it easy,
Laying in a hammock, where it's nice and breezy,
Been sleepin' off the night before, 'cause when the sun goes down,
We'll be back for more.

'Cause when the sun goes down, we'll be groovin',
When the sun goes down we'll be feelin' alright,
When the sun sinks down, over the water,
Everything gets hotter when the sun goes down.

This old guitar, and my dark sunglasses,
This sweet concoction is as smooth as molasses,
Nothing to do, but breath all day,
Until the big moon rises and its time to play.

'Cause when the sun goes down, we'll be groovin',
When the sun goes down we'll be feelin' alright,
When the sun sinks down, over the water,
Everything gets hotter when the sun goes down.

'Cause when the sun goes down, we'll be groovin',
When the sun goes down we'll be feelin' alright,
When the sun sinks down, over the water,
Everything gets hotter when the sun goes down.

'Cause when the sun goes down, we'll be groovin',
When the sun goes down we'll be feelin' alright,
When the sun sinks down, over the water,
Everything gets hotter when the sun goes down.

When the sun goes down, we'll be groovin',
When the sun goes down, we'll be feelin' alright,
When the sun sinks down, over the water,
She thinks crackers sexy when the sun goes down .")
  #song 319
  Song.create!(artist:  "John Michael Montgomery",
               title: "Letters From Home",
               rank: 319,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Tony Lane, David Lee",
               year: 2004,
               lyrics: "My Dearest son, it's almost June
I hope this letter catches up with you
And finds you well
It's been dry
But they're callin' for rain
And everything's the same old same
In Johnsonville
Your stubborn old daddy
Ain't said too much
But I'm sure you know
He sends his love

And she goes on
In a letter from home

I hold it up and show my buddies
Like we ain't scared
And our boots ain't muddy
And they all laugh
Like there's something funny
'Bout the way I talk
When I say, 'Mamma sends her best, y'all'

I fold it up and put it in my shirt
Pick up my gun and get back to work
And it keeps me drivin' on
Waitin' on letters from home

My dearest love, it's almost dawn
I've been lyin' here all night long
Wonderin' where you might be
I saw your mamma
And I showed her the ring
Man on the television
Said something
So I couldn't sleep
But I'll be alright,
I'm just missin' you
And this is me kissin' you
X's and O's in a letter from home

I hold it up and show my buddies
Like we ain't scared
And our boots ain't muddy
And they all laugh
'Cause she calls me honey,
But they take it hard
'Cause I don't read the good parts

I fold it up and put it in my shirt
Pick up my gun and get back to work
And it keeps me drivin' on
Waitin' on letters from home

Dear son, I know I ain't written
I'm Sittin' here tonight alone in the kitchen
It occurs to me
I might not have said it
So I'll say it now
'Son, You make me proud'

I hold it up and show my buddies
Like we ain't scared
And our boots ain't muddy
But no one laughs
'Cause there's ain't nothin' funny
When a soldier cries
And I just wipe my eyes

I fold it up and put it in my shirt
Pick up my gun and get back to work
And it keeps me drivin' on
Waitin' on letters from home .")
  #song 320
  Song.create!(artist:  "Toby Keith",
               title: "American Soldier",
               rank: 320,
               copyright: "Tokeco Tunes, Roba Music",
               writers: "Chuck Cannon, Toby Keith",
               year: 2003,
               lyrics: "I'm just trying to be a father,
Raise a daughter and a son,
Be a lover to their mother,
Everything to everyone.
Up and at 'em bright and early,
I'm all business in my suit,
Yeah, I'm dressed up for success,
From my head down to my boots.
I don't do it for money
There's bills that I can't pay.
I don't do it for the glory,
I just do it anyway.
Providing for our future's my responsibility,
Yeah I'm real good under pressure,
Being all that I can be.

And I can't call in sick on Mondays
When the weekends been too strong,
I just work straight through the holidays,
And sometimes all night long.
You can bet that I stand ready
When the wolf growls at the door,
Hey, I'm solid, hey I'm steady,
Hey I'm true down to the core.

And I will always do my duty,
No matter what the price,
I've counted up the cost,
I know the sacrifice.
Oh, and I don't want to die for you,
But if dyin's asked of me,
I'll bear that cross with honor,
'Cause freedom don't come free.

I'm an American soldier, an American,
Beside my brothers and my sisters
I will proudly take a stand,
When liberty's in jeopardy
I will always do what's right,
I'm out here on the front lines,
So you can sleep in peace tonight.
American soldier, I'm an American,

American soldier,
An American .")
  #song 321
  Song.create!(artist:  "Rascal Flatts",
               title: "Mayberry",
               rank: 321,
               copyright: "Peermusic Publishing",
               writers: "Arlos Darrel Smith",
               year: 2002,
               lyrics: "Sometimes it feels like this world is spinning faster
Than it did in the old days
So naturally, we have more natural disasters
From the strain of a fast pace
Sunday was a day of rest
Now, it's one more day for progress
And we can't slow down ?cause more is best
It's all an endless process

I miss Mayberry
Sitting on the porch drinking ice-cold cherry Coke
Where everything is black and white
Picking on a six string
Where people pass by and you call them by their first name
Watching the clouds roll by
Bye, bye

Sometimes I can hear this old earth shouting
Through the trees as the wind blows
That's when I climb up here on this mountain
To look through God's window
Now I can't fly
But I got two feet that get me high up here
Above the noise and city streets
My worries disappear

Sometimes I dream I'm driving down an old dirt road
Not even listed on a map
I pass a dad and son carrying a fishing pole
But I always wake up every time I try to turn back

Bye, bye
(I miss Mayberry, I miss Mayberry) .")
  #song 322
  Song.create!(artist:  "Sara Evans",
               title: "Suds in the Bucket",
               rank: 322,
               copyright: "Warner/Chappell Music, Inc, Mike Curb Music",
               writers: "Billy Montana, Tammy Wagoner (jenai)",
               year: 2003,
               lyrics: "She was in the back yard
Say it was a little past nine
When her prince pulled up
A white pick-up truck
Her folks should a seen it comin'
It was only just a matter of time
Plenty old enough
And you can't stop love
She stuck a note on the screen door
Sorry but I got to go
And that was all she wrote
Her Mama's heart was broke
And that was all she wrote
And so the story goes

Now her Daddy's in the kitchen
Starin' out the window
Scratchin' and a rackin' his brains
How can 18 years just up and walk away
Our little pony tailed girl
Growed up to be a woman
Now she's gone in the blink of an eye
She left the suds in the bucket
And the clothes hangin' out on the line

Now don't you wonder what the preacher's
Gonna preach about Sunday morn
Nothing quite like this
Has happened here before
Well he must of been looker
A smooth talkin' son of a gun
For such a grounded girl
To just up and run
Of course you can't fence time
And you can't stop love

Now all the biddies in the beauty shop
Gossip goin' non-stop
Sippin' on pink lemonade
How could 18 years just up and walk away
Our little pony tailed girl
Growed up to be a woman
Now she's gone in the blink of an eye
She left the suds in the bucket
And the clothes hangin' out on the line

Yee Hooo

She's got her pretty little bare feet
Hangin' out the window
And they're headed up to Vegas tonight
How could 18 years just up and walk away
Our little pony tailed girl
Growed up to be a woman
Now she's gone in the blink of an eye
She left the suds in the bucket
And the clothes hangin' out on the line
She left the suds in the bucket
And the clothes hangin' out on the line

She was in the backyard
Say it was a little past nine
When her prince pulled up
A white pick-up truck
Plenty old enough
And you can't stop love
And no you can't fence time
And you can't stop love .")
  #song 323
  Song.create!(artist:  "Tim McGraw",
               title: "Watch The Wind Blow By",
               rank: 323,
               copyright: "Universal Music Publishing Group, Spirit Music Group",
               writers: "Anders Osborne, Dylan Y. Altman",
               year: 2002,
               lyrics: "The creek goes ripplin' by
I been barefootin' all day with my baby
Brown leaves have started fallin'
Leadin' the way
I like it best just like this
Doin' nothin' all the way
So let's lay down in the tall grass
Dreamin' away

And all I want to do is let it be
And be with you and watch the wind blow by
And all I want to see is you and me
Go on forever like the clear blue sky
Slowly, there's only you and I
And all I want to do is watch the wind blow by

Girl, you know you told me not so long ago
To let it come, then let it pass
And all your troubles and your sorrows
They won't last
So let me kiss you now little darlin'
Beneath this autumn moon
Cold winds, another season
Will be here soon

And all I want to do is let it be
And be with you and watch the wind blow by
And all I want to see is you and me
Go on forever like the clear blue sky
Slowly, there's only you and I
And all I want to do is watch the wind blow by

All I want to do is watch the wind blow by
And all I want to do is watch the wind blow by .")
  #song 324
  Song.create!(artist:  "Keith Urban",
               title: "Days Go By",
               rank: 324,
               copyright: "Universal Music Publishing Group",
               writers: "Keith Lionel Urban, Monty Powell",
               year: 2004,
               lyrics: "Whoa!

Oh yeah yeah

I'm changing lanes and talkin' on the phone
Drivin' way too fast.
And the interstate's jammed with gunners like me
Afraid of comin' in last.
But somewhere in the race we run,
We're coming undone

And days go by,
I can feel 'em flyin'
Like a hand out the window in the wind.
The cars go by,
Yeah it's all we've been given,
So you better start livin' right now
'Cause days go by,
Oh and a woo-hoo

Out on the roof just the other night
I watched the world flash by,
Headlights, taillights,
Running through a river of neon signs.
But somewhere in the rush I felt,
We're losing ourselves

And days go by,
I can feel 'em flyin'
Like a hand out the window in the wind.
The cars go by,
Yeah it's all we've been given,
So you better start livin' right now
Days go by,
Oh and a woo-hoo
Yeah the days go by
Oh and a woo-hoo

We think about tomorrow then it slips away.
Oh, yes, it does.
We talk about forever but we've only got today

And the days go by,
I can feel 'em flyin'
Like a hand out the window as the cars go by
Yeah it's all we've been given,
So you better start livin',
You better start livin',
Better start livin' right now!

'Cause days go by,
I can feel 'em flyin'
Like a hand out the window in the wind.
The cars go by,
Yeah it's all we've been given,
So you better start livin' right now
'Cause days go by,
Oh and a woo-hoo
Yeah these days go by
Oh and a woo-hoo

Oh!
So take 'em by the hand,
They're yours and mine.
Take 'em by the hand,
And live your life.
Take 'em by the hand,
Don't let 'em all fly by!

Come on, come on now,
Yeah!
Come on now!
Oh and a woo-hoo!
Don't you know the days go by .")
  #song 325
  Song.create!(artist:  "Gretchen Wilson",
               title: "Redneck Woman",
               rank: 325,
               copyright: "Roba Music, Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Reservoir Media Management Inc",
               writers: "John D. Rich, Gretchen Frances N Wilson",
               year: 2004,
               lyrics: "Well I ain't never
been the barbie doll type
no I can't swig that sweet champagne
I'd rather drink beer all night
In a tavern or in a honky tonk
I want a 4 wheel drive tailgate
I've got posters on my wall of Skynard, Kid and Straight
Some people look down on my
But I don't give a rip
I stand barefooted in my own front yard with a baby in on my hip

'Cause I'm redneck woman
I ain't no high class broad
I'm just a product of my rasin'
And I say 'hey why'all' and 'Yee Haw'
And I keep my Christmas lights on my front porch all year long
And I know all the words to every Charley Daniels song
So here's to all my sisters out there keeping it country
Let me get a big 'Hell Yea' from the redneck girls like me
Hell yeah
Hell yeah

Victoria's Secret
Well their stuff's real nice
Oh but I can buy the same damn thing on a WalMart shelf half price
And still look sexy
Just as sexy
As those models on TV
No I don't need no designer tag to make my men want me
You might think I'm trashy
A little too hard core
But get in my neck of the woods
I'm just the girl next door

Hey I'm redneck woman
I ain't no high class broad
I'm just a product of my rasin'
And I say 'hey why'all' and 'Yee Haw'
And I keep my Christmas lights on my front porch all year long
And I know all the words to every Tanya Tucker song
So here's to all my sisters out there keeping it country
Let me get a big 'Hell Yea' from the redneck girls like me
Hell yeah
Hell yeah

I'm redneck woman
I ain't no high class broad
I'm just a product of my rasin'
And I say 'hey why'all' and 'Yee Haw'
And I keep my Christmas lights on my front porch all year long
And I know all the words to every Ol' Bo Seafus song
So here's to all my sisters out there keeping it country
Let me get a big 'Hell Yea' from the redneck girls like me
Hell yeah
Hell yeah

Hell yeah
Hell yeah
Hell yeah
Hell yeah

I said hell yeah .")
  #song 326
  Song.create!(artist:  "Buddy Jewell",
               title: "Sweet Southern Comfort",
               rank: 326,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Reservoir Media Management Inc",
               writers: "Rodney Clawson, Brad Crisler",
               year: 2003,
               lyrics: "Misty sunrise in my hometown
Rows of cotton 'bout knee high
Mrs. Baker down the dirt road
Still got clothes out on the line

Irwin Nichols there with Judge Lee
Playin' checkers at the gin
When I dream about the southland
This is where it all begins (from)

Carolina down to Georgia
Smell the jasmine and magnolia
Sleepy sweet home Alabama
Roll tide roll
Muddy water Mississippi
Blessed Graceland whispers to me
Carry on, carry on
Sweet southern comfort, carry on

Catching catfish on the river
Chasing fireflies by the creek
Kissing Gary William's sister
On the porch Home Coming week

With rusty cars and weeping willows
Keepin' watch out in the yard
Just a snapshot of down home Dixie
Could be anywhere you are (in)

Carolina down to Georgia
Smell the jasmine and magnolia
Sleepy sweet home Alabama
Roll tide roll
Muddy water Mississippi
Blessed Graceland whispers to me
Carry on, carry on
Sweet southern comfort, carry on

As I sit here, I'm surrounded
By these priceless memories
I don't have to think about it
There's no place I'd rather be (than)

Carolina down to Georgia
Smell the jasmine and magnolia
Sleepy sweet home Alabama
Roll tide roll
Muddy water Mississippi
Blessed Graceland whispers to me
Carry on, carry on
Sweet southern comfort, carry on .")
  #song 327
  Song.create!(artist:  "Montgomery Gentry",
               title: "If You Ever Stop Loving Me",
               rank: 327,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Tom Shapiro, Bob Dipiero, Rivers Rutherford",
               year: 2004,
               lyrics: "My old man's backhand used to land,
Hard on the side of my head.
I just learned to stay out of his way.
There's been streetfights, blue lights,
Long nights with the world sittin' on my chest:
It just showed me how much I could take.
Hard times, bad luck.
Sometimes, life sucks.

That's all right, I'm ok.
It ain't nothin' but another day.
But only God knows where I'd be,
If you ever stopped lovin' me.

The bank man, the boss man, the lawman,
All tryin' to get their hands on me.
And I ain't even done a danged thing wrong.
I've been waylaid, freight-trained, short-changed,
By bigger an' badder men.
An' all I got to say is: 'Bring it on.'
Hard rain, rough road,
So my life goes.

That's all right, I'm ok.
It ain't nothin' but another day.
But only God knows where I'd be,
If you ever stopped lovin' me.

I need you,
Gotta have you,
In my life, on my side,
Every day I'm alive,
Every might when I'm greedy an' needing,
You!

That's all right, I'm ok.
It ain't nothin' but another day.
But only God knows where I'd be,
If you ever stopped lovin' me.

It ain't nothin' but another day.
But only God knows where I'd be,
If you ever stopped lovin' me.

Baby, never stop lovin' me.

Ah, just see, what your lovin' does to me .")
  #song 328
  Song.create!(artist:  "Kenny Chesney",
               title: "I Go Back",
               rank: 328,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Kenneth Chesney, Kenneth A. Chesne",
               year: 2004,
               lyrics: "'Jack and Diane' painted a picture of my life and my dreams,
Suddenly this crazy world made more sense to me
Well I heard it today and I couldn't help but sing along
'Cause every time I hear that song

I go back to a two-toned short bed Chevy
Drivin' my first love out to the levy
Livin' life with no sense of time
And I go back to the feel of a fifty yard line
A blanket, a girl, some raspberry wine
Wishin' time would stop right in its tracks
Every time I hear that song, I go back, I go back

I used to rock all night long to 'Keep On Rockin' Me Baby'
At frat parties, college bars, just tryin' to impress the ladies
I heard it today and I couldn't help but sing along
'Cause every time I hear that song

I go back to the smell of an old gym floor
And the taste of salt on the Carolina shore
After graduation and drinkin' goodbye to friends
And I go back to watchin summer fade to fall
Growin' up too fast and I do recall
Wishin' time would stop right in its tracks
Every time I hear that song, I go back, I go back

We all have a song that somehow stamped our lives
Takes us to another place and time

So I go back to a pew, preacher and a choir
Singin' 'bout God, brimstone, and fire
And the smell of Sunday chicken after church
And I go back to the loss of a real good friend
And the sixteen summers I shared with him
Now 'Only The Good Die Young' stops me in my tracks
Every time I hear that song, I go back, I go back

To the feel of a fifty yard line
A blanket, a girl, some raspberry wine
I go back (I go back)
To watchin' summer fade to fall
Growin' up too fast and I do recall
I go back (I go back)
To the loss of a real good friend
And the sixteen summers I shared with him
I go back, I go back, I go back .")
  #song 329
  Song.create!(artist:  "Brad Paisley",
               title: "Little Moments",
               rank: 329,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Brad Paisley, Chris Dubois",
               year: 2003,
               lyrics: "Well, I'll never forget the first time that I heard
That pretty mouth say that dirty word
And I can't even remember now what she backed my truck into
But she covered her mouth and her face got red
And she just looked so darn cute
That I couldn't' even act like I was mad
Yeah, I live for little moments like that

Well, that's just like last year on my birthday
She lost all track of time and burnt the cake
And every smoke detector in the house was goin' off
And she was just about to cry until I took her in my arms
And I tried not to let her see me laugh
Yeah, I live for little moments like that

I know she's not perfect but she tries so hard for me
And I thank God that she isn't 'cause how boring would that be
It's the little imperfections, it's the sudden change in plans
When she misreads the directions and we're lost but holding hands
Yeah, I live for little moments like that

When she's layin' on my shoulder on the sofa in the dark
And about the time she falls asleep so does my right arm
And I want so bad to move it 'cause it's tinglin' and it's numb
But she looks so much like an angel that I don't wanna wake her up
Yeah, I live for little moments
When she steals my heart again and doesn't even know it
Yeah, I live for little moments like that .")
  #song 330
  Song.create!(artist:  "Reba McEntire",
               title: "Somebody",
               rank: 330,
               copyright: "Universal Music Publishing Group, Warner/Chappell Music, Inc",
               writers: "Annie Tate, David Allen Berg, Sam M. Tate",
               year: 2003,
               lyrics: "At a diner down on Broadway they make small talk
When she brings his eggs and fills his coffee cup
He jokes about his love life
And tells her he's about ready to give up
that's when she says I've been there before
But keep on looking because maybe who your looking for is

Somebody in the next car
Somebody on the morning train
Somebody in the coffee shop that you walk right by everyday
Somebody that you look at but never really see
Somewhere out there is somebody

Across town in a crowded elevator
He can't forget the things that waitress said
He usually reads the paper
But today he reads a strangers face instead
Its that blue eyed girl from two floors up
Maybe she's the one
Maybe he could fall in love with

Somebody in the next car
Somebody on the morning train
Somebody in the coffee shop that you walk right by everyday
Somebody that you look at but never really see
Somewhere out there is somebody

Now they laugh about the moment that it happened
The moment they both missed until that day
When he saws his future in her eyes
Instead of just another friendly face
And he wonders why he searched so long
When she was always there at that diner waitin on

Somebody in the next car
Somebody on the morning train
Somebody in the coffee shop that you walk right by everyday
Somebody that you look at but never really see
Somewhere out there
Oh somewhere out there is somebody .")
  #song 331
  Song.create!(artist:  "Sara Evans",
               title: "Perfect",
               rank: 331,
               copyright: "Peermusic Publishing, Universal Music Publishing Group",
               writers: "Benjamin Steven Lauren, Justin Long, Randy Martin, William Thomas Donaldson, Philip Moreton",
               year: 2003,
               lyrics: "If you don't take me to Paris
On a lover's getaway
It's all right
It's all right
If I'd rather wear your T-shirt
Than a sexy negligee
It's all right
It's all right
Every dinner doesn't have to be candlelit
It's kinda nice to know that it doesn't have to be

Perfect
Baby every little piece
Of the puzzle doesn't always fit
Perfectly
Love can be rough around the edges
Tattered at the seams
But honey if it's good enough for you
It's good enough for me

If your mother doesn't like
The way I treat her baby boy
It's all right
It's all right
If in every wedding picture
My daddy looks annoyed
It's all right
It's all right
Don't you knoe that fairy tales tell a lie
Real love and real life doesn't have to be

Perfect
Baby every little piece
Of the puzzle doesn't always fit
Perfectly
Love can be rough around the edges
Tattered at the seams
But honey if it's good enough for you
It's good enough for me

You don't mind if I show up late for everthing
And when you lose your cool It's kinda cute to me
Ain't it nice to know that we don't have to be

Perfect
Baby every little piece
Of the puzzle doesn't always fit
Perfectly
Love can be rough around the edges
Tattered at the seams
But honey if it's good enough for you
It's good enough for me

It's good enough for me (perfect)
Yeah good enough for me (perfect)
Good enogh for me (perfect) .")
  #song 332
  Song.create!(artist:  "Brad Paisley",
               title: "Whiskey Lullaby (f. Alison Krauss)",
               rank: 332,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Reservoir One Music, Reservoir Media Management Inc",
               writers: "Bill Anderson, Jon Randall",
               year: 2003,
               lyrics: "She put him out like the burnin' end of a midnight cigarette
She broke his heart, he spent his whole life tryin' to forget
We watched him drink his pain away a little at a time
But he never could get drunk enough to get her off his mind
Until the night

He put that bottle to his head and pulled the trigger
And finally drank away her memory
Life is short, but this time it was bigger
Than the strength he had to get up off his knees
We found him with his face down in the pillow
With a note that said, 'I'll love her till I die.'
And when we buried him beneath the willow
The angels sang a whiskey lullaby

The rumors flew but nobody knew how much she blamed herself
For years and years she tried to hide the whiskey on her breath
She finally drank her pain away a little at a time
But she never could get drunk enough to get him off her mind
Until the night

She put that bottle to her head and pulled the trigger
And finally drank away his memory
Life is short, but this time it was bigger
Than the strength she had to get up off her knees
We found her with her face down in the pillow
Clinging to his picture for dear life
We laid her next to him beneath the willow
While the angels sang a whiskey lullaby .")
  #song 333
  Song.create!(artist:  "George Strait",
               title: "I Hate Everything",
               rank: 333,
               copyright: "Universal Music Publishing Group",
               writers: "Gary Stefan Harrison, Robert Keith Stegall",
               year: 2004,
               lyrics: "He was sitting there beside me throwing doubles down
When he ordered up his third one he looked around
Then he looked at me
And said, 'I do believe
I'll have one more.'
He said, 'I hate this bar and I hate to drink,
But on second thought, tonight I think, I hate everything.'

Then he opened up his billfold and threw a twenty down,
And a faded photograph fell out and hit the ground.
I picked it up,
He said, 'thank you, bud.'
I put it in his hand.
He said, 'I probably oughta throw this one away
'cause she's the reason I feel this way.
I hate everything.'

I hate my job
And I hate my life
And if it weren't for my two kids
I'd hate my ex wife.
I know I should move on and try to start again,
But I just can't get over her leaving me for him.
Then he shook his head, looked down at his ring, said, 'I hate everything.'

Said, 'that one bedroom apartment where I get my mail,
Is really not a home, it's more like a jail,
With a swimming pool, and a parking lot view.
Man, it's just great.
I hate summer, winter, fall and spring.
Red and yellow, purple, blue and green.
I hate everything.'

I hate my job
And I hate my life
And if it weren't for my two kids
I'd hate my ex wife.
I know I should move on and try to start again,
But I just can't get over her leaving me for him.
Then he shook his head, looked down at his ring, said, 'I hate everything.'

So I pulled out my phone and I called my house.
I said, 'Babe, I'm coming home, we're gonna work this out.'
I payed for his drinks
And I told him thanks
Thanks for everything .")
  #song 334
  Song.create!(artist:  "Andy Griggs",
               title: "She Thinks She Needs Me",
               rank: 334,
               copyright: "Sony/ATV Music Publishing LLC, Reservoir Media Management Inc",
               writers: "Clay Mills, Shane Minor, Sonny Lemaire",
               year: 2004,
               lyrics: "She thinks I walk on water
She thinks I hung the moon
She tells me every morning
They just don't make men like you

She thinks I got it together
Swears I'm tough as nails
But I don't have the heart to tell her
She don't know me that well

She don't know how much I need her
She don't know I'd fall apart
Without her kiss, without her touch
Without her faithful loving arms
She don't know it's all about her
She don't know I can't live without her
She's my world, she's my everything
She thinks she needs me

Sometimes she cries on my shoulder
When she's lyin' next to me
She don't know that when I hold her
She's really holding me, holding me

She don't know how much I need her
She don't know I'd fall apart
Without her kiss, without her touch
Without her faithful loving arms
She don't know it's all about her
She don't know I can't live without her
She's my world, she's my everything
She thinks she needs me

Funny thing is, she thinks she's the lucky one

She don't know how much I need her
She don't know I'd fall apart
Without her kiss, without her touch
Without her faithful loving arms
She don't know it's all about her
She don't know I can't live without her
She's my world, she's my everything
She thinks she needs me .")
  #song 335
  Song.create!(artist:  "Phil Vassar",
               title: "In A Real Love",
               rank: 335,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Phil Vassar, Craig Wiseman",
               year: 2004,
               lyrics: "I was eighteen makin' minimum wage
With a letterman jacket and a Chevrolet
I thought I was cool, man I ruled the school
You were cum laude with the strawberry lips
You had the whole world danglin' at your fingertips
Your senior year and I was Daddy's worst fear
We ran off on graduation night
Thought a copule of left hand rings would make everything right

With a little bit of love, little bit a learn
Little bit of watchin' a few more candles burn
Findin' out what life was
A little bit of fuss, a little bit of fight
Little bit of kiss and makin' up all night
And all day wakin' up in a real love

Twenty two workin' double overtime
I was spendin' dollars and makin' dimes
We were overdrawn and barely hangin' on
Then one night you came to me
With tears in your eyes and an E P T
And said guess what, baby ready or not
I just smiled but I was scared to death
How am I gonna have a kid when I'm still a kid myself

With a little bit of love, little bit a learn
Little bit of watchin' a few more candles burn
Findin' out what life was
A little bit of fuss, a little bit of fight
Little bit of kiss and makin' up all night
And all day wakin' up in a real love

It all adds up to a real love .")
  #song 336
  Song.create!(artist:  "Kenny Chesney",
               title: "There Goes My Life",
               rank: 336,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Neil Thrasher, Wendell Mobley",
               year: 2004,
               lyrics: "All he could think about was I'm to young for this
Got my whole life ahead
Hell I'm just a kid myself
How'm I gonna raise one

All he could see were his dreams going up in smoke
So much for ditching this town and hanging out on the coast
Oh well, those plans are long gone

And he said
There goes my life
There goes my future, my everything
Might as well kiss it all good-bye
There goes my life

A couple years of up all night and a few thousand diapers later
That mistake he thought he made covers up the refrigerator
Oh yeah, he loves that little girl.

Momma's waiting to tuck her in
As she stumbles up those stairs
She smiles back at him dragging that teddy bear
Sleep tight, blue eyes and bouncing curls

He smiles
There goes my life
There goes my future my everything
I love you, daddy goodnight
There goes my life

She had that Honda loaded down
With Abercrombie clothes and 15 pairs of shoes and his American express
He checked the oil and slammed the hood, said your good to go
She hugged the both and headed off to the west coast

He cried
There goes my life
There goes my future, my everything
I love you
Baby good-bye

There goes my life
There goes my life
Baby good-bye .")
  #song 337
  Song.create!(artist:  "Lonestar",
               title: "Lets Be Us Again",
               rank: 337,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Ole MM, Universal Music Publishing Group, Ole Media Management Lp, BMG Rights Management US, LLC",
               writers: "Tommy James, Richie Mcdonald, Maribeth Derry",
               year: 2004,
               lyrics: "Tell me what I have to do tonight
'Cause I'd do anything to make it right
Let's be us again

I'm sorry for the way I lost my head
I don't know why I said the things I said
Let's be us again

Here I stand
With everything to lose
And all I know is I don't want to ever see the end
Baby please, I'm reaching out for you
Won't you open up your heart and let me come back in
Let's be us again

Oh us again

Look at me, I'm way past pride
Isn't there some way that we can try
To be us again
Even if it takes a while
I'll wait right here until I see that smile
That says we're us again

Here I stand
With everything to lose
And all I know is I don't want to ever see the end
Baby please, I'm reaching out for you
Won't you open up your heart and let me come back in
Let's be us

Baby baby what would I do
I can't imagine life without you

Here I stand
With everything to lose
And all I know is I don't want to ever see the end
Baby please, I'm reaching out for you
Won't you open up your heart and let me come back in

Oh, here I am
I'm reaching out for you
So won't you open up your heart and let me come back in
Let's be us again

Oh let's be us again .")
  #song 338
  Song.create!(artist:  "Terri Clark",
               title: "Girls Lie Too",
               rank: 338,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Kelley Lovelace, Connie Harrington, Tim Nichols",
               year: 2004,
               lyrics: "So she can't go out tonight again:
Her sister's sick, she's gotta baby-sit.
Yeah, that sounds like a pretty good excuse.
Now you didn't hear any of this from me,
But things aren't always what they seem.
Brace yourself, this may come as a shock to you.

Girls lie too,
An' we don't care how much money you make,
Or what you drive or what you weigh,
Size don't matter anyway.
Girls lie too,
Don't think you're the only ones,
Who bend it, break it, stretch it some.
We learn from you,
Girls lie too.

We can't wait to hear about your round of golf.
We love to see deer heads hanging on the wall,
An' we like Hooter's for their hot wings too.
Other guys never cross our minds.
We don't wonder what it might be like.
How could it be any better than it is with you?

Yeah, girls lie too,
We always forgive and forget.
The cards and flowers you never sent,
Will never be brought up again.
Girls lie too.
Old gray sweatpants turn us on.
We like your friends and we love your mom.
And that's the truth,
Girls lie too.

Yeah that's the truth,
Girls lie too.

No, we don't care how much hair you have.
Yeah, that looks good!
Comb it over like that.
Oh .")
  #song 339
  Song.create!(artist:  "Billy Currington",
               title: "I Got A Feelin'",
               rank: 339,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Casey Beathard, Billy Currington, James Carson Chamberlain",
               year: 2003,
               lyrics: "I don't want to rush this thing
I don't want to jump the gun
I really want to say those three little words
But I'm gona bite my tongue
Yeah, I'm just gonna lay on back
Leave it on cruise control
I'm gona hold it all inside
till the right time comes down the road

I gotta feelin'
My heads a reelin'
My hearts a screamin'
I'm about to bust loose
Bottled up emotion
Its more than a notion
It starts with an I and ends with a you
I gotta feelin
Are you feelin it to?

I guess I've always said it now
So much for hopin you'd go first
Don't leave me hangin out here on the line
baby its your turn
Say you couldn't sleep last night
Swore that you could feel me breathe
That you want me there by your side
Yeah baby I know what you mean

I gotta feelin
My heads a reelin'
My hearts a screamin'
I'm a bout to bust loose
Bottled up emotion
Its more than a notion
It starts with an I and ends with an you
I gotta feelin'
You're feelin it too

Bottled up emotion
Its more than a notion
It starts with an I and ends with an you
I gotta feelin'
You're feelin' it too
You're feelin' it too
Yeah, you're feelin' it too
You're feelin' it too
Oh yeah .")
  #song 340
  Song.create!(artist:  "Joe Nichols",
               title: "If Nobody Believed In You",
               rank: 340,
               copyright: "Nettwerk One Music (canada)ltd",
               writers: "Harley Allen",
               year: 2004,
               lyrics: "I watched him take the two strike call
He hadn't tried to swing at all
I guess he had all he could take
He walked away for goodness sake
His father's voice was loud and mean
You won't amount to anything

That little boy quit tryin'
He just walked away
There were teardrops on his face
Tell me how would you feel
You'd probably give up too
If nobody believed in you

That old man said one more try
I know I'm not too old to drive
I promised son I'll do my best
This time I'm gonna pass the test
Give me the keys dad and get in
His father never drove again

That old man quit tryin'
He just turned away
And there were tear drops on his face
Tell me how you'd feel
You'd probably give up too
If nobody believed in you

We'd take his name out of schools
The lawyers say it breaks the rules
The Pledge of Allegiance can't be read
And under God should not be said
I wonder how much he will take
I just pray it's not too late

What if God quit tryin'
He just turned away
There were tear drops on his face
Tell me how would you feel
You'd probably give up too
If nobody believed in you

Tell me how would you feel
You'd probably give up too
If nobody believed in you .")
  #song 341
  Song.create!(artist:  "Brooks & Dunn",
               title: "That's What It's All About",
               rank: 341,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Steve Mcewan, Craig Wiseman",
               year: 2004,
               lyrics: "Well you work and you slave
And you spend all day
in your thankless job
hen you jump in your Ford
And you're door to door
with the home-bound mob
Then you pull in the drive
and you hit the chair
And the one that you love
is waiting there

And, hey
That's what it's all about
Hey, this is the life
I couldn't live without
It's a moment frozen there in time
Where the reasons all begin to rhyme
Where love's a little bigger
And you finally start figuring out
That's what it's all about

Well, they won't go to bed
Or do what you said or eat their food
They cry and they fuzz and you can't
even cuss 'cause they'll say it, too
You're tired and you're numb
And you're stressed and you're mad
And she smiles
and says 'I love you, Dad'

And, hey
That's what it's all about
Hey, this is the life
I couldn't live without
It's a moment frozen there in time
Where the reasons all begin to rhyme
Where love's a little bigger
And you finally start figuring out
That's what it's all about .")
  #song 342
  Song.create!(artist:  "Tracy Lawrence",
               title: "Paint Me A Birmingham",
               rank: 342,
               copyright: "Warner/Chappell Music, Inc, Reservoir One Music, Reservoir Media Management Inc, Hcmg Publishing LLC",
               writers: "Buck A. Moore, Gary L. Duffey",
               year: 2004,
               lyrics: "He was sitting there, his brush in hand
Paining waves as they danced upon the sand
With every stroke-he brought to life
The deeper of the ocean against the morning sky
I asked him if he only painted ocean scenes
He said for twenty dollars I'll paint you anything

Could you paint me a Birmingham
Make it look just the way I plan
A little house on the edge of town
Porch going all the way around
Put her there on the front yard swing
Cotton dress make it early spring
For a while she'll be mine again
If you could paint me a Birmingham

He looked at me with knowing eyes
and took a canvas from the bag there by his side
Picked up a brush and said to me
Son just where in this picture would you like to be
I said if there's any way you can
Could you paint me back into her arms again

Could you paint me a Birmingham
Make it look just the way I plan
A little house on the edge of town
Porch going all the way around
Put her there on the front yard swing
Cotton dress make it early spring
For a while she'll be mine again
If you could paint me a Birmingham

Paint me a Birmingham
Make it look just the way I plan
A little house on the edge of town
Porch going all the way around
Put her there on the front yard swing
Cotton dress make it early spring
For a while she'll be mine again
If you could paint me a Birmingham

O paint me a Birmingham .")
  #song 343
  Song.create!(artist:  "Gretchen Wilson",
               title: "Here For The Party",
               rank: 343,
               copyright: "Roba Music, Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Reservoir",
               writers: "Kenny Alphin, John D. Rich, Gretchen Frances N Wilson",
               year: 2004,
               lyrics: "Well I'm an eight ball shooting double-fisted drinking son of a gun
I wear my jeans a little tight
Just to watch the little boys come undone
I'm here for the beer and the ball busting band
Gonna get a little crazy just because I can

You know I'm here for the party
And I ain't leaving till they throw me out
Gonna have a little fun gonna get me some
You know I'm here, I'm here for the party

I may not be a ten but the boys say I clean up good
And if I give 'em half a chance
For some rowdy romance you know they would
I've been waiting all week just to have a good time
So bring on them cowboys and their pick-up lines

You know I'm here for the party
And I ain't leaving till they throw me out
Gonna have a little fun gonna get me some
You know I'm here, I'm here for the party

Don't want no purple hooter shooter just some jack on the rocks
Don't mind me if I start that trashy talk

You know I'm here for the party
And I ain't leaving till they throw me out
Gonna have a little fun gonna get me some
You know I'm here, I'm here for the party

You know I'm here for the party
And I ain't leaving till they throw me out
Gonna have a little fun gonna get me some
You know I'm here, I'm here for the party .")
  #song 344
  Song.create!(artist:  "Toby Keith",
               title: "Whiskey Girl",
               rank: 344,
               copyright: "Tokeco Tunes, Sony/ATV Music Publishing LLC",
               writers: "Scott Emerick, Toby Keit",
               year: 2003,
               lyrics: "Don't my baby look good in them blue jeans?
Tight on the top with a belly button ring a little tattoo
Somewhere in between
She only shows to me

Hey we're going out dancin' she's ready tonight
So damn good-lookin' boys it ain't even right
And when bar tender says for the lady
What's it gonna be?
I tell him man

She ain't into wine and roses
Beer just makes her turn up her nose and
She can't stand the thought of sippin' champagne
No Cuervo Gold Margaritas
Just ain't enough good burn in Tequila she needs
Somethin' with a little more edge and a little more plain
She's my little whiskey Girl
She's my little whiskey Girl
My Ragged-on-the-edges girl
Ah, but I like 'em rough

Baby got a '69 mustang
Four on the floor, and you ought to hear the pipes ring
I jump behind the wheel and it's away we go
Hey, I drive too fast, but she don't care
Blue bandanna tied all up in her hair
Just sittin' there
Singin' every song on the radio

She ain't into wine and roses
Beer just makes her turn up her nose
And, she can't stand the thought of sippin' champagne
No Cuervo Gold Margaritas
Just ain't enough good burn in Tequila she needs
Somethin' with a little more edge and a little more plain
She's my little whiskey Girl
She's my little whiskey Girl
My Ragged-on-the-edges girl
Ah, but I like 'em rough

No Cuervo Gold Margaritas
Just ain't enough good burn in Tequila she needs
Somethin' with a little more edge and a little more plain
She's my little whiskey Girl
Oh she's my little whiskey Girl
My Ragged-on-the-edges girl
Ah, but I like 'em rough
Yeah, I like 'em rough
I like 'em rough .")
  #song 335
  Song.create!(artist:  "George Strait",
               title: "Desperately",
               rank: 335,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Douglas Harden Lubahn",
               year: 2003,
               lyrics: "Every night it's the same
I hear you callin' my name
You're lyin' next to me
I give into your charms
You disappear in my arms
I realize it's just a dream, but

Desperately, I long to feel your touch
But you left me all alone in love

And now I
Shake the sleep from my head
And try to crawl out of bed
Today is just another day
I make the coffee for one
I turn the radio on
Pretend that everything's ok, but

Desperately, I long to feel your touch
But you left me all alone in love

And now I
Know there's no reason to smile
It's gonna take me awhile
'Cause I still love you desperately

Desperately, I long to feel your touch
But you left me all alone in love

And now I
Watch the sun goin' down
There ain't nobody around
I feel a night in the breeze
I keep on tellin' myself
I don't need nobody else
And I can do as I please, but

Desperately, I long to feel your touch
But you left me all alone in love

And now I

Desperately, I long to feel your touch
But you left me all alone in love

And now
Every night it's the same
I hear you callin' my name
I still love you desperately
I still love you
I still want you
I still love you desperately... .")
  #song 346
  Song.create!(artist:  "Lonestar",
               title: "My Front Porch Looking In",
               rank: 346,
               copyright: "Warner/Chappell Music, Inc, Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Ole MM, Ole Media Management Lp, Hori Pro Entertainment Group",
               writers: "Richie Mc Donald, Frank Joseph Myers, Don Pfrimmer",
               year: 2003,
               lyrics: "Oh yeah
Yeah oh yeah

The only ground I ever owned was sticking to my shoes
Now I look at my front porch and this panoramic view
I can sit and watch the fields fill up
With rays of glowing sun
Or watch the moon lay on the fences
Like that's where it was hung
My blessings are in front of me
It's not about the land
I'll never beat the view
From my front porch looking in

There's a carrot top who can barely walk
With a sippy cup of milk
A little blue eyed blonde with shoes on wrong
'Cause she likes to dress herself
And the most beautiful girl holding both of them
And the view I love the most
Is my front porch looking in, yeah

I've traveled here and everywhere
Following my job
I've seen the paintings from the air
Brushed by the hand of God
The mountains and the canyons reach from sea to shining sea
But I can't wait to get back home
To the one he made for me
It's anywhere I'll ever go and everywhere I've been
Nothing takes my breath away
Like my front porch looking in

There's a carrot top who can barely walk
With a sippy cup of milk
A little blue eyed blonde with shoes on wrong
'Cause she likes to dress herself
And the most beautiful girl holding both of them
Yeah the view I love the most
Is my front porch looking in

I see what beautiful is about
When I'm looking in
Not when I'm looking out

There's a carrot top who can barely walk
With a sippy cup of milk
A little blue eyed blonde with shoes on wrong
'Cause she likes to dress herself
And the most beautiful girl holding both of them
Yeah the view I love the most

Oh, the view I love the most
Is my front porch looking in
Yeah
Oh, there's a carrot top who can barely walk
(From my front porch looking in)
A little blue eyed blonde with shoes on wrong, yeah
And the most beautiful girl
(Beautiful girl
From my front porch looking in)
Holding both of them
Oh, yeah .")
  #song 347
  Song.create!(artist:  "Toby Keith",
               title: "Beer For My Horses",
               rank: 347,
               copyright: "Tokeco Tunes, Sony/ATV Music Publishing LLC, Round Hill Music Big Loud Songs",
               writers: "Scott Emerick, Toby Keith",
               year: 2002,
               lyrics: "Well a man come on the 6 o'clock news
Said somebody's been shot, somebody's been abused
Somebody blew up a building, somebody stole a car
Somebody got away, somebody didn't get too far yeah
They didn't get too far

Grandpappy told my pappy, back in my day, son
A man had to answer for the wicked that he done
Take all the rope in Texas find a tall oak tree,
Round up all them bad boys hang them high in the street
For all the people to see

That justice is the one thing you should always find
You got to saddle up your boys, you got to draw a hard line
When the gun smoke settles we'll sing a victory tune
And we'll all meet back at the local saloon
We'll raise up our glasses against evil forces singing
Whiskey for my men, beer for my horses

We got too many gangsters doing dirty deeds
Too much corruption, and crime in the streets
It's time the long arm of the law put a few more in the ground
Send 'em all to their maker and he'll settle 'em down
You can bet he'll set 'em down

'Cause justice is the one thing you should always find
You got to saddle up your boys, you got to draw a hard line
When the gun smoke settles we'll sing a victory tune
We'll all meet back at the local saloon
And we'll raise up our glasses against evil forces singing
Whiskey for my men, beer for my horses
Whiskey for my men, beer for my horses

You know justice is the one thing you should always find
You got to saddle up your boys, you got to draw a hard line
When the gun smoke settles we'll sing a victory tune
And we'll all meet back at the local saloon
And we'll raise up our glasses against evil forces singing
Whiskey for my men, beer for my horses
Singing whiskey for my men, beer for my horses .")
  #song 348
  Song.create!(artist:  "Mark Wills",
               title: "19 Somethin'",
               rank: 348,
               copyright: "EMI Music Publishing, Universal Music Publishing Group, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "David Cory Lee, Chris Dubois",
               year: 2005,
               lyrics: "I saw Star Wars at least eight times
Had the Pac Man pattern memorized
And I've seen the stuff they put inside
Stretch Armstrong
I was Roger Staubach in my back yard
Had a shoe box full of baseball cards
And a couple of Evel Knievel scars
On my right arm
I was a kid when Elvis died
And my momma cried

It was nineteen seventy somethin'
In the world that I grew up in
Farrah Fawcett harido days
Bell bottoms and eight tracks tapes
Lookin' back now I can see me
And oh man did I look cheesy
But I wouldn't trade those days for nothin'
Nineteen seventy somethin'

It was the dawning of a new decade
When we got our first microwave
And Dad broke down and finally shaved
Them old sideburns off
I took the stickers off of my Rubik's Cube
Watched MTV all afternoon
And my first love was Daisy Duke
In them cut off jeans
A space shuttle fell out of the sky
And the whole world cried

It was nineteen eighty somethin'
In the world that I grew up in
Skatin' rinks and black Trans Am's
Big hair and parachute pants
Lookin' back now I can see me
And oh man did I look cheesy
But I wouldn't trade those days for nothin'
Nineteen eighty somethin'

Now I've got a mortgage and an S.U.V.
And all this responsibility
Makes me wish sometimes

It was nineteen seventy somethin'
In the world that I grew up in
Farrah Fawcett harido days
Bell bottoms and eight tracks tapes
Lookin' back now I can see me
And oh man did I look cheesy
But I wouldn't trade those days for nothin'
Nineteen seventy somethin'

Oh was it eighty somethin'
It was nineteen somethin' .")
  #song 349
  Song.create!(artist:  "Alan Jackson",
               title: "It's Five O' Clock Somewhere (f. Jimmy Buffett)",
               rank: 349,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Jim Brown, Don Rollins",
               year: 2003,
               lyrics: "The sun is hot and that old clock is movin' slow,
An' so am I.
Work day passes like molasses in wintertime,
But it's July.
I'm gettin' paid by the hour, an' older by the minute.
My boss just pushed me over the limit.
I'd like to call him somethin',
I think I'll just call it a day.

Pour me somethin' tall an' strong,
Make it a 'Hurricane' before I go insane.
It's only half-past twelve but I don't care.
It's five o'clock somewhere.

Oh, this lunch break is gonna take all afternoon,
An' half the night.
Tomorrow mornin', I know there'll be hell to pay,
Hey, but that's all right.
I ain't had a day off now in over a year.
Our Jamaican vacation's gonna start right here.
Hit the 'phones for me,
You can tell 'em I just sailed away.

An' pour me somethin' tall an' strong,
Make it a 'Hurricane' before I go insane.
It's only half-past twelve but I don't care.
It's five o'clock somewhere.

I could pay off my tab, pour myself in a cab,
An' be back to work before two.
At a moment like this, I can't help but wonder,
What would Jimmy Buffet do?

Funny you should ask that because I'd say:
Pour me somethin' tall an' strong,
Make it a 'Hurricane' before I go insane.
It's only half-past twelve but I don't care.

Pour me somethin' tall an' strong,
Make it a 'Hurricane' before I go insane.
It's only half-past twelve but I don't care.
He don't care.
I don't care.
It's five o'clock somewhere.

What time zone am on? What country am I in?
It doesn't matter, it's five o'clock somewhere.
It's always on five in Margaritaville, come to think of it.
Yeah, I heard that.
You been there haven't you.
Yes sir.

I seen your boat there.
I've been to Margaritaville a few times.
All right, that's good.
Stumbled all the way back.
OK. Just wanna make sure you can keep it between the navigational beacons.
Bring the booze, I tell you.
All right. Well, it's five o'clock. Let's go somewhere.
I'm ready, crank it up.
Let's get out of here.
I'm gone.
Let's get out of here .")
  #song 350
  Song.create!(artist:  "Brooks & Dunn",
               title: "Red Dirt Road",
               rank: 350,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Kix Brooks, Ronnie Dunn",
               year: 2003,
               lyrics: "I was raised off of Rural Route 3
Out past where the blacktop ends
We'd walk to church on Sunday morning
Race barefoot back to Johnson's fence

That's where I first saw Mary
On that roadside picking blackberries
That summer I turned a corner in my soul
Down that red dirt road

It's where I drank my first beer
It's where I found Jesus
Where I wrecked my first car,
I tore it all to pieces
I learned the path to heaven
Is full of sinners and believers
Learned that happiness on earth
Ain't just for high achievers
I've learned,
I've come to know
There's life at both ends
Of that red dirt road

Her daddy didn't like me much
In my shackled up GTO
I'd sneak out in the middle of the night
Throw rocks at her bedroom window

We'd turn out the headlights
Drive by the moonlight
Talk about what the future might hold
Down that red dirt road

It's where I drank my first beer
It's where I found Jesus
Where I wrecked my first car,
I tore it all to pieces
I learned the path to heaven
Is full of sinners and believers
Learned that happiness on earth
Ain't just for high achievers
I've learned,
I've come to know
There's life at both ends
Of that red dirt road

I went out into the world
And I came back in
I lost Mary,
Oh I got her back again
And driving home tonight,
Feels like I've found a long lost friend

It's where I drank my first beer
It's where I found Jesus
Where I wrecked my first car,
I tore it all to pieces
I learned the path to heaven
Is full of sinners and believers
Learned that happiness on earth
Ain't just for high achievers
I've learned,
I've come to know
There's life at both ends
Of that red dirt road .")

  #song 351
  Song.create!(artist:  "Dierks Bentley",
               title: "What Was I Thinkin'",
               rank: 351,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Brett Beavers, Deric Ruttan, Dierks Bentley",
               year: 2003,
               lyrics: "Becky was a beauty from south Alabama
Her daddy had a heart like a nine pound hammer
Think he even did a little time in the slammer
What was I thinkin'?

She snuck out one night and met me by the front gate
Her daddy came out a wavin' that twelve gauge
We tore out the drive, he peppered my tailgate
What was I thinkin'?

Oh I knew there'd be hell to pay
But that crossed my mind a little too late

'Cause I was thinkin' bout a little white tank top
Sittin' right there in the middle by me
I was thinkin' bout a long kiss,
Man just gotta get goin' where the night might lead
I know what I was feelin', but what was I thinkin'?

By the county line the cops were nippin' on our heels
Pulled off the road and kicked it in four wheel
Shut off the lights, we tore through a cornfield
What was I thinkin'?'
Out the other side she was hollerin' 'Faster!'
Took a dirt road, had the radio blastin'
Hit the honky tonk for a little close dancin'
What was I thinkin'?

Oh I knew there'd be hell to pay,
But that crossed my mind a little too late

'Cause I was thinkin' bout a little white tank top
Sittin right there in the middle by me,
I was thinkin' bout a long kiss
Man just gotta get goin' where the night might lead
I know what I was feelin', but what was I thinkin'?

When a mountain of a man with a 'Born to Kill' tatoo
Tried to cut in, I knocked out his front tooth
We ran outside hood-slidin' like Bo Duke
What was I thinkin'?

I finally got her home at a half-past too late
Her daddy's in a lawn chair, sittin' on the driveway
Put it in park as he started my way
What was I thinkin'?
Oh what was I thinkin'?
Oh what was I thinkin'?
Then she gave a come-an-get-me grin,
And like a bullet we were gone again

'Cause I was thinkin' bout a little white tank top
Sittin' right there in the middle by me
I was thinkin' bout a long kiss
Man just gotta get goin' where the night might lead
I know what I was feelin'
Hey I know what I was feelin'
But what was I thinkin'?

I know what I was feelin'
What was I thinkin' .")

  #song 352
  Song.create!(artist:  "Diamond Rio",
               title: "I Believe",
               rank: 352,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd.",
               writers: "Donald Ewing Ii, Donald R Ewing Ii, Donny Kees",
               year: 2002,
               lyrics: "Every now and then,
Soft as breath upon my skin,
I feel you come back again.

And it's like you haven't been,
Gone a moment from my side.
Like the tears were never cried,
Like the hands of time are holding you and me.

And with all my heart I'm sure,
We're closer than we ever were.
I don't have to hear or see,
I've got all the proof I need.
There are more than angels watching over me.
I believe, oh I believe.

Now when you die your life goes on,
It doesn't end here when you're gone.
Every soul is filled with light,
It never ends and if I'm right.
Our love can even reach across eternity,
I believe, oh I believe.

Forever, you're a part of me.
Forever, in the heart of me.
I will hold you even longer if I can.
Oh the people who don't see the most,
See that I believe in ghosts.
And if that makes me crazy, then I am
'Cause I believe

Oh, I believe
There are more than angels watching over me.
I believe, oh I believe.

Every now and then,
Soft as breath upon my skin,
I feel you come back again.
And I believe.

'in by Metallizepp' .")
  #song 353
  Song.create!(artist:  "Joe Nichols",
               title: "Brokenheartsville",
               rank: 353,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Blake Mevis, Clint Daniels, Clint Allen Daniels, Donny Kees, Randy Boudreaux",
               year: 2002,
               lyrics: "He wore that cowboy hat to cover up his horns
Sweet talking forked tongue had a tempting charm
Before I turned around that girl was gone
All I can say is bartender pour me something strong

Here's to the past they can kiss my glass
I hope she's happy with him
Here's to the girl who wrecked my world
That angel who did me in
I think the devil drives a Coup de Ville
I watched them drive away over the hill
Not against her will, I've got time to kill
Down in Brokenheartsville

It was long and chrome sittin' in the lot
Fire engine red that thing was hot
He revved it up she waved goodbye
Love's gone to hell and so have I

Here's to the past they can kiss my glass
I hope she's happy with him
Here's to the girl who wrecked my world
That angel who did me in
I think the devil drives a Coup de Ville
I watched them drive away over the hill
Not against her will, I've got time to kill
Down in Brokenheartsville

Here's to the past they can kiss my glass
I hope she's happy with him
Here's to the girl who wrecked my world
That angel who did me in
I think the devil drives a Coup de Ville
I watched them drive away over the hill
Not against her will, I've got time to kill
Down in Brokenheartsville .")
  #song 354
  Song.create!(artist:  "Tim McGraw",
               title: "Real Good Man",
               rank: 354,
               copyright: "Universal Music Publishing Group",
               writers: "George G. Iii Teren, Melvern Rivers Ii Rutherford",
               year: 2002,
               lyrics: "Girl you've never known no one like me
Up there in your high society
They might tell you I'm no good
Girl they need to understand
Just who I am
I may be a real bad boy
But baby I'm a real good man

I may drink too much and play too loud
Hang out with a rough and rowdy crowd
That don't mean I don't respect
My Mama or my Uncle Sam
Yes sir, yes ma'am
I may be a real bad boy
But baby I'm a real good man

I might have a reckless streak
At least a country-mile wide
If you're gonna run with me
It's gonna be a wild ride
When it comes to loving you
I've got velvet hands
I'll show you how a real bad boy
Can be a real good man

I take all the good times I can get
I'm too young for growing up just yet
Ain't much I can promise you
Except to do the best I can
I'll be damned
I may be a real bad boy
But baby I'm a real good man

I may be a real bad boy
Oh but baby I'm a real good man
Yes I am .")
  #song 355
  Song.create!(artist:  "Brad Paisley",
               title: "Celebrity",
               rank: 355,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Brad Paisley",
               year: 2003,
               lyrics: "Someday I'm gonna be famous, do I have talent well no
These days you don't really need it thanks to reality shows
Can't wait to date a supermodel, can't wait to sue my dad
Can't wait to wreck a Ferrari on my way to rehab

'Cause when you're a celebrity
It's adios reality
You can act just like a fool
People think you're cool
Just 'cause you're on TV
I can throw a major fit
When my latte isn't just how I like it
When they say I've gone insane
I'll blame it on the fame
And the pressures that go with
Being a celebrity

I'll get to cry to Barbara Walters when things don't go my way
And I'll get community service no matter which law I break
I'll make the supermarket tabloids, they'll write some awful stuff
But the more they run my name down the more my price goes up

'Cause when you're a celebrity
It adios reality
No matter what you do
People think you're cool
Just 'cause you're on TV
I can fall in and out of love
Have marriages that barely last a month
When they go down the drain
I'll blame it on the fame
And say it's just so tough
Being a celebrity

So let's hitch up the wagons and head out west
To the land of the fun and the sun
We'll be real world bachelor jackass millionaires
Hey hey Hollywood here we come

'Cause when you're a celebrity
It adios reality
No matter what you do
People think you're cool
Just 'cause you're on TV .")
  #song 356
  Song.create!(artist:  "Kenny Chesney",
               title: "No Shoes, No Shirts, No Problems",
               rank: 356,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Casey Beathard, Casey Michael Beathard",
               year: 2002,
               lyrics: "I've been up to my neck
Workin' six days a week.
Wearin' holes in the soles
Of the shoes on my feet.
Been dreamin' of gettin' away
Since I don't know.
Ain't no better time than now for Mexico

No shoes, no shirt, and no problems
Blues, what blues?
Hey I forgot 'em.
The sun and the sand,
And a drink in my hand with no bottom,
And no shoes, no shirt, and no problems,
No problems.

Want a towel on a chair
In the sand by the sea,
Want to look through my shades,
And see you there with me.
Want to soak up life for a while,
In laid back mode.
No boss, no clock, no stress, no dress code

No shoes, no shirt, no problems
Blues, what blues?
Hey I forgot 'em.
The sun and the sand,
And a drink in my hand with no bottom,
And no shoes, no shirt, and no problems,

Babe lets get packed
Tank tops and flip flops,
If you got 'em.
And no shoes, and no shirt, and no problems,
No problems .")
  #song 357
  Song.create!(artist:  "Darryl Worley",
               title: "Have You Forgotten?",
               rank: 357,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Wynn Varble, Darryl Worley",
               year: 2003,
               lyrics: "I hear people sayin'. We Don't need this war
But I say there's some things worth fightin' for
What about our freedom and this piece of ground?
We didn't get to keep em' by backing down
They say we don't realize the mess we're gettin in
Before you start your preachin' let me ask you this my friend

Have you forgotten how it felt that day
To see your homeland under fire
And her people blown away
Have you forgotten when those towers fell
We had neighbors still inside goin through a livin hell
And you say we shouldn't worry about Bin Laden
Have you forgotten?

They took all the footage off my TV
Said it's too disturbin for you and me
It'll just breed anger is what the experts say
If it was up to me I'd show it every day
Some say this country's just out lookin' for a fight
Well after 9-11 man I'd have to say that's right

Have you forgotten how it felt that day
To see your homeland under fire
And her people blown away
Have you forgotten when those towers fell
We had neighbors still inside goin through a livin hell
And we vow to get the ones behind Bin Laden
Have you forgotten?

I'v been there with the soldiers
Who've gone away to war
And you can bet that they remember just what they're fighting for

Have you forgotten
All the people killed
Yes some went down like heroes
In that Pennsylvania field
Have you forgotten
About our Pentagon
All the loved ones that we lost
And those left to carry on
Don't you tell me not to worry 'bout Bin Laden

Have you forgotten?
Have you forgotten?
Have you forgotten? .")
  #song 358
  Song.create!(artist:  "Keith Urban",
               title: "Raining On Sunday",
               rank: 358,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd., Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Darrell R Brown, Radney M. Foster",
               year: 2002,
               lyrics: "It ticks just like a Timex
It never lets up on you
Who said life was easy?
The job is never through

It'll run us till we're ragged
It'll harden our hearts
And love could use a day of rest
Before we both start falling apart

Pray that it's raining on Sunday
Storming like crazy
We'll hide under the covers all afternoon
Baby whatever comes Monday
Can take care of itself
'Cause we've got better things
That we can do
When it's raining on Sunday

You love is like religion
Across in Mexico
And your kiss is like the innocence
Of a prayer nailed to a door

Oh surrender is much sweeter
When we both let it go
Let the water wash our bodies clean
And love wash our souls

And pray that it's raining on Sunday
Storming like crazy
And we'll hide under the covers all afternoon
And baby whatever comes Monday
Can take care of itself
'Cause we've got better things
That we can do
When it's raining on Sunday

Pray that it's raining on Sunday
Storming like crazy
We'll hide under the covers all afternoon
Baby whatever comes Monday
Can take care of itself
'Cause we got better things
That we can do
When it's raining on Sunday
When it's raining on Sunday
When it's raining on Sunday
Let it rain .")
  #song 359
  Song.create!(artist:  "Shania Twain",
               title: "Forever And For Always",
               rank: 359,
               copyright: "Universal Music Publishing Group",
               writers: "Robert John Lange, Shania Twain",
               year: 2003,
               lyrics: "In your arms I can still feel the way you
Want me when you hold me
I can still hear the words you whispered
When you told me
I can stay right here forever in your arms

And there ain't no way
I'm lettin' you go now
And there ain't no way
And there ain't not how
I'll never see that day

Cause I'm keeping you
Forever and for always
We will be together all of our days
Want to wake up every
Morning to your sweet face, always
Mm, baby

In your heart I can still hear
A beat for every time you kiss me
And when we're apart,
I know how much you miss me
I can feel your love for me in your heart

And there ain't no way
I'm lettin' you go now
And there ain't now way
And there ain't no how
I'll never see that day

Cause I'm keeping you
Forever and for always
We will be together all of our days
Want to wake up every
Morning to your sweet face, always

In your eyes (I can still see The look of the one)
I can still see The look of the one who really loves me (I can still feel the way that you want)
The one who wouldn't put anything
Else in the world above me (I can still see love for me)
I can Still see love for me in your eyes (I still see the love)

And there ain't no way
I'm lettin' you go now
And there ain't no way
And there ain't no how
I'll never see that day

Cause I'm keeping you
Forever and for always
We will be together all of our days
Want to wake up every
Morning to your sweet face, always

I'm keeping you
Forever and for always
We will be together all of our days
Want to wake up every
Morning to your sweet face,

I'm keeping you forever and for always
I'm keeping you forever
Yes I'm keeping baby
Forever, in your arms .")
  #song 360
  Song.create!(artist:  "Gary Allan",
               title: "Man to Man",
               rank: 360,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Jamie O'hara",
               year: 2001,
               lyrics: "You're throwin' around a lot of serious accusations
Ain't too hard to tell what you're insinuatin'
You think I'm the one who stole her away
And if not fo rme she'd still be yours today
We're both men here so why play games
Why don't we call a spade a spade

Man to man
Tell me the truth, tell me
Were you ever there when she needed you
Man to man
Look me in the eye, tell me
If you really loved her
Why'd you make her cry
Man to man
Who cheated who
You're the one to blame
Tell me it ain't true
Man to man
Man to man

Well you think al you anger is justified
Me, I think, it's just your wounded pride
What did you really think she was gonna do
She's a real woman, not a doormat for you
You want her back, but it's too late
Why don't we just cut to the chase

Man to man
Tell me the truth, tell me
Were you ever there when she needed you
Man to man
Look me in the eye, tell me
If you really loved her
Why'd you make her cry
Man to man
Who cheated who
You're the one to blame
Tell me it ain't true
Man to man

Man to man
Tell me the truth, tell me
Were you ever there when she needed you
Man to man
Look me in the eye, tell me
If you really loved her
Why'd you make her cry
Man to man
Who cheated who
You're the one to blame
Tell me it ain't true
Man to man
Man to man
Man to man .")
  #song 361
  Song.create!(artist:  "Kenny Chesney",
               title: "Big Star",
               rank: 361,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC",
               writers: "Stephony Smith",
               year: 2002,
               lyrics: "She was aware of her insecurities
As she took the stage
But she was convinced if she got up there
That she'd be discovered some day
So she belted it
She hit the high notes fearlessly
Oh, she melted them
She brought them to their feet

She was a big star
At Banana Joe's Bar
Where she sang karaoke every night
She said if you work hard to get where you are
It feels good in the hot spotlight
She was a big star

She made the local cable shows
Where the camera fell in love with her face
After a couple of weekends
The groupies were crawling all over the place
And she signed autographs
Like she was Garth Brooks in a skirt
And in the aftermath
That small time town was hers

She was a big star
At Banana Joe's Bar
Where she sang karaoke every night
She said if you work hard to get where you are
It feels good in the hot spotlight
She was a big star

Hey, she doesn't care anymore
That her high school girlfriends cut her down
The only thought she entertains is where they aren't
And where she is now

A few old neighbors swear they are certain
She slept her way to the top
She knows you don't get where you're goin'
Unless you got something they ain't got
So she sings tonight
To twenty thousand plus
And the young girls scream out loud
Man that could be us

She's a big star
She eats caviar
Just before she performs every night
She says if you work hard to get where you are
It feels good in the hot spotlight
She's a big star .")
  #song 362
  Song.create!(artist:  "Randy Travis",
               title: "Three Wooden Crosses",
               rank: 362,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Mike Curb Music",
               writers: "Kim Williams, Doug Johnson",
               year: 2002,
               lyrics: "A farmer and a teacher, a hooker and a preacher,
Ridin' on a midnight bus bound for Mexico
One's headed for vacation, one for higher education,
An' two of them were searchin' for lost souls
That driver never ever saw the stop sign
An' eighteen wheelers can't stop on a dime

There are three wooden crosses on the right side of the highway,
Why there's not four of them, Heaven only knows
I guess it's not what you take when you leave this world behind you,
It's what you leave behind you when you go

That farmer left a harvest, a home and eighty acres,
The faith an' love for growin' things in his young son's heart
An' that teacher left her wisdom in the minds of lots of children:
Did her best to give 'em all a better start
An' that preacher whispered, 'Can't you see the Promised Land?'
As he laid his blood-stained bible in that hooker's hand

There are three wooden crosses on the right side of the highway,
Why there's not four of them, Heaven only knows
I guess it's not what you take when you leave this world behind you,
It's what you leave behind you when you go

That's the story that our preacher told last Sunday
As he held that blood-stained bible up,
For all of us to see
He said 'Bless the farmer, and the teacher, an' the preacher
Who gave this Bible to my mama,
Who read it to me'

There are three wooden crosses on the right side of the highway,
Why there's not four of them, now I guess we know
It's not what you take when you leave this world behind you,
It's what you leave behind you when you go

There are three wooden crosses on the right side of the highway .")
  #song 363
  Song.create!(artist:  "Terri Clark",
               title: "I Just Wanna Be Mad",
               rank: 363,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Kelley Lovelace, Lee Miller, Lee Thomas Miller",
               year: 2003,
               lyrics: "Last night we went to bed not talkin'
'Cause we'd already said too much
I faced the wall you faced the window
Bound and determined not to touch

We've been married seven years now
Some days it feels like twenty-one
I'm still mad at you this mornin'
Coffee's ready if you want some
I've been up since five thinkin' 'bout me and you
And I gotta tell ya the conclusion I've come to

I'll never leave I'll never stray
My love for you will never change
But I ain't ready to make up
We'll get around to that
I think I'm right I think you're wrong
I'll probably give in before long
Please don't make me smile
I just wanna be mad for a while

For now you might as well forget it
Don't run your fingers through my hair
Yeah that's right I'm bein' stubborn
No I don't wanna go back upstairs
I'm gonna leave for work without a goodbye kiss
But as I'm drivin' off just remember this

I'll never leave I'll never stray
My love for you will never change
But I ain't ready to make up
We'll get around to that
I think I'm right I think you're wrong
I'll probably give in before long
Please don't make me smile
I just wanna be mad for a while

I'll never leave I'll never stray
My love for you will never change
But I ain't ready to make up
We'll get around to that
I think I'm right I think you're wrong
I'll probably give in before long
Please don't make me smile
I just wanna be mad for a while .")
#song 364
  Song.create!(artist:  "Blake Shelton",
               title: "The Baby",
               rank: 364,
               copyright: "Kobalt Music Publishing Ltd., Peermusic Publishing, Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Nettwerk One Music (canada)ltd",
               writers: "Andrew Myrie, Linton White, Andrae Sutherland",
               year: 2003,
               lyrics: "My brothers said that I was rotten to the core
I was the youngest child, so I got by with more
I guess she was tired by the time I came along, she'd laugh until she cried
I could do no wrong. She would always save me, because I was her baby

I worked a factory in Ohio, a shrimp boat in the bayou
I drove a truck in Birmingham
Turned 21 in Cincinnati, I called home to mom and daddy
I said your boy is now a man
She said I don't care if your 80, you'll always be my baby

She loved that photograph of our whole family
She'd always point us out for all her friends to see
That's Greg he's doing great, he really loves his job
And Ronny with his to kids, how 'bout that wife he's got
And that one's kind of crazy, but that one's my baby

I got a call in Alabama said come on home to Louisiana
And come as fast as you can fly
'Cause your mama really needs you, and says shes gotta see you
She might not make it through the night
The whole way I drove 80 so she could see her baby

She looked like she'd been sleepin'
And my family had been weeping by the time that I got to her side
And I knew that she'd been take and my heart it was breakin'
I never got to say goodbye
I softly kissed that lady and cried just like a baby .")
  #song 365
  Song.create!(artist:  "Keith Urban",
               title: "Who Wouldn't Wanna Be Me",
               rank: 365,
               copyright: "Universal Music Publishing Group",
               writers: "Keith Lionel Urban, Monty Powell",
               year: 2002,
               lyrics: "I got no money in my pockets
I got a hole in my jeans
I had a job and I lost it
But it won't get to me
Cause I'm ridin' with my baby
And its a brand new day
Were on the wings of an angel, flying away

And the sun is shining
This road keeps winding
Through the prettiest country from Georgia too Tennessee
And I got the one I love beside me, but your close behind me
I'm alive and I'm free
Who wouldn't want to be me?

She's strumming on my six string,
Across the Prairies
She's stomping out a Rhythm, and singing to me, the sweetest song

I got no money in my pockets
I got a hole in my jeans
Were on the wings of an angel, and I'm free
She's strumming on my six string, Its across the prairies
She's stomping out a rhythm, and singing too me .")
  #song 366
  Song.create!(artist:  "Jimmy Wayne",
               title: "Stay Gone",
               rank: 366,
               copyright: "Reservoir Media Management Inc",
               writers: "Billy Kirsch, Jimmy Wayne Barber",
               year: 2003,
               lyrics: "I've found peace of mind, I'm feeling good again
I'm on the other side, back among the living
Ain't a cloud in the sky
All my tears have been cried
And I can finally say

Baby baby stay
Stay right where you are
I like it this way
It's good for my heart
I haven't felt like this
In God knows how long
I know everything's gonna be OK
If you just stay gone

I still love you and I will forever
We can't hide the truth
We know each other better
When we try to make it work
We both end up hurt
It ain't supposed to be that way

Baby baby stay
Stay right where you are
I like it this way
It's good for my heart
I haven't felt like this
In God knows how long
I know everything's gonna be OK
If you just stay gone

When we try to make it work
We both end up hurt
Love ain't supposed to be that way

Baby baby stay
Stay right where you are
I like it this way
It's good for my heart
I haven't felt like this
In God knows how long
I know everything's gonna be OK
If you just stay gone

I know everything's gonna be OK
If you just stay gone .")
  #song 367
  Song.create!(artist:  "Alan Jackson",
               title: "That'd Be Alright",
               rank: 367,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Mark Daniel Sanders, Tia M. Sillers, Tim J. Nichols",
               year: 2002,
               lyrics: "If money grew on hackberry trees,
And time wasn't such a luxury,
If love was lovesick over me,
That'd be alright.

If I could keep the wind in my sails,
Keep a hold of the tiger by the tail,
A half a ham sandwich in my lunch pail,
That'd be alright.

Yeah, that'd be alright.
That'd be alright.
If everybody, everywhere,
Had a lighter load to bear,
And a little bigger piece of the pie.
We'd be livin' us a pretty good life,
And that'd be alright.

Hey, go heavy on the good and light on the bad,
A hair more happy and a shade less sad.
Turn all the negative down just a tad,
That'd be alright.

If my dear ol' dog never got old,
If the family farm never got sold.
If another bad joke never got told,
That'd be alright.

Yeah, that'd be alright.
That'd be alright.
If everybody, everywhere,
Had a lighter load to bear,
And a little bigger piece of the pie.
We'd be livin' us a pretty good life,
And that'd be alright.

Yeah, that'd be alright.
That'd be alright.
If everybody, everywhere,
Had a lighter load to bear,
And a little bigger piece of the pie.
We'd be livin' us a pretty good life,
And that'd be alright.

Yeah, that'd be alright.
That'd be alright.
If everybody, everywhere,
Had a lighter load to bear,
And a little bigger piece of the pie.
We'd be livin' us a pretty good life,
And that'd be alright.

Yeah, that'd be alright.
That'd be alright.

Yeah, that'd be alright.
That'd be alright.

That'd be alright .")
  #song 368
  Song.create!(artist:  "Tim McGraw",
               title: "She's My Kind Of Rain",
               rank: 368,
               copyright: "BMG Rights Management US, LLC",
               writers: "Robin Lerner, Tommy Lee James",
               year: 2002,
               lyrics: "She's my kind of rain
Like love in a drunken sky
She's confetti falling down all night
She sits quietly there
Black water in a jar Says baby why you trembling like you are

So I wait
And I try
I confess like a child

She's my kind of rain
Like love from a drunken sky
Confetti falling down all night
She's my kind of rain

She's the sunset's shadow
She's like Rembrandt's light
She's the history that's made at night
She's my lost companion
She's my dreamin' tree
Together in this brief eternity

Summer days
Winter snow
She's all things to behold

She's my kind of rain
Like love from a drunken sky
Confetti falling down all night
She's my kind of rain

So I wait
And I try
I confess all my crimes

She's my kind of rain
Like love from a drunken sky
Confetti falling down all night
She's my kind of rain

She's my kind of rain
Like love from a drunken sky
Confetti falling down all night
She's my kind of rain

She's my kind of rain
Ohhhhh rain on me
She's my kind of rain .")
  #song 369
  Song.create!(artist:  "Montgomery Gentry",
               title: "Speed",
               rank: 369,
               copyright: "EMI Music Publishing, Sony/ATV Music Publishing LLC",
               writers: "Don Kenneth Ramage",
               year: 2002,
               lyrics: "I'm tired of spinning my wheels
I need to find a place where my heart can go to heal
I need to get there pretty quick
Hey mister, what you got out on that lot you can sell me in a pinch
Maybe one of them souped up muscle cars
The kind that makes you think you're stronger than you are
Color don't matter, no I don't need leather seats
All that really concerns me is:

Speed, an' how fast will it go
Can it get me over her quickly
Zero to sixty, can it outrun her memory
Yeah, what I really need
Is an open road
An' a whole lot of speed

I'd like to trade in this old truck
'Cause it makes me think of her and that just slows me up
See, it's the first place we made love, where we used to sit and talk
On the tailgate all night long, but now she's gone
An' I need to move on
So give me:

Speed, an' how fast will it go
Can it get me over her quickly
Zero to sixty, can it outrun her memory
Yeah, what I really need
Is an open road
An' a whole lot of speed

Throw me them keys so I can put some miles between us
Tear off that rear view mirror, there's nothing left to see here, yeah
Let me lean on that gas, oh she catches up fast
So give me:

Speed, an' how fast will it go
Can it get me over her quickly
Zero to sixty, can it outrun her memory
Yeah, what I really need
Is an open road
An' a whole lot of speed

That's what I need
I'm tired of spinning my wheels
I'm tired of spinning my wheels .")
  #song 370
  Song.create!(artist:  "Buddy Jewell",
               title: "Help Pour Out the Rain",
               rank: 370,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Buddy Jewell",
               year: 2003,
               lyrics: "The moment was custom-made to order:
I was ridin' with my daughter on our way back from Monroe.
An' like children do, she started playin' twenty questions,
But I never could've guessed one would touch me to my soul.

She said: Daddy, when we get to Heaven, can I taste the Milky Way?
Are we goin' there to visit, or are we goin' there to stay?
Am I gonna see my Grandpa? Can I have a pair of wings?
An' do you think that God could use another Angel,
To help pour out the rain?

Well, I won't lie: I pulled that car right over,
An' I sat there on the shoulder tryin' to dry my misty eyes.
An' I whispered: Lord, I want to thank you for my children.
Cause your innocence that fills them often takes me by surprise.

Like: 'Daddy, when we get to Heaven, can I taste the Milky Way?
Are we goin' there to visit, or are we goin' there to stay?
Am I gonna see my Grandpa? Can I have a pair of wings?
An' do you think that God could use another Angel,
To help pour out the rain?

Well, I thought about it later on,
An' a smile came to my face.
An' when I tucked her in to bed,
I got down on my knees an' prayed.

Lord, when I get to Heaven, can I taste the Milky Way?
I don't want to come to visit 'cause I'm comin' home to stay?
An' I can't wait to see my family and meet Jesus face to face.
An' do you think, Lord, you could use just one more Angel,
To help pour out the rain?

Mmmm, can I help pour out the rain? .")

  #song 371
  Song.create!(artist:  "Chris Cagle",
               title: "What A Beautiful Day",
               rank: 371,
               copyright: "Universal Music Publishing Group",
               writers: "Chris Cagle, Monty Powell",
               year: 2003,
               lyrics: "Day one
I stumbled through hello on Fifth Avenue
Day two
We grabbed a bite to eat and talked all afternoon
Caught a movie on day fourteen
And day sixty seven she said I love you to me

Oh what a feeling
What a wonderful emotion
Yeah what a life
Counting my blessings and knowing
Ooh we had our ups and downs
All along the way
She had a chance to leave but chose to stay
What a beautiful day
What a beautiful day

Day one sixteen
I asked her what she was doing for the rest of my life
Day one eighty nine
Oh I almost lost that girl to my foolish pride
But she said I do on day four eighty two
And gave me a son on day seven sixty one

Oh what a feeling
What a wonderful emotion
Yeah what a life
Counting my blessings and knowing
Ooh we had our ups and downs
All along the way
She had a chance to leave but chose to stay
What a beautiful day

Day eighteen thousand two hundred and fifty three
Well honey that's fifty years
Yeah here's to you and me

Oh what a feeling
What a wonderful emotion
Yeah what a life
Counting my blessings and knowing
Ooh we had our ups and downs
All along the way
She had a chance to leave but chose to stay
What a beautiful day
What a beautiful day

Day one I thank God I said hello on Fifth Avenue .")
  #song 372
  Song.create!(artist:  "Rascal Flatts",
               title: "Love You Out Loud",
               rank: 372,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, Reservoir One Music",
               writers: "Stephanie Lynne Smith, James L. Moore, Aaron Charles Rice, Britt Nicole, Ethan John Luck",
               year: 2002,
               lyrics: "I have always been a little shy
I've always been the quiet type till now
And I never let my feelings show
I never let anybody know
Just how much I was so deep in love
But now that you're in my arms

I'm gonna stand on a rooftop, climb up a mountaintop
Baby, scream and shout
I want to sing it on the radio, show it on a video
Baby, leave no doubt
I want the whole world to know just what I'm all about
I love to love you out loud

You keep bringing out the free in me
What you do to my heart just makes me melt
And I don't think I can resist
But I've never been one to kiss and tell
A love this true can't be subdued
So I'm gonna let out a yell

I'm gonna stand on a rooftop, climb up a mountaintop
Baby, scream and shout
I want to sing it on the radio, show it on a video
Baby, leave no doubt
I want the whole world to know just what I'm all about
I love to love you out loud

Baby, I want the whole world to see
Just how good your love looks on me

I'm gonna stand on a rooftop, climb up a mountaintop
Baby, scream and shout
I want to sing it on the radio, show it on a video
Baby, leave no doubt
I want the whole world to know just what I'm all about
I love to love you out loud

Baby, I love to love you out loud
Yeah, I love to love you out loud .")
  #song 373
  Song.create!(artist:  "Martina McBride",
               title: "This One's For The Girls",
               rank: 373,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Reservoir One Music, Reservoir Media Management Inc",
               writers: "Hillary Lindsey, Chris Lindsey, Aimee Mayo",
               year: 2003,
               lyrics: "This is for all you girls about thirteen,
High school can be so rough can be so mean,
Hold on to on to your innocence,
Stand your ground when everybody's givin' in.

This one's for the girls

This is for all you girls about twenty five,
In little apartments just tryin' to get by,
Livin' on on dreams and spaghetti-o's,
Wonderin' where your life is gonna go.

This one's for the girls,
Who've ever had a broken heart,
Who've wished upon a shooting star,
You're beautiful the way you are,
This one's for the girls,
Who love without holdin' back,
Who dream with everything they have,
All around the world,
This one's for the girls.

This is for all you girls about forty two,
Tossin' pennies into the fountain of youth,
Every laugh line on your face,
Made you who you are today.

This one's for the girls,
Who've ever had a broken heart,
Who've wished upon a shooting star,
You're beautiful the way you are,
This one's for the girls,
Who love without holdin' back,
Who dream with everything they have,
All around the world,
This one's for the girls.

Yeah we're all the same inside,
From one to ninety nine

This one's for the girls,
Who've ever had a broken heart,
Who've wished upon a shooting star,
You're beautiful the way you are,
This one's for the girls,
Who love without holdin' back,
Who dream with everything they have,
All around the world,
This one's for the girls .")
#song 374
  Song.create!(artist:  "Jeff Bates",
               title: "The Love Song",
               rank: 374,
               copyright: "Universal Music Publishing Group, Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Casey Beathard, Jeff Bates, Kenny Beard",
               year: 2003,
               lyrics: "First time I felt it, think I was five years old.
I was scared, had a nightmare;
Momma was there for me to hold.

Daddy, he was different; he never said it much,
But I heard him loud and clear when he brought home that ball and glove.
Then it took on a brand new meaning,
I wasn't just a boy no more, when she moved in next door.

Where you from? What's your name?
want to go to my game?
Got the keys to my Dad's old truck.
Turn the radio off to remember the song,
We held hands and there we was,
Love.

Pages kept on turnin',
There I was with someone else.
First time in my lifetime,
I wasn't livin' for myself.
I knew I wasn't fallin',
Anywhere I'd fell before.

This place was different:
It was deeper, it was more.
Then it took on a brand new meaning,
Yeah, it was strong and it was true,
Knew what I had to do.

Found a ring, hit my knees,
Couldn't talk, couldn't breathe,
My heart had me all choked up.

Said: 'I do,' as we cried; wedding bells,
Waved goodbye, the whole church knew it was:
Love.

Then it took on a brand new meaning,
When the doctor said:
'It's time to watch your miracle arrive.'

Thank the Lord, cut the cord.
Take her home, help her grow.
And complete the circle of love.
Love.

That's the circle of love .")
  #song 375
  Song.create!(artist:  "Gary Allan",
               title: "Tough Little Boys",
               rank: 375,
               copyright: "EMI Music Publishing, Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Kobalt Music Publishing Ltd., Spirit Music Group, Nettwerk One Music (canada)ltd",
               writers: "Harley Allen, Steve Wariner",
               year: 2003,
               lyrics: "Well I never once
Backed down from a punch
Well I'd take it square on the chin
Well I found out fast
A bully's just that
You've got to stand up to him
So I didn't cry when I got a black eye
As bad as it hurt, I just grinned
But when tough little boys grow up to be dads
They turn into big babies again.

Scared me to death
When you took your first steps
And I'd fall every time you fell down
Your first day of school, I cried like a fool
And I followed your school bus to town
Well I didn't cry, when Old Yeller died
At least not in front of my friends
But when tough little boys grow up to be dads
They turn into big babies again

Well I'm a grown man
And as strong as I am
Sometimes its hard to believe
That one little girl, with little blonde curls
Could totally terrify me
If you were to ask
My wife would just laugh
She'd say 'I know all about men
How when tough little boys grow up to be dads
They turn into big babies again'

Well I know one day, I'll give you away
And I'm gonna stand there and smile
And when I get home, and I'm all alone
Well, I'll sit in your room for a while
Well I didn't cry when Old Yeller died
At least not in front of my friends
But when tough little boys grow up to be dads
They turn into big babies again
When tough little boys grow up to be dads
They turn into big babies again .")
  #song 376
  Song.create!(artist:  "Kenny Chesney",
               title: "The Good Stuff",
               rank: 376,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group, Spirit Music Group",
               writers: "Craig Michael Wiseman, Jim Collins",
               year: 2002,
               lyrics: "Well, me and my lady had our first big fight
So I drove around till I saw the neon light
The corner bar, it just seemed right so I pulled up
Not a soul around but the old bar keep
Down at the end lookin' half asleep
But he walked up and said, what'll it be?
I said the good stuff
He didn't reach around for the whiskey
He didn't pour me a beer
His blue eyes kinda went misty
He said you can't find that here

'Cause its the first long kiss on a second date
Momma's all worried when you get home late
And droppin' the ring in the spaghetti plate
'Cause you're hands are shakin' so much
And its the way that she looks with her eyes and her hair
Eatin' burnt suppers the whole first year
And askin' for seconds to keep her from tearin' up
Yeah man, that's the good stuff

He grabbed a carton of milk and he poured a glass
And I smiled and said I'll have some of that
We sat there and talked as an hour passed like old friends
I saw a black and white picture and it caught my stare
It was a pretty girl with bu-font hair
He said, that's my Bonnie, taken 'bout a year after we wed
He said, I spent five years in the bar when the cancer took her from me
But I've been sober three years now
'Cause the one thing's stronger than the whiskey

Was the sight of her holdin' my baby girl
The way she adored that string of pearls
I gave her the day that our youngest boy Earl
Married his high school love
And its a new t-shirt sayin' I'm a grandpa
Bein' right there as our time got small
And holdin' her hand when good the Lord called her up
Yeah man that's the good stuff

He said, when you get home she'll start to cry
When she says, I'm sorry, say so am I
Look into those eyes so deep in love and drink it up
'Cause that's the good stuff

That's the good stuff .")
  #song 377
  Song.create!(artist:  "Alan Jackson",
               title: "Drive (For Daddy Gene)",
               rank: 377,
               copyright: "Peermusic Publishing, Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Alan Durrant, Roy Jackson, Mark Rowe",
               year: 2002,
               lyrics: "It was painted red the stripe was white
It was eighteen feet from the bow to the stern light
Secondhand from a dealer in Atlanta
I rode up with daddy when he went there to get her
We put on a shine; put on a motor
Built out of love, made for the water
Ran her for years, 'til the transom got rotten
A piece of my childhood that will never be forgotten

It was just on old plywood boat
With a '75 Johnson with electric choke
A young boy two hands on the wheel
I can't replace the way it made me feel
And I would turn her sharp
And I would make her whine
He'd say, 'you can't beat the way an old wood boat rides'
Just a little lake across the Alabama line
But I was king of the ocean
When daddy let me drive

Just an old half ton shortbed ford
My uncle bought new in '64
Daddy got it right 'cause the engine was smoking
A couple of burnt valves and he had it going
He let me drive her when we'd haul off a load
Down a dirt strip where we'd dump trash off of Thigpen Road
I'd sit up in the seat and stretch my feet out to the pedals
Smiling like a hero that just received his medal

It was just an old hand-me-down Ford
With a three-speed on the column and a dent in the door
A young boy two hands on the wheel
I can't replace the way it mode me feel
And I would press that clutch
And I would keep it right
And he'd say, 'a little slower son you're doing just fine'
Just a dirt rood with trash on each side
But I was Mario Andretti
When daddy let me drive

I'm grown up now three daughters of my own
I let them drive my old Jeep across the pasture at our home
Maybe one day they'll reach back in their file
And pull out that old memory
And think of me and smile and say

It was just an old worn out Jeep
Rusty old floorboard, hot on my feet
A young girl two hands on the wheel
I can't replace the way it made me feel
And he'd say, 'turn it left and steer it right,
Straighten up girl, you're doing just fine'
Just a little valley by the river where we'd ride
But I was high on a mountain
When daddy let me drive

When daddy let me drive
Oh he let me drive
Daddy let me drive
It's just an old plywood boat
With a '75 Johnson with electric choke .")
  #song 378
  Song.create!(artist:  "George Strait",
               title: "Living And Living Well",
               rank: 378,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Walt Disney Music Company",
               writers: "Mark Nesler, Tom Shapiro, Anthony Martin",
               year: 2001,
               lyrics: "Had a nice little life
A little boat a little beach
A little routine I liked
A blue ocean view
Free to go with the flow
Anywhere that I wanted to
But the moment you set foot on my shore
That's when I knew

There's a difference in
Living and living well
You can't have it all
All by yourself
Something's always missing
'Til you share it with someone else
There's a difference in living and living well

'Til you smiled with me
Thought that I had it good
As good as it could be
From the back of my deck
Caught a fish caught a breeze
And a thousand red sunsets
But sitting here with you girl I just saw
The best one yet

There's a difference in
Living and living well
You can't have it all
All by yourself
Something's always missing
'Til you share it with someone else
There's a difference in living and living well

My days are brighter
My sky a deeper blue
My nights are sweeter
When I'm with you

There's a difference in
Living and living well
You can't have it all
All by yourself
Something's always missing
'Til you share it with someone else
There's a difference in living and living well .")
  #song 379
  Song.create!(artist:  "Steve Holy",
               title: "Good Morning Beautiful",
               rank: 379,
               copyright: "Life Of The Record Music",
               writers: "Lyle Cerney",
               year: 2000,
               lyrics: "Good morning beautiful
How was your night
Mine was wonderful with you by my side
When I open my eyes
And see your sweet face
It's a good morning beautiful day

I couldn't see the light
I didn't know day from night
I had no reason to care
But since you came along
I can face the dawn
'Cause I know you'll be there

Good morning beautiful
How was your night
Mine was wonderful
With you by my side
When I open my eyes
And see your sweet face
It's a good morning beautiful day

I never worry
If it's raining outside
'Cause in here with you girl
The sun always shines

Good morning beautiful
How was your night
MIne was wonderful
With you by my side
When I open my eyes
And see your sweet face
It's a good morning beautiful day .")
  #song 380
  Song.create!(artist:  "Darryl Worley",
               title: "I Miss My Friend",
               rank: 380,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Walt Disney Music Company, BMG Rights Management US, LLC",
               writers: "Mark Eugene Nesler, Anthony Martin, Tom Shapiro",
               year: 2002,
               lyrics: "I miss the look of surrender in your eyes
The way your soft brown hair would fall
I miss the power of your kiss when we made love
Oh But baby most of all

I miss my friend
The one my heart and soul confided in
The one I felt the safest with
The one who knew just what to say to make me laugh again
And let the light back in
I miss my friend

I miss the colors that you brought into my life
Your golden smile, those blue-green eyes
I miss your gentle voice in lonely times like now
Saying it'll be alright

I miss my friend
The one my heart and soul confided in
The one I felt the safest with
The one who knew just what to say to make me laugh again
And let the light back in
I miss my friend

I miss those times
I miss those nights
I even miss our silly fights
The making up
The morning talks
And those late afternoon walks
I miss my friend .")
  #song 381
  Song.create!(artist:  "Toby Keith",
               title: "My List",
               rank: 381,
               copyright: "Walt Disney Music Company, Universal Music Publishing Group, DO Write Music LLC",
               writers: "Tim James, Antonina Armato",
               year: 2001,
               lyrics: "Under an old brass paperweight is my list of things to do today
Go to the bank and the hardware store, put a new lock on the cellar door
I cross 'em off as I get 'em done but when the sun is set
There's still more than a few things left I haven't got to yet

Go for a walk, say a little prayer
Take a deep breath of mountain air
Put on my glove and play some catch
It's time that I make time for that
Wade the shore and cast a line
Look up a long lost friend of mine
Sit on the porch and give my girl a kiss
Start livin', that's the next thing on my list

Wouldn't change the course of fate but cuttin' the grass just had to wait
Cause I've got more important things like pushin' my kid on the backyard swing
I won't break my back for a million bucks I can't take to my grave
So why put off for tomorrow what I could get done today

Like go for a walk, say a little prayer
Take a deep breath of mountain air
Put on my glove and play some catch
It's time that I make time for that
Wade the shore and cast a line
Look up a long lost friend of mine
Sit on the porch and give my girl a kiss
Start livin', that's the next thing on my list

Raise a little hell, laugh 'til it hurts
Put an extra five in the plate at church
Call up my folks just to chat
It's time that I make time for that
Stay up late, then oversleep
Show her what she means to me
Catch up on all the things I've always missed
Just start livin', that's the next thing on my list

Under an old brass paperweight
Is my list of things to do today .")
  #song 382
  Song.create!(artist:  "Steve Azar",
               title: "I Don't Have To Be Me ('Til Monday)",
               rank: 382,
               copyright: "Universal Music Publishing Group",
               writers: "Dan H. Shipley, Jason David Young",
               year: 2002,
               lyrics: "I got me a brand new car waiting in the driveway
Shinin' like a bright new star, I've be wishin' on it everyday
To take me away from here
So I called in to where I work, told a little white lie
No my back don't really hurt, but that's my alibi
My temporary ticket to anywhere but there
Call it an early weekend, call it goin' off the deep in
Call it what you want, I made up my mind

I don't have to be me 'til Monday
Friday, Saturday, Sunday
I ain't gonna face reality
Three days without punching a time clock
Three nights of goin' non-stop
No work and all play
I don't have to be 'til Monday
Yeah

I can do what I want to do, be who I want to be
I got no one to answer to, soon as I turn the key
A cash machine, gasoline and we're outta here
Call it an early weekend, call it goin' off thedeep end
Baby, you and me, we can leave it all behind

I don't have to be me 'til Monday
Friday, Saturday, Sunday
I ain't gonna face reality
Three days without punching a time clock
Three nights of goin' non-stop
No work and all play
I don't have to be 'til Monday
Yeah

Oh, there days without punching a time clock
Three nights of goin' non-stop
No work and all play
I don't have to be me 'til Monday

I don't have to be me 'til Monday
(I don't have to be me)
I don't have to be me 'til Moday
(I don't have to be me)
I don't have to be me 'til Monday .")
  #song 383
  Song.create!(artist:  "Diamond Rio",
               title: "Beautiful Mess",
               rank: 383,
               copyright: "EMI Music Publishing, Warner/Chappell Music, Inc, Affiliated Publishers, Inc, BMG Rights Management US, LLC",
               writers: "Sonny Lemaire, Clay Mills, Shane Allen Minor",
               year: 2002,
               lyrics: "I'm going out of my mind these days
Like I'm walking 'round in a haze
Can't think straight, can't concentrate
I need a shave
I go to work and I look tired
The boss says son you're gonna get fired
This ain't your style
And behind my coffee cup
I just smile

What a beautiful mess, what a beautiful mess I'm in
Spending all my time with you
There's nothing that I'd rather do
What a sweet addiction that I'm caught up in
I can't get enough
Or stop this hunger for your love
What a beautiful mess, what a beautiful mess I'm in

This morning I put salt in my coffee
I put my shoes on the wrong feet
Losing my mind I swear
You might be the death of me
But I don't care

What a beautiful mess, what a beautiful mess I'm in
Spending all my time with you
There's nothing that I'd rather do
What a sweet addiction that I'm caught up in
I can't get enough
Or stop this hunger for your love
What a beautiful mess, what a beautiful mess I'm in

Is it your eyes, is it your smile
All I know is that you're driving me wild

What a beautiful mess, what a beautiful mess I'm in
Spending all my time with you
There's nothing that I'd rather do
What a sweet addiction that I'm caught up in
I can't get enough
Or stop this hunger for your love
What a beautiful mess, what a beautiful mess I'm in .")
#song 384
  Song.create!(artist:  "Martina McBride",
               title: "Blessed",
               rank: 384,
               copyright: "Kobalt Music Publishing Ltd.",
               writers: "Vincent Grant Gill",
               year: 2001,
               lyrics: "I get kissed by the sun
Each morning
Put my feet on a hardwood floor
I get to hear my children laughing
Down the hall through the
Bedroom door
Sometimes I sit on my
Front porch swing
Just soaking up the day
I think to myself, I think to myself
This world is a beautiful place

I have been blessed
And I feel like I've found my way
I thank God for all I've been given
At the end of every day
I have been blessed
With so much more than I deserve
To be here with the ones
That love me
To love them so much it hurts
I have been blessed

Across a crowded room
I know you know what I'm thinking
By the way I look at you
And when we're lying in the quiet
And no words have to be said
I think to myself, I think to myself
This love is a beautiful gift

I have been blessed
And I feel like I've found my way
I thank God for all I've been given
At the end of every day
I have been blessed
With so much more than I deserve
To be here with the ones
That love me
To love them so much it hurts
I have been blessed

When I'm singing my kids to sleep
When I feel you holding me
I know

I have been blessed
And I feel like I've found my way
I thank God for all I've been given
At the end of every day
I have been blessed
With so much more than I deserve
To be here with the ones
That love me
To love them so much it hurts
I have been blessed .")
  #song 385
  Song.create!(artist:  "Joe Nichols",
               title: "The Impossible",
               rank: 385,
               copyright: "Sony/ATV Music Publishing LLC, BMG Rights Management US, LLC",
               writers: "Kelley Lovelace, Lee Thomas Miller",
               year: 2002,
               lyrics: "My dad chased monsters from the dark
He checked underneath my bed
And he could lift me with one arm way up over top his head
He could loosen rusty bolts with a quick turn of his wrench
And he pulled splinters from his hand and never even flinched
In thirteen years I'd never seen him cry
But the day that Grandpa died I realized

Unsinkable ships sink, unbreakable walls break
Sometimes the things you think'll never happen
Happen just like that
Unbendable steel bends
If the fury of the wind is unstoppable
I've learned to never underestimate the impossible

Then there was my junior year, Billy had a brand new car
It was late, the road was wet
I guess the curve was just too sharp
I walked away without a scratch they brought the helicopter in
Billy couldn't feel his legs, they said he'd never walk again
But Billy said he would and his Mom and Daddy prayed
And the day we graduated he stood up to say

Unsinkable ships sink, unbreakable walls break
Sometimes the things you think would never happen
Happen just like that
Unbendable steel bends
If the fury of the wind is unstoppable
I've learned to never underestimate the impossible

So don't tell me that it's over, don't give up on you and me
'Cause there's no such thing as hopeless if you believe

Unsinkable ships sink, unbreakable walls break
Sometimes the things you think would never happen
Happen just like that
Unbendable steel bends
If the fury of the wind is unstoppable
I've learned to never underestimate the impossible .")
  #song 386
  Song.create!(artist:  "Tracy Byrd",
               title: "Ten Rounds With Jose Cuervo",
               rank: 386,
               copyright: "Sony/ATV Music Publishing LLC, Round Hill Music Big Loud Songs",
               writers: "Casey Beathard, Casey Michael Beathard, Marla Cannon-Goodman, Michael Heeney, Michael P Heeney",
               year: 2001,
               lyrics: "I walked in the band just started
The singer couldn't carry a tune in a bucket
Was on a mission to drown her memory but
I thought no way with all this raucous

But after one round with Jose Cuervo
I caught my boots tapping long with the beat
And after two rounds with Jose Cuervo
That band was sounding pretty darn good to me

Then some stranger asked me to dance
And I revealed to her my two left feet
Said don't get me wrong I'm glad you asked
But tonight's about me and an old memory

Then after three rounds with Jose Cuervo
I let her lead me out on the floor
And after four rounds with Jose Cuervo
I was showing off moves never seen before

Well around five or round six
I forgot what I came to forget
And after round seven or was it eight
I bought a round for the whole damn place

And after nine rounds with Jose Cuervo
They were countin' me out I was about to give in
And after ten rounds with Jose Cuervo
I lost count and started counting again

One round with Jose Cuervo
Two rounds with Jose Cuervo
Three rounds with Jose Cuervo .")
  #song 387
  Song.create!(artist:  "Tommy Shane Steiner",
               title: "What If She's An Angel",
               rank: 387,
               copyright: "Sony/ATV Music Publishing LLC, Ole MM, Ole Media Management Lp",
               writers: "Bryan Wayne",
               year: 2002,
               lyrics: "There's a man standing on the corner
With a sign saying, 'will work for food'
You know the man
You see him every morning
The one you never give your money to
You can sit there with your window rolled up
Wondering when the lights going to turn green
Never knowing what a couple more bucks
In his pocket might mean

What if he's an angel sent here from Heaven
And he's making certain that you're doing your best
To take the time to help one another
Brother are you going to pass that test
You can go on with your day to day
Trying to forget what you saw in his face
Knowing deep down it could have been his saving grace
What if he's an angel

There's a man
There's a woman
Living right above you in apartment G
There's a lot of noise coming from the ceiling
And it don't sound like harmony
You can sit there with your TV turned up
While the words and his anger fly
Come tomorrow when you see her with her shades on
Can you look her in the eye

What if she's sent here from heaven
And she's making certain that you're doing your best
To take the time to help one another
Brother are you going to pass that test
You can go on with your day to day
Trying to forget what you saw in her face
Knowing deep down it could have been her saving grace
What if she's an angel

A little girl on daddy's lap
Hiding her disease with a baseball cap
You can turn the channel
Most people do
But what if you were sitting in her daddy's shoes

Maybe she's an angel
Sent here from Heaven
And she's making certain you're doing your best
To take the time to help one another
Brother are you going to pass that test
You can go on with your day to day
Trying to forget what you saw in her face
Knowing deep down it could have been her saving grace
What if she's an angel .")
  #song 388
  Song.create!(artist:  "Gary Allan",
               title: "The One",
               rank: 388,
               copyright: "Kobalt Music Publishing Ltd., Lucky Girl Music, Universal Music Publishing Group",
               writers: "Garret Lee, Mark Daniel Prendergast, Stephen Joseph Garrigan, Vincent Thomas May",
               year: 2001,
               lyrics: "No rush though I need your touch
I won't rush your heart
Until you feel on solid ground
Until your strength is found, girl

I'll fill those canyons in your soul
Like a river lead you home
And I'll walk a step behind
In the shadows so you shine
Just ask it will be done
And I will prove my love
Until you're sure that I'm the one

Somebody else was here before
He treated you unkind
And broken wings need time to heal
Before a heart can fly, girl

I'll fill those canyons in your soul
Like a river lead you home
And I'll walk a step behind
In the shadows so you shine
Just ask it will be done
And I will prove my love
Until you're sure that I'm the one

Trust in me and you'll find a heart so true
All I want to do is give the best of me to you
And stand beside you

Just ask it will be done
And I will prove my love
Until you're sure that I'm the one .")
  #song 389
  Song.create!(artist:  "Keith Urban",
               title: "Somebody Like You",
               rank: 389,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "John Shanks, Keith Urban",
               year: 2002,
               lyrics: "There's a new wind blowing like I've never known
I'm breathing deeper than I've even done
And it sure feels good to finally feel the way I do
And I want to love somebody, love somebody like you

And I let go of all my lonely yesterdays
I've forgiven myself for the mistakes I've made
Now there's just one thing the only thing I want to do
I want to love somebody, love somebody like you

Yeah I want to feel the sunshine
Shining down on me and you
When you put your arms around me
You let me know there's nothing in this world I can't do

I used to run in circles going nowhere fast
I'd take one step forward and took two steps back
I couldn't walk a straight line even if I wanted to
I want to love somebody, love somebody like you

Oh here we go now, ooo, yeah,
Hey I want to love ya baby,
Oh oh, oh oh

Yea I want to feel the sunshine
Shining down on me and you
When you put your arms around me
Well baby there ain't nothing in this world I can't do

Sometime it's hard for me to understand
But your teaching me to be a better man
I don't want to take this life for granted like I used to do, no no
I want to love somebody, love somebody like you

I'm ready to love somebody, love somebody like you
I want to love somebody, love somebody like you

Hey I want to love ya baby, ah uh

I want to be the man in the middle of the night shining like it's true
I wanna to be the man that you run to whenever I call on you
When everything that loved someone finally found it's way
I wanna be a better man I can see it in you yeah

Hey I want to love you baby

Ah ah yeah, na na na na na .")
  #song 390
  Song.create!(artist:  "Rascal Flatts",
               title: "I'm Movin' On",
               rank: 390,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "David Vincent Williams, Phillip A. White",
               year: 2000,
               lyrics: "I've dealt with my ghosts and faced all my demons
Finally content with a past I regret
I've found you find strength in your moments of weakness
For once I'm at peace with myself
I've been burdened with blame, trapped in the past for too long
I'm movin' on

I've lived in this place and I know all the faces
Each one is different but they're always the same
They mean no harm but it's time that I face it
They'll never allow me to change
But I never dreamed home would end up where I don't belong
I'm movin' on

I'm movin' on
At last I can see life has been patiently waiting for me
And I know there's no guarentee's, but I'm not alone
There comes a time in everyone's life
When all you can see are the years passing by
And I have made up my mind that those days are gone

I sold what I could and packed what I couldn't
Stopped to fill up on my way out of town
I've loved like I should but lived like
I shouldn't
I had to lose everything to find out
Maybe forgiveness will find me somewhere down this road
I'm movin' on

I'm movin' on
I'm movin' on .")
  #song 391
  Song.create!(artist:  "Jo Dee Messina",
               title: "Bring On The Rain (f. Tim McGraw)",
               rank: 391,
               copyright: "Warner/Chappell Music, Inc",
               writers: "Billy Montana, Helen Darling",
               year: 2000,
               lyrics: "Another day has almost come and gone
Can't imagine what else could go wrong
Sometimes I'd like to hide away somewhere and lock the door
A single battle lost but not the war

'Cause tomorrow's another day
And I'm thirsty anyway
So bring on the rain

It's almost like the hard times circle 'round
A couple drops and they all start coming down
Yeah, I might feel defeated,
And I might hang my head
I might be barely breathing, but I'm not dead

Tomorrow's another day
And I'm thirsty anyway
So bring on the rain

No I'm not gonna let it get me down
I'm not gonna cry
And I'm not gonna lose any sleep tonight

'Cause tomorrow's another day
And I'm not afraid
So bring on the rain

Tomorrow's another day
And I'm thirsty anyway
So bring on the rain

Bring on
Bring on the rain

No I'm not gonna let it get me down
I'm not gonna cry

So bring on the rain

Bring on
Bring on the rain

Bring on the rain
Bring on the rain
Bring on the rain .")
  #song 392
  Song.create!(artist:  "Brooks & Dunn",
               title: "The Long Goodbye",
               rank: 392,
               copyright: "Universal Music Publishing Group",
               writers: "Paul Joseph Gerald Mary Brady, Ronan Keating",
               year: 2001,
               lyrics: "I know they say if you love somebody
You should set them free
But it sure is hard to do
It sure is hard to do
I know they say if you don't come back again
Then it's meant to be (so they say)
Those words don't pull me through
'Cause I'm still in love with you
I spend each day here waiting for a miracle
But it's just you and me goin' through the mill
Climbin' up a hill

This is the long goodbye
Somebody tell me why
Two lovers in love can't make it
Just what kinda love keeps breaking a heart
No matter how hard I try
I always make you cry
Come on, baby, it's over let's face it
All that's happening here is a long goodbye

Sometimes I ask my heart did we really
Give our love a chance (just one more chance)
But I know without a doubt
We turned it inside out
And if we walked away
Would it make more sense
But it tears me up inside
Just to think we still could try
How long must we keep running on a carousel
Goin' 'round and 'round and never getting anywhere
On a wing and prayer

This is the long goodbye
Somebody tell me why
Two lovers in love can't make it
Just what kinda love keeps breaking a heart
No matter how hard I try
I always make you cry
Come on, baby, it's over let's face it
All that's happening here is a long goodbye

Long goodbye
Long goodbye .")
  #song 393
  Song.create!(artist:  "Kenny Chesney",
               title: "Young",
               rank: 393,
               copyright: "Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Aron Erlichman, Daniel Patrick Lohner, George Arthur Ragan",
               year: 2002,
               lyrics: "Lookin' back now makes me laugh
We were growin' our hair, we were cuttin' class
Knew it all already there was nothin' to learn
We were strikin' matches just to watch em burn

Listening to our music just a little too loud
We were hangin' in there with the outkast crowd
Headin' to the rapids with some discount beer
It was a long train trestle but we had no fear

And man I don't know where the time goes
But it sure goes fast just like that

We were wanna be rebels who didn't have a clue
In our rock and roll t-shirts and our typically bad attitudes
Had no excuses for the things that we'd done
We were brave, we were crazy, we were mostly young, young

Talked a good game when we were out with the guys
But in the back seat we were awkward and shy
Girls were a mystery that we couldn't explain
And I guess there are somethings that are never gonna change

And man I don't know where the time goes
But it sure goes fast just like that

We were wanna be rebels who didn't have a clue
In our rock and roll t-shirts and our typically bad attitudes
Had no excuses for the things that we'd done
We were brave, we were crazy, we were mostly young, young

Young, ya we were wishin' we were older
Young, hey I wish it wasn't over

Man I don't know where the time goes
But it sure goes fast just like that

We were wanna be rebels who didn't have a clue
In our rock and roll t-shirts and our typically bad attitudes
Had no excuses for the things that we'd done
We were brave, we were crazy, we were mostly young, young

And wishin' we were older
And I wish it wasn't over .")
#song 394
  Song.create!(artist:  "Sara Evans",
               title: "I Keep Looking",
               rank: 394,
               copyright: "Sony/ATV Music Publishing LLC, BMG Rights Management US, LLC",
               writers: "Sara Evans, Tom Shapiro, Tony Martin",
               year: 2000,
               lyrics: "Back when I was young
Couldn't wait to grow up
Get away and get out on my own
And looking back now
Ain't it funny how
I've been trying to get back home, yeah

When my low self esteem
Needs a man loving me
And I find me a perfect catch
Then I see my friends
Having wild weekends
Then I don't want to get quite so attached
Just as soon as I get what I want
I get unsatisfied
Good is good but could be better

I keep looking, I keep looking for
I keep looking for something more
I always wonder what's on the other side
Of the number two door
I keep looking
Looking for something more

Well, the straight haired girls
They all want curls
And the brunettes want to be blonde
It's your typical thing
You got ying you want yang
It just goes on and on
They say, hey, it's only human
To never be satisfied
Well I guess that I'm as human as the next one

Oh, I keep looking
I keep looking for
I keep looking for something more
I always wonder what's on the other side
Of the number two door
Yeah, I keep looking
Looking for something more

Just as soon as I get what I want
I get unsatisfied
Hey, good is good but could be better

I keep looking
I keep looking for
I keep looking for something more
I always wonder what's on the other side
Of the number two door
I keep looking
Looking for something more
Oh, looking for something more .")
  #song 395
  Song.create!(artist:  "Tim McGraw",
               title: "The Cowboy in Me",
               rank: 395,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Al Anderson, Jeffrey Steele, Craig Michael Wiseman",
               year: 2001,
               lyrics: "I don't know why I act the way I do
Like I ain't got a single thing to lose
Sometimes I'm my own worst enemy
I guess that's just the cowboy in me

I got a life that most would love to have
But sometimes I still wake up fightin' mad
At where this road I'm heading down might lead
I guess that's just the cowboy in me

The urge to run, the restlessness
The heart of stone I sometimes get
The things I've done for foolish pride
The me that's never satisfied
The face that's in the mirror when I don't like what I see
I guess that's just the cowboy in me

The urge to run, the restlessness
The heart of stone I sometimes get
The things I've done for foolish pride
The me that's never satisfied
The face that's in the mirror when I don't like what I see
I guess that's just the cowboy in me

Girl I know there's times you must have thought
There ain't a line you've drawn I haven't crossed
But you set your mind to see this love on through
I guess that's just the cowboy in you

We ride and never worry about the fall
I guess that's just the cowboy in us all .")
  #song 396
  Song.create!(artist:  "Emerson Drive",
               title: "I Should Be Sleeping",
               rank: 396,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "A Drew, Shaye Smith",
               year: 2002,
               lyrics: "I never knew there were such great movies
On TV at three a.m.
I'd have never guessed that at midnight Tuesday
I could have pizza ordered in
I've never been a real night owl
But these days I'm all turned around
There's only one thing I'm sure of right now

I should be sleeping
Instead of keeping these late hours
I've been keeping
I've been pacing and retracing
Every step of every move
And even though I'm feeling so right
I'm so happy still I know
I should be sleeping
Instead of dreaming about you

I never knew that I was funny
Till I went and made you laugh
Never liked a girl calling me honey
But you did and I liked that
I keep thinking about your smile
And trying to read between the lines
Looks like I'll be up for awhile

I should be sleeping
Instead of keeping these late hours
I've been keeping
I've been pacing and retracing
Every step of every move
And even though I'm feeling so right
I'm so happy still I know
I should be sleeping
Instead of dreaming about you

After just three dates
And one great kiss
It's way too soon to be thinking like this

I should be sleeping
Instead of keeping these late hours
I've been keeping
I've been pacing and retracing
Every step of every move
And even though I'm feeling so right
I'm so happy still I know
I should be sleeping
Instead of dreaming about you .")
  #song 397
  Song.create!(artist:  "Chris Cagle",
               title: "I Breathe In, I Breate Out",
               rank: 397,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Chris Cagle, Jon Robbin",
               year: 2000,
               lyrics: "Lately I've been runnin'
Into our old friends
And somewhere in the small talk
Someone always asks where you've been
So I tell them what you told me
And they can't believe we're through
They ask me what I'm doin' now
And in case you're wonderin' too

I breathe in I breathe out
Put one foot in front of the other
Take one day at a time
'Til you find
I'm that someone you can't live without
Until then
I breathe in and breathe out

I've got every reason
To find someone new
'Cause you swore up and down to me
That I've seen the last of you
But the way that you loved me
Girl, left me hopin' and holdin' on
So until this world stops turning round
And my heart believes you're gone

I breathe in I breathe out
Put one foot in front of the other
Take one day at a time
'Til you find
I'm that someone you can't live without
Until then
I breathe in and breathe out

We were meant to be
Girl, there's no doubt
And if it takes the rest of my life
For you to figure it out .")
  #song 398
  Song.create!(artist:  "Alan Jackson",
               title: "Where Were You (When The World Stopped Turning)",
               rank: 398,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Alan Jackson",
               year: 2002,
               lyrics: "Where were you when the world stopped turning on that September day?
Were you in the yard with your wife and children
Or working on some stage in L.A.?
Did you stand there in shock at the sight of that black smoke
Risin' against that blue sky?
Did you shout out in anger, in fear for your neighbor
Or did you just sit down and cry?

Did you weep for the children who lost their dear loved ones
And pray for the ones who don't know?
Did you rejoice for the people who walked from the rubble
And sob for the ones left below?
Did you burst out with pride for the red, white and blue
And the heroes who died just doin' what they do?
Did you look up to heaven for some kind of answer
And look at yourself and what really matters?

I'm just a singer of simple songs
I'm not a real political man
I watch CNN but I'm not sure I can tell
You the difference in Iraq and Iran
But I know Jesus and I talk to God
And I remember this from when I was young
Faith, hope and love are some good things He gave us
And the greatest is love

Where were you when the world stopped turning on that September day?
Were you teaching a class full of innocent children
Or driving down some cold interstate?
Did you feel guilty 'cause you're a survivor
In a crowded room did you feel alone?
Did you call up your mother and tell her you loved her?
Did you dust off that Bible at home?

Did you open your eyes, hope it never happened
Close your eyes and not go to sleep?
Did you notice the sunset the first time in ages
Or speak to some stranger on the street?
Did you lay down at night and think of tomorrow
Or go out and buy you a gun?
Did you turn off that violent old movie you're watchin'
And turn on 'I Love Lucy' reruns?

Did you go to a church and hold hands with some strangers
Did you stand in line and give your own blood?
Did you just stay home and cling tight to your family
Thank God you had somebody to love?

I'm just a singer of simple songs
I'm not a real political man
I watch CNN but I'm not sure I can tell
You the difference in Iraq and Iran
But I know Jesus and I talk to God
And I remember this from when I was young
Faith, hope and love are some good things He gave us
And the greatest is love

I'm just a singer of simple songs
I'm not a real political man
I watch CNN but I'm not sure I can tell
You the difference in Iraq and Iran
But I know Jesus and I talk to God
And I remember this from when I was young
Faith, hope and love are some good things He gave us
And the greatest is love

And the greatest is love.
And the greatest is love.

Where were you when the world stopped turning on that September day? .")
  #song 399
  Song.create!(artist:  "Brad Paisley",
               title: "I'm Gonna Miss Her",
               rank: 399,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Brad Paisley, Frank Rogers",
               year: 2001,
               lyrics: "Well I love her
And I love to fish
I spend all day out on this lake
And hell is all I catch
Today she met me at the door
Said I would have to choose
If I hit that fishin' hole today
She'd be packin' all her things
And she'd be gone by noon

Well I'm gonna miss her
When I get home
But right now I'm on this lake shore
And I'm sittin' in the sun
I'm sure it'll hit me
When I walk through that door tonight
That I'm gonna miss her
Oh, lookie there, I've got a bite

Now there's a chance that if I hurry
I could beg her to stay
But that water's right
And the weather's perfect
No tellin' what I might catch today

So I'm gonna miss her
When I get home
But right now I'm on this lake shore
And I'm sittin' in the sun
I'm sure it'll hit me
When I walk through that door tonight
Yeah, I'm gonna miss her
Oh, lookie there, another bite

Yeah, I'm gonna miss her
Oh, lookie there, I've got a bite .")
  #song 400
  Song.create!(artist:  "Phil Vassar",
               title: "That's When I Love You",
               rank: 400,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Julie Vassar, Phil Vassar",
               year: 2000,
               lyrics: "When I hear you stop and laugh out loud
When you're fallin' fast asleep
When you're in the middle of a crowd
When you're lyin' close to me
When I hear you say my name
When you're high and when you're low
When you don't need me to explain
?cause you already know
When you smile that way
I know, every night and every day

That's when I love you, when I need you
When I care about you
That's when I know, without a doubt
That I can't live without you
Every day I find another reason
Every season we go through
And every little thing you do
That's when I love you

When you're drivin' in your car
When you dance and sing to the radio
When you're walkin' underneath the stars
Anywhere and everywhere you go
When you're dressed up or you're dressed down
When you're talkin' on the phone
With a million people all around
Or when you're all alone
When you're near, or you're far
You're in my heart no matter where you are

That's when I love you, when I need you
When I care about you
That's when I know, without a doubt
That I can't live without you
Every day I find another reason
Every season we go through
And every little thing you do
That's when I love you .")
  #song 401
  Song.create!(artist:  "Martina McBride",
               title: "Where Would You Be",
               rank: 401,
               copyright: "Warner/Chappell Music, Inc, Ole MM, Ole Media Management Lp",
               writers: "Rick Ferrell, Rachel Christine Proctor",
               year: 2001,
               lyrics: "I wonder where your heart is
Cause it sure don't feel like its here
Sometimes I think you wish
That I would just disappear
Have I got it all wrong
Have you felt this way long
Are you already gone

Do you feel lonely
When you're here by my side
Does the sound of freedom
Echo in your mind
Do you wish you were by yourself
Or that I was someone else
Anyone else

Where would you be
If you weren't here with me
Where would you go
If you were single and free
Who would you love
Would it be me
Where would you be

I don't wanna hold you back
No I don't wanna slow you down
I don't wanna make you feel
Like you're all tied up and bound
'Cause that's not what love's about
If there's no chance we could work it out
Oh tell me now
Tell me, tell me now

Where would you be
If you weren't here with me
Where would you go
If you were single and free
Who would you love
Would it be me
Where would you be

Have I become the enemy
Is it hard to be yourself in my company

Where would you be
If you weren't here with me
Where would you go
If you were single and free
Who would you love
Would it be me
Where would you be

Oh tell me, tell me now
Where will you be
Where will you go
Who will you love
Would it be me
Where would you be .")
  #song 402
  Song.create!(artist:  "Toby Keith",
               title: "Courtesy Of The Red, White And Blue (The Angry American)",
               rank: 402,
               copyright: "Tokeco Tunes",
               writers: "Toby Keith",
               year: 2002,
               lyrics: "American girls and American guys
We'll always stand up and salute
We'll always recognize
When we see Old Glory flying
There's a lot of men dead
So we can sleep in peace at night when we lay down our head

My daddy served in the army
Where he lost his right eye but he flew a flag out in our yard
Until the day that he died
He wanted my mother, my brother, my sister and me
To grow up and live happy
In the land of the free

Now this nation that I love has fallen under attack
A mighty sucker punch came flyin' in from somewhere in the back
Soon as we could see clearly
Through our big black eye
Man, we lit up your world
Like the fourth of July

Hey Uncle Sam, put your name at the top of his list
And the Statue of Liberty started shakin' her fist
And the eagle will fly man, it's gonna be hell
When you hear mother freedom start ringin' her bell
And it feels like the whole wide world is raining down on you
Brought to you courtesy of the red white and blue

Justice will be served and the battle will rage
This big dog will fight when you rattle his cage
And you'll be sorry that you messed with
The U.S. of A.
'Cause we'll put a boot in your ass
It's the American way

Hey uncle sam put your name at the top of his list
And the Statue of Liberty started shakin' her fist
And the eagle will fly it's gonna be hell
When you hear mother freedom start ringin' her bell
And it feels like the whole wide world is raining down on you
Brought to you courtesy of the red white and blue

Oh oh of the red white and blue
Oh oh of my red white and blue .")
  #song 403
  Song.create!(artist:  "Brad Paisley",
               title: "Wrapped Around",
               rank: 403,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Brad Paisley, Charles Dubois, Chris Dubois, John Lovelace, Kelley Lovelace",
               year: 2001,
               lyrics: "Every day I clock out
And head straight to her house
We cuddle up on the couch
But it always ends the same ole way
I'm drivin' home and it's incredibly late
Somethin's got to change, ?cause

I've been wrapped around her finger
Since the first time we went out
Every day, and every night she's all I think about
I need that girl beside me
When the lights go out
I think it's time to put a ring on the finger
I'm wrapped around

Went to the bank took out a loan
Went and bought the perfect stone
Called up her dad on the phone
I'm takin' him to dinner Sunday night
I've never been so nervous in my life
I want to do this right, ?cause

I've been wrapped around her finger
Since the first time we went out
Every day, and every night she's all I think about
I need that girl beside me
When the lights go out
I think it's time to put a ring on the finger
I'm wrapped around

Yes sir I love her very much
I know it's only been seven months
But that's long enough

I've been wrapped around her finger
Since the first time we went out
Every day, and every night she's all I think about
I need that girl beside me
When the lights go out
I think it's time to put a ring on the finger
I'm wrapped around

I think it's time to put a ring on the finger
I'm wrapped around .")
  #song 404
  Song.create!(artist:  "George Strait",
               title: "Run",
               rank: 404,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Warner/Chappell Music, Inc",
               writers: "Shaffer Smith, Joseph Michael Henryk R. Wills, Dwane M. Ii Weir, Dan Croll, Quincy Matthew Hanley",
               year: 2001,
               lyrics: "If there's a plane or a bus leaving Dallas
I hope you're on it
If there's a train moving fast down the tracks
I hope you caught it

Cause I swear out there ain't where you ought to be
So catch a ride, catch a cab
Don't you know I miss you bad
But don't you walk to me

Baby run, cut a path across the blue skies
Straight in a straight line
You can't get here fast enough

Find a truck and fire it up
Lean on the gas and off the clutch
Leave Dallas in the dust
I need you in a rush
So baby run

If you ain't got a suit case
Get a box or an old brown paper sack
And pack it light or pack it heavy
Take a truck, take a Chevy
Baby just come back

There's a shortcut to the highway out of town
Why don't you take it
Don't let that speed limit slow you down
Go on and break it

Baby run, cut a path across the blue skies
Straight in a straight line
You can't get here fast enough

Find a truck and fire it up
Lean on the gas and off the clutch
Leave Dallas in the dust
I need you in a rush
So baby run

Baby run
Oh baby run
Baby run .")
  #song 405
  Song.create!(artist:  "Alan Jackson",
               title: "Work In Progress",
               rank: 405,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Alan Jackson",
               year: 2002,
               lyrics: "Okay I forgot about the trash
I didn't trim the long hairs on my mustache
I didn't buy you a ring I believe it was back in '93
Alright I admit it I forgot our anniversary
I didn't pick the baby this morning at the nursery
That ain't no big thing but it's a gold star for me

You get tired and disgusted with me
But I can't be just what you want me to be
I still love you and I try real hard
I swear one day you'll have a brand new car
I even asked God to try to help me
He looked down from Heaven and said to tell you please
Just be patient, I'm a work in progress

I'm sorry I was mad for waiting in the truck
It seemed like hours you getting all dressed up
Just to go to Shoney's on a Wednesday night
I read that book you gave me about Mars and Venus
I think it's soaking in but probably need to re-read it
But I can see now what you've been saying is right

You get tired and disgusted with me
But I can't be just what you want me to be
I still love you and I try real hard
I swear one day you'll have a brand new car
I even asked God to try to help me
He looked down from Heaven and said to tell you please
Just be patient, I'm a work in progress

I got laid off and we're behind on our bills
I can't cross that picket line down at the mill
I may not have money but I've got my pride
Yeah I know Christmas is just around the corner
And all my love can't keep us any warmer
But look at the bright side
We're both still alive

You get tired and disgusted with me
But I can't be just what you want me to be
I still love you and I try real hard
I swear one day you'll have a brand new car
I even asked God to try to help me
He looked down from Heaven and said to tell you please
Just be patient, I'm a work in progress

Oh honey just be patient now
I'm a work in progress .")
  #song 406
  Song.create!(artist:  "Phil Vassar",
               title: "American Child",
               rank: 406,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Guo Yue, Guo Yi",
               year: 2002,
               lyrics: "I was ten I was thin I was playing first base
With a second hand glove and dirt on my face
In nowhere Virginia who'd ever figure
That kid in the yard would go very far

?Cause 419 Lakewood had no silver spoons
Just an old beat up upright that played out of tune
Now I'm singing and living the life that I love
And when I count my blessings I thank god I was

An American child
An American child
?Cause dreams can grow wild
Born inside an American child

7 lbs. 3 oz she's got my nose
And she's into my heart as deep as it goes
With a promise that's more than just someone's last name
Anyone's equal, in late August came

An American child
An American child
My Grandfather would have been 80 today
But in '45 he fell down beside

An American child
An American child
?Cause dreams can grow wild
Born inside an American child .")
  #song 407
  Song.create!(artist:  "Andy Griggs",
               title: "Tonight I Wanna Be Your Man",
               rank: 407,
               copyright: "Universal Music Publishing Group",
               writers: "Melvern Rivers Ii Rutherford, Troy Verges",
               year: 2002,
               lyrics: "Baby light a couple candles
Lock the bedroom door
Put on some sweet soul music
Throw a blanket on the floor
Surrender to my patient hands
All week I've been your husband
Tonight I want to be your man

It hit me just this morning
When I passed you in the hall
I swear I caught you lookin'
Like you don't know me at all
Let me show you who I am
All week I've been your husband
Tonight I want to be your man

And I will always be your cover when you're cold
And when the world lines up against you
I'm the safe place you can go
But now and then we need to find some time
To be in love, just be in love

Now the whole world's in bed sleeping
I think we're finally alone
And if the telephone starts ringing
We'll pretend like we're not home
'Cause any fool would understand
That all week I've been your husband
Tonight I want to be your man

So let me show you who I am
All week I've been your husband
Tonight I want to be your man
Just forget about that wedding band
All week I've been your husband
Tonight I want to be your man .")
  #song 408
  Song.create!(artist:  "Brooks & Dunn",
               title: "Ain't Nothing 'Bout You",
               rank: 408,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Melvern Rivers Ii Rutherford, Tom C Shapiro",
               year: 2001,
               lyrics: "Once I thought that love was something I could never do
Never knew that I could feel this much
But this yearning in the deep part of my heart for you
Is more than a reaction to your touch
It's a perfect passion and I can't get enough

The way you look, the way you laugh,
The way you love with all you have,
There ain't nothing 'bout you that don't do something for me
The way you kiss, the way you cry,
The way you move when you walk by
There ain't nothing 'bout you (there ain't nothing bout you)
That don't do something for me

In my life I've been hammered by some heavy blows
That never knocked me off my feet
All you gotta do is smile at me and down I go
And baby it's no mystery why I surrender
Girl you got everything

The way you look, the way you laugh,
The way you love with all you have,
There ain't nothing 'bout you that don't do something for me
The way you kiss, the way you cry,
The way you move when you walk by
There ain't nothing 'bout you (there ain't nothing bout you)
That don't do something for me

I love your attitude, your rose tattoo, your every thought
Your smile, your lips and girl the list goes on and on and on

The way you look, the way you laugh,
The way you love with all you have,
There ain't nothing 'bout you that don't do something for me
The way you kiss, the way you cry,
The way you move when you walk by
There ain't nothing 'bout you (there ain't nothing bout you)
That don't do something for me

The way you look, the way you laugh,
The way you love with all you have,
Your dance, your drive, you make me feel alive

The way you talk, the way you tease
By now I think you see
There ain't nothing 'bout you that don't do something for me .")
  #song 409
  Song.create!(artist:  "Travis Tritt",
               title: "It's A Great Day To Be Alive",
               rank: 409,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Darrell Scott",
               year: 2000,
               lyrics: "I got rice cooking in the microwave
I got a three day beard I don't plan to shave
It's a goofy thing but I just gotta say, hey,
I'm doing alright

Yeah I think I'll make me some home-made soup
I'm feeling pretty good and that's the truth
It's neither drink nor drug induced, no,
I'm just doing alright

And it's a great day to be alive
I know the sun's still shining
When I close my eyes
There's some hard times in the neighborhood
But why can't every day be just this good

It's been fifteen years since I left home
I said good luck to every seed I'd sown
Gave it my best and then I left it alone
I hope they're doing alright

Now I look in the mirror and what do I see
A lone wolf there staring back at me
Long in the tooth but harmless as can be
Lord I guess he's doin' alright

And it's a great day to be alive
I know the sun's still shining
When I close my eyes
There's some hard times in the neighborhood
But why can't every day be just this good

Sometimes it's lonely, sometimes it's only me
And the shadows that fill this room
Sometimes I'm falling, desperately calling
Howling at the moon, ah-ooh, ah-ooh

Well, I might go get me a new tattoo
Or take my old Harley for three day cruise
Might even grow me a fu man chu

And it's a great day to be alive
I know the sun's still shining
When I close my eyes
There's some hard times in the neighborhood
But why can't every day be just this good

And it's a great day to be alive
I know the sun's still shining
When I close my eyes
There's some hard times in the neighborhood
But why can't every day be just this good, ah-ooh, oh yeah .")
  #song 410
  Song.create!(artist:  "Kenny Chesney",
               title: "Don't Happen Twice",
               rank: 410,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Curtis Lance, Thom Mchugh",
               year: 2000,
               lyrics: "I haven't seen you in forever
You haven't changed a bit
You didn't think that I'd remember
But how could I forget

We sang 'Bobby McGee' on the hood of my car
Made a wish on every star
In that clear September sky
One bottle of wine, two Dixie cups
At three a.m. I fell in love
For the first time in my life
And that's somethin'
That just don't happen twice

To this day I still taste that first kiss
How I prayed it wouldn't end
In a way seeing you like this
I guess it never really did

We sang 'Bobby McGee' on the hood of my car
Made a wish on every star
In that clear September sky
One bottle of wine, two Dixie cups
At three a.m. I fell in love
For the first time in my life
And that's somethin'
That just don't happen twice

One bottle of wine, two Dixie cups
At three a.m. I fell in love
For the first time in my life
And that's somethin'
That just don't happen twice

That's somethin', baby that's somethin'
Oh that's somethin'
That just don't happen twice .")
  #song 411
  Song.create!(artist:  "Toby Keith",
               title: "You Shouldn't Kiss Me Like This",
               rank: 411,
               copyright: "Tokeco Tunes",
               writers: "Toby Keith",
               year: 1999,
               lyrics: "I've got a funny feeling
The moment that your lips touched mine
Something shot right through me
My heart skipped a beat in time
There's a different feel about you tonight
It's got me thinking lots of crazy things
I even think I saw a flash of light
It felt like electricity

You shouldn't kiss me like this
Unless you mean it like that
'Cause I'll just close my eyes
And I won't know where I'm at
We'll get lost on this dance floor spinning around
And around and around and around

They're all watching us now
They think we're falling in love
They'd never believe we're just friends
When you kiss me like this I think you mean it like that
If you do baby kiss me again

Everybody swears we'd make a perfect pair
But dancing is as far as it goes
Girl you've never moved me
Quite the way you moved me tonight
I just wanted you to know
I just wanted you to know

You shouldn't kiss me like this
Unless you mean it like that
'Cause I'll just close my eyes
And I won't know where I'm at
We'll get lost on this dance floor spinning around
And around and around and around

They're all watching us now
They think we're falling in love
They'd never believe we're just friends
When you kiss me like this I think you mean it like that
If you do baby kiss me again
Kiss me again .")
  #song 412
  Song.create!(artist:  "Lonestar",
               title: "I'm Already There (Message From Home)",
               rank: 412,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Ole MM",
               writers: "Richie Mcdonald, Frank Myers, Gary Baker",
               year: 2001,
               lyrics: "He called her on the road
From a lonely, cold hotel room
Just to hear her say I love you one more time
But when he heard the sound
Of the kids laughing in the background
He had to wipe away a tear from his eye
A little voice came on the phone
Said, 'Daddy when you coming home?'
He said the first thing that came to his mind

I'm already there
Take a look around
I'm the sunshine in your hair
I'm the shadow on the ground
I'm the whisper in the wind
I'm your imaginary friend
And I know I'm in your prayers
Oh, I'm already there

She got back on the phone
Said I really miss you, darling
Don't worry about the kids -- they'll be all right
Wish I was in your arms
Lying right there beside you
But I know that I'll be in your dreams tonight
And I'll gently kiss your lips
Touch you with my fingertips
So turn out the light and close your eyes

I'm already there
Don't make a sound
I'm the beat in your heart
I'm the moonlight shining down
I'm the whisper in the wind
And I'll be there until the end
Can you feel the love that we share?
Oh, I'm already there

We may be a thousand miles apart
But I'll be with you wherever you are

I'm already there
Take a look around
I'm the sunshine in your hair
I'm the shadow on the ground
I'm the whisper in the wind
And I'll be there until the end
Can you feel the love that we share?
Oh, I'm already there
Oh, I'm already there .")
  #song 413
  Song.create!(artist:  "Diamond Rio",
               title: "The Last Time",
               rank: 413,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Jamie Jones, John Keller, Jason R. Pennock, Gary St. Clair",
               year: 2001,
               lyrics: "Last night I had a crazy dream
A wish was granted just for me
It could be for anything
I didn't ask for money
Or a mansion in Malibu
I simply wished for one more day with you!

One more day, one more time
One more sunset, maybe I'd be satisfied
But then again, I know what it would do
Leave me wishing still for one more day with you
One more day

First thing I'd do is pray for time to crawl
I'd unplug the telephone and keep the t.v off
I'd hold you every second
Say a million I love you's
That's what I'd do with one more day with you

One more day, one more time
One more sunset maybe I'd be satisfied
But then again I know what it would do
Leave me wishing still for one more day with you

One more day, one more time
One more sunset maybe I'd be satisfied
But then again I know what it would do
Leave me wishing still for one more day
Leave me wishing still for one more day
Leave me wishing still for one more day with you .")
  #song 414
  Song.create!(artist:  "Montgomery Gentry",
               title: "She Couldn't Change Me",
               rank: 414,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Gary Nicholson, Chris Knight",
               year: 2001,
               lyrics: "Sometimes I think what turned her on was my ole' broke down boots
She wanted her a real cowboy, it was a phase she was going through
Not one week after she moved in and I caught her paintin' the bedroom blue
Brought home a bottle of pink Chablis, poured out my best home-brew
I was sittin' on the porch in my over-alls as she packed her things to leave
She changed her mind when she couldn't change me

She changed direction, headed out West
She changed her tune to some Hip-Hop mess
Her dark brown hair went to blonde and her pretty blue eyes went to green
She changed everything when she couldn't change me

She was sittin' beside the ocean, lookin' out at the waves
Watchin' how they keep on rollin'
But always seemed the same
She called and said she'd been thinkin' about all those quiet country nights
And whatever she thought was so wrong with me
Suddenly seemed alright
I was sittin' on the porch in my over-alls when her truck pulled into view
She said I changed my mind when I couldn't change you

She changed direction, headed back home
She changed her tune, it's all Haggard and Jones
Had her dark brown hair pulled back and the bluest eyes you ever seen
She changed everything when she couldn't change me

She said I guess when you love someone, you just gotta let it be
She changed her mind when she couldn't change me

She changed direction, she's back in my arms
She thought it through, had a change of heart
She said I guess when you love someone, you just gotta let it be
She changed her mind when she couldn't change me

Yeah
Yeah
She changed it all when she couldn't change me

She couldn't change me
She couldn't change me .")
  #song 415
  Song.create!(artist:  "Jessica Andrews",
               title: "Who I Am",
               rank: 415,
               copyright: "Universal Music Publishing Group, Reservoir Media Management Inc",
               writers: "Brett James, Troy Verges",
               year: 2001,
               lyrics: "If I live to be a hundred
And never see the seven wonders
That'll be alright
If I don't make it to the big leagues
If I never win a Grammy
I'm gonna be just fine
'Cause I know exactly who I am

I am Rosemary's granddaughter
The spitting image of my father
And when the day is done
My momma's still my biggest fan
Sometimes I'm clueless and I'm clumsy
But I've got friends who love me
And they know just where I stand
It's all a part of me
And that's who I am

So when I make big mistake
When I fall flat on my face
I know I'll be alright
Should my tender heart be broken
I will cry those teardrops knowin'
I will be just fine
'Cause nothin' changes who I am

I am Rosemary's granddaughter
The spitting image of my father
And when the day is done
My momma's still my biggest fan
Sometimes I'm clueless and I'm clumsy
But I've got friends who love me
And they know just where I stand
It's all a part of me
And that's who I am

I'm a saint and I'm a sinner
I'm a loser, I'm a winner
I'm am steady and unstable
I am young but I'm able

I am Rosemary's granddaughter
The spitting image of my father
And when the day is done
My momma's still my biggest fan
Sometimes I'm clueless and I'm clumsy
But I've got friends who love me
And they know just where I stand
It's all a part of me
And that's who I am

I am Rosemary's granddaughter
The spitting image of my father
And when the day is done
My momma's still my biggest fan
Sometimes I'm clueless and I'm clumsy
But I've got friends who love me
And they know just where I stand
It's all a part of me
And that's who I am .")
  #song 416
  Song.create!(artist:  "Blake Shelton",
               title: "Austin",
               rank: 416,
               copyright: "Peermusic Publishing",
               writers: "David Kent, Kirsti Manna",
               year: 2001,
               lyrics: "She left without leavin' a number
Said she needed to clear her mind
He figured she'd gone back to Austin
'Cause she talked about it all the time
It was almost a year before she called him up
Three rings and an answering machine is what she got

If your callin' 'bout the car I sold it
If this is Tuesday night I'm bowlin'
If you've got somethin' to sell your wastin' your time, I'm not buyin'
If it's anybody else wait for the tone you know what to do
And P.S. if this is Austin I still love you

The telephone fell to the counter
She heard but she couldn't believe
What kind of man would hang on that long
What kind of love that must be
She waited three days and then she tried again
She didn't know what she'd say
But she heard three rings and then

If it's Friday night I'm at the ball game
And first thing Saturday if it don't rain
I'm headed out to the lake and I'll be gone all weekend long
But I'll call you back when I get home on Sunday afternoon
And P.S. if this is Austin I still love you

Well this time she left her number
But not another word
When she waited by the phone on Sunday evening
And this is what he heard

If your callin' 'bout my heart it's still yours
I should have listened to it a little more
Then it wouldn't have taken me so long
To know where I belong
And by the way boy this is no machine your talkin' to
Can't you tell this is Austin and I still love you

I still love you .")
  #song 417
  Song.create!(artist:  "Cyndi Thomson",
               title: "What I Really Meant To Say",
               rank: 417,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Chris Waters, Cyndi Goodman, Tommy Lee James",
               year: 2001,
               lyrics: "It took me by surprise
When I saw you standing there
Close enough to touch
Breathing the same air
You asked me how I've been
I guess that's when I smiled and said just fine,
Oh but baby, I was lying

What I really meant to say
Is I'm dying here inside
And I miss you more each day,
There's not a night I haven't cried
And baby here's the truth
I'm still in love with you,
That's what I really meant to say

And as you walked away
The echo of my words
Cut just like a knife
Cut so deep it hurt
I held back the tears
Held onto my pride
And watched you go
I wonder if you'll ever know

What I really meant to say
Is I'm dying here inside
And I miss more each day
There's not a night I haven't cried
And baby here's the truth
I'm still in love with you

What I really meant to say
Is I'm really not that strong
No matter how I try
I'm still holdin' on
And here's the honest truth
I'm still in love with you

Yeah, that's what I really meant to say
That's what I really meant to say
That's what I really meant to say .")
  #song 418
  Song.create!(artist:  "Jamie O'Neal",
               title: "When I Think About Angels",
               rank: 418,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Roxie Dean, Jamie O'neal, Sonny Tillis",
               year: 2000,
               lyrics: "Why does the color of my coffee match your eyes
Why do I see you when a stranger passes by
I swear I hear you in the whisper of the wind
I feel you when the sun is dancin' on my skin
And when it's raining
You won't find me complainin' 'cause

When I think about rain
I think about singing
When I think about singing
It's a heavenly tune
When I think about heaven then
I think about angels
When I think about angels
I think about you

The taste of sugar sure reminds me of your kiss
I like the way that they
Both linger on my lips
Kisses remind me of a field of butterflies
Must be the way my heart is fluttering inside
Beautiful distraction
You make every thought a chain reaction

When I think about rain
I think about singing
When I think about singing
It's a heavenly tune
When I think about heaven then
I think about angels
When I think about angels
I think about you

Anywhere I go
Anything I do
Everything around me baby
Makes me think of you

When I think about rain
I think about singing
When I think about singing
It's a heavenly tune
When I think about heaven then
I think about angels
When I think about angels
I think about you

When I think about rain
I think about singing
When I think about singing
It's a heavenly tune
When I think about heaven then
I think about angels
When I think about angels
I think about you

When I think about rain
I think about singing
When I think about singing
It's a heavenly tune
When I think about heaven then
I think about angels
When I think about angels
I think about you .")
  #song 419
  Song.create!(artist:  "Tim McGraw",
               title: "Grown Men Don't Cry",
               rank: 419,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC",
               writers: "Tom Douglas, Steve Seskin",
               year: 2001,
               lyrics: "I pulled into the shopping center
And saw a little boy wrapped around the legs of his mother
Like ice cream melting, they embraced
Years of bad decisions running down her face
All morning I'd been thinking my life's so hard
And they wore everything they owned, living in a car
I wanted to tell them it would be ok
But I just got in my Suburban, and I, I drove away

I don't know why they say grown men don't cry
I don't know why they say grown men don't cry

Keep having this dream about my old man
I'm ten years old and he's holding my hand
We're talking on the front porch watching the sun go down
But it was just a dream, he was a slave to his job and he couldn't be around
So many things I want to say to him
I just placed a rose on his grave and I talked to the wind

I don't know why they say grown men don't cry
I don't know why they say grown men don't cry

I'm sitting here with my kids and my wife and everything that I hold dear in my life
We say Grace and thank the Lord, got so much to be thankful for
Then its up the stairs and off to bed
And my little girl says 'I haven't had my story yet'
Everything weighing on my mind disappears, just like that
When she lifts her head off her pillow and says, 'I love you dad'

I don't know why they say grown men don't cry
I don't know why they say grown men don't cry .")
  #song 420
  Song.create!(artist:  "Toby Keith",
               title: "I'm Just Talkin' About Tonight",
               rank: 420,
               copyright: "Tokeco Tunes, Sony/ATV Music Publishing LLC, Round Hill Music Big Loud Songs",
               writers: "Scott Emerick, Toby Keith ",
               year: 2001,
               lyrics: "Well, I'm not talkin' bout locking down forever, baby.
That would be too demanding.
I'm just talkin' bout two lonely people
who might reach a little understanding.
I'm not talkin' bout knocking out heaven
with whether we're wrong or we're right.
I'm not talkin' bout hooking up and hanging out.
I'm just talkin' bout tonight.

You were sitting on your bar stool
talking to some fool who
didn't have a clue.
I guess he couldn't see
you were looking right at me
cause I was looking at you too.
Then it's 'do you wanna dance',
'have we ever met'
You said 'hold your horses boy
I ain't that easy to get'

Well, I'm not talkin' bout locking down forever, baby.
That would be too demanding.
I'm just talkin' bout two lonely people
who might reach a little understanding.
I'm not talkin' bout knocking out heaven
with whether we're wrong or we're right.
I'm not talkin' bout hooking up and hanging out.
I'm just talkin' bout tonight.

She said 'I only take it slow.
By now you oughta know
that I ain't digging this.
If we can start as friends
the weekend just might end
with a little kiss.'
She said 'I'm a lady looking
for a man in my life
who will make a good husband.
I'll make a good wife.'
Heeheeeasy now.

Well, I'm not talkin' bout locking down forever, baby.
That would be too demanding.
I'm just talkin' bout two lonely people
who might reach a little understanding.
I'm not talkin' bout knocking out heaven
with whether we're wrong or we're right.
I'm not talkin' bout hooking up and hanging out.
I'm just talkin' bout tonight.

Yeah, I'm just talkin' bout a little bit later tonight .")
  #song 421
  Song.create!(artist:  "Brooks & Dunn",
               title: "Only In America",
               rank: 421,
               copyright: "Peermusic Publishing, Sony/ATV Music Publishing LLC",
               writers: "Randall Jay Rogers, Kix Brooks, Don Cook",
               year: 2001,
               lyrics: "Sun coming' up over New York City
School bus driver in a traffic jam
Staring out at the faces in a rear view mirror
Lookin' at the promise of the Promised Land

One kid dreams of fame and fortune
One kid helps pay the rent
One could end up going to prison
One just might be president

Only in America
Dreamin' in red white and blue
Only in America
Where we dream as big as we want to

We all get a chance
Everybody gets to dance
Only in America

Sun goin' down on an LA freeway
Newly weds in the back of a limosine
A welder's son and a banker's daughter
All they want is every thing

She came out here to be an actress
He was a singer in a band
They might just go back to Oklahoma
And talk about the stars they could have been

Only in America
Dreamin' in red white and blue
Only in America
Where we dream as big as we want to

We all get a chance
Everybody gets to dance
Only in America

Only in America
Dreamin' in red white and blue
Only in America
Where we dream as big as we want to

We all get a chance
Everybody gets to dance
Only in America

Yeah Only in America

Only in America

Where we dream in red white and blue
Yeah we dream as big as we want to

Only in America .")
  #song 422
  Song.create!(artist:  "Keith Urban",
               title: "Where The Blacktop Ends",
               rank: 422,
               copyright: "Steve Wariner Music",
               writers: "Allen Shamblin, Steve Noel Wariner",
               year: 1999,
               lyrics: "I'm gonna kick off my shoes
And run in bare feet
Where the grass and the dirt
And the gravel all meet

I'm going back to the well
Gonna visit old friends
And feed my soul
Where the blacktop ends

I'm looking down the barrel of Friday night
I'm riding on a river of freeway lights
Look out city I'm country bound
Till Monday rolls around

I'm gonna kick off my shoes
And run in bare feet
Where the grass and the dirt
And the gravel all meet

Working in the grind is an uphill road
I'm punching that clock and carrying that load
I bust it all week and then I'm free
The weekend belongs to me

I'm gonna kick off my shoes
And run in bare feet
Where the grass and the dirt
And the gravel all meet

So, give me some fresh air
Give me that farm
And give me some time
With you in my arms

Far away from the hussle
And the pressure and the noise

Mmm,

I'm gonna kick off my shoes
And run in bare feet
Where the grass and the dirt
And the gravel all meet

I'm goin' back to the well
Gonna visit old friends
And feed my soul
You betcha

Yeah,

I'm going back to the well
Gonna visit old friends
And feed my soul
Where the blacktop ends

Where the blacktop ends
Where the blacktop ends
Where the blacktop ends .")
  #song 423
  Song.create!(artist:  "Keith Urban",
               title: "But For The Grace Of God",
               rank: 423,
               copyright: "Universal Music Publishing Group",
               writers: "Charlotte Caffey, Jane Wiedlin, Keith Lionel Urban",
               year: 1999,
               lyrics: "I can hear the neighbors
They're arguin' again
And there hasn't been peace on our street
Since who knows when
I don't mean to listen in
But the shoutin' is so loud
I turn up the radio to drown it out
And silently I say a little prayer

But for the grace of God go I
I must've been born a lucky guy
Heaven only knows how I've been blessed
With the gift of your love
And I look around and all I see
Is your happiness embracing me
Oh Lord I'd be lost
But for the grace of God

I can see that old man
He's walking past our door
And I've been told that he's rich
But he seems so poor
'Cause no one comes to call on him
And his phone it never rings
He wanders through his empty home
Surrounded by his things
And silently I say a little prayer, yes I do

But for the grace of God go I
I must've been born a lucky guy
Heaven only knows how I've been blessed
With the gift of your love
And I look around and all I see
Is your happiness embracing me
Oh Lord I'd be lost
But for the grace of God

I look around and all I see
Is your happiness embracing me
Oh Lord I'd be lost
But for the grace of God
Oh Lord I'd be lost
But for the grace of God yeah

Ooo ooo I'd be lost
But for the grace of God, yeah
Ooo ooo ooo ooo ooo .")
  #song 424
  Song.create!(artist:  "Trisha Yearwood",
               title: "I Would've Love You Anyway",
               rank: 424,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Mary Danna, Troy Verges",
               year: 2001,
               lyrics: "If I'da known the way that this would end
If I'da read the last page first
If I'da had the strength to walk away
If I'da known how this would hurt

I would've loved you anyway
I'd do it all the same
Not a second I would change
Not a touch that I would trade
Had I known my heart would break
I'da loved you anyway

It's bitter sweet to look back now
The memories withered on the vine
Just to hold you close to me
For a moment in time

I would've loved you anyway
I'd do it all the same
Not a second I would change
Not a touch that I would trade
Had I known my heart would break
I'da loved you anyway

Even if I'd seen it coming
I'd still'd see me running
Straight into your arms

I would've loved you anyway
I'd do it all the same
Not a second I would change
Not a touch that I would trade
Had I known my heart would break
I'da loved you anyway

I would've loved you anyway .")
  #song 425
  Song.create!(artist:  "Sara Evans",
               title: "I Could Not Ask For More",
               rank: 425,
               copyright: "Universal Music Publishing Group, Realsongs",
               writers: "Diane Eve Warren",
               year: 2000,
               lyrics: "Lying here with you
Listening to the rain
Smile just to see the smile upon your face
These are the moments I thank God that I'm alive
And these are the moments I'll remember all my life
I've found all I've waited for
And I could not ask for more

Looking in your eyes
Seeing all I need
Everything you are is everything in me
These are the moments
I know heaven must exist
And these are the moments
I know all I need is this
I've found all I've waited for, yeah
And I could not ask for more

I could not ask for more than this time together
I could not ask for more than this time with you
And every prayer has been answered
Every dream I've had's come true
Yeah, right here in this moment
Is right where I'm meant to be
Here with you here with me
Yeah

These are the moments I thank God that I'm alive
And these are the moments I'll remember all my life
I've found all I've waited for
And I could not ask for more

I could not ask for more than this time together
I could not ask for more than this time with you
And every prayer has been answered
Every dream I've had's come true
Yeah, right here in this moment
Is right where I'm meant to be
Oh, here with you here with me
No, I could not ask for more
Than this love you gave me
Cause it's all I've waited for
And I could not ask for more
No, yeah
No, I could not ask for more .")
  #song 426
  Song.create!(artist:  "Tim McGraw",
               title: "My Next Thirty Years",
               rank: 426,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc",
               writers: "Marv Green, Chris Lindsey, Aimee Mayo, Bill Luther",
               year: 1999,
               lyrics: "I think I'll take a moment celebrate my age
End of an era and the turning of a page
Now it's time to focus in on where I go from here
Lord have mercy on my next thirty years

In my next thirty years I'm gonna have some fun
Try to forget about all the crazy things I've done
Maybe now I've conquered all my adolescent fears
And I'll do it better in my next thirty years

My next thirty years I'm gonna settle all the scores
Cry a little less, laugh a little more
Find a world of happiness without the hate and fear
Figure out just what I'm doin' here in my next thirty years

For my next thirty years I'm gonna watch my weight
Eat a few more salads and not stay up so late
Drink a little lemonade and not so many beers
Maybe I'll remember my next thirty years

My next thirty years will be the best years of my life
Raise a little family and hang out with my wife
Spend precious moments with the ones that I hold dear
Make up for lost time here in my next thirty years
In my next thirty years .")
  #song 427
  Song.create!(artist:  "Dixie Chicks",
               title: "If I Fall You're Going Down With Me",
               rank: 427,
               copyright: "Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Annie Leslie Roboff, Matraca Maria Berg",
               year: 1999,
               lyrics: "Was it the pull of the moon now baby
That led you to my door
You say the night's got you acting crazy
I think it's something more
I've never felt the Earth move honey
Until you shook my tree
Nobody runs from the law now baby
Of love and gravity it pulls you so strong
Baby you gotta hold on

If I fall you're going down with me
You're going down with me baby if I fall
You can't take back every little chill you give me
You're going down with me baby heart and all ooh yeah

We're hanging right on the edge now baby
The wind is getting stronger
We're hanging on by a thread now honey
We can't hold on much longer
It's a long way down but it's too late

If I fall you're going down with me
You're going down with me baby if I fall
You can't take back every little chill you give me
You're going down with me baby heart and all ooh yeah

Ooh baby I couldn't get any higher
This time I'm willing to dance on the wire
If I fall
If I fall

'Cause if I fall you're going down with me
You're going down with me baby if I fall
You can't take back every little chill you gave me
You're going down with me baby heart and all

If I fall
If I fall
Oh yeah .")
  #song 428
  Song.create!(artist:  "Jo Dee Messina",
               title: "Burn",
               rank: 428,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Tina Arena, Pam Reswick, Steve Werfel",
               year: 2000,
               lyrics: "
Do you wanna be a poet and write
Do you wanna be an actor up in lights
Do you wanna be a soldier and fight for love
Do you wanna travel the world
Do you wanna be a diver for pearls
Or climb a mountain and touch the clouds above

Be anyone you want to be
Bring to life your fantasies
But I want something in return

I want you to burn
Burn for me baby
Like a candle in my night
Oh burn
Burn for me burn for me

Are you gonna be a gambler and deal
Are you gonna be a doctor and heal
Or go to heaven and touch God's face
Are you gonna be a dreamer who sleeps
Are you gonna be a sinner who weeps
Or an angel under grace

I'll lay down on your bed of coals
Offer up my heart and soul
But in return

I want you to burn
Burn for me baby
Like a candle in my night
Oh burn
Burn for me burn for me

Laugh for me, cry for me
Pray for me, fly for me
Live for me, die for me

I want you to burn
Burn for me baby
Like a candle in my night
Oh burn
Burn for me burn for me .")
  #song 429
  Song.create!(artist:  "Carolyn Dawn Johnson",
               title: "Complicated",
               rank: 429,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Edward Jr. Hinson, Dwayne Johnson, Katari T. Cox, Malcolm Greenidge, Anthony Henderson, Stanley Howse",
               year: 2001,
               lyrics: "I'm so scared that the way that I feel
Is written all over my face
When you walk into the room I wanna find a hiding place
We used to laugh, we used to hug
The way that old friends do
But now a smile and a touch of your hand
Just make me come unglued
It's such a contradiction, do I lie or tell the truth
Is it fact or fiction the way I feel for you

It's so complicated
I'm so frustrated
I wanna hold you close
I wanna push you away
I wanna make you go
I wanna make you stay
Should I say it
Should I tell you how I feel
Oh I want you to know
But then again I don't
It's so complicated

Just when I think I'm under control
I think I finally got a grip
Another friend tells me that
My name is always on your lips
They say I'm more than just a friend
They say I must be blind
Well I admit that I've seen you watch me
From the corner of your eye
Oh it's so confusing I wish you'd just confess
But think of what I'd be losing
If your answer wasn't yes

It's so complicated
I'm so frustrated
I wanna hold you close
I wanna push you away
I wanna make you go
I wanna make you stay
Should I say it
Should I tell you how I feel
Oh I want you to know
But then again I don't
It's so complicated

I hate it cause I've waited so long
For someone like you

Should I say it
Should I tell you how I feel
Oh I want you to know
But then again I don't
It's so complicated .")
  #song 430
  Song.create!(artist:  "Lee Ann Womack",
               title: "Ashes By Now",
               rank: 430,
               copyright: "Conexion Media Group, Inc",
               writers: "Rodney Crowell",
               year: 2000,
               lyrics: "This mornin is Monday
Where are you now?
Teasin' my memory
Tellin' me how
To lay low when I don't want to
Your love is elusive
This I know now
It's makin' me crazy
Leavin' me out in the open
When I don't want to

Baby, I can't go through this again
I don't need to go down more than I've already been
Just like a wildfire, you're runnin' all over town
As much as you've burned me baby, I should be ashes by now

The moments of pleasure
Never do last
They're gone like a suitcase
Full of your past
Long gone
And in a hurry

Baby, I can't go through this again
I don't need to go down more than I've already been
Just like a wildfire, you're runnin' all over town
As much as you've burned me baby, I should be ashes by now

You're runnin' all over town
As much as you burned me baby, I should be ashes by now

Just like a wildfire, you're runnin' all over town
As much as you've burned me baby, I should be ashes by now
As much as you've burned me baby, I should be ashes by now

As much as you've burned me baby, I should be ashes by now .")
  #song 431
  Song.create!(artist:  "Alan Jackson",
               title: "Where I Come From",
               rank: 431,
               copyright: "Warner/Chappell Music, Inc",
               writers: "Alan Eugene Jackson",
               year: 2000,
               lyrics: "Well I was rollin' wheels and shiftin' gears
'Round that Jersey Turnpike
When Barney stopped me with his gun
Ten minutes after midnight
Said sir you broke the limit in this rusty ol' truck
I don't know about that accent son
Just where did you come from

I said where I come from
It's cornbread and chicken
Where I come from a lotta front porch sittin'
Where I come from, tryin' to make a livin'
And workin' hard to get to heaven
Where I come from

Well I was south of Detroit City
I pulled in this country kitchen
To try their brand of barbecue
The sign said finger-lickin'
Well I paid the tab and the lady asked me
How'd you like my biscuit
I'll be honest with you ma'am
It ain't like mama fixed it

'Cause where I come from
It's cornbread and chicken
Where I come from a lotta front porch pickin'
Where I come from, tryin' to make a livin'
And workin' hard to get to heaven
Where I come from

I was chasin' sun on 101
Somewhere around Ventura
I lost a universal joint and I had to use my finger
This tall lady stopped and asked
If I had plans for dinner
Said no thanks ma'am, back home
We like the girls that sing soprano

'Cause where I come from
It's cornbread and chicken
Where I come from a lotta front porch sitiin'
Where I come from, tryin' to make a livin'
And workin' hard to get to heaven
Where I come from

Well I was headed home on 65
Somewhere around Kentucky
And the CB rang for a bobtail rig
That's rollin' on like thunder
Well I answered him and he asked me
Aren't you from out in Tulsa
No, but you might'a seen me there
I just dropped a load of salsa

Where I come from
It's cornbread and chicken
Where I come from a lotta front porch pickin'
Where I come from, tryin' to make a livin'
And workin' hard to get to heaven
Where I come from

Where I come from yeah
It's cornbread and chicken
Where I come from a lotta back porch pickin'
Where I come from, tryin' to make a livin'
And workin' hard to get to heaven
Where I come from
Where I come from

Yeah where I come from
A lotta front porch sittin'
Starin' up at heaven
Where I come from
Where I come from
Tryin' to make a livin'
Oh where I come from
Where I come from .")
  #song 432
  Song.create!(artist:  "Trick Pony",
               title: "On A Night Like This",
               rank: 432,
               copyright: "Warner/Chappell Music, Inc",
               writers: "Doug Kahan, Karen Ruth Staley",
               year: 2001,
               lyrics: "Now, when I was a young girl
My dad warned me about
The opposite sex
He's say, 'Yeah, that just what you need
Some crazy boy with wild ideas
You know what always happens next.'

I tried to heed his advice
But now I'm thinking it sound kinda nice

It's just what I need on a night like this
A long walk in the dark
Someone I can't resist
A little rendezvous, a little mystery
When I look at you
I think that's just what I need

Now, my momma told me,
'In this dog-eat-dog world
Baby, you gotta work
Harder than a man'
And she'd say, 'Yeah,
That's just what you need
Some romance book Romeo
Calling you at work
And messing up your plans

But night falls, I slow down
I start dreaming
'Bout you coming around

It's just what I need on a night like this
A long walk in the dark
Someone I can't resist
A little rendezvous, a little mystery
When I look at you
I think that's just what I need

Moonlight, sweet bliss
Melting with every kiss
Dancing real slow
When you hold me close I know

It's just what I need on a night like this
A long walk in the dark
Someone I can't resist
A little rendezvous, a little mystery
When I look at you
I think that's just what I need .")
  #song 433
  Song.create!(artist:  "Brad Paisley",
               title: "Two People Fell In Love",
               rank: 433,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd., Spirit Music Group",
               writers: "Brad Paisley, John Lovelace, Kelley Lovelace, Tim Owens",
               year: 2001,
               lyrics: "A baby's born in the middle of the night in a local delivery room
They grab his feet, smack him 'til he cries he goes home the next afternoon
'Fore you know it he's off to school and then he graduates in May
Goes out and gets a PHD and then cures all sorts of things
Wins a Nobel prize and saves a million different lives
The world's a better place for all he's done
It's funny when you think about the reason he's alive
Is all because two people fell in love

Right now at a picnic shelter down by Caney creek
You'll find potato salad, hot dogs and baked beans
The whole Wilson family's lined up fillin' their paper plates
They've drove or flown in here from fifteen different states
Well Stanley Wilson says that sixty years ago he knew
That Miss Emma Tucker was the one
Now five generations get together every June
All because two people fell in love

There ain't nothin' not affected
When two hearts get connected
All that is, will be, or ever was
Every single choice we make
Every breath we get to take
Is all because two people feel in love

Well, I recall a young man who was driftin' aimlessly
And a young waitress who seemed lonesome as could be
But in a little cafe right off of Fourteenth Avenue
With a whole lotta help from up above
We met and things sure turned around for me and you
And all because two people fell in love

Baby, there ain't nothin' not affected
When two hearts get connected
All that is, will be, or ever was
I'm glad your dad could not resist
Your mama's charms and you exist
All because two people fell in love

You know, to me it's all so clear
Every one of us is here
All because two people fell in love

A baby's born in the middle of the night in a local delivery room
They grab his feet, smack him 'til he cries he goes home the next afternoon .")
  #song 434
  Song.create!(artist:  "Tim McGraw",
               title: "Angry All The Time",
               rank: 434,
               copyright: "Bluewater Music",
               writers: "Bruce Robison",
               year: 2001,
               lyrics: "Here we are
What is left of a husband and a wife with four good kids
Who have a way of gettin on with their lives
And I'm not old but I'm getting a whole lot older every day
It's too late to keep from goin' crazy
I've got to get away

The reasons that I can't stay, don't have a thing to do with being in love
And I understand that lovin' a man shouldn't have to be this rough
And you ain't the only one who feels like this world's left you far behind
I don't know why you gotta be angry all the time

Our boys are strong now, the spittin' image of you when you were young
I hope someday they can see past what you have become
I remember every time I said I'd never leave
But what I can't live with is memories of the way you used to be

The reasons that I can't stay, don't have a thing to do with being in love
I understand that lovin a man shouldn't have to be this rough
You ain't the only one who feels like this world's left you far behind
I don't know why you gotta be angry all the time

Twenty years, have came and went, since I walked out of your door
I never quite made it back, to the one I was before
And God it hurts me to think of you
For the light in your eyes was gone
And sometimes I don't know why this old world can't leave well enough alone

The reasons that I can't stay, don't have a thing to do with being in love
And I understand that lovin, a man shouldn't have to be this rough
You ain't the only one who feels like this world's left you far behind
I don't know why you gotta be angry all the time [Repeat x3] .")
  #song 435
  Song.create!(artist:  "Gary Allan",
               title: "Right Where I Need To Be",
               rank: 435,
               copyright: "Sony/ATV Music Publishing LLC, Round Hill Music Big Loud Songs",
               writers: "Casey Beathard, Casey Michael Beathard, Kendell Marvel, Kendell Wayne Marvel",
               year: 1999,
               lyrics: "There's a plane flyin' outta here tonight:
Destination, New Orleans.
Boss man says my big promotion's on the line:
He says that's right where I need to be.

Lately I've been on the road more than I've been home.
All this leavin' her alone is killin' me.
And holdin' her, right now, has got me thinkin' more an' more,
This is right where I need to be.

Where when I hear her I can see her,
I can smell her sweet perfume.
I can feel her skin against me when I sleep.
Where I won't miss her, I can kiss her anytime that I want to.
Yeah, that's right where I need to be.
Yeah, that's right where I need to be.
Wow.

There's a plane flyin' outta here tonight,
With an empty first-class seat.
'Cause I've finally got all my priorities in line,
And I'm right where I need to be.

Where when I hear her I can see her,
I can smell her sweet perfume.
I can feel her skin against me when I sleep.
Where I won't miss her, I can kiss her anytime that I want to.
Yeah, that's right where I need to be.
Yeah, I'm right where I need to be .")
  #song 436
  Song.create!(artist:  "Jamie O'Neal",
               title: "There Is No Arizona",
               rank: 436,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Jamie O'neal, Lisa A Drew, Shaye Smith",
               year: 2000,
               lyrics: "He promised her a new and better life out in Arizona
Underneath the blue neverending sky, swore that he was gonna
Get things in order he'd send for her
When he left her behind, it never crossed her mind

There is no Arizona
No painted desert, no Sedona
If there was a Grand Canyon
She could fill it up with the lies he's told her
But they don't exist, those dreams he sold her
She'll wak up and find
There is no Arizona

She got a postcard with no return address postmarked Tombstone
It said, I don't know where I'm goin' next, but when I do I'll let you know
May, June, July, she wonders why
She's still waiting, she'll keep waiting ?cause

There is no Arizona
No painted desert, no Sedona
If there was a Grand Canyon
She could fill it up with the lies he's told her
But they don't exist, those dreams he sold her
She'll get there and find
There is no Arizona

Each day the sun sets into the west
Her heart sinks lower in her chest
Friends keep asking when she's going
Finally she tells them, don't you know

There is no Arizona
No painted desert, no Sedona
If there was a Grand Canyon
She could fill it up with the lies he's told her
But they don't exist, those dreams he sold her
She'll get there and find
There is no Arizona .")
  #song 437
  Song.create!(artist:  "Faith Hill",
               title: "If My Heart Had Wings",
               rank: 437,
               copyright: "J. Fred Knobloch Music, BMG Rights Management US, LLC",
               writers: "Annie Leslie Roboff, James Fred Knobloch",
               year: 1999,
               lyrics: "Damn these old wheels
Rolling too slow
I stare down this white line
With so far to go

Headlights keep coming
Loneliness humming along
Who poured this rain
Who made these clouds

I stare through this windshield
Thinking out loud
Time keeps on crawling
Love keeps on calling me home

I'd jump all these mountains and take to the skies
Sail through the heavens with stars in my eyes

If my heart had wings
I would fly to you and lie
Beside you as you dream
If my heart had wings

We both committed
We both agreed
You do what you have to to get what you need
Feeling you near me with so many miles in between
Lord, it ain't easy out here in the dark
To keep us together so far apart

If my heart had wings
I would fly to you and lie
Beside you as you dream
If my heart had wings

Stuck on this circle
Spinning around
Cut loose from this rope
That's tying me down

If my heart had wings
I would fly to you and lie
Beside you as you dream
If my heart had
If my heart had wings
I would fly to you and lie
Beside you as you dream
If my heart had wings .")
  #song 438
  Song.create!(artist:  "Toby Keith",
               title: "How Do You Like Me Now?!",
               rank: 438,
               copyright: "Tokeco Tunes, Roba Music, Sony/ATV Music Publishing LLC",
               writers: "Chuck Cannon, Toby Keith",
               year: 1999,
               lyrics: "Yeah I was always the crazy one
I broke into the stadium
And I wrote your number on the 50 yard line
You were always the perfect one
And the valedictorian so
Under your number I wrote 'call for a good time'

I only wanted to catch your attention
But you overlooked me somehow
Besides you had too many boyfriends to mention
And I played my guitar too loud.

How do you like me now?
How do you like me now,
Now that I'm on my way?
Do you still think I'm crazy
Standin' here today?
I couldn't make you love me
But I always dreamed about living in your radio
How do you like me now?

When I took off to Tennessee
I heard that you made fun of me
Never imagined I'd make it this far
Then you married into money girl
Ain't it a cruel and funny world?
He took your dreams and tore them apart.

He never comes home
And you're always alone
And your kids hear you cryin' down the hall
Alarm clock starts ringin'
Who could that be singin'
Its me baby, with your wake up call!

How do you like me now?
How do you like me now,
Now that I'm on my way?
Do you still think I'm crazy
Standin' here today?
I couldn't make you love me
But I always dreamed about living in your radio
How do you like me now?

Tell me baby
I will preach on .")
  #song 439
  Song.create!(artist:  "Chad Brock",
               title: "Yes!",
               rank: 439,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, Spirit Music Group",
               writers: "Stephony Smith, Chad Brock, Jim Collins",
               year: 2000,
               lyrics: "She moved into my old apartment
That's how we got this whole thing started
She called and said that i had mail waiting there for me
I told her that i'd come and get it
How could i know in just a minute
That i'd be standing face to face with my own destiny

Ohhhh how we sat there talking just like we were old friends
Ohhhh then i asked her can i see you again

She said yes! and i said wow!
She said when and i said how about right now
Love can't wait then i asked if she believed in fate
And she said yes

The days flew by just like a fast train
And nothing else has been on my brain
Except the thought of how she makes me the man i want to be
She's the one i long for a million reasons
Loving her is just like breathing
It's easy and it's obvious she was made for me

Ohhh then it happened one night looking in her eyes
Ohhh when i popped the question much to my sur-prize

She said yes! and i said wow!
She said when and i said how about right now
Love can't wait then i asked if she believed in fate
And she said yes

So we called a preacher, family and friends
And nothing's been the same since?

She said yes! and i said wow!
She said when and i said how about right now
Love can't wait then i asked if she believed in fate
And she said yes .")
  #song 440
  Song.create!(artist:  "Lonestar",
               title: "What About Now",
               rank: 440,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Ron Harbin, Aaron Barker, Anthony Smith",
               year: 1999,
               lyrics: "The sign in the window said for sale or trade
On the last remaining dinosaur Detroit made
Seven hundred dollars was a heck of a deal
For a four-hundred-horsepower jukebox on wheels

And that road rolls out like a welcome mat
I don't know where it goes, but it beats where we're at
We always said someday, somehow
We were gonna get away, gonna blow this town

What about now? How 'bout tonight?
Baby, for once let's don't think twice
Let's take that spin that never ends
That we've been talking about

What about now? Why should we wait?
We can chase these dreams down the interstate
And be long gone 'fore the world moves on and makes another round
What about now?

We've been puttin' this off, baby, long enough
Just give me the word, and we'll be kickin' up dust
We both know it's just a matter of time
Till our hearts start racin' for that county line

What about now? How 'bout tonight?
Baby, for once let's don't think twice
Let's take that spin that never ends
That we've been talking about

What about now? Why should we wait?
We can chase these dreams down the interstate
And be long gone 'fore the world moves on and makes another round
What about now?

We could hang around this town forever making plans
But there won't ever be a better time to take this chance

What about now? How 'bout tonight?
Baby, for once let's don't think twice
Let's take that spin that never ends
That we've been talking about

What about now? Why should we wait?
We can chase these dreams down the interstate
And be long gone 'fore the world moves on and makes another round
What about now?

What about now? .")
  #song 441
  Song.create!(artist:  "Dixie Chicks",
               title: "Cowboy Take Me Away",
               rank: 441,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Kobalt Music Publishing Ltd.",
               writers: "Martha Maguire, Marcus Hummon",
               year: 1999,
               lyrics: "I said, I wanna touch the earth
I wanna break it in my hands
I wanna grow something wild and unruly
I wanna sleep on the hard ground
In the comfort of your arms
On a pillow of blue bonnets
In a blanket made of stars
Oh, it sounds good to me

I said, cowboy take me away
Fly this girl as high as you can into the wild blue
Set me free, oh, I pray
Closer to heaven above and closer to you
Closer to you

I wanna walk and not run
I wanna skip and not fall
I wanna look at the horizon and not see a building standing tall
I wanna be the only one for miles and miles
Except for maybe you and your simple smile
Oh, it sounds good to me
Yes, it sounds so good to me

Cowboy take me away
Fly this girl as high as you can into the wild blue
Set me free, oh, I pray
Closer to heaven above and closer to you
Closer to you

I said, I wanna touch the earth
I wanna break it in my hands
I wanna grow something wild and unruly
Oh, it sounds so good to me

Cowboy take me away
Fly this girl as high as you can into the wild blue
Set me free, oh, I pray
Closer to heaven above and closer to you
Closer to you
Closer to you
Cowboy take me away
Closer to you .")
  #song 442
  Song.create!(artist:  "Faith Hill",
               title: "The Way You Love Me",
               rank: 442,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, Ole Media Management Lp",
               writers: "Keith Follese, Michael William Dulaney",
               year: 1999,
               lyrics: "If I could grant
You one wish
I'd wish you could see the way you kiss
Ooh, I love watching you

Baby
When you're driving me crazy
Ooh, I love the way you
Love the way you love me

There's nowhere else I'd rather be
Ooh, to feel the way I feel with your arms around me
I only wish that you could see the way you love me
The way you love me

It's not right
It's not fair
What you're missing over there
Someday I'll find a way to show you

Just how lucky I am to know you
Ooh, I love the way you
Love the way you love me
There's nowhere else I'd rather be

Ooh, to feel the way I feel with your arms around me
I only wish that you could see the way you love me
The way you love me
You're the million reasons why

There's love reflecting in my eyes
Ooh, I love the way you
Love the way you love me
There's nowhere else I'd rather be

Ooh, to feel the way I feel with your arms around me
I only wish that you could see the way you love me
The way you love me
The way you love me

Ooh, the way you love me
The way you love me .")
  #song 443
  Song.create!(artist:  "George Strait",
               title: "The Best Day",
               rank: 443,
               copyright: "Warner/Chappell Music, Inc, Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Carson Chamberlain, Dean Dillon",
               year: 2000,
               lyrics: "We loaded up my old station wagon
With a tent, Coleman, sleeping bags
Some fishin' poles, a cooler of cokes,
Three days before we had to back
When your seven and your in your seventh heaven
Going campin' in the wild outdoors
As we turned off on that old dirt road he look at me and swore
Dad, this could be the best day of my life
Been dreamin' day and night about the fun we'll have
Just me and you doing what I've always wanted to
I'm the luckiest boy alive
This is the best day of my life

His fifteenth birthday rolled around
Classic cars were his thing
When I pulled in the drive with that old 'vette
I thought that boy would go insane
When you're in your teens your dreams revolve around four spinning wheels
We're not zoned in till it was new again
And as he sat behind the wheel
He said Dad this could be the best day of my life
Been dreamin' day and night about the fun we've had
Just me and you doin' what I've always wanted too
I'm the luckiest boy around
This is the best day of my life

Standin' in the middle of the room
Back of the church with our tuxes on
Lookin' at him I said
I can't believe son that you've grown
He said dad this could be the best day of my life
Been dreamin' day and night
About being like you
Just me and her
Watching you and mom I've learned
I'm the luckiest man alive
This is the best day of my life

I'm the luckiest man alive
This is the best day of my life .")
  #song 444
  Song.create!(artist:  "Lee Ann Womack",
               title: "I Hope You Dance",
               rank: 444,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Tia Sillers, Mark Sanders",
               year: 2000,
               lyrics: "I hope you never lose your sense of wonder
You get your fill to eat but always keep that hunger
May you never take one single breath for granted
God forbid love ever leave you empty handed
I hope you still feel small when you stand beside the ocean
Whenever one door closes I hope one more opens
Promise me that you'll give faith a fighting chance
And when you get the choice to sit it out or dance

I hope you dance
I hope you dance

I hope you never fear those mountains in the distance
Never settle for the path of least resistance
Livin' might mean takin' chances, but they're worth takin'
Lovin' might be a mistake, but it's worth makin'
Don't let some Hellbent heart leave you bitter
When you come close to sellin' out, reconsider
Give the heavens above more than just a passing glance
And when you get the choice to sit it out or dance

I hope you dance (Time is a wheel in constant motion always rolling us along)
I hope you dance

I hope you dance (Tell me who wants to look back on their years and wonder)
I hope you dance (Where those years have gone?)

I hope you still feel small when you stand beside the ocean
Whenever one door closes I hope one more opens
Promise me that you'll give faith a fighting chance
And when you get the choice to sit it out or dance

Dance

I hope you dance
I hope you dance (Time is a wheel in constant motion always rolling us along)
I hope you dance (Tell me who wants to look back on their years and wonder?) .")
  #song 445
  Song.create!(artist:  "Tim McGraw",
               title: "My Best Friend",
               rank: 445,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Evert Abbing, Tijn Touber",
               year: 1999,
               lyrics: "I never had no one that I could count on
I've been let down so many times
I was tired of hurtin' so tired of searchin'
Til you walked into my life
It was a feelin' I'd never known
And for the first time I didn't feel alone

You're more than a lover
There could never be another
To make me feel the way you do
Oh we just get closer
I fall in love all over
Every time I look at you
I don't know where I'd be
Without you here with me
Life with you makes perfect sense
You're my best friend
You're my best friend

You stand by me you believe in me
Like nobody ever has
When my world goes crazy
You're right there to save me
You make me see how much I have
And I still tremble when we touch
And oh the look in your eyes
When we make love

You're more than a lover
There could never be another
To make me feel the way you do
Oh we just get closer
I fall in love all over
Every time I look at you
And I don't know where I'd be
Without you here with me
Life with you makes perfect sense
You're my best friend
You're my best friend

You're more than a lover
There could never be another
To make me feel the way you do
And oh we just get closer
I fall in love all over
Every time I look at you
And I don't know where I'd be
Without you here with me
Life with you makes perfect sense
You're my best friend
You're my best friend
You're my best friend .")
  #song 446
  Song.create!(artist:  "Jo Dee Messina",
               title: "That's The Way",
               rank: 446,
               copyright: "BMG Rights Management US, LLC",
               writers: "Annie Roboff, Mary Holladay (holly) Lamar",
               year: 2000,
               lyrics: "Everybody wants an easy ride
On the merry-go-round that we call life
Take your drive on cruise control
Then you wait to find out it's a winding road
I had my dreams in view
When the money ran out and the engine blew

Hung my tears out to dry
Then my dreams fell out of that clear blue sky
And I, I was walkin' the clouds
Feelin' so safe and sound
Then somethin' else knocks me down

Well, Oh, That's the way it is
You gotta roll with the punches
That's the way it goes
You gotta bend when the wind blows
You live you learn
You crash and burn
It's hit or miss
And that's the way it is

One fine day you wake up
Completely, hopelessly fallen in love
He's just what you're lookin' for
The only problem is, that the man's not sure
Another guy'll give you everything

The only problem is you don't feel a thing
Well I know from experience
Nothin's ever gonna make perfect sense
Oh, one day you get what you want
But it's not what you think
Then you get what you need

Well, Oh, That's the way it is
You gotta roll with the punches
That's the way it goes
You gotta bend when the wind blows
You live you learn
You crash and burn
It's hit or miss
And that's the way it is

Yeah they say your soul is growin'
But sometimes I feel like throwin' somethin'

Well, Oh, That's the way it is
You gotta roll with the punches
That's the way it goes
You gotta bend when the wind blows
Well, That's the way it is
You gotta roll with the punches
That's the way it goes
You gotta bend when the wind blows
You live you learn
You crash and burn
It's hit or miss
And that's the way it is .")
  #song 447
  Song.create!(artist:  "SHeDAISY",
               title: "I Will... But",
               rank: 447,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd.",
               writers: "Kristyn Osborn",
               year: 1999,
               lyrics: "I won't be bored
I won't be ignored
Hey!

I won't be your dirty secret
I won't be your cure-all pill
And I won't run to fetch the water
Just to tumble down the hill

I won't be your Friday paycheck
I won't be the prize you flaunt
And I won't be your Martha Stewart, baby
Or your all-night restaurant

But I will, I will, I will be your everything
If you make me feel like a woman should
I will, I will, I will be the whole shebang
You know I will, but

I won't be your crutch to lean on
I won't wear stiletto heels
I won't walk a mile in your shoes
Just so I know how it feels

I won't be your obligation
I won't be your Barbie doll
I won't be the portrait of perfection
To adorn you wall

But I will, I will, I will be your everything
If you make me feel like a woman should
I will, I will, I will be the whole shebang
You know I will, but

Hey, you know, you know I will
All right

I won't be your lifetime girlfriend
I won't be just one of the guys
I won't be your mama's favorite
I refuse to be the last in line

But I will, I will, I will be your everything
If you make me feel like a woman should
I will, I will, I will be the whole shebang
You know I will...but

Yeah, I will, I will, I will be your everything
I will, I will be the whole shebang
I will, I will be your everything
I will, I will, I will, I will, yeah

You know I will
You know, you know I will
You know I will
You know, you know I will, yeah .")
  #song 448
  Song.create!(artist:  "Alan Jackson",
               title: "It Must Be Love",
               rank: 448,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd., Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Alan Jackson",
               year: 1999,
               lyrics: "First I get cold then hot,
Think I'm on fire I'm not,
Oh, what a pain I've got, it must be love
There's nothing I can do,
All that I want is you,
Look what I'm going through, it must be love

It must be love, it must be love,
I fall like a sparrow, and fly like a dove,
You must be the dream I've been dreamin' of
Oh, what a feelin', it must be love

Something is wrong alright,
I think of you all night,
Can't sleep 'till mornin' light, it must be love,
Seein' you in my dreams,
Holdin' you close to me,
Oh, what else can it be, it must be love,

It must be love, it must be love,
I fall like a sparrow, and fly like a dove,
You must be the dream I've been dreamin' of
Oh, what a feelin', it must be love

It must be love, it must be love,
I fall like a sparrow, and fly like a dove,
You must be the dream I've been dreamin' of
Oh, what a feelin', it must be love

It must be love, it must be love,
I fall like a sparrow, and fly like a dove,
You must be the dream I've been dreamin' of
Oh, what a feelin', it must be love

It must be love, it must be love,
I fall like a sparrow, and fly like a dove,
You must be the dream I've been dreamin' of
Oh, what a feelin', it must be love

It must be love, it must be love,
I fall like a sparrow, and fly like a dove,
You must be the dream I've been dreamin' of
Oh, what a feelin', it must be love .")
  #song 449
  Song.create!(artist:  "Andy Griggs",
               title: "She's More",
               rank: 449,
               copyright: "Warner/Chappell Music, Inc, Spirit Music Group, BMG Rights Management US, LLC",
               writers: "Rob Crosby, Liz Hengber",
               year: 1999,
               lyrics: "I like blue eyes, hers are green
Not like the woman of my dreams
And her hair's not quite as long as I had planned
Five foot three isn't tall
She's not the girl I pictured at all
In those paint by number fantasies I've had

So it took me by complete surprise
When my heart got lost in those deep green eyes
She's not at all what I was looking for
She's more

No, it wasn't at first sight
But the moment I looked twice
I saw the woman I was born to love
Her laughter fills my soul
And when I hold her I don't want to let go
When it comes to her I can't get enough

So it took me by complete surprise
When my heart got lost in those deep green eyes
She's not at all what I was looking for
She's more

More than I dreamed of
More than any man deserves
I couldn't ask for more
Than a love like hers

So it took me by complete surprise
When my heart got lost in those deep green eyes
She's not at all what I was looking for
She's more .")
  #song 450
  Song.create!(artist:  "Faith Hill",
               title: "Breathe",
               rank: 450,
               copyright: "BMG Rights Management US, LLC",
               writers: "Benjamin Curtis, Brandon Curtis, Josh Garza",
               year: 1999,
               lyrics: "I can feel the magic floating in the air
Being with you get's me that way
I watch the sunlight dance across your face
And I've never been this swept away

All my thoughts just seem to settle on the breeze
When I'm lying wrapped up in your arms
The whole world just fades away
The only thing I hear
Is the beating of your heart

'Cause I can feel you breathe
It's washing over me
And suddenly I'm melting into you
There's nothing left to prove
Baby, all we need is just to be
Caught up in the touch
Slow and steady rush
Baby, isn't that the way that love's suppose to be
I can feel you breathe, just breathe

In a way I know my heart is waking up
As all the walls come tumbling down
Closer than I've ever felt before
And I know, and you know
There's no need for words right now

I can feel you breathe
Washing over me
And suddenly I'm melting into you
There's nothing left to prove
Baby, all we need is just to be
Caught up in the touch
Slow and steady rush
Baby, isn't that the way that love's suppose to be
I can feel you breathe, just breathe

Caught up in the touch
Slow and steady rush
Baby, isn't that the way that love's suppose to be
I can feel you breathe, just breathe

I can feel the magic floating in the air
Being with you gets me that way .")
  #song 451
  Song.create!(artist:  "Rascal Flatts",
               title: "Prayin' For Daylight",
               rank: 451,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group",
               writers: "Steve Bogard, Richard C. Giles",
               year: 2000,
               lyrics: "I don't want to spend another lonely night... Ooooh
I've got the lights turned up, the door is locked
The bedroom tv's on
Doin' the only thing that gets me through
The night since you've been gone

Prayin' for daylight
Waitin' for that mornin' sun
So I can act like my whole life ain't goin' wrong
Baby come back to me
I swear I'll make it right
Don't make me spend another lonely night
Prayin' for daylight (Prayin' for daylight)

I made a bad miscalculation, bettin'
You would never leave (Ooooh)
'Cause if you're gettin' on with your new life
Then where does that leave me?

Prayin' for daylight
Waitin' for that mornin' sun
So I can act like my whole life ain't goin' wrong
Baby come back to me
I swear I'll make it right
Don't make me spend another lonely night (spend... another lonely night)
Prayin' for daylight (Prayin' for daylight)
Prayin' for daylight (Prayin' for daylight)

Prayin' for daylight
Hopin' that I didn't wait too long (I didn't wait too long)
But this is just the dark before the dawn......Oh

Deep in my heart I know that you love me
As much as I love you (You know I love you girl)
And that you must be lyin' somewhere
Lookin' up to heaven too

Prayin' for daylight
Waitin' for that mornin' sun
So I can act like my whole life ain't goin' wrong
Baby come back to me
I swear I'll make it right
Don't make me spend another lonely night
(Make me spend another lonely night)

Prayin' for daylight
Waitin' for that mornin' sun
So I can act like my whole life ain't goin' wrong
Baby come back to me
I swear I'll make it right
Don't make me spend another lonely night (another lonely night)
Prayin' for daylight (Prayin' for daylight)

Prayin' for daylight

Prayin' for daylight
(I don't want to spend another lonely night) Ooooh
Prayin' for daylight .")
  #song 452
  Song.create!(artist:  "Keith Urban",
               title: "Your Everything",
               rank: 452,
               copyright: "Warner/Chappell Music, Inc, Reservoir Media Management Inc",
               writers: "Christopher Marsh Lindsey, Robert Joseph Regan",
               year: 1999,
               lyrics: "The first time I looked in your eyes I knew
That I would do anything for you
The first time you touched my face I felt
Like I've never felt with anyone else

I wanna give back what you've giving to me
And I wanna witness all of your dreams
Now that you've shown me who I really am
I wanna be more than just your man

I wanna be the wind that fills your sails
And be the hand that lifts your veil
And be the moon that moves your tide
The sun coming up in your eyes
Be the wheels that never rust
And be the spark that lights you up
All that you've been dreaming of and more
So much more, I wanna be your everything

When you wake up, I'll be the first thing you see
And when it gets dark you can reach out for me
I'll cherish your words and I'll finish your thoughts
And I'll be your compass baby, when you get lost

I wanna be the wind that fills your sails
And be the hand that lifts your veil
And be the moon that moves your tide
The sun coming up in your eyes
Be the wheels that never rust
And be the spark that lights you up
All that you've been dreaming of and more
So much more, I wanna be your everything

Be the wheels that never rust
And be the spark that lights you up
All that you've been dreaming of and more
So much more, I wanna be your everything
I wanna be your everything .")
  #song 453
  Song.create!(artist:  "Martina McBride",
               title: "Love's The Only House",
               rank: 453,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Buzz Cason, Tom Douglas",
               year: 1999,
               lyrics: "I was standing in the grocery store line
The one they marked express
When this woman came through with about 25 things
And I said don't you know that more is less
She said this world is moving so fast
I just get more behind with every day
And every morning when I make my coffee
I can't believe my life's turned out this way
All I could say was

Love's the only house big enough for all the pain in the world
Love's the only house big enough for all the pain

He was walking by the other day and I said
Hey baby how you been?
Yeah I got me a little girl now and she's 4 years old
And she's got her daddy's little grin
And you only want what you can't have
And baby you can't have me now
I gave my heart to another
Yeah I'm a mother and he's a father and we're a family
And we've got each other
And I found out the hard way that

Love's the only house big enough for all the pain in the world
Love's the only house big enough for all the pain

You drive three miles from all this prosperity
Down across the river and you see a ghetto there
And we got children walking around with guns
And they got knives with drugs and pain to spare
And here I am in my clean, white shirt
With a little money in my pocket and a nice warm home
And we got teenagers walkin' around in a culture of darkness
Livin' together alone, and all I can say is

Love's the only house big enough for all the pain in the world
Love's the only house big enough for all the pain

And I can't explain it and I can't understand
But I'll come down and get my hands dirty and together we'll make a stand
Somewhere cross the parking lot some bands playin' out of tune
City streets are gonna burn if we don't do something soon
And senorita can't quit cryin', baby's due now any day
Don Juan left, got sick of tryin'
No one there to show him the way
She came down to the grocery store and
She said I, I wanna buy a little carton of milk but I don't have any money
I said hey I'll cover you honey cause the pain's gotta go somewhere
Yeah the pain's gotta go someplace
So come on down to my house
Don't you know that

Love's the only house big enough for all the pain in the world
Love's the only house big enough for all the pain

Don't you know that
Love's the only house big enough for all the pain in the world
Love's the only house big enough for all the pain .")
  #song 454
  Song.create!(artist:  "Tracy Lawrence",
               title: "Lessons Learned",
               rank: 454,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd.",
               writers: "L. Boone, Paul Nelson, Tracy Lawrence",
               year: 2000,
               lyrics: "I was ten years old the day I got caught
With some dime store candy that I never bought
I hung my head and I faced the wall
As Daddy showed me wrong from right
He said this hurts me more than it does you
There's just some things son, that you just don't do
Is anything I'm sayin' getting through
Daddy I can see the light
Oh Lessons learned, man they sure run deep
They don't go away and they don't come cheap
Oh there's no way around it
Cause this world turns on lessons learned
Granddaddy was a man I loved
He bought me my first ball and glove
Even taught me how to drive his old truck
Circling that ol' town square
He spoke of life with a slow southern drawl
I never heard him cause I knew it all
But I sure listened when I got the call
That he was no longer there
Oh Lessons learned, man they sure run deep
They don't go away and they don't come cheap
Oh there's no way around it
Cause this world turns
On lessons learned .")
  #song 455
  Song.create!(artist:  "Kenny Rogers",
               title: "Buy Me A Rose (f. Alison Krauss, Billy Dean)",
               rank: 455,
               copyright: "BMG Rights Management US, LLC",
               writers: "Erik W Hickenlooper, Jim Funk",
               year: 1999,
               lyrics: "He works hard to give her all he thinks she wants
A three car garage, her own credit cards
He pulls in late to wake her up with a kiss good night
If he could only read her mind, she'd say:

Buy me a rose, call me from work
Open a door for me, what would it hurt
Show me you love me by the look in your eyes
These are the little things I need the most in my life

Now the days have grown to years of feeling all alone
And she can't help but wonder what she's doing wrong
Cause lately she'd try anything to turn his head
Would it make a difference if she said:

Buy me a rose, call me from work
Open a door for me, what would it hurt
Show me you love me by the look in your eyes
These are the little things I need the most in my life

And the more that he lives the less that he tries
To show her the love that he hold inside
And the more that she gives the more that he sees
This is a story of you and me

So I bought you a rose on the way home from work
Opened the door to a heart that I hurt
And I hope you notice this look in my eyes
Cause I'm gonna make things right
I'm gonna hold you tonight
Do all those little things
For the rest of your life .")
  #song 456
  Song.create!(artist:  "Clay Walker",
               title: "The Chain Of Love",
               rank: 456,
               copyright: "Ole MM, Waterdance Music, Ole Media Management Lp",
               writers: "Jonnie Barnett, Rory Lee",
               year: 1999,
               lyrics: "He was driving home one evening
In his beat-up Pontiac
When an old lady flagged him down
Her Mercedes had a flat

He could see that she was frightened
Standing out there in the snow
'Til he said 'I'm here to help you, ma'm
By the way, my name's Joe.'

She said 'I'm from St. Louis
And I'm only passing through
I must've seen a hundred cars go by
This is awful nice of you.'

When he changed the tire
And closed her trunk
And was about to drive away
She said 'How much do I owe you?'
Here's what he had to say

'You don't owe me a thing
I've been there too
Someone once helped me out
Just the way I'm helping you
If you really wanna pay me back
Here's what you do
Don't let the chain of love end with you.'

Well, a few miles down the road
The lady saw a small cafe
She went in to grab a bite to eat
And then be on her way

But she couldn't help but notice
How the waitress smiled so sweet
She must have been eight months
Along and dead on her feet

No, she didn't know her story
And she probably never will
When the waitress went to get her
Change from a hundred dollar bill

The lady slipped right out the door
And on a nakpin left a note
There were tears in the waitress' eyes
When she read what she wrote

'You don't owe me a thing
I've been there too
Someone once helped me out
Just the way I'm helping you
If you really wanna pay me back
Here's what you do
Don't let the chain of love end with you.'

That night when she got
Home from work
The waitress climbed into bed
She was thinking about the money
And what the lady's note had said
As her husband lay there sleeping
She whispered soft and low
'Everything's gonna be alright
I love you, Joe. '")
  #song 457
  Song.create!(artist:  "Lonestar",
               title: "Smile",
               rank: 457,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Ole MM, Universal Music Publishing Group, Reservoir One Music",
               writers: "Christopher Charles Lloyd, Leon Ware, Curtis Jr. Roberson, Syreeta Wright, Curtis James Jackson",
               year: 1999,
               lyrics: "I still remember the night we met
You said you loved my smile
But your love was like a summer breeze
Oh it lasted for a while

I could hold on a little tighter I know
But when you love someone you gotta let 'em go so

I'm gonna smile 'cause I want to make you happy
Laugh so you can't see me cry
I'm gonna let you go in stlye
And even if it kills me
I'm gonna smile

Kiss me once for the good times baby
Kiss me twice for goodbye
You can't help what you don't feel
And it doesn't matter why

Give me a chance to bow out gracefully
Because that's how I want you to remember me

I'm gonna smile, 'cause I want to make you happy
Laugh, so you can't see me cry
I'm gonna let you go in style
And even if it kills me
I'm gonna smile

I'm gonna smile so you can find the courage
Laugh, so you won't see me hurtin'
I'm gonna let you go in style
And even if it kills me
I'm gonna smile .")
  #song 458
  Song.create!(artist:  "Phil Vassar",
               title: "Carlene",
               rank: 458,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Charlie Black, Phil Vassar, Phil Vasser, Rory Michael Bourke",
               year: 2000,
               lyrics: "I was lousy at math, a failed historian
Carlene was the valedictorian
I was the quarterback in the back of the class
She was the whiz kid in the horned-rimmed glasses
It'd been a long time since I'd been around
Since I'd set foot on my old stomping ground
I ended up by the old schoolyard
And this redhead pulls up in a blue sports car and says
?I'll bet you don't remember, I guess it's been forever?
It took a second to put it all together, I said

Oo-la-la-la-la-la lookin' good Carlene
Oh-my-my-my-my-my time's been good to you
If you know what I mean, girl you glitter like Hollywood
Good God Carlene, you sure are lookin' good

She said, ?Hop in, let's go for a spin
Tell me what you been doing boy, and where you been?
I said, ?I write songs, you probably didn't know
I finally got a couple on country radio?
She said, ?What a switch, ain't it funny
I've got a PHD., Now I'm modeling for money
Imagine little miss four point o
Smiling for the camera on the cover of Vogue?
I said, ?You've come long way since graduation
Let me say without reservation
You've surpassed everyone's expectations?

Oo-la-la-la-la-la lookin' good Carlene
Oh-my-my-my-my-my time's been good to you
If you know what I mean, girl you glitter like Hollywood
Good God Carlene, you sure are lookin' good

I said, ?I hope to see you again someday?
She said, ?Another ten years is too long to wait
Pick me up at my Mama's at eight, ? I said

Oo-la-la-la-la-la lookin' good Carlene
Oh-my-my-my-my-my time's been good to you
If you know what I mean, girl you glitter like Hollywood
Good God Carlene, you sure are lookin' good .")
  #song 459
  Song.create!(artist:  "Mark Wills",
               title: "Back At One",
               rank: 459,
               copyright: "Universal Music Publishing Group",
               writers: "Brian Kelly Mcknight",
               year: 2000,
               lyrics: "It's undeniable, that we should be together
It's unbelievable, how I used to say that I'd fall never
It's basis you need to know you know just how I feel
And let me show you now that I'm for real
If all things in time, time will reveal

You're like a dream come true

Just want to be with you

Girl, it's plain to see that you're the only one for me and

Repeat steps 1 through 3

Make you fall in love with me
If ever I believe my work is done
Then I start back at one

It's so incredible the way things work themselves out
And unemotional once you know what it's all about
And undesirable for us to be apart
I never would have made it very far
'Cause you know you hold the keys to me heart 'cause

You're like a dream come true

Just want to be with you

Girl, it's plain to see that you're the only one for me and

Repeat steps 1 through 3

Make you fall in love with me
If ever I believe my work is done
Then I start back at one

Say farewell to the darkened night
I see the coming of the sun
I feel like a little child whose life has just begun
You came and breathed new life
Into this lonely heart of mine
You threw out the lifeline just in the nick of time

You're like a dream come true

Just want to be with you

Girl, it's plain to see that you're the only one for me and

Repeat steps 1 through 3

Make you fall in love with me
If ever I believe my work is done
Then I start back at one

You're like a dream come true

Just want to be with you

Girl, it's plain to see that you're the only one for me and

Repeat steps 1 through 3

Make you fall in love with me
If ever I believe my work is done
Then I start back at one .")
  #song 460
  Song.create!(artist:  "Joe Diffie",
               title: "It's Always Something",
               rank: 460,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group, Round Hill Music Big Loud Songs",
               writers: "Marv Green, Aimee Mayo",
               year: 1999,
               lyrics: "Downtown, stoplight, red car rolls by.
What else can I do, but wonder if it's you.
Scanning the stations, every song they're playin'
is like a soundtrack, to the love we had.
I should be over you by now, but every time I turn around...

It's always somethin' every day remindin' me.
Everywhere I go, there's a memory.
Ready and awaitin' to catch me off guard,
There's no way to prepare my heart.
What I wouldn't give, what I wouldn't do.
If only I could just stop missin' you.
I'd let go of our love, like it was never nothing.
But it's always something.

Lunch time, a new place.
Waitress says her name.
Why does it have to be, Emily.
Corner store, stop in.
Someone ask, 'How ya been?'
I say I wouldn't know.
I really gotta go.
Wish I could forget you, but I don't know how.
Cause every time I turn around......

It's always somethin' every day remindin' me.
Everywhere I go, there's a memory.
Ready and awaitin' to catch me off guard,
There's no way to prepare my heart.
What I wouldn't give, what I wouldn't do.
If only I could just stop missin' you.
I'd let go of our love, like it was never nothing.
But it's always something .")
  #song 461
  Song.create!(artist:  "Clay Davidson",
               title: "Unconditional",
               rank: 461,
               copyright: "Warner/Chappell Music, Inc, Universal Music Publishing Group, Spirit Music Group",
               writers: "Deanna Bryant, Liz Hengber, Rivers Rutherford",
               year: 2000,
               lyrics: "Daddy waited up, in the kitchen by himself
I came stumbling in that night
With liquor on my breath
He said, 'Son I know you live here
But this is still my home
It's my way or the highway'
So I said alright, I'm gone

And before I slammed that door
I said, 'I hate you'
But he just shook his head, and said ok

But you can't stop my love for you
It'll be here that's a given
As long as I am living on this earth
One thing is true
You will turn away, forget me
Curse my name but love won't let me let you go
Son always know, my love is unconditional

Life is like a circle
Slowing turning on itself
And girl it took losing you
To finally know how daddy felt
But we stood in this bedroom
A year ago today
Hangin' on to pride and anger
As we threw our love away

And before you slammed that door
You said, 'I hate you'
But tonight if only you could hear me say

That you can't stop my love for you
It'll be here that's a given
As long as I am living on this earth
One thing is true
You will turn away, forget me
Curse my name but love won't let me let you go
Girl always know
My love is unconditional

You will turn away, forget me
Curse my name, but you can even let me go
But you need to know
My love is unconditional .")
  #song 462
  Song.create!(artist:  "Aaron Tippin",
               title: "Kiss This",
               rank: 462,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Mike Curb Music, BMG Rights Management US, LLC, Thea Later Music",
               writers: "Phillip A. Douglas, Aaron Dupree Tippin, Thea Tippin",
               year: 2000,
               lyrics: "She was a woman on a mission
Here to drown him and forget him
So I set her up again to wash him down
She has just about succeed
With that low-down, no good cheating
Good for nothing, came strutting through the crowd
Oh he was laying it on so thick
He never missed a lick
Professing his never-ending love
Oh but I never will forget
When she stood up and said
'So I guess you think we are just gonna kiss and make up don't ya?'

That's when she said

Why don't you kiss, kiss this!
And I don't mean on my rosy red lips
And me and you
(Me and you)
We're through
(We're through)
And there's only one thing left for you to do
You just come on over hereon last time
Pucker up and close your eyes
And kiss this, goodbye

Well, the next thing I recall
She had him back against the wall
Chewing him like a bulldog on a bone
She was putting him in his place
I mean right up in his face
Dragging him down a list of done did wrongs
It was just about now
That the crowd gathered around
They come to watch him pay for his every sin
She called him everything under the sun
And when we thought she was done
She rared back and she let him have it again, MAN

She said...she said

Why don't you kiss, kiss this!
And I don't mean on my rosy red lips
And me and you
(me and you)
We're through
(we're through)
And there's only one thing left for you to do
You just come on over here one last time
Pucker up and close your eyes
And kiss this,

Why don't you kiss, kiss this!
And I don't mean on my rosy red lips
And me and you
(me and you)
We're through
(we're through)
And there's only one thing left for you to do
You just come on over here one last time
Pucker up and close your eyes
And kiss this, goodbye

Kiss this, goodbye

See ya .")
  #song 463
  Song.create!(artist:  "Collin Raye",
               title: "Couldn't Last A Moment",
               rank: 463,
               copyright: "Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Danny Wells, Jeffrey Steele",
               year: 2000,
               lyrics: "I thought it was over
I thought I could move on
But I was wrong
I woke up last night
Calling your name
Feeling the blame baby
Thought I could quit you but
I still miss your love
What was I thinking
Thinking that I could still
Walk down the street
Without you by my side
Or make it through one night

Alone I lied
I said a lot of things
I didn't really mean
How can I make you see
What matters most to me
Girl I shoulda known it
I couldn't last a moment without you
You've got every right
To turn and walk away
I can't make you stay
I broke your heart

I wasted so much precious time baby
I see you with your friends
Wearing a smile again
What was I thinking
Thinking that I could still
Walk down the street
Without you by my side
Or make it through one night

Alone I lied
I said a lot of things
I didn't really mean
How can I make you see
What matters most to me
Girl I shoulda known it
I couldn't last a moment without you
Thought I could quit you
But I still miss your love
What was I thinking
Thinking that I could still
Walk down the street
Without you by my side
Or make it through one night

Alone I lied
I said a lot of things
I didn't really mean
How can I make you see
What matters most to me
Girl I shoulda known it
I couldn't last a moment without you
Oh girl I shoulda known it
I couldn't last a moment
Without you .")
  #song 464
  Song.create!(artist:  "LeAnn Rimes",
               title: "I Need You",
               rank: 464,
               copyright: "EMI Music Publishing, Kobalt Music Publishing Ltd., Warner/Chappell Music, Inc, Universal Music Publishing Group, Mike Curb Music, Capitol Christian Music Group",
               writers: "Dennis Matkosky, Ty Lacy",
               year: 1998,
               lyrics: "I don't need a lot of things
I can get by with nothing
With all the blessings life can bring
I've always needed something
But I've got all I want
When it comes to loving you
You're my only reason
You're my only truth

I need you like water
Like breath, like rain
I need you like mercy
From heaven's gate
There's a freedom in your arms
That carries me through
I need you

You're the hope that moves me
To courage again, oh yeah
You're the love that rescues me
When the cold winds, rage
And it's so amazing
'Cause that's just how you are
And I can't turn back now
'Cause you've brought me too far

I need you like water
Like breath, like rain
I need you like mercy
From heaven's gate
There's a freedom in your arms
That carries me through
I need you
Oh yes I do

I need you like water
Like breath, like rain
I need you like mercy
From heaven's gate
There's a freedom in your arms
That carries me through
I need you
Oh yes I do
I need you
I need you .")
  #song 465
  Song.create!(artist:  "Reba McEntire",
               title: "I'll Be",
               rank: 465,
               copyright: "Alfred",
               writers: "Diane Eve Warren",
               year: 1999,
               lyrics: "When darkness falls upon your heart and soul.
I'll be the light that shines for you.
When you forget how beautiful you are
I'll be there to remind you.
When you can't find your way,
I'll find my way to you.
When troubles come around,
I will come to you.

I'll be your shoulder when you need someone to lean on.
Be your shelter.
When you need someone to see you through.
I'll be there to carry you.
I'll be there.
I'll be the rock that will be strong for you.
The one that will hold on to you.
When you feel that rain falling down.
When there's nobody else around.
I'll be.

And when you're there with no one there to hold.
I'll be the arms that reach for you.
And when you feel your faith is running low.
I'll be there to believe in you.
When all you find are lies.
I'll be the truth you need.
When you need someone to run to.
You can run to me

I'll be your shoulder when you need someone to lean on.
Be your shelter.
When you need someone to see you through.
I'll be there to carry you.
I'll be there.
I'll be the rock that will be strong for you.
The one that will hold on to you.
When you feel that rain falling down.
When there's nobody else around.
I'll be.

I'll be the sun.
When your heart's filled with rain.
I'll be the one.
To chase the rain away.

I'll be your shoulder when you need someone to lean on.
Be your shelter.
When you need someone to see you through.
I'll be there to carry you.
I'll be there.
I'll be the rock that will be strong for you.
The one that will hold on to you.
When you feel that rain falling down.
When there's nobody else around.
I'll be.
I'll be .")
  #song 466
  Song.create!(artist:  "Vince Gill",
               title: "Feels Like Love",
               rank: 466,
               copyright: "Kobalt Music Publishing Ltd.",
               writers: "Vincent Grant Gill",
               year: 2000,
               lyrics: "Look what my heart has gotten into
The sweetest gift I ever knew
It's even better than my favorite shoes
Look what my heart has found in you

Feels like sunshine, feels like rain
Lord it feels like love finally called my name
I want to jump and shout I want to sing and dance
Lord it feels like love wants a second chance

Look what my heart can clearly see
How much I crave your company
A true companion I will always be
Look what my heart has done to me

Feels like sunshine, feels like rain
Lord it feels like love finally called my name
I want to jump and shout I want to sing and dance
Lord it feels like love wants a second chance

Looks like my heart has become
The safest place for us to run
I'll be here for you when the day is done
Looks like my heart has found someone

Feels like sunshine, feels like rain
Lord it feels like love finally called my name
I want to jump and shout I want to sing and dance
Lord it feels like love wants a second chance .")
  #song 467
  Song.create!(artist:  "Eric Heatherly",
               title: "Flowers On The Wall",
               rank: 467,
               copyright: "Warner/Chappell Music, Inc",
               writers: "Lewis Dewitt",
               year: 2000,
               lyrics: "I keep hearin' your concern about my happiness
But all that thought you've given me is, conscience I guess
If I were walkin' in your shoes, I wouldn't worry none
While you and your friends are worryin''bout me, I'm havin' lots of fun

Countin' flowers on the wall, that don't bother me at all
Playin' solitaire 'til dawn, with a deck of fifty-one
Smokin' cigarettes and watchin' Captain Kangaroo
Now don't tell me
I've nothin' to do

Last night I dressed in tails, pretended I was on the town
As long as I can dream it's hard to slow this swinger down
So please don't give a thought to me, I'm really doin' fine
You can always find me here and havin' quite a time

It's good to see you, I must go, I know I look a fright
Anyway, my eyes are not accustomed to this light
And my shoes are not accustomed to this hard concrete
So I must go back to my room and make my day complete

Countin' flowers on the wall, that don't bother me at all
Playin' solitaire 'til dawn, with a deck of fifty-one
Smokin' cigarettes and watchin' Captain Kangaroo
Now don't tell me
I've nothin' to do .")
  #song 468
  Song.create!(artist:  "Lonestar",
               title: "Amazed",
               rank: 468,
               copyright: "Reservoir Media Management Inc",
               writers: "Aimee Mayo, Chris Lindsey, Marv Green",
               year: 1999,
               lyrics: "Every time our eyes meet
This feeling inside me
Is almost more than I can take
Baby when you touch me
I can feel how much you love me
And it just blows me away
I've never been this close to anyone,
Or anything
I can hear your thoughts, I can see your dreams

I don't know how you do what you do
I'm so in love with you
It just keeps getting better
I want to spend the rest of my life,
with you by my side
Forever and ever
Every little thing that you do,
Baby I'm amazed by you.

The smell of your skin,
The taste of your kiss,
The way you whisper in the dark.
Your hair all around me,
Baby you surround me
Touch every place in my heart
And it feels like the first time
Every time.
I want to spend the whole night
In your eyes

I don't know how you do what you do
I'm so in love with you
It just keeps getting better
I want to spend the rest of my life,
with you by my side
Forever and ever
Every little thing that you do,
Baby I'm amazed by you.

I don't know how you do what you do
I'm so in love with you
It just keeps getting better
I want to spend the rest of my life,
with you by my side
Forever and ever
Every little thing that you do,
Baby I'm amazed by you .")
  #song 469
  Song.create!(artist:  "George Strait",
               title: "Write This Down",
               rank: 469,
               copyright: "Roba Music, Universal Music Publishing Group",
               writers: "Dana Hunt Oglesby, Kent Marshall N Robbins",
               year: 1999,
               lyrics: "I never saw the end in sight; fools are kind of blind.
Thought everything was going alright, but I was running out of time.
'Cause you had one foot out the door, I swear I didn't see
But if you're really going away, here's some final words from me.

Baby, write this down, take a little note to remind you in case you didn't know,
Tell yourself I love you and I don't want you to go, write this down.
Take my words, read 'em every day, keep 'em close by, don't you let 'em fade away,
So you'll remember what I forgot to say, write this down.

I'll sign it at the bottom of the page, I'll swear under oath
'Cause every single word is true, and I think you need to know,
So use it as a bookmark, stick it on your 'frigerator door,
Hang it in a picture frame up above the mantel where you'll see it for sure.

Baby, write this down, take a little note to remind you in case you didn't know,
Tell yourself I love you and I don't want you to go, write this down.
Take my words, read 'em every day, keep 'em close by, don't you let 'em fade away,
So you'll remember what I forgot to say, write this down.

You can find a chisel, I can find a stone.
Folks will be reading these words, long after we're gone.

Baby, write this down, take a little note to remind you in case you didn't know,
Tell yourself I love you and I don't want you to go, write this down.
Take my words, read 'em every day, keep 'em close by, don't you let 'em fade away,
So you'll remember what I forgot to say, write this down.

Oh I love you and I don't want you to go, baby write this down .")
  #song 470
  Song.create!(artist:  "Joe Dee Messina",
               title: "Lesson in Leavin'",
               rank: 470,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Demi Music Corp. D/B/A Lichelle Music Company",
               writers: "Brent Maher, Randy Goodrum",
               year: 1998,
               lyrics: "Somebody's gonna give you
A lesson in leavin'
Somebody's gonna give you back
What you've been givin'
And I hope that I'm around
To watch 'em knock you down
It's like you to love 'em and leave 'em
Just like you loved me and left me
It's like you to do that sort of thing
Over and over again
You're a fool-hearted man

I hear you've been askin' about me
From some of my friends
Well, you'd better believe
I'm not goin' through that again
You're the kind of man
A woman thinks she can change
Oh, but the only thing changin'
Is my way of thinkin'
And I'm thinkin' that maybe someday

Somebody's gonna give you
A lesson in losin'
Somebody's gonna do to you
What you've been doin'
And I hope that I'm around
To watch 'em knock you down
Somebody's gonna give you
A lesson in hurtin'
Somebody's gonna leave you
With your fire burnin'
And no way to put it out
Baby, there ain't no doubt
You're a fool-hearted man

Yeah, you're the kind of man
A woman thinks she can change
But the only thing changin'
Is my way of thinkin'
And I'm thinkin' that maybe someday

Somebody's gonna give you
A lesson in leavin'
Somebody's gonna give you back
What you've been givin'
And I hope that I'm around
To watch 'em knock you down
It's like you to love 'em and leave 'em
Just like you loved me and left me
It's like you to do that sort of thing
Over and over again
You're a fool-hearted man

Somebody's gonna give you
A lesson in leavin'
Somebody's gonna give you back
What you've been givin'
And I hope that I'm around
To watch 'em knock you down
It's like you to love 'em and leave 'em
Just like you loved me and left me
It's like you to do that sort of thing
Over and over again
You're a fool-hearted man

Oh, a fool-hearted man
Oh, whoa
Oh, a fool-hearted man .")
  #song 471
  Song.create!(artist:  "Kenny Chesney",
               title: "How Forever Feels",
               rank: 471,
               copyright: "Warner/Chappell Music, Inc",
               writers: "Tony Carl Mullins, Wendell Mobley, Wendell Lee Mobley",
               year: 1999,
               lyrics: "Big orange ball, sinkin' in the water
Toes in the sand, couldn't get much hotter
Little umbrella shaped margaritas
Coconut oil, tan senioritas
Now I know how Jimmy Buffet feels

Hands on the wheel, cruisin' down the interstate
Gas pedal sticks, carries my car away
I was going fast as a Rambler goes
I could feel the speed from my head to my toes
Now I know how Richard Petty feels

I've been around the block a time or two
Done almost everything a boy can do
I've done some livin', yeah I've had fun
But there is one thing that I haven't done

Saved two months, bought a little diamond
Tonight's the night, feels like perfect timin'
Down on one knee on momma's front steps,
Man I'm gonna die if she really says yes

I want to know how forever feels

I've been around the block a time or two
Done almost everything a boy can do
I've done some livin', yeah I've had fun
But there is one thing that I haven't done

I want to know how forever feels
Hey, I want to know how forever feels
Girl, I want to know how forever feels .")
  #song 472
  Song.create!(artist:  "Tim McGraw",
               title: "Please Remember Me",
               rank: 472,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Rodney Crowell, Will Jennings",
               year: 1999,
               lyrics: "All our tears have reached the sea
Part of you will live in me
Way down deep inside my heart
The days keep coming without fail
New wind is gonna find your sail
That's where your journey starts

You'll find better love
Strong as it ever was
Deep as the river runs
Warm as the morning sun
Please remember me

Just like the waves down by the shore
We're gonna keep on comin' back for more
'Cause we don't ever want to stop
Out in this brave new world you'll see
Ov'r the valleys and the peaks
And I can see you on the top

You'll find better love
Strong as it ever was
Deep as the river runs
Warm as the morning sun
Please remember me

Remember me when you're out walking
When snow falls high outside your door
Late at night when you're not sleeping
And moonlight falls across your floor
And I can't hurt you anymore

You'll find better love
Strong as it ever was
Deep as the river runs
Warm as the morning sun
Please remember me

Please remember me .")
  #song 473
  Song.create!(artist:  "Kenny Chesney",
               title: "You Had Me From Hello",
               rank: 473,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd.",
               writers: "Donald Ewing Ii, Donald R Ewing Ii, Kenneth Chesney, Kenneth A. Chesney",
               year: 1999,
               lyrics: "One word, that's all was said,
Something in your voice called me, caused me to turn my head.
Your smile just captured me, you were in my future as far as I could see.
And I don't know how it happened, but it happens still.
You ask me if I love you, if I always will

Well, you had me from 'Hello'
I felt love start to grow the moment I looked into your eyes,
You won me, it was over from the start.
You completely stole my heart, and now you won't let go.
I never even had a chance you know?
You had me from 'Hello'

Inside I built a wall so high around my heart, I thought I'd never fall.
One touch, you brought it down
Bricks of my defenses scattered on the ground
And I swore to me that I wasn't going to love again
The last time was the last time I'd let someone in

Well, you had me from 'Hello'
I felt love start to grow the moment I looked into your eyes,
You won me, it was over from the start.
You completely stole my heart, and now you won't let go.
I never even had a chance you know?
You had me from 'Hello'

That's all you said
Something in your voice calls me, caused me to turn my head
You had me from 'Hello'
You had me from 'Hello'
Girl, I've loved you from 'Hello'")
  #song 474
  Song.create!(artist:  "Jo Dee Messina",
               title: "Stand Beside Me",
               rank: 474,
               copyright: "Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Stephen Allen Davis",
               year: 1998,
               lyrics: "He left me cryin' late one Sunday night outside of Boulder
He said he had to find himself out on the road
I guess when love goes wrong
You've gotta learn to be strong

So I worked two jobs
And I moved three times
I ended up south of Memphis, workin' down in Riverside
I may not be so lucky in love
But the one thing I'm sure of

I want a man that stands beside me
Not in front of or behind me
Give me two arms that want to hold me, not own me
And I'll give all the love in my heart
Stand beside me
Be true, don't tell lies to me
I'm not lookin' for a fantasy
I want a man that who stands beside me

I didn't expect to see him, one hot July morning
His hair was longer but his eyes were the same old blue
He said, 'I've missed you for so long. Oh baby, what can I do?'
I said, 'I want a man that stands beside me
Not in front of or behind me
Give me two arms that want to hold me, not own me
And I'll give all the love in my heart.'

It's hard to
Tell him, 'No' when I want him so bad
But I've got to be true to my heart
This time

I'm not lookin' for a fantasy
I want a man who stands beside me
I want a man
Who stands beside me

Stand beside me
Stand beside me .")
  #song 475
  Song.create!(artist:  "Andy Griggs",
               title: "You Won't Ever Be Lonely",
               rank: 475,
               copyright: "Sony/ATV Music Publishing LLC, The Music Force LLC",
               writers: "Andy Griggs, Brett Jones",
               year: 1999,
               lyrics: "Life may not always go your way
And every once in awhile you might have a bad day
But I promise you now you won't ever be lonely
The sky turns dark and everything goes wrong
Run to me and I'll leave the light on
And I promise you now you won't ever be lonely

For as long as I live
There will always be a place you belong
Here beside me
Heart and soul baby -- you only
And I promise you now you won't ever be lonely

It's still gonna snow and it's still gonna rain
The wind's gonna blow on a cold winter day
And I promise you now you won't ever be lonely
You're safe from the world wrapped in my arms
And I'll never let go
Baby, here's where it starts
And I promise you now you won't ever be lonely
Here's a shoulder you can cry on
And a love you can rely on
For as long as I live
There will always be a place you belong
Here beside me
Heart and soul baby -- you only
And I promise you now you won't ever be lonely
No, no, you won't ever be lonely .")
  #song 476
  Song.create!(artist:  "Mark Chesnutt",
               title: "I Don't Want To Miss A Thing",
               rank: 476,
               copyright: "Sony/ATV Music Publishing LLC, Walt Disney Music Company, Universal Music Publishing Group, Realsongs",
               writers: "Diane Warren",
               year: 1999,
               lyrics: "I could stay awake just to hear you breathing
Watch you smile while you are sleeping
While you're far away and dreaming
I could spend my life in this sweet surrender
I could stay lost in this moment forever
Where every moment spent with you
Is a moment I treasure

I don't want to close my eyes
I don't want to fall asleep
'Cause I'd miss you
And I don't want to miss a thing
'Cause even when I dream of you
The sweetest dream will never do
I'd still miss you
And I don't want to miss a thing

Lying close to you, feeling your heart beating
And I'm wondering what your dreaming
Wondering if it's me your seeing
And then I kiss your eyes, and thank God we're together
I just want to stay with you
In this moment forever

I don't want to close my eyes
I don't want to fall asleep
'Cause I'd miss you
And I don't want to miss a thing
'Cause even when I dream of you
The sweetest dream will never do
I'd still miss you
And I don't want to miss a thing

I don't want to miss one smile
I don't want to miss one kiss
Well, I just want to be with you, right here with you
Just like this
I just want to hold you close
Feel your heart so close to mine
And just stay here in this moment
For all the rest of time

I don't want to close my eyes
I don't want to fall asleep
'Cause I'd miss you
And I don't want to miss a thing
'Cause even when I dream of you
The sweetest dream will never do
I'd still miss you
And I don't want to miss a thing .")
  #song 477
  Song.create!(artist:  "Diamond Rio",
               title: "Unbelievable",
               rank: 477,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Sunshine Anderson, Tamara Savage, Robert Smith",
               year: 1998,
               lyrics: "She's so kissable, huggable,
Lovable, unbelievable
She's a mouthful of anything and
Everything a man could want
She ain't typical, she's
Unpredictable, she's available,
It's a miracle
How my heart stumbled into
Someone so kissable, huggable,
Lovable, unbelievable

Up 'til now my life has been
So lonely and boring
I never thought I would find
Someone so

Elegant, intelligent, heaven sent,
All my money spent
I put a big down payment on that
Itty bitty diamond ring
She's so beautiful,
It's indisputable, it's undeniable,
She's got-to-havable
She's music to my ears,
Makes my heart sing, so kissable,
Huggable, lovable, unbelievable

There's so many things
I want to tell her
Like I love her
But every time I talk
I start to stutter

She's so elegant, intelligent,
Heaven sent, all my money spent
I put a big down payment on that
Itty bitty diamond ring
She's beautiful,
It's indisputable, it's undeniable,
She's got-to-havable
She's music to my ears,
Makes my heart sing, so kissable,
Huggable, lovable, unbelievable
Kissable, huggable, lovable, unbelievable (x2) .")
  #song 478
  Song.create!(artist:  "Dixie Chicks",
               title: "You Were Mine",
               rank: 478,
               copyright: "Sony/ATV Music Publishing LLC, Kobalt Music Publishing Ltd.",
               writers: "Emily Robison, Martha Maguire",
               year: 1998,
               lyrics: "I can't find a reason to let go
Even though you've found a new love
And she's what your dreams are made of
I can find a reason to hang on
What went wrong can be forgiven
Without you it ain't worth livin' alone

Sometimes I wake up crying at night
And sometimes I scream out your name
What right does she have to take you away
When for so long you were mine

I took out all the pictures of our wedding day
It was a time of love and laughter
Happy ever after
But even those old pictures have begun to fade
Please tell me she's not real
And that you're really coming home to stay

Sometimes I wake up crying at night
And sometimes I scream out your name
What right does she have to take your heart away
When for so long you were mine

I can give you two good reasons
To show you love's not blind
He's two and she's four and you know
They adore you
So how can I tell them you've changed your mind

Sometimes I wake up crying at night
And sometimes I scream out your name
What right does she have to take your heart away
When for so long you were mine
I remember when you were mine .")
  #song 479
  Song.create!(artist:  "Chely Wright",
               title: "Single White Female",
               rank: 479,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Carolyn Dawn Johnson, Shaye Smith",
               year: 1999,
               lyrics: "I know that every morning you go thumbing
Through the personal want ads
You grab the latest copy, a cup of coffee
And settle in for a good laugh
I've been trying to catch your eye but I'm so shy
I'm hoping and praying that
Today's edition is gonna catch your attention
Cause there's a

Single white female
Looking for that special lover
To put it in a nutshell
A one women man who doesn't want no other
Oh, you never can tell
She just might be your dream come true
A single white female
Is looking for a man like you

Yeah, I'm a little nervous I'm not sure if I shoulda put it in writing
It might have been a little reckless, a little desperate
But I think I did the right thing
I couldn't go on living keep it hidden
So I'm telling you everything
It's my confession I hope you get the message
There's a

Single white female
Looking for that special lover
To put it in a nutshell
A one women man who doesn't want no other
Oh, you never can tell
She just might be your dream come true
A single white female
Is looking for a man like you

It's my confession I hope you get the message

Single white female
Looking for that special lover
To put it in a nutshell
A one women man who doesn't want no other
Oh, you never can tell
She just might be your dream come true
A single white female
Is looking for a man like you

Looking for a man like you
Looking for a man like you .")
  #song 480
  Song.create!(artist:  "Tim McGraw",
               title: "Something Like That",
               rank: 480,
               copyright: "Warner/Chappell Music, Inc, Ole MM, Ole Media Management Lp, BMG Rights Management US, LLC",
               writers: "Rick Ferrell, Keith Follese",
               year: 1999,
               lyrics: "It was Labor Day weekend I was seventeen
I bought a Coke and some gasoline
And I drove out to the County Fair
And when I saw her for the first time
She was standing there in the ticket line
And it all started right then and there
Oh a sailor's sky made a perfect sunset
And that's a day I'll never forget

I had a barbecue stain on my white t-shirt
She was killing me in that mini-skirt
Skippin' rocks on the river by the railroad tracks
She had a sun tan line and red lipstick
I worked so hard for that first kiss
A heart don't forget something like that

It was five years later on a south-bound plane
I was headed down to New Orleans
To meet some friends of mine for the Mardi Gras
When I heard a voice from the past
Coming from a few rows back
When I looked I couldn't believe just what I saw

She said I bet you don't remember me
And I said only every other memory

I had a barbecue stain on my white t-shirt
You were killing me in that mini-skirt
Skippin' rocks on the river by the railroad tracks

You had a sun tan line and red lipstick
I worked so hard for that first kiss
A heart don't forget something like that

Like an old photograph
Time can make a feeling fade
But the memory of the first love
Never fades away

I had a barbecue stain on my white t-shirt
She was killing me in that mini-skirt
Skippin' rocks on the river by the railroad tracks

She had a sun tan line and red lipstick
I worked so hard for that first kiss
A heart don't forget, no a heart don't forget
I said a heart don't forget something like that
Oh, not something like that .")
  #song 481
  Song.create!(artist:  "Alabama",
               title: "God Must Have Spent A LIttle More Time On You (f. NSYNC)",
               rank: 481,
               copyright: "Universal Music Publishing Group",
               writers: "Carl Allen Sturken, Evan A. Rogers",
               year: 1999,
               lyrics: "Can this be true?
Tell me, can this be real?
How can I put into words what I feel?
My life was complete
I thought I was whole
Why do I feel like I'm losing control?

I never thought that love could feel like this
And you've changed my world with just one kiss
How can it be that right here with me
There's an angel?
It's a miracle

Your love is like a river
Peaceful and deep
Your soul is like a secret
That I never could keep
When I look into your eyes
I know that it's true
God must have spent
A little more time
On you
(A little more time, yes he did baby)

In all of creation, all things great and small
You are the one that surpasses them all
More precious than any diamond or pearl
They broke the mold when you came in this world

And I'm trying hard to figure out
Just how I ever did without
The warmth of your smile
The heart of a child
That's deep inside
Leaves me purified

Your love is like a river
Peaceful and deep
Your soul is like a secret
That I never could keep
When I look into your eyes
I know that it's true
God must have spent
A little more time
On you

Never thought that love could feel like this
And you've changed my world with just one kiss
How can it be that right here with me
There's an angel?
It's a miracle

Your love is like a river
Peaceful and deep
Your soul is like a secret
That I never could keep
When I look into your eyes
I know that it's true
God must have spent
A little more time
On you

God must have spent
A little more time, on you
(on you, on you, you, you, oh yeah)
A little more time
On you .")
  #song 482
  Song.create!(artist:  "Mark Wills",
               title: "Wish You Were Here",
               rank: 482,
               copyright: "Universal Music Publishing Group",
               writers: "Barry Alan Gibb, Maurice Ernest Gibb, Robin Hugh Gibb",
               year: 1998,
               lyrics: "They kissed goodbye at the terminal gate
She said, 'You're gonna be late if you don't go'
He held her tight, said, 'I'll be alright
I'll call you tonight to let you know'
He bought a postcard, on the front it just said Heaven
With a picture of the ocean and the beach
And the simple words he wrote her
Said he loved her and they told her
How he'd hold her if his arms would reach

Wish you were here, wish you could see this place
Wish you were near, I wish I could touch your face
The weather's nice, it's paradise
It's summertime all year and there's some folks we know
They say, 'Hello, I miss you so, wish you were here'

She got a call that night but it wasn't from him
It didn't sink in right away, ma'am the plane went down
Our crews have searched the ground
No survivors found she heard him say
But somehow she got a postcard in the mail
That just said Heaven with a picture of the ocean and the beach
And the simple words he wrote her
Said he loves and they told her
How he'd love her if his arms would reach

Wish you were here, wish you could see this place
Wish you were near, I wish I could touch your face
The weather's nice, it's paradise
It's summertime all year and there's some folks we know
They say, 'Hello, I miss you so, wish you were here'

The weather's nice, in paradise
It's summertime all year and all the folks we know
They say, 'Hello, I miss you so, wish you were here'
Wish you were here .")
  #song 483
  Song.create!(artist:  "SHeDAISY",
               title: "Little Goodbyes",
               rank: 483,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Kristyn Osborn",
               year: 1999,
               lyrics: "I'm gonna tell you something you don't want to hear
You never listen when I talk
Maybe you'll listen when I walk
So I made up my mind and made a sandwich
And I didn't shed a tear
I gave you one last minute of my time
In this mess I left behind

You'll come home tonight
And turn on the light
Don't you be surprised to find
My little good-byes

Empty hangers by the closet floor
Lipstick tube on the bathroom floor
Unpaid bills by the kitchen phone
I took the Beatles, left Billy Joel
My little good-byes

I'm sure you're sure I'll be back in just an hour or two
You'll tape a Hallmark to my door
They always said it better than you
And if ya wonderin' if you're gonna hear from me
Well take a real good look around boy
And it won't be hard to see

When you come home tonight
And turn on the light
Don't you be surprised to find
My little good-byes

Took your favorite Dodgers hat
Left the litter, but I took the cat
My little good-byes
Loaded up the TV in the back of my car
Have fun watching the VCR
My little good-byes
Little Good-byes, oh baby, little good-byes

So cry to your mom and your sympathetic friends
And tell 'em how the story ends

My Little Good-byes
Took the hourglass left the sand
Now you got time on your hands

My little good-byes
Took the statue from Japan
Funny little Buddha man

My little good-byes
Changed my voice on the machine
Or there'll be little good-byes with every ring
My little good-byes

Left the pictures took the frames
Got the umbrella, here comes the rain
Yeah, yeah, yeah
Rain, yeah, yeah, yeah
Hey, yeah, yeah, yeah
Hey, yeah, yeah, yeah
Little Good-byes .")
  #song 484
  Song.create!(artist:  "Martina McBride",
               title: "Whatever You Say",
               rank: 484,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, Universal Music Publishing Group, BMG Rights Management US, LLC",
               writers: "Tony Martin, Ed Hill",
               year: 1997,
               lyrics: "You think I'm always makin'
Something out of nothin'
You're sayin' everything's okay.
You've always got an answer
Before I ask the question.
Whatever you say.

Now we can change the subject
Pretend I never brought it up,
Same old story anyway.
Later we can work it out
Right now you're talked out.
Yeah, whatever you say.

Oh I know you can hear me
But I'm not sure you're listening
I hear what you're sayin'
But still there's something missin'.
Whether I go, whether I stay
Right now depends on
Whatever you say.

You say yes you need me
And no you wouldn't leave me
And that should be enough to make me stay.
And even though I want to
I don't hear I love you
In whatever you say.

Oh I know you can hear me
But I'm not sure you're listening
I hear what you're sayin'
But still there's something missin'.
Whether I go, whether I stay
Right now depends on
Whatever you say.

Oh whether I go, whether I stay
Right now depends on
Whatever you say.
Whatever you say.
Whatever you say .")
  #song 485
  Song.create!(artist:  "Joe Diffie",
               title: "A Night To Remember",
               rank: 485,
               copyright: "Peermusic Publishing, Warner/Chappell Music, Inc",
               writers: "Terry Lynn Welborn, Max T. Barnes",
               year: 1999,
               lyrics: "Been one tough week
Dead on my feet
But I've made plans for tonight
When I'm feeling blue
Know just what to do
And how to make it right
Seems like this forever
Gonna have myself a night to remember

Dim the lights
Lock the door
Spread your pictures on the floor
Blow the dust off of our past
Let it all come flooding back
Cause it ain't easy being strong
And when I can't forget your gone
I just surrender
And have myself a night to remember

Sad ain't my style
But once awhile
I just have to give in
Cause a women like you
Is so hard to lose
You just don't want it to end
I know this can't go on forever
So tonight Ill have a night to remember

Dim the lights
Lock the door
Spread your pictures on the floor
Blow the dust off of our past
Let it all come flooding back
Cause it ain't easy being strong
And when I can't forget your gone
I just surrender
And have myself a night to remember .")
  #song 486
  Song.create!(artist:  "Dixie Chicks",
               title: "Ready To Run",
               rank: 486,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group, Kobalt Music Publishing Ltd.",
               writers: "Martha Maguire, Marcus Hummon",
               year: 1999,
               lyrics: "When the train rolls by
I'm gonna be ready this time
When the boy gets that look in his eye
I'm gonna be ready this time
When my momma says I look good in white
I'm gonna be ready this time

Oh yeah
Ready, ready, ready, ready, ready to run
All I'm ready to do is have some fun
What's all this talk about love

I feel the wind blow through my hair
I'm gonna be ready this time
I'll buy a ticket to anywhere
I'm gonna be ready this time
You see it feels like I'm starting to care
And I'm going to be ready this time

Oh yeah
Ready, ready, ready, ready, ready to run
All I'm ready to do is have some fun
What's all this talk about love

I'm ready to run
I'm ready to run

Oh
Ready, ready, ready, ready, ready to run
All I'm ready to do is have some fun
What's all this talk about love
I'm ready to run
I'm ready to run
I'm ready to run
I'm ready to run, ready to run,
Ready to run, yeah I'm ready to run, I'm ready
Whoa I'm ready to run, I'm ready, ready to run
(I'm ready) I'm ready to run
I'm ready .")
  #song 487
  Song.create!(artist:  "Lee Ann Womack",
               title: "I'll Think Of A Reason Later",
               rank: 487,
               copyright: "Sony/ATV Music Publishing LLC, Warner/Chappell Music, Inc, BMG Rights Management US, LLC",
               writers: "Tony Martin, Tim Nichols",
               year: 1998,
               lyrics: "I heard he was gonna marry some girl from Denver
Then my sister came over, had the Sunday paper with her
There was the girl on the social page
Lookin' in love and all engaged
We decided she don't take a very good picture

It may be my family's redneck nature
Rubbin' off, bringin' out unlady-like behavior
It sure ain't Christian to judge a stranger
But I don't like her
She may be an angel who spends all winter
Bringin' the homeless blankets and dinner
A regular Nobel Peace Prize winner
But I really hate her
I'll think of a reason later

I drew horns and blacked out her tooth with a marker
Childish, yes, but she made such a thin little target
I couldn't be happier on my own
But I've got the slightest of a jealous bone
And seein' her with him tends to enlarge it

It may be my family's redneck nature
Rubbin' off, bringin' out unlady-like behavior
It sure ain't Christian to judge a stranger
But I don't like her
She may be an angel who spends all winter
Bringin' the homeless blankets and dinner
A regular Nobel Peace Prize winner
But I really hate her
I'll think of a reason later

Inside her head may lay all the answers
For curin' diseases from baldness to cancer
Salt of the earth and a real good dancer
But I really hate her
I'll think of a reason later

Well, it was just one tooth
Did I mention I don't particularly care for her?
She makes me sick .")
  #song 488
  Song.create!(artist:  "Steve Wariner",
               title: "Two Tear Drops",
               rank: 488,
               copyright: "Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Steve Noel Wariner, Bill Anderson",
               year: 1999,
               lyrics: "Two teardrops were floatin' down the river
One teardrop said to the other
I'm from the soft blue eyes of a woman in love
I'm a tear of joy she couldn't carry
She was so happy she just got married
I was on her cheek when she wiped me away with her glove
I could tell from the look on her face she didn't need me
So I drifted on down and caught me a ride to the sea

The other tear said we've got a connection
I'm a tear of sorrow born of rejection
I'm from the sad brown eyes of her old flame
She told him they would be lifelong companions
Left him with questions and not any answers
I was on his cheek as he stood there calling her name
I could tell he had a lot of my friends for company
So I drifted on down and caught me a ride to the sea

Oh the ocean's a little bit bigger tonight
Two more teardrops somebody cried
One of them happy and one of them bluer than blue
The tide goes out and the tide comes in
And someday they'll be teardrops again
Released in a moment of pleasure or a moment of pain
Then they drift on down and ride to the sea again

Last night I sat in the waiting room
The nurse walked in and gave me the news
It's a baby girl and they're both fine
An old man sittin' not 10 feet away
Just lost his wife and he said to me
You've got a brand new angel and I've lost mine
I guess the good Lord giveth and the good Lord taketh away
And we both wiped a teardrop from our face

Oh the ocean's a little bit bigger tonight
Two more teardrops somebody cried
One of them happy and one of them bluer than blue
The tide goes out and the tide comes in
A whole new circle of life begins
Where tears are a part of the pleasure and part of the pain
'Til they drift on down and ride to the sea again

Two teardrops floatin' down the river
Two teardrops floatin' down the river .")
  #song 489
  Song.create!(artist:  "Faith Hill",
               title: "The Secret Of Life",
               rank: 489,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Gretchen Peters",
               year: 1998,
               lyrics: "Couple of guys sittin' around drinkin'
Down at the Starlight Bar
One of 'em says, 'You know I've been thinking'
Other one says, 'That won't get you too far'
He says, 'This is your life, and welcome to it
It's just workin' and drinkin' and dreams
Ad on TV says ? Just Do It'
Hell if I know what that means'
The secret of life is a good cup of coffee
The secret of life is keep your eye on the ball
The secret of life is a beautiful woman
And Marilyn stares down from the barroom wall
'You and me, we're just a couple of zeros
Just a couple of down-and-outs
But movie stars and football heroes
What have they got to be unhappy about?'
So they turn to the bartender, 'Sam, what do you think?
What's the key that unlocks that door?'
Sam don't say nothin', just wipes off the bar
And he pours them a couple more
'Cause the secret of life is in Sam's martinis
The secret of life is in Marilyn's eyes
The secret of life is in Monday Night Football
Rolling Stones records and Mom's apple pie
Sam looks up from his Sunday paper
Says, 'Boys, you're on the wrong track
The secret of life is there ain't no secret
And you don't get your money back'
Hey
The secret of life is gettin' up early
The secret of life is stayin' up late
The secret of life is try not to hurry
But don't wait
Don't wait
The secret of life is a good cup of coffee
The secret of life is keep your eye on the ball
The secret of life is to find the right woman
The secret of life is nothin' at all
Oh, it's nothin' at all
The secret of life
Couple of guys sittin' around drinkin'
Down at the Starlight Bar
One of 'em says, 'You know I've been thinking'
Other one says, 'That won't get you too far'
'That won't get you too far' .")
  #song 490
  Song.create!(artist:  "Billy Ray Cyrus",
               title: "Busy Man",
               rank: 490,
               copyright: "Universal Music Publishing Group",
               writers: "George G. Iii Teren, Robert Joseph Regan",
               year: 1998,
               lyrics: "There's a little boy out in the driveway
His basketball in hand
Saying Daddy could we play a little one on one
You pat him on the back and say not now son
I'm a busy man

His sister's out on the sidewalk
Setting up a lemonade stand
Hey Daddy don't you want to buy a glass from me
You say maybe later, can't you see I'm a busy man

You got to go, got to run
Hit it hard and get it done
Everyone can see you're going far
You got responsibilities
A crazy schedule that you keep
And when you say that time's a-wasting
You don't know how right you are
Busy man

There's a woman in the bedroom crying
Saying I thought we had plans
You say honey I'm sorry I'll make it up
When the job slows down and I'm not such
A busy man

You got to go, got to run
Hit it hard and get it done
Everyone can see you're going far
You got responsibilities
A crazy schedule that you keep
And when you say that time's a-wasting
You don't know how right you are
Busy man

Have you ever seen a headstone with these words
'If only I had spent more time at work'

There's a call one day from the office
They need you down in Birmingham
You say no way, the weekend's mine
I got plans with the kids and a date with my wife
I'm a busy man

You got to go, got to run
Hit it hard and get it done
Everyone can see you're going far
You got responsibilities
A crazy schedule that you keep
And when you say that time's a-wasting
You don't know how right you are
Busy man .")
  #song 491
  Song.create!(artist:  "Martina McBride",
               title: "I Love You",
               rank: 491,
               copyright: "Universal Music Publishing Group",
               writers: "Gordon Mills",
               year: 1999,
               lyrics: "
The sun is shinin' everyday
Clouds never get in the way
For you and me.
I've known you just a week or two
But,baby,I'm so into you
Can hardly breathe.

And I'm in so totally wrapped up
Emotionally attracted,so physically actin'
So recklessly I need you
So desperately sure as the sky is blue
Baby, I love you.

I never knew that I could feel like this
Can hardly wait til our next kiss
You're so cool.
If I'm dreamin' please don't wake me up
Cause, baby, I can't get enough
Of what you do.

And I'm in so totally wrapped up
Emotionally attracted,so physically actin'
So recklessly I need you
So desperately sure as the sky is blue
Baby, I love you.

And I'm in so electrically charged up kinetically
Actin' erratically, need you fanatically
You get to me magically
Sure as the sky is blue
Baby, I love you.

I can't believe that this is real.
The way I feel.
Baby,I'm gone head over heels.

And I'm in so totally wrapped up
Emotionally attracted,so physically actin'
So recklessly I need you
So desperately sure as the sky is blue
Baby, I love you.

Baby, I love you.
Do you love me too?
Baby, I love you .")
  #song 492
  Song.create!(artist:  "Alan Jackson",
               title: "Little Man",
               rank: 492,
               copyright: "Warner/Chappell Music, Inc",
               writers: "Alan Eugene Jackson",
               year: 1998,
               lyrics: "I remember walk'in round the court square sidewalk
Lookin' in windows at things I couldn't want
There's johnson's hardware and morgans jewelry
And the ol' Lee king's apothecary
They ware the little man
The little man

I go back now and the stores are all empty
Except for an old coke sign from 1950
Boarded up like they never existed
Or renovated and called historic districts
There goes the little man
There goes the little man

Now the court square's just a set of streets
That the people go round but they seldom think
Bout the little man that built this town
Before the big money shut em down
And killed the little man
Oh the little man

He pumped your gas and he cleaned your glass
And one cold rainy night he fixed your flat
The new stores came where you do it yourself
You buy a lotto ticket and food off the shelf
Forget about the little man
Forget about that little man

He hung on there for a few more years
But he couldn't sell slurpees
And he wouldn't sell beer
Now the bank rents the station
To a down the road
And sell velvet Elvis and
Second-hand clothes
There goes little man
There goes another little man

Now the are lined up in a concrete strip
You can buy the world with just one trip
And save a penny cause it's jumbo size
They don't even realize
They'er killin' the little man
Oh the little man

It wasn't long when I was a child
An old black man came with his plow
He broke the ground where we grew our garden
Back before we'd all forgot about the little man
The little man
Long live the little man
God bless the little man .")
  #song 493
  Song.create!(artist:  "Sara Evans",
               title: "No Place That Far",
               rank: 493,
               copyright: "Sony/ATV Music Publishing LLC",
               writers: "Sara Evans, Tom Shapiro, Tony Martin",
               year: 1998,
               lyrics: "I can't imagine, any greater fear
Then waken up, with out you near
And though the sun, will still shine on
My whole world, would all be gone
But not for long

If I had to run
If I had to crawl
If I had to swim a hundred rivers
Just to climb a thousand walls
Always know that I would find a way
To get to where you are
There's no place that far

It wouldn't matter, why we're apart
The lonely miles, two stubborn hearts
And nothing short, of God above
Could turn me away, from your love
I need you that much

If I had to run
If I had to crawl
If I had to swim a hundred rivers
Just to climb a thousand walls
Always know that I would find a way
To get to where you are
There's no place that far

Oh if I had to run
(If I had to run)
Of I had to crawl
(If I had to crawl)
If I had to swim a hundred rivers
Just to climb a thousand walls
Always know that I would find a way
To get to where you are
There's no place that far
Baby there's no place that far .")
  #song 494
  Song.create!(artist:  "Collin Raye",
               title: "Anyone Else",
               rank: 494,
               copyright: "Universal Music Publishing Group",
               writers: "Radney M. Foster",
               year: 1998,
               lyrics: "Baby I bought wine and roses
On my way home
Yeah, I know we can't afford it
But life doesn't last too long
This world has gone crazy
And knocked us both to our knees
I'm fumbling for the words to try
And tell you I love you
For hanging onto me
When anyone else

Would've been long gone
Packed it up and headed back home
And not a soul would blame you
After what I put you through
Yeah, anyone else
Would've gone insane
Called the game on account of rain
Anyone else, anyone else, anyone else,
Anyone but you

Mayday, baby, all the rivets
Popped loose it's a total tailspin
Too much grindstone
Too little time with you, girl
You know it's wearing thin
Baby let's trade in these teardrops
For warm kisses on skin
You got a drawer full of IOU's
And it's high time
You called those things in
When anyone else
Would've been long gone

You've passed up so many chances
To walk right out the door
Instead of dodging
A champagne cork
And watching the wet clothes
Fall to the floor
When anyone else
Would've been long gone
Packed it up and headed back home

And you can hide behind
A Cheshire cat grin
But you know it's true
Yeah, anyone else
Would've gone insane
Called the game on account of rain
Anyone else, anyone else,
Anyone but you
Anyone else, girl, anyone but you .")
  #song 495
  Song.create!(artist:  "Tim McGraw",
               title: "For A Little While",
               rank: 495,
               copyright: "EMI Music Publishing, Peermusic Publishing, Warner/Chappell Music, Inc, Mike Curb Music, Carlin America Inc",
               writers: "Jerry Vandiver, Steve Mandile, Phil Vassar",
               year: 1997,
               lyrics: "Hot sun, dancin' on the river
We'd sit on the bank and watch the world roll by
Our feet in the water, she'd press her lips to mine
We were so long on love but short on time
She could be honeymoon sweet and a little wild
And she was mine for a little while

I laugh every time when I think about us
We sent the summer out in style
She's gone but she left me with a smile
Cause she was mine for a little while

We'd take a drive and park down next to airport road
Put the seats back and watch the planes leave town
She always said nobody's strong enough to tie her down
But I wasn't lookin' for that anyhow
I knew she'd leave but I didn't know when
It matters, but didn't back then

I laugh when I start to think about us
We sent the summer out in style
She's gone but she left me with a smile
?Cause she was mine for a little while

I keep seeing pictures now of me and her
And those summer nights
My minds filled with her, oh and it's alright

I laugh when I start to think about us
We sent the summer out in style
She's gone but she left me with a smile
?Cause she was mine for a little while .")
  #song 496
  Song.create!(artist:  "Chad Brock",
               title: "Ordinary Life",
               rank: 496,
               copyright: "BMG Rights Management US, LLC",
               writers: "B. Baker, C. Harrington",
               year: 2001,
               lyrics: "Shelly's at the kitchen table, cup of coffee the morning paper
When he walks in she's so surprised to see the tears in his eyes
He says, I love you, I'm so sorry, but bigger dreams are waiting for me
But I can't do this anymore

Pay the bills, watch TV, day in, day out the same routine
Mow the grass, fix the leak, just to fix it again
We go to church, go to work, so picture perfect that it hurts
I feel like I'm trapped inside this ordinary life

Shelly's at the kitchen table, crayons, construction paper
'Hey Mom, look what I drew, it's a picture of me and you'
And later when he says his prayers, she runs her fingers through his hair
So thankful for every day

They pay their bills, watch TV, day in, day out, it's all they need
Mow the grass, fix the leak, just to fix it again
They go to church, go to school, everyday it's something new
Precious are the days as they go by in their ordinary life

Phone rings, he's calling from the airport
It's midnight, he's all alone again
He says, I can't believe how much I've missed
And what I wouldn't give

To pay the bills, watch TV, day in, day out the same routine
Mow the grass, fix the leak, just to fix it again
Go to church, go to work, I can't tell you how this hurts
I miss my son, I miss my wife and my ordinary life .")
  #song 497
  Song.create!(artist:  "John Michael Montgomery",
               title: "Hold Onto Me",
               rank: 497,
               copyright: "Kobalt Music Publishing Ltd., Sony/ATV Music Publishing LLC, Universal Music Publishing Group",
               writers: "Marcus Hummon, Chris Brown, Laura Licata",
               year: 1998,
               lyrics: "I want to wake up each morning
With you for the rest of my life
I want to feel your heart beating
And just get lost in your eyes
You can tell me your secrets
You can let me feel your pain
You can show me your weakness
And never be ashamed

Hold on to me when your world's turnin' cold
When it feels like your life's spinnin' out of control
You're hopin', prayin', tryin' so hard to believe
Hold on to me when there's no middle ground
And every emotion is comin' unwound
And you don't know if you can hold on to your dreams
Baby you can hold on to me

I want to lay down each evenin'
With you right here by my side
I want to get drunk on your laughter
And wipe all the tears when you cry
You can scare me with your darkness
You can blind me with your light
Throw your worries out the window baby
On your wildest night

Hold on to me when your world's turnin' cold
When it feels like your life's spinnin' out of control
You're hopin', prayin', tryin' so hard to believe
Hold on to me when there's no middle ground
And every emotion is comin' unwound
And you don't know if you can hold on to your dreams
Baby you can hold on to me

Hold on to me when there's no middle ground
And every emotion is comin' unwound
And you don't know if you can hold on to your dreams
Baby you can hold on I said
Baby you can hold on I said
Baby you can hold on to me .")
  #song 498
  Song.create!(artist:  "Steve Wariner",
               title: "I'm Already taken",
               rank: 498,
               copyright: "Steve Wariner Music",
               writers: "Steve Noel Wariner",
               year: 1982,
               lyrics: "My little third grade hand wrote I love you
On a note of yellow paper
And sent it to the front of the row to a little blonde-haired girl
The blonde-haired girl just opened it and read it to herself
I was so embarrassed when she turned around and said

I'm already taken
You spoke up too late
I love somebody else
So you'll just have to wait

The years flew by so quickly
And there we were in junior high
I realized that I still loved her so
So I called her up and told her exactly how I felt
Then she said there's something that you really ought to know

I'm already taken
You spoke up too late
I love somebody else
So you'll just have to wait

So wait I did and never changed my love for that little blonde-haired girl
Who's' now the mother of our little blonde-haired boy
Who's to our hearts so close
I hate to think how fast he's growing up
Last night I overheard them as she tucked him into bed
He said, Mommy will you marry me
And this is what she said

I'm already taken
You spoke up too late
I love your daddy son
So you'll just have to wait

I'm already taken
You spoke up too late
I love your daddy son
So you'll just have to wait .")
  #song 499
  Song.create!(artist:  "Montgomery",
               title: "Lonely And Gone",
               rank: 499,
               copyright: "Universal Music Publishing Group",
               writers: "Bill Mccorvey, David Lowell Gibson, Greg Crowe",
               year: 1999,
               lyrics: "Pulled in the driveway, picked up the paper
Found my key, and unlocked the door
I walked in, felt like a stranger
Like I'd never been there before
As I wandered room to room
It was silent as a tomb
Could have heard a teardrop
Could have heard a heartbreak
Never saw the flood come
Even though I felt the rain
Never heard a house sound
So loud with memories
Where there used to be
A happy home
In the house on the corner
Of lonely and gone
If she talked about leaving, I wasn't listening
If she showed me a sign, well I never saw
Did she stop believing, did I stop giving
Can't put my finger on what went wrong
Now the quietest noise I'm told, is the sound of letting go
Could have heard a teardrop
Could have heard a heartbreak
Never saw the flood come, even though I felt the rain
Never heard a house sound, so loud with memories
Where there used to be a happy home
In the house on the corner
Of lonely and gone
Whoa...never heard a house sound
So loud with memories
Where there used to be a happy home
In the house on the corner
In the house on the corner
Of lonely and gone .")
  #song 500
  Song.create!(artist:  "Alan Jackson",
               title: "Gone Crazy",
               rank: 500,
               copyright: "Warner/Chappell Music, Inc",
               writers: "Alan Eugene Jackson",
               year: 1998,
               lyrics: "Here I am all alone again tonight
In this old empty house
It's hard to learn what you don't think you need
You can't live without
Never leave the sound of the telephone
But ever since you left
I've been gone

Gone crazy, goin' out of my mind
I've asked myself the reason,
At least a thousand times
Goin' up and down this hallway
Tryin' to leave the pain behind
Ever since you left,
I've been gone

I never saw your face this many times
When you were really here
The things you said I never understood
Are now crystal clear
I never spent this much time at home
But ever since you left,
I've been gone

Gone crazy, goin' out of my mind
I've asked myself the reason,
At least a thousand times
Goin' up and down this hallway
Tryin' to leave the pain behind
Ever since you left,
I've been gone

Gone crazy, goin' out of my mind
I've asked myself the reason,
At least a thousand times
Goin' up and down this hallway
Tryin' to leave the pain behind
Ever since you left,
I've been gone

I've been gone
I've been gone
I've been gone
(Gone)
Gone (gone)
I've gone (gone)
I've been gone .")


p "Created #{Song.count} Country songs"

  end

end
