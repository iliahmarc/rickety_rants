namespace :country_elements do
  desc "TODO"
  task seed_country_elements: :environment do
    include PTableHelper
    include SongsHelper

    CountryElement.destroy_all
    @singularize = true
    seed_elements(Song)
    @words_output.each do |word, count|
      CountryElement.create!(word: word.capitalize, count: count)
    end

    p "#{@freq_sum} total words in #{CountryElement.count} CountryElements. Global word count: #{@word_total}. Global unique words: #{@unique_words}"

  end
end
