class Ckeditor::Picture < Ckeditor::Asset
  has_attached_file :data,
                  styles: { content: '800>', thumb: '118x100#'},
                  storage: :s3,
                  #refactor w/attachment_file.rb?
                  access_key_id: ENV['AWS_ACCESS_KEY_ID'],
                  secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
                  s3_region: ENV['AWS_REGION'],
                  bucket: ENV['S3_BUCKET_NAME'],
                  path: "/:class/:attachment/:id_partition/:style/:filename",
                  url: ":s3_domain_url"

  validates_attachment_presence :data
  validates_attachment_size :data, less_than: 3.megabytes
  validates_attachment_content_type :data, content_type: /\Aimage\/.*\Z/

  def url_content
    url(:content)
  end
end
