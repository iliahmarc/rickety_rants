class Song < ApplicationRecord
  #validates_format_of :slug, :without => /^\d/

  def to_param
    slug
  end

  #overrides find to find_by_slug:
  def self.find(slug)
    slug.to_i == 0 ? find_by(slug: slug) : super
  end

  def self.search(search)
    where("lyrics ILIKE ?", "%#{search}%" )
  end

  include PgSearch
  pg_search_scope :search_by_artist,
                  :against => {
                    :artist => 'A',
                    :title => 'B',
                    :year => 'C',
                  },
                  :using => {
                    :tsearch => { any_word: false },
                    :trigram => {
                      :only => [:artist, :title],
                      :threshold => 0.2,
                    },
                  },
                  :order_within_rank => "year DESC"
end
