class Rant < ApplicationRecord
  belongs_to :author
  validates :author_id, presence: true
  validates :title, presence: true, length: { maximum: 140 }
  validates :body, presence: true
  has_attached_file :image, styles: {
    med: "400x175" }

  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  validates_attachment_size :image, less_than: 200.kilobytes
  #validates :image_file_name, presence: true

  def to_param
    "#{id}-#{title.parameterize}"
  end
end
