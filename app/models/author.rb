class Author < ApplicationRecord
  has_many :rants
  validates :name, presence: true, length: { maximum: 15 }
  validates :password, presence: true, length: { maximum: 15 }
  validates :email, presence: true, length: { maximum: 40 }
  has_secure_password

  def Author.digest(string) #list 8.21
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
end
