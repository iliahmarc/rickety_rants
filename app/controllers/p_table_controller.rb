class PTableController < ApplicationController

  def index
    @genre = params[:genre]
    get_genre_vars #provides variables below
    get_elements(@element_model)
    get_display_element(@element_model)
    get_stats(@element_model, @song_model)
    get_one_timers(@element_model)
  end

private

    def get_elements(element_model)
      @elements = []
      element_model.all.order(count: :desc).each do |element|
        word = element.word
        count = element.count
        @elements.insert(-1, [word, count])
        if @elements.size == 118 then
          break
        end
      end
    end

    def get_display_element(element_model)
      if params[:word]
        @element = element_model.find_by(word: params[:word])
        if params[:rank]
          @rank = params[:rank].to_i
        end
      else
        @element = element_model.order('count desc').limit(118).sample
        @rank = @elements.index([@element.word, @element.count]) + 1
      end
    end

    def get_stats(element_model, song_model)
      @global_count = element_model.sum(:count)
      unique_count = element_model.count
      @percent_unique = ((unique_count.to_f / @global_count)*100).round(1)
      @song_count = song_model.count #needs to be dynamic also.
      elements_sum = @elements.map { |k, v| v }.sum #81728
      @ptable_mass = ((elements_sum.to_f / @global_count) * 100).round(0)
      @lyrical_mass = ((@element.count.to_f / @global_count) * 100).round(2)
    end

    def get_one_timers(element_model)
      @one_timers = []
      element_model.all.order(count: :asc).find_all do |element|
        word = element.word
        @one_timers.insert(0, word)
        break if element.count > 1
      end
      return @one_timer = @one_timers.sample
    end

end
