class RantsController < ApplicationController
  before_action :find_rant, except: [:index, :new, :create]
  before_action :require_login, except: [:show, :index]

  def index
    @rants = Rant.all.order('created_at ASC')
    if params[:page]
    get_page
    else
      @page = 1
    end
    @page_offset = (@page-1) * 6  #(6 texts/page)
  end

  def show
    @rants = Rant.all
  end

  def new
    @rant = Rant.new
  end

  def create
    @rant = current_author.rants.build(rant_params)
    if @rant.save
      flash[:success] = "'#{@rant.title}' successfully saved"
      redirect_to rant_path(@rant)
    else
      flash.now[:danger] = "Error saving new Rant"
      render 'new'
    end
  end

  def destroy
    @rant.destroy
    flash[:success] = "'#{@rant.title}' successfully deleted"
    redirect_to rants_path
  end

  def edit
  end

  def update
    @rant.update(rant_params)
    flash[:success] = "'#{@rant.title}' successfully updated"
    redirect_to rant_path(@rant)
  end

  private

    def rant_params
      params.require(:rant).permit(:title, :body, :image, :data, :author_id) ##ckeditor
    end

    def find_rant
      @rant = Rant.find(params[:id])
    end

    def get_page
      total_pages = (Rant.count/6) + 1
      page_params = params[:page].to_i
      if page_params > 0 && page_params <= total_pages
        @page = page_params
      else
        @page = 1
      end
    end
end
