class RapsController < ApplicationController
  before_action :set_genre
  before_action :get_genre_vars
  before_action :require_login, except: [:show, :create, :index]

  def create
    get_song(Rap) #songs_helper
    save_params
    if @box_ids
      get_lyrics(@box_ids, Rap) #songs_helper
    end
    render 'show'
  end

  def show
    get_song(Rap)
    save_params
  end

  def index
    get_song(Rap)
    save_params
    if params[:search].blank?
      @search_results = Rap.all.order("artist DESC", "year ASC")
    else
      perform_search(Rap)
    end
  end

  private

    def set_genre
      @genre = "hiphop"
    end

end
