class StaticPagesController < ApplicationController

  def home
    get_song(Rap)
    get_song(Song)
  end

  def about
  end

  def contact
  end

  def faqs
  end

end
