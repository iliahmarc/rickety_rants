class SongsController < ApplicationController
  before_action :set_genre
  before_action :get_genre_vars
  before_action :require_login, except: [:show, :create, :index]

  def create
    get_song(Song)
    save_params #strong params?
    if @box_ids
      get_lyrics(@box_ids, Song)
    end
    render 'show'
  end

  def show
    get_song(Song)
    save_params
  end

  def index
    get_song(Song)
    save_params
    if params[:search].blank?
      @search_results = Song.all.order("artist DESC", "year ASC")
    else
      perform_search(Song)
    end
  end

  private

    def set_genre
      @genre = "country"
    end

end