class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :get_navbar_songs
  include SessionsHelper
  include PTableHelper
  include SongsHelper

  def require_login
    unless logged_in?
      redirect_to rants_path
      flash[:danger] = 'Please log in.'
    end
  end

end
