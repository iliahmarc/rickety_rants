class WordCountController < ApplicationController

  #This is separate from lyric-counting. It is a basic word-counter for users to input text. Not currently an active part of the site.

  def new
  end

  def create
    save_params
    if @output.nil?
      @output = 100000
    end
    render 'new'
  end

  private

    def save_params ##strong params not required?
      @checkbox = params[:checkbox]
      @words = params[:user_input]
      @output = params[:output]
    end
end
