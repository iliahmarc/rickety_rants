class SessionsController < ApplicationController

  def new
  end

  def create
    author = Author.find_by(email: params[:session][:email].downcase)
    if author && author.authenticate(params[:session][:password])
        log_in author
        flash[:success] = "'#{author.name}' is logged in"
        redirect_to root_url
    else
      flash.now[:danger] = 'Invalid login attempt'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

end
