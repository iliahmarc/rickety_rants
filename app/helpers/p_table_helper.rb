module PTableHelper

  def count_words(string, n)
    @frequency = Hash.new(0)
    words = string.downcase.split(/\s+/)#array
    @word_total = words.size
    if @checkbox
      delete_exceptions(words)
    end
    get_frequency(words)
    get_output(n)
  end

  private

    def raw_count(all_songs)
      all_songs.split(/\s+/).size
    end

    def delete_exceptions(words) #slow-ish (.5 sec @ 200songs)
      words.delete_if{|x| ['the', 'i', 'you', 'a', 'and', 'to', 'it', 'me', 'my', "i'm", 'in', 'on', 'that', 'like', 'up', 'we', 'got', 'your', 'get', 'is', 'of', 'all', 'so', 'for', "don't", 'with', 'but', 'this', 'if', 'at', 'be', 'do', 'just', 'when', 'no', 'cause'].include? x }
    end

    def get_frequency(words)
      unless @singularize
        words.each { |word| @frequency[word.delete("\"'*:|.,!?;({[\/]})") ] += 1 }
      else
        words.each { |word| @frequency[word.delete("\"'*:|.,!?;({[\/]})").singularize ] += 1 }
      end
    end

    def get_output(n)
      @unique_words = @frequency.count
      @percent_unique = ((@unique_words.to_f / @word_total.to_f) * 100).round(1)
      @words_output = @frequency.sort_by{ |k, v| v}.reverse[0..(n-1)] #hash =>array
      return @words_output.map {|k, v| "#{k}: #{v}"}
    end

    def seed_elements(song_model) # note 1
      box_ids = *((song_model.first.id)..(song_model.last.id))
      all_songs = get_lyrics(box_ids, song_model)
      count_words(all_songs, -1)
      @freq_sum = @words_output.map {|word, count| count}.sum
    end

end

#note 1: get_rap_elements method used to populate RapElement db

# Other Notes: word.gsub!(/^\'|\'?$/, '') removed for improved performance (no longer keeps apostrophes)
# .singularize too slow for live use in production (8-12 sec load time for <300 songs). Also, produces strange words. Used for populating RapElement db only.