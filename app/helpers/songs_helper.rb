module SongsHelper

  def get_navbar_songs
    @rap_song = Rap.all.sample
    @country_song = Song.all.sample
  end

  def get_song(song_model)
    @songs = song_model.all
    if params[:id]
      @song = song_model.find(params[:id])
    elsif params[:title] #remove
      @song = song_model.find_by(title: params[:title]) #remove
    else
      @song = song_model.all.sample
    end
  end

  def save_params #share - move to raps helper
    @box_ids = params[:box_ids]
    @checkbox = params[:checkbox]
    @output = params[:output]
    @lyric_search = params[:lyric_search]
    if @lyric_search
      @lyric_search_checkbox = true
    else
      @lyric_search_checkbox = false
    end
  end

  def get_genre_vars
    if @genre == "country"
      get_song(Song)
      @element_model = CountryElement
      @song_model = Song
      @genre_inverse = "hiphop"
      @genre_path = country_path(@song.id)
      @genre_post_path = country_index_path
      @genre_search_path = country_songs_path(anchor: "buttons")
      @show_all_songs = country_songs_path(search: " ")
    else
      get_song(Rap)
      @element_model = RapElement
      @song_model = Rap
      @genre_inverse = "country"
      @genre_path = hiphop_path(@song.id)
      @genre_post_path = hiphop_index_path
      @genre_search_path = hiphop_songs_path(anchor: "buttons")
      @show_all_songs = hiphop_songs_path(search: " ", anchor: "buttons")
    end
  end

  def get_lyrics(box_ids, song_model)
    @picked_lyrics = []
    @picked_songs = []
    box_ids.each do |id|
      picked_song = song_model.find(id)
      @picked_songs.insert(-1, picked_song)
      picked_lyric = picked_song.lyrics.squish!
      @picked_lyrics.insert(0, picked_lyric)
    end
    @all_lyrics = @picked_lyrics.map!(&:inspect).join
  end


  # def count_words(string, n)
    # located in p_table_helper (to assist with .rake counts)
  # end

  def perform_search(song_model)
    unless @lyric_search
      if params[:search].include?(",")
        @search_results = []
        params[:search].split(',').each do |artist|
          songs = song_model.search_by_artist(artist)
          songs.each do |song|
            @search_results.insert(0, song)
          end
        end
        return @search_results
      else
        @search_results = song_model.search_by_artist(params[:search]).reverse
      end
    else
      @search_results = song_model.search(params[:search])
    end
  end
end
