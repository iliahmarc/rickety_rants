require 'test_helper'

class RantTest < ActiveSupport::TestCase

  def setup
    @author = authors(:fakeauthor)
    @rant = @author.rants.build(title: "First Rant", body: "I hate smelly things.")
  end

  test "rant should be valid" do
    assert @rant.valid?
  end

  test "author_id should be present" do
    @rant.author_id = nil
    assert_not @rant.valid?
  end

end
