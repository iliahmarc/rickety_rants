require 'test_helper'

class AuthorTest < ActiveSupport::TestCase

  def setup
    @author = Author.new(name: "Example Author", email: "author@example.com", password: "foobar", password_confirmation: "foobar")
  end

  test "author should be valid" do
    assert @author.valid?
  end

  test "name should be present" do
    @author.name = '   '
    assert_not @author.valid?
  end

  test "name should not be too long" do
    @author.name = "a" * 16
    assert_not @author.valid?
  end

  test "password should be present (not blank)" do
    @author.password = @author.password_confirmation = " " * 6
    assert_not @author.valid?
  end

  test "email should be present" do
    @author.email = '   '
    assert_not @author.valid?
  end

  test "email should not be too long" do
    @author.email = "a" * 41
    assert_not @author.valid?
  end
end
