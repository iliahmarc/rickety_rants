require 'test_helper'

class RantsControllerTest < ActionDispatch::IntegrationTest

  test "should get index" do
    get rants_path
    assert_response :success
    assert_select "title", "The Rants | Rickety Rants"
  end

  test "should get show" do
    get rant_path(id:1)
    assert_response :success
    assert_select "title", "First Rant | Rickety Rants"
  end

end
