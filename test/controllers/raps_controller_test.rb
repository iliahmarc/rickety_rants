require 'test_helper'

class RapsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @rap_song = raps(:one)
  end

  test "should get show with song.id AND song.slug" do
    get hiphop_url(@rap_song.id)
    assert_response :success
    assert_select "title", "Hiphop Songs | Rickety Rants"
    get hiphop_url(@rap_song.slug)
    assert_response :success
  end

  test "should post create" do
    post hiphop_index_url
    assert_response :success
  end

  test "should get index" do
    get hiphop_songs_url
    assert_response :success
  end

end
