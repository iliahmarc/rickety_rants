require 'test_helper'

class SongsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @rap_song = raps(:one).id
    @rap_song2 = raps(:two).id
    @country_song = songs(:one)
  end


  test "should get show with song.id AND song.slug" do
    get country_path(@country_song.id)
    assert_response :success
    assert_select "title", "Country Songs | Rickety Rants"
    get country_path(@country_song.slug)
    assert_response :success
  end

  test "should post create" do
    post hiphop_index_path(@rap_song2)
    assert_response :success
    assert_select "title", "Hiphop Songs | Rickety Rants"
    post country_index_path
    assert_select "title", "Country Songs | Rickety Rants"
  end

  test "should get index" do
    get hiphop_songs_path
    assert_response :success
    assert_select "title", "Hiphop Search | Rickety Rants"
    get country_songs_path
    assert_select "title", "Country Search | Rickety Rants"
  end

end
