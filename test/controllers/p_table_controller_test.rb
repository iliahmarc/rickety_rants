require 'test_helper'

class PTableControllerTest < ActionDispatch::IntegrationTest
  test "should get hiphop index" do
    get periodic_table_path(genre: 'hiphop')
    assert_response :success
  end

  test "should get country index" do
    get periodic_table_path(genre: 'country')
    assert_response :success
  end

end
