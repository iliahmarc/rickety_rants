require 'test_helper'

class WordCountControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get word_counter_path
    assert_response :success
    assert_select "title", "Word Counter | Rickety Rants"
  end

  test "should post create" do
    post word_counter_path
    assert_response :success
    assert_select "title", "Word Counter | Rickety Rants"
  end

end
