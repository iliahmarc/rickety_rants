require 'test_helper'

class RantIndexTest < ActionDispatch::IntegrationTest
  def setup
    @rant = rants(:one)
    @rant2 = rants(:two)
  end

  test "rant/index page should show rants" do
    get rants_path
    assert_template 'rants/index'
    assert_select 'div.home-cubes a', count: 14
    #UPDATE TEST once rant index pages are complete 1.13.17
    #assert_select 'div#sidebar-body', @rant2.body
  end
end
