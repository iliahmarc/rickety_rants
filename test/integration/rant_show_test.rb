require 'test_helper'
#currently includes index test as well (consider moving to own file)
class RantShowTest < ActionDispatch::IntegrationTest
  def setup
    @rant = rants(:one)
    @rant2 = rants(:two)
  end

  test "rant/show page should show rant on sidebar and main view" do
    get rant_path(@rant.id)
    assert_template 'rants/show'
    #assert_select 'div#sidebar-rant-cube', count: 7
    assert_select 'h2', @rant.title
    #assert_select 'div.rant-title', @rant.title && @rant2.title
    assert_match @rant.body, response.body
  end

end
