require 'test_helper'

class WordCountTest < ActionDispatch::IntegrationTest

  def setup
    @string_8 = "one two three four five six seven eight"
  end

  test "word counter filter should count words properly" do
    get word_counter_path
    assert_select 'form'
    #word filter OFF:
    post word_counter_path, params: { user_input: 'Bandit and but but',                          checkbox: nil }
    assert_template 'word_count/new'
    assert_select 'div h4'
    assert_match 'But: 2 |', response.body
    assert_match 'And: 1', response.body
    assert_match 'Bandit: 1', response.body
    #word filter ON:
    post word_counter_path, params: { user_input: 'Bandit and but but',                          checkbox: '1' }
    assert_template 'word_count/new'
    assert_match 'Bandit: 1', response.body
    assert_no_match 'And: 1', response.body
  end

  test "word counter regex should remove special characters" do
    post word_counter_path, params: { user_input: "test'*:|.,!?;({[]})ing",                          checkbox: nil }
    assert_template 'word_count/new'
    assert_match 'Testing: 1', response.body
  end

  test "word counter output should change with checkbox" do
    post word_counter_path, params: { user_input: @string_8,                               output: 8}
    assert_select 'b#words_output', count:8
    post word_counter_path, params: { user_input: @string_8,  output: 5}
    assert_select 'b#words_output', count:5
  end
end
