require 'test_helper'

class ImageUploadTest < ActionDispatch::IntegrationTest

  def setup
    @rant = rants(:one)
    @author = authors(:fakeauthor)
  end

  test "Valid image should save. Invalid image should not save." do
    log_in_as(@author)
    get new_rant_path
    #valid image:
    #pic = fixture_file_upload('test/fixtures/pic.jpeg', 'image/jpeg')
    assert_difference 'Rant.count', 1 do
      post rants_path, params: { rant: { title: @rant.title, body: @rant.body, author_id: @author.id } }#, image: pic
    #note: posting "image: pic" with params raises a permission denied' error. chmod 777 file paths to no avail. Bug?
    end
    follow_redirect!
    assert_select 'div.alert.alert-success'
    assert_template 'rants/show'
  end
end
