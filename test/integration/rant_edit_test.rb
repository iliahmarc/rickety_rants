require 'test_helper'

class RantEditTest < ActionDispatch::IntegrationTest

  def setup
    @rant = rants(:one)
    @author = authors(:fakeauthor)
  end

  test "should edit rant, redirect to rant/show only if logged in" do
    #not logged in:
    get edit_rant_path(@rant)
    follow_redirect!
    assert_select 'div.alert.alert-danger'
    assert_template 'rants/index'
    #logged in:
    log_in_as(@author)
    get edit_rant_path(@rant)
    assert_template 'rants/edit'
    patch rant_path(@rant), params: { rant: { title: @rant.title, body: @rant.body, author_id: @rant.author_id } }
    assert_redirected_to rant_path(@rant)
    follow_redirect!
    assert_template 'rants/show'
    assert_select 'div.alert.alert-success'
    assert_select 'h2', @rant.title
  end

end
