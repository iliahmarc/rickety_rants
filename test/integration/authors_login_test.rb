require 'test_helper'

class AuthorsLoginTest < ActionDispatch::IntegrationTest

  def setup
    @author = authors(:fakeauthor)
  end

  test "login with invalid information" do
    get fatkat_in_path
    assert_template 'sessions/new'
    post fatkat_in_path, params: { session: { email: "", password: "" } }
    assert_not is_logged_in?
    assert_template 'sessions/new'
    assert_not flash.empty?
    assert_select 'div.alert.alert-danger'
    get root_path
    assert flash.empty?
  end

  test "login with valid information followed by logout" do
    get fatkat_in_path
    log_in_as(@author)
    assert is_logged_in?
    assert_redirected_to root_url
    follow_redirect!
    assert_select 'div.alert.alert-success'
    assert_template 'static_pages/home'
    assert_select "a[href=?]", new_rant_path, count: 1
    assert_select "a[href=?]", fatkat_out_path, count: 1
    delete fatkat_out_path
    assert_not is_logged_in?
    assert_select "a[href=?]", fatkat_out_path, count: 0
    assert_select "a[href=?]", new_rant_path, count: 0
  end

end
