require 'test_helper'

class RantNewTest < ActionDispatch::IntegrationTest

  def setup
    @author = authors(:fakeauthor)
    @rant = rants(:one)
  end

  test "New Rant page should load simple_form" do
    get new_rant_path
    follow_redirect!
    assert_template 'rants/index'
    log_in_as(@author)
    get new_rant_path
    assert_select 'div.ckeditor'
    assert_select 'input#rant_title'
    assert_select 'input.btn'
  end

  test "Valid rant should save, forward to show page if logged in" do
    #not logged in:
    assert_no_difference 'Rant.count', 1 do
      post rants_path, params: { rant: { title: "Barnyard Title",
                            body: "Barnyard Body", } }
    end
    log_in_as(@author)
    assert is_logged_in?
    assert_difference 'Rant.count', 1 do
      post rants_path, params: { rant: { title: @rant.title,
                            body: @rant.body, author_id: @rant.author_id } }
    end
    follow_redirect!
    assert_not flash.empty?
    assert_select 'div.alert.alert-success'
    assert_template 'rants/show'
    assert_select 'h2', 'First Rant'
  end

  test "invalid post should not save" do
    log_in_as(@author)
    assert_no_difference 'Rant.count' do
    post rants_path, params: { rant: { title: ' ',
                                          body: ' ' } }
    end
    assert_template 'rants/new'
    assert_select 'div.alert.alert-danger'
    assert_select 'div#error_explanation'
  end


end
