require 'test_helper'

class RantDestroyTest < ActionDispatch::IntegrationTest

  def setup
    @rant = rants(:one)
    @author = authors(:fakeauthor)
  end

  test "should delete rant, redirect to rants/index if logged in" do
    #not logged in:
    assert_no_difference 'Rant.count' do
      delete rant_path(@rant.id)
    end
    log_in_as(@author)
    assert is_logged_in?
    assert_difference 'Rant.count', -1 do
      delete rant_path(@rant.id)
    end
    assert_redirected_to rants_path
    follow_redirect!
    assert_template 'rants/index'
    assert_no_match @rant.body, response.body
    assert_select 'div.alert.alert-success', "'First Rant' successfully deleted"
  end
end
