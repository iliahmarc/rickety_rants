require 'test_helper'

class SongsShowTest < ActionDispatch::IntegrationTest

  def setup
    @lyrics1 = raps(:one).lyrics
    @lyrics2 = raps(:two).lyrics
    @lyrics_combined = "#{@lyrics2} + #{@lyrics1}"
    @country_song = songs(:one)
    @rap_song = raps(:one)
  end

  test "song-cube links should get right song" do
    get hiphop_path(@rap_song.slug)
    current_song = assigns(:song) #Song.first reference
    assert_equal current_song.rank, 1
    assert_match @lyrics1, response.body
    assert_select 'p.lyrics'
    assert_not_empty 'span.rap-count'
    #check that new id matches new respective song:
    get hiphop_path(id:2)
    current_song = assigns(:song)
    assert_equal current_song.rank, 2
    assert_match @lyrics2, response.body
    assert_select 'p.lyrics'
    assert_not_empty 'span.rap-count'
  end

  test "song-scroller should render songs w/form buttons" do
    get country_path(@country_song.slug)
    assert_select 'div.song-cube', count: 2
    assert_select 'input#box_ids_', count: 3
    assert_select 'input#output_100', count: 1
    assert_select 'input#output_500000', count: 1
    assert_select 'input.submit', count: 2
  end

  test "user-checked song-cubes should post and output in view" do
    post hiphop_index_path(@rap_song.id, box_ids:[1, 2], output:10)
    output_param = assigns(:output)
    current_songs = assigns(:all_lyrics)
    assert_includes current_songs, @lyrics1 && @lyrics2
    assert_equal count_words(current_songs, output_param.to_i).count, count_words(@lyrics_combined, 10).count
    assert_not_empty 'h4.rap-count'
  end

  test "count_words should respond to 2nd argument(output, # of words)" do
    assert_not_equal count_words(@lyrics2, 10), count_words(@lyrics2, 25)
  end

  test "lyrics output contain writer and copyright" do
    get country_path(id:1, genre: 'country')
    assert_select 'p.lyrics'
    assert_select 'div.sources'
    assert_match @country_song.copyright, response.body
    assert_match @country_song.writers, response.body
  end
end
