require 'test_helper'

class PTableTest < ActionDispatch::IntegrationTest

def setup
  @element_1 = rap_elements(:element_1)
  @element_118 = rap_elements(:element_118)
  @element_120 = rap_elements(:element_120)
  @country_element_1 = country_elements(:element_1)
end

  test "ptable should contain 118 Rap elements" do
    get periodic_table_path(genre: 'hiphop')
    assert_template 'p_table/index'
    assert_select 'a#word', count:118
    assert_match @element_1.word, response.body
    assert_match @element_118.word, response.body
    assert_no_match @element_120, response.body
  end

  test "ptable should contain 118 Country elements" do
    get periodic_table_path(genre: 'country')
    assert_template 'p_table/index'
    assert_select 'a#word', count:118
    assert_match @country_element_1.word, response.body
  end

  test "hiphop ptable stats should be present and accurate" do
    #based on 200 elements, See rap_elements.yml for math notes
    get periodic_table_path(genre: 'hiphop')
    global_count = assigns(:global_count)
    assert_match "#{global_count}", response.body
    assert_equal "#{global_count}", "220100"
    percent_unique = assigns(:percent_unique)
    assert_match "#{percent_unique}%", response.body
    assert_equal "#{percent_unique}%", "0.1%"
    song_count = assigns(:song_count)
    assert_match "#{song_count}", response.body
    assert_equal "#{song_count}", "2"
    ptable_mass = assigns(:ptable_mass)
    assert_match "#{ptable_mass}%", response.body
    assert_equal "#{ptable_mass}%", "61%"
  end

  test "Hiphop ptable should show stats for clicked element" do
    #see .yml notes for math
    get periodic_table_path(genre: 'hiphop', word: @element_118.word)
    assert_match @element_118.count.to_s, response.body
    assert_no_match @element_1.count.to_s, response.body
    lyrical_mass = assigns(:lyrical_mass)
    assert_match "#{lyrical_mass}", response.body
    assert_equal "#{lyrical_mass}", "0.51"
  end

  test "Country ptable should show stats for clicked element" do
    #see .yml notes for math
    get periodic_table_path(genre: 'country', word: @element_118.word)
    assert_match @element_118.count.to_s, response.body
    assert_no_match @element_1.count.to_s, response.body
    lyrical_mass = assigns(:lyrical_mass)
    assert_match "#{lyrical_mass}", response.body
    assert_equal "#{lyrical_mass}", "0.51"
  end
end
