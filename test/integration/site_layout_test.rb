require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  def setup
    @rant = rants(:one)
    @rant2 = rants(:two)
  end

  test "links should work throughout" do
    #tests for home page plus links:
    get root_url
    assert_template 'static_pages/home'
    assert_select "a[href=?]", about_path, count: 2
    assert_select "a[href=?]", root_path, count: 3
    assert_select "a[href=?]", contact_path, count: 2
    assert_select "a[href=?]", "http://rubyonrails.org/", count: 1
    #tests for about page and header/footer persistence:
    get about_path
    assert_template 'static_pages/about'
    assert_select 'header.navbar'
    assert_select 'footer.footer'
    #test contact page template:
    get contact_path
    assert_template 'static_pages/contact'
    #test counter template:
    get word_counter_path
    assert_template 'word_count/new'
    assert_select 'title', full_title('Word Counter')

    #test songs index page:
    get country_songs_path
    assert_template 'songs/index'
    #test songs show page:
    get country_path(id:1)
    assert_template 'songs/show'
    assert_select 'title', full_title('Country Songs')
    assert_select 'h5.title a', count: 2
    #test rant show page:
    get rant_path(id:1)
    assert_template 'rants/show'
    assert_select 'title', full_title('First Rant')
    assert_select "a[href=?]", rants_path, count: 7
    assert_select "a[href=?]", rant_path(id: "1-first-rant"), count: 2
    #test rant index page:
    get rants_path
    assert_template 'rants/index'
    assert_select 'title', full_title('The Rants')
    #also tests Vanity URL for rants.
    assert_select "a[href=?]", rant_path(id: "#{@rant.id}-#{@rant.title.parameterize}"), count:2
    assert_select "a[href=?]", rant_path(id: "#{@rant2.id}-#{@rant2.title.parameterize}"), count:2
  end
end
