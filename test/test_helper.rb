ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  include ApplicationHelper #to access full_title method
  include PTableHelper #to access count_words method
  #include PTableHelper #use once testing begins
  # Add more helper methods to be used by all tests here...

  # Returns true if a test user is logged in.
  def is_logged_in?
    !session[:author_id].nil?
  end

  # Log in as a particular user.
  def log_in_as(author)
    session[:author_id] = author.id
  end
end

class ActionDispatch::IntegrationTest

  # Log in as a particular user.
  def log_in_as(author, password: 'password')
    post fatkat_in_path, params: { session: { email: author.email,
                                          password: password } }
  end
end