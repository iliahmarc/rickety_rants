Paperclip::Attachment.default_options[:url] = ':s3_domain_url'
Paperclip::Attachment.default_options[:path] = '/:class/:attachment/:id_partition/:style/:filename'

#Paperclip::Attachment.default_options[:s3_host_name] = 'rickety-rants.s3.amazonaws.com'
#source of above: https://devcenter.heroku.com/articles/paperclip-s3#define-the-file-attribute-in-the-model