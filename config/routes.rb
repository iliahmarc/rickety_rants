Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'


  root 'static_pages#home'
  get '/about',   to: 'static_pages#about'
  get '/contact', to: 'static_pages#contact'
  get '/faqs',   to: 'static_pages#faqs'

  get '/periodic_table', to: 'p_table#index'

  get '/country_songs', to: 'songs#index'
  resources :country, only: [:show], to: 'songs#show'
  resources :country, only: [:create], to: 'songs#create'

  get '/hiphop_songs', to: 'raps#index'
  resources :hiphop, only: [:show], to: 'raps#show'
  resources :hiphop, only: [:create], to: 'raps#create'

  get    '/fatkat_in',   to: 'sessions#new'
  post   '/fatkat_in',   to: 'sessions#create'
  delete '/fatkat_out',  to: 'sessions#destroy'

  resources :rants

  #currently only being used to test counter function. Refactor.
  get '/word_counter', to: 'word_count#new'
  post '/word_counter', to: 'word_count#create'

  get "/sitemap", to: 'sitemap#index.text'
end
