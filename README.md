# README

updated 1/31/2017

* Ruby version 2.3.1
* Rails version 5.0.0.1
* 500 Country Music Songs
* 500 Hip Hop Songs
* Full Text Searching
* Secure login
* CKEditor image upload with Amazon Web Service integration
* PostgreSQL
* Heroku Deployed

RicketyRants.com is a lyrical calculator combined with a text-message blog filled with sometimes controversial "rants". The site has many features, including the worlds first interactive Periodic Table of Hip Hop and Periodic Table of Country Music Lyrics. A custom-built word-calculator sifts through over half-a-million lyrics, compiling a periodic table with each genres top 118 words (by frequency). These words are summarized in their respective periodic tables.

Users may search for songs by artists, titles, year, and/or individual song lyrics via full text queries. Search results list each song with a check-box, which can be toggled on and off for making custom lyric counts.